import { FirebaseApp } from 'firebase/app';
import {
    getAuth,
    signInWithPopup,
    FacebookAuthProvider,
    User,
} from 'firebase/auth';

export const LoginFacebook =
    (app: FirebaseApp) =>
    async (): Promise<{
        user: User;
        token?: string;
    }> => {
        const provider = new FacebookAuthProvider();
        const auth = getAuth(app);
        const result = await signInWithPopup(auth, provider);
        const credential = FacebookAuthProvider.credentialFromResult(result);
        const token = credential?.accessToken;
        const user = result.user;
        return { user, token };
    };
export default LoginFacebook;
