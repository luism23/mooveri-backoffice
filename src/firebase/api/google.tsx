import { FirebaseApp } from 'firebase/app';
import {
    getAuth,
    signInWithPopup,
    GoogleAuthProvider,
    User,
} from 'firebase/auth';

export const LoginGoogle =
    (app: FirebaseApp) =>
    async (): Promise<{
        user: User;
        token?: string;
    }> => {
        const provider = new GoogleAuthProvider();
        const auth = getAuth(app);
        const result = await signInWithPopup(auth, provider);
        // This gives you a Google Access Token. You can use it to access the Google API.
        const credential = GoogleAuthProvider.credentialFromResult(result);
        const token = credential?.accessToken;
        // The signed-in user info.
        const user = result.user;
        return { user, token };
    };
export default LoginGoogle;
