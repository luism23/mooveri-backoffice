import { FirebaseApp } from 'firebase/app';
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';

const firebaseConfig = {
    apiKey: process.env['NEXT_PUBLIC_FIREBASE_APIKEY'],
    authDomain: process.env['NEXT_PUBLIC_FIREBASE_AUTHDOMINE'],
    projectId: process.env['NEXT_PUBLIC_FIREBASE_PROJECTID'],
    storageBucket: process.env['NEXT_PUBLIC_FIREBASE_STORAGEBUCKET'],
    messagingSenderId: process.env['NEXT_PUBLIC_FIREBASE_MESSAGINGSENDERID'],
    appId: process.env['NEXT_PUBLIC_FIREBASE_APPID'],
};

// Initialize Firebase
const app: FirebaseApp = !firebase?.apps?.length
    ? firebase.initializeApp(firebaseConfig)
    : firebase.app();

export default app;
