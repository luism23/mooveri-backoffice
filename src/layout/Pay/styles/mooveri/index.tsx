import { LayoutClassProps } from '@/layout/Pay/Base';

export const mooveri: LayoutClassProps = {
    classNameContent: `
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: 'mooveri',
};
