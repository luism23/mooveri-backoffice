import { LayoutClassProps } from '@/layout/Pay/Base';

export const _default: LayoutClassProps = {
    classNameContent: `
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: '_default',
};
