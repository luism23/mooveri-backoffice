import { LayoutClassProps } from '@/layout/Pay/Base';

export const tolinkme: LayoutClassProps = {
    classNameContent: `
    bg-gradient-darkAqua-brightPink
    width-p-100
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: 'tolinkme',
    styleTemplateHeader2: 'tolinkme2',
};
