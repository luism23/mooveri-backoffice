import CSS from 'csstype';
import { PropsWithChildren } from 'react';

import Header, { HeaderStyles } from '@/components/Header';

import { Theme, ThemesType } from '@/config/theme';
import { useUser } from '@/hook/useUser';

export interface LayoutClassProps {
    className?: string;
    classNameContent?: string;
    styleContent?: CSS.Properties;

    styleTemplateHeader?: HeaderStyles | ThemesType;
    styleTemplateHeader2?: HeaderStyles | ThemesType;
}

export interface LayoutPayProps extends PropsWithChildren {
    className?: string;
    style?: CSS.Properties;
}

export interface LayoutProps extends LayoutPayProps, LayoutClassProps {}

export const LayoutPay = ({
    classNameContent = '',
    styleContent = {},

    styleTemplateHeader = Theme?.styleTemplate ?? '_default',
    styleTemplateHeader2 = Theme?.styleTemplate ?? '_default',
    className = '',
    style = {},
    children,
}: LayoutProps) => {
    const { user } = useUser();
    return (
        <div
            className={`${className} ${classNameContent}`}
            style={{ ...styleContent, ...style }}
        >
            {user ? (
                <>
                    <Header styleTemplate={styleTemplateHeader} />
                </>
            ) : (
                <>
                    <Header styleTemplate={styleTemplateHeader2} />
                </>
            )}
            {children}
        </div>
    );
};
export default LayoutPay;
