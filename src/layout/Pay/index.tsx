import { useMemo } from 'react';

import { LayoutPay as LayoutPay_, LayoutPayProps } from '@/layout/Pay/Base';

import * as styles from '@/layout/Pay/styles';

import { Theme, ThemesType } from '@/config/theme';

export const LayoutPayStyle = { ...styles } as const;

export type LayoutPayStyles = keyof typeof LayoutPayStyle;

export interface LayoutProps extends LayoutPayProps {
    styleTemplate?: LayoutPayStyles | ThemesType;
}

export const Layout = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: LayoutProps) => {
    const Style = useMemo(
        () =>
            LayoutPayStyle[styleTemplate as LayoutPayStyles] ??
            LayoutPayStyle._default,
        [styleTemplate]
    );
    return <LayoutPay_ {...props} {...Style} />;
};

export const LayoutPay = Layout;

export default Layout;
