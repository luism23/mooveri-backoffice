import { SelectLangStyles } from '@/components/SelectLang';
import CSS from 'csstype';

export interface LayoutLoginClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;
    classNameCapa?: string;
    styleCapa?: CSS.Properties;
    classNameContentTop?: string;
    classNameContentGoBack?: string;
    classNameContentSkip?: string;
    classNameContentLogo?: string;
    sizeContentLogo?: number;
    classNameContentChildren?: string;
    classNameContentLinkTerm?: string;
    styleTemplateSelectLang?: SelectLangStyles;
}

export const _default: LayoutLoginClassProps = {
    classNameContent: `
        height-vh-min-100 
        flex flex-column 
        p-t-27 p-b-15 
        pos-r
    `,
    classNameContentTop: ``,
    classNameContentGoBack: `
        pos-a top-30 left-42
    `,
    classNameContentSkip: `
        pos-a top-30 right-42
    `,
    classNameContentLogo: `
        m-h-auto
    `,
    sizeContentLogo: 180,
    classNameContentChildren: `
        p-v-20 m-v-auto
    `,
    styleContent: {},
};
