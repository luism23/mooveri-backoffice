import { LayoutLoginClassProps } from '@/layout/Login/styles/default';

export const mooveri: LayoutLoginClassProps = {
    classNameContent: `
        height-vh-min-100 
        flex flex-column 
        p-t-27 p-b-100 
    `,
    classNameContentTop: `
        flex
        flex-align-center
        flex-justify-between
        flex-nowrap
        p-h-17
    `,
    classNameContentGoBack: `
        width-p-25
    `,
    classNameContentSkip: `
        width-p-25
        flex 
        flex-justify-right
        pos-r
        z-index-3
    `,
    classNameContentLogo: `
        width-md-p-max-50
        width-p-max-27
        m-h-auto
        flex 
        flex-justify-center
    `,
    sizeContentLogo: 180,
    classNameContentChildren: `
        p-v-20 m-v-auto
        p-h-17
        pos-r
    `,
    styleContent: {},
    classNameContentLinkTerm: `
        pos-r
    `,
    styleTemplateSelectLang: 'tolinkme',
};
export const mooveriBackoffice: LayoutLoginClassProps = mooveri;
