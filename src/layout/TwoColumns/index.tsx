import { useMemo } from 'react';

import * as styles from '@/layout/TwoColumns/styles';

import { Theme, ThemesType } from '@/config/theme';

import { TwoColumnsBaseProps, TwoColumnsBase } from '@/layout/TwoColumns/Base';

export const TwoColumnsStyle = { ...styles } as const;

export type TwoColumnsStyles = keyof typeof TwoColumnsStyle;

export interface TwoColumnsProps extends TwoColumnsBaseProps {
    styleTemplate?: TwoColumnsStyles | ThemesType;
}

export const TwoColumns = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: TwoColumnsProps) => {
    const Style = useMemo(
        () =>
            TwoColumnsStyle[styleTemplate as TwoColumnsStyles] ??
            TwoColumnsStyle._default,
        [styleTemplate]
    );

    return <TwoColumnsBase {...Style} {...props} />;
};
export default TwoColumns;
