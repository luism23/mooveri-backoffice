# TwoColumns

## Dependencies

[TwoColumns](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/TwoColumns)

```js
import { TwoColumns } from '@/components/TwoColumns';
```

## Import

```js
import { TwoColumns, TwoColumnsStyles } from '@/components/TwoColumns';
```

## Props

```tsx
interface TwoColumnsProps {}
```

## Use

```js
<TwoColumns>TwoColumns</TwoColumns>
```
