import Bg, { BgProps } from '@/components/Bg';
import ContentWidth, { ContentWidthProps } from '@/components/ContentWidth';
import GoBack, { GoBackProps } from '@/components/GoBack';
import Image, { ImageProps } from '@/components/Image';
import Img from '@/components/Img';
import Link, { LinkStyles, LinkProps } from '@/components/Link';
import { LinkTerm } from '@/components/Links';
import Theme, { ThemesType } from '@/config/theme';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import { PropsWithChildren } from 'react';

export interface TwoColumnsDataProps {
    title?: string;
    text?: string;
    img?: ImageProps;
    bg?: BgProps;
}

export interface TwoColumnsClassProps extends TwoColumnsDataProps {
    classNameContent?: string;

    classNameColLef?: string;
    classNameContentBg?: string;
    classNameTitleLeft?: string;
    classNameTextLeft?: string;
    classNameImgDownLeft?: string;

    classNameColRight?: string;

    TopProps?: ContentWidthProps;
    classNameContentGoBack?: string;
    GoBackProps?: GoBackProps;
    sizeContentLogo?: number;
    classNameContentLogo?: string;
    ImgProps?: ImageProps;
    classNameContentLink?: string;
    LinkProps?: LinkProps;

    classNameContentChildren?: string;

    classNameContentLinkTerm?: string;
    styleTemplateLinkTerm?: LinkStyles | ThemesType;
}

export interface TwoColumnsBaseProps extends PropsWithChildren {
    showGoBack?: boolean;
    showLogo?: boolean;
    showLink?: boolean;
}

export interface TwoColumnsProps
    extends TwoColumnsClassProps,
        TwoColumnsBaseProps {}

export const TwoColumnsBase = ({
    classNameContent = '',

    classNameColLef = '',
    classNameContentBg = '',
    classNameTitleLeft = '',
    classNameTextLeft = '',
    classNameImgDownLeft = '',

    classNameColRight = '',

    TopProps = {},
    classNameContentGoBack = '',
    GoBackProps = {},
    sizeContentLogo = 120,
    classNameContentLogo = '',
    ImgProps = {},
    classNameContentLink = '',
    LinkProps = {},

    classNameContentChildren = '',

    classNameContentLinkTerm = '',
    styleTemplateLinkTerm = Theme.styleTemplate ?? '_default',

    showGoBack = true,
    showLogo = true,
    showLink = true,

    title = '',
    text = '',
    img,
    bg,

    children,
}: TwoColumnsProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameColLef}>
                    <div className={classNameContentBg}>
                        <Bg {...bg} />
                    </div>
                    <div className={classNameTitleLeft}>{_t(title)}</div>
                    <div className={classNameTextLeft}>{_t(text)}</div>
                    <Image {...img} className={classNameImgDownLeft} />
                </div>
                <div className={classNameColRight}>
                    <ContentWidth {...TopProps}>
                        <div className={classNameContentGoBack}>
                            {showGoBack && <GoBack {...GoBackProps} />}
                        </div>
                        <div className={classNameContentLogo}>
                            <ContentWidth
                                size={sizeContentLogo}
                                className={classNameContentLogo}
                            >
                                {showLogo && (
                                    <Link href={url.home} className="d-block">
                                        <Img {...ImgProps} />
                                    </Link>
                                )}
                            </ContentWidth>
                        </div>
                        <div className={classNameContentLink}>
                            {showLink && <Link {...LinkProps} />}
                        </div>
                    </ContentWidth>
                    <div className={classNameContentChildren}>{children}</div>
                    <div className={`${classNameContentLinkTerm}`}>
                        <LinkTerm styleTemplate={styleTemplateLinkTerm} />
                    </div>
                </div>
            </div>
        </>
    );
};
export default TwoColumnsBase;
