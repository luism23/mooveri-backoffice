import url from '@/data/routes';
import { TwoColumnsClassProps } from '@/layout/TwoColumns/Base';

export const mooveri: TwoColumnsClassProps = {
    classNameContent: `
        flex
    `,
    classNameColLef: `
        flex-12 flex-md-6
        pos-r
        height-vh-100
        overflow-auto
        flex
        flex-column
        flex-nowrap
        p-15
        p-md-l-31
        p-md-t-45
        d-none 
        d-md-flex
    `,
    classNameContentBg: `
        pos-f 
        top-0
        left-0 
        width-p-50
        height-p-100
        z-index--1
    `,
    classNameTitleLeft: `
        font-nunito
        m-b-24
    `,
    classNameTextLeft: `
        font-nunito
        m-b-24
        color-white
        font-18
    `,
    classNameImgDownLeft: `
        m-t-auto
        m-l-auto
    `,
    classNameColRight: `
        flex-12 flex-md-6
        height-vh-100
        overflow-auto
        bg-white
        flex
        flex-column
        flex-nowrap
    `,
    classNameContentChildren: `
        width-p-100  
        m-v-auto
    `,
    classNameContentLinkTerm: `
        p-v-20
    `,
    styleTemplateLinkTerm: 'mooveri',
    TopProps: {
        size: 530,
        className: `
            p-15
            p-md-t-30
            p-md-b-8
            flex
            flex-align-center
            m-h-auto
        `,
    },
    classNameContentGoBack: `
        flex-4
    `,
    classNameContentLogo: `
        flex-4
        text-center
        flex
        flex-column
        flex-align-center
        flex-nowrap
    `,
    classNameContentLink: `
        flex-4
        text-right
    `,
    GoBackProps: {
        styleTemplate: 'mooveri',
    },
    ImgProps: {
        src: 'logo.png',
        styleTemplate: 'mooveri',
        className: `
            width-50
        `,
    },
    sizeContentLogo: 120,
    LinkProps: {
        styleTemplate: 'mooveri3',
        children: 'Link',
        href: url.home,
    },
};

export const mooveriCustomer: TwoColumnsClassProps = {
    ...mooveri,
    title: 'Book your Move online.',
    text: 'Compare guaranteed moving quotes from the best moving companies Book with Mooveri and save 45% on your moving costs!',
    bg: {
        src: 'banner-home.png',
        styleTemplate: 'mooveri',
        capas: [
            {
                background: 'var(--black)',
                opacity: 0.45,
            },
        ],
    },
    img: {
        src: 'logo-green.svg',
        styleTemplate: 'mooveri',
    },
};

export const mooveriCustomerLogin: TwoColumnsClassProps = {
    ...mooveriCustomer,
    LinkProps: {
        styleTemplate: 'mooveri3',
        children: 'Become Mover!',
        href: url.company.register,
    },
};

export const mooveriCustomerRegister: TwoColumnsClassProps = {
    ...mooveriCustomer,
    LinkProps: {
        styleTemplate: 'mooveri3',
        children: 'Costumer Login!',
        href: url.login,
    },
};

export const mooveriCompany: TwoColumnsClassProps = {
    ...mooveri,
    title: 'Become one of our Movers.',
    text: 'We joint end customers with carriers to help both parties to close the move. Just 10% Commission.',
    bg: {
        src: 'banner-login-company.png',
        styleTemplate: 'mooveri',
        capas: [
            {
                background: 'var(--black)',
                opacity: 0.58,
            },
        ],
    },
    img: {
        src: 'logo-green.svg',
        styleTemplate: 'mooveri',
    },
    LinkProps: {
        styleTemplate: 'mooveri3',
        children: 'Costumer Login!',
        href: url.login,
    },
};

export const mooveriBackoffice: TwoColumnsClassProps = {
    ...mooveri,
    title: 'BackofficeStrong',
    text: 'Welcome to Backoffice',
    bg: {
        src: 'banner-login-company.png',
        styleTemplate: 'mooveri',
        capas: [
            {
                background: 'var(--black)',
                opacity: 0.58,
            },
        ],
    },
    img: {
        src: 'logo-green.svg',
        styleTemplate: 'mooveri',
    },
    LinkProps: {
        styleTemplate: 'mooveri3',
        children: 'Costumer Login!',
        href: url.login,
    },
};
