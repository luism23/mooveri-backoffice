import { useMemo } from 'react';

import { LayoutBase as LayoutBase_, LayoutBaseProps } from '@/layout/Base/Base';

import * as styles from '@/layout/Base/styles';

import { Theme, ThemesType } from '@/config/theme';

export const LayoutBaseStyle = { ...styles } as const;

export type LayoutBaseStyles = keyof typeof LayoutBaseStyle;

export interface LayoutProps extends LayoutBaseProps {
    styleTemplate?: LayoutBaseStyles | ThemesType;
}

export const Layout = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: LayoutProps) => {
    const Style = useMemo(
        () =>
            LayoutBaseStyle[styleTemplate as LayoutBaseStyles] ??
            LayoutBaseStyle._default,
        [styleTemplate]
    );
    return <LayoutBase_ {...props} {...Style} />;
};

export const LayoutBase = Layout;

export default Layout;
