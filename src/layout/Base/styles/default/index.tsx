import { LayoutClassProps } from '@/layout/Base/Base';

export const _default: LayoutClassProps = {
    classNameContent: `
        bg-white
        container
        p-sm-h-15
    `,
    styleContent: {
        minHeight: 'calc(100vh - var(--sizeHeader,0px))',
    },
    styleTemplateHeader: '_default',
};
