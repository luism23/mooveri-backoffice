import CSS from 'csstype';
import { PropsWithChildren } from 'react';

import Header, { HeaderStyles } from '@/components/Header';

import { Theme, ThemesType } from '@/config/theme';

export interface LayoutClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;

    styleTemplateHeader?: HeaderStyles | ThemesType;
}

export interface LayoutBaseProps extends PropsWithChildren {
    className?: string;
    style?: CSS.Properties;
}

export interface LayoutProps extends LayoutBaseProps, LayoutClassProps {}

export const LayoutBase = ({
    classNameContent = '',
    styleContent = {},

    styleTemplateHeader = Theme?.styleTemplate ?? '_default',

    className = '',
    style = {},
    children,
}: LayoutProps) => {
    return (
        <div
            className={`${className} ${classNameContent}`}
            style={{ ...styleContent, ...style }}
        >
            <Header styleTemplate={styleTemplateHeader} />
            {children}
        </div>
    );
};
export default LayoutBase;
