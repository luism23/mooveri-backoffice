import { Story, Meta } from "@storybook/react";

import { LayoutProps, Layout } from "./index";

export default {
    title: "Layout/Base",
    component: Layout,
} as Meta;

const Template: Story<LayoutProps> = (args) => (
    <Layout {...args}>Test Children</Layout>
);

export const Index = Template.bind({});
Index.args = {};
