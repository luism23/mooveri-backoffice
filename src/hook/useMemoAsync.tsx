import { useEffect, useState } from 'react';

export interface useMemoAsyncProps<Q, R> {
    query: Q;
    onLoad: (data: Q) => Promise<R>;
}

export const useMemoAsync = <Q, R>({
    query,
    onLoad,
}: useMemoAsyncProps<Q, R>) => {
    const [result, setResult] = useState<R | null>(null);
    const [loader, setLoader] = useState(false);

    const onRequest = async () => {
        setLoader(true);

        const respond = await onLoad(query);
        setResult(respond);

        setLoader(false);
    };
    useEffect(() => {
        onRequest();
    }, [query]);

    return {
        result,
        loader,
    };
};
