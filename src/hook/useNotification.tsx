import { Notification, NotificationStyles } from '@/components/Notification';
import { NotificationType } from '@/components/Notification/Base';
import { ThemesType } from '@/config/theme';
import log from '@/functions/log';
import { useMemo } from 'react';
import { useLocalStorage } from 'uselocalstoragenextjs';

export interface NotificationLoginProps {
    styleTemplate?: NotificationStyles | ThemesType;
    type?: NotificationType;
    message: string;
}

export const useNotification = () => {
    const { value, load, setLocalStorage } = useLocalStorage({
        name: 'notification',
        defaultValue: '',
        parse: (v: any) => {
            try {
                return JSON.parse(v);
            } catch (error) {
                return {};
            }
        },
    });

    const notification: NotificationLoginProps = useMemo(() => value, [value]);

    const pop = (props: NotificationLoginProps) => {
        log('notification', props, 'deeppink');
        setLocalStorage(props);
    };

    const reset = () => {
        setLocalStorage({
            type: 'normal',
            message: '',
        });
    };

    return {
        load,
        notification,
        pop,
        reset,
    };
};
export const GlobalNotification = () => {
    const { notification, reset, load } = useNotification();
    return (
        <>
            {load && notification.message && (
                <Notification
                    type={notification.type}
                    reset={reset}
                    styleTemplate={notification.styleTemplate}
                >
                    {`${notification.message}`}
                </Notification>
            )}
        </>
    );
};
