import { useEffect, useState } from 'react';
import io from 'socket.io-client';
import axios from 'axios';
import { URL } from '@/api/tolinkme/_';
import { useUser } from './useUser';

export interface NotificationInfo {
    EVENT: string;
    createdAt: string;
    user: {
        username: string;
    };
}

export interface useNotificationSocketProps {
    skip?: number;
}

export const useNotificationSocket = (skipValue: number) => {
    const [msgs, setMsgs] = useState<NotificationInfo[]>([]);
    const [skipNotifications, setSkipNotifications] = useState(skipValue);
    const { user } = useUser();

    const onLoadNotifycaction = async () => {
        if (!user?.id) {
            return;
        }
        try {
            const request = await axios.get(
                `${URL}/service/notifications/${user.id}?skip=${skipNotifications}`
            );
            const data = request.data;
            setMsgs(
                data.map((d: any) => ({
                    EVENT: d.EVENT,
                    createdAt: d.createdAt,
                    user: {
                        username: d.user?.username,
                    },
                }))
            );
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        onLoadNotifycaction();
    }, [skipNotifications, user]);

    useEffect(() => {
        const socket = io('wss://backends.tolnk.me', {
            transports: ['websocket'],
        });

        socket.on(`${user?.id}`, (d) => {
            const data = d.data;

            if (!data) {
                return;
            }
            if (!data.EVENT) {
                return;
            }
            if (!data.createdAt) {
                return;
            }
            if (!data.user?.username) {
                return;
            }

            setMsgs((x) => [
                {
                    EVENT: data.EVENT,
                    createdAt: data.createdAt,
                    user: {
                        username: data.user?.username,
                    },
                },
                ...x,
            ]);
        });
    }, []);

    const loadMoreNotifications = () => {
        setSkipNotifications(skipNotifications + 5);
    };

    const handleScroll = (event: any) => {
        const { target } = event;
        if (target.scrollTop + target.clientHeight === target.scrollHeight) {
            loadMoreNotifications();
        }
    };

    return {
        msgs,
        handleScroll,
    };
};
