import { SubmitResult } from '@/components/Form/Base';
import { useMemo } from 'react';
import { useLocalStorage } from 'uselocalstoragenextjs';
import jwt_decode from 'jwt-decode';
import log from '@/functions/log';
import { NotificationLoginProps } from '@/hook/useNotification';

export type UserRoles = 'client' | 'admin' | 'company' | 'backoffice';

export interface UserLoginProps {
    id?: string;
    token?: string;
    name?: string;
    img?: string;
    role?: UserRoles;
    email?: string;
    stripe_id?: string;
    notification?: NotificationLoginProps;
    verify?: {
        email?: boolean;
        phone?: boolean;
        company?: boolean;
    };
}

export interface UserLoginPaymentProps {
    id?: string;
    token?: string;
    name?: string;
    img?: string;
    role?: UserRoles;
    email?: string;
    stripe_id?: string;
    notification?: NotificationLoginProps;
    verify?: {
        email?: boolean;
        phone?: boolean;
        company?: boolean;
    };
}

export const useUser = () => {
    const { value, load, setLocalStorage } = useLocalStorage({
        name: 'user',
        defaultValue: '',
        parse: (v: any) => {
            try {
                return JSON.parse(v);
            } catch (error) {
                return {};
            }
        },
    });

    const user: UserLoginProps = useMemo(() => value, [value]);

    const onLogin = (data: SubmitResult<UserLoginProps>) => {
        if (data.status == 'ok') {
            setLocalStorage(data.data);
        }
    };
    const onLogOut = () => {
        setLocalStorage('');
    };

    return {
        load,
        user,
        updateUser: (data?: UserLoginProps) => {
            setLocalStorage(data);
        },
        onLogin,
        onLogOut,
    };
};

export const validateTokenUser = (user: UserLoginProps) => {
    if (!user.token) {
        return false;
    }
    log('USER', user, 'aqua');
    try {
        const token = jwt_decode(user.token);
        log('USER TOKEN ok', token, 'green');
        return true;
    } catch (error) {
        log('USER TOKEN Error', error, 'red');
        return false;
    }
};
