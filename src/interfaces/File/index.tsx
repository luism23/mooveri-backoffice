export interface FileData {
    text?: string;
    fileData: any;
    tipe?: 'img' | 'audio' | 'video' | 'others';
}

export const ExtensionesImagenesConst = [
    'jpg',
    'jpeg',
    'png',
    'gif',
    'bmp',
    'svg',
    'webp',
] as const;
export const ExtensionesImagenes = [...ExtensionesImagenesConst];
export type ExtensionesImagenesType = (typeof ExtensionesImagenesConst)[number];

export const ExtensionesVideosConst = [
    'mp4',
    'avi',
    'mov',
    'mkv',
    'wmv',
    'flv',
    'webm',
    'aep',
    'pr',
] as const;
export const ExtensionesVideos = [...ExtensionesVideosConst];
export type ExtensionesVideosType = (typeof ExtensionesVideosConst)[number];

export const ExtensionesAudioConst = [
    'mp3',
    'wav',
    'flac',
    'aac',
    'ogg',
    'wma',
] as const;
export const ExtensionesAudio = [...ExtensionesAudioConst];
export type ExtensionesAudioType = (typeof ExtensionesAudioConst)[number];

export const ExtensionesDis2DConst = ['ai', 'ps', 'svg', 'xd', 'cdr'] as const;
export const ExtensionesDis2D = [...ExtensionesDis2DConst];
export type ExtensionesDis2DType = (typeof ExtensionesDis2DConst)[number];

export const ExtensionesDis3DConst = ['blend', 'obj', 'dxf', 'dwg'] as const;
export const ExtensionesDis3D = [...ExtensionesDis3DConst];
export type ExtensionesDis3DType = (typeof ExtensionesDis3DConst)[number];

export const ExtensionesMusicProductionConst = [
    'fl',
    'alp',
    'ptf',
    'midi',
] as const;
export const ExtensionesMusicProduction = [...ExtensionesMusicProductionConst];
export type ExtensionesMusicProductionType =
    (typeof ExtensionesMusicProductionConst)[number];

export const ExtensionesTextConst = ['docx', 'odt', 'txt', 'pdf'] as const;
export const ExtensionesText = [...ExtensionesTextConst];
export type ExtensionesTextType = (typeof ExtensionesTextConst)[number];
