import { InputFileDataProps } from '@/components/Input/File/Base';

export interface ItemScanProps extends InputFileDataProps {
    name?: string;
    bbox?: number[];
}

export type ScanItemScanProps = (
    data: ItemScanProps
) => Promise<ItemScanProps[]> | ItemScanProps[];

export type ScanItemSocketProps = (data: ItemScanProps) => void;
