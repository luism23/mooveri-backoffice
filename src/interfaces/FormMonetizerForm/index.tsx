export type DataFormMonetizerFormInputs =
    | 'fullName'
    | 'lastName'
    | 'country'
    | 'state'
    | 'city'
    | 'zipPostalCode'
    | 'confirm18'
    | 'Street';

export type DataFormMonetizerForm<V = any> = {
    [id in DataFormMonetizerFormInputs]?: V;
};
