export interface DataCodePhone {
    phone: string;
    code: string;
}
