export type DataChatMessegesInputs = 'chat';

export type DataChatMesseges<V = string> = {
    [id in DataChatMessegesInputs]?: V;
};
