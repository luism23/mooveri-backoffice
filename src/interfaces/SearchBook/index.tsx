export type DataSearchBookInputs =
    | 'date'
    | 'moveSize'
    | 'movingFrom'
    | 'movingTo'
    | 'scan'
    | 'youStairs'
    | 'numberStairs'
    | 'doStorage'
    | 'doPacking';

export type DataSearchBook<V = string> = {
    [id in DataSearchBookInputs]?: V;
};
