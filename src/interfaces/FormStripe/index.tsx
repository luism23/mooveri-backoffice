export type DataFormStripeInputs =
    | 'firstName'
    | 'LastName'
    | 'MiddleName'
    | 'country'
    | 'state'
    | 'stripe_before'
    | 'stripe_after'
    | 'city'
    | 'phone';

export type DataFormStripe<V = any> = {
    [id in DataFormStripeInputs]?: V;
};
