import { InfoProfileEditDataProps } from '@/components/InfoProfileEdit/Base';
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import { MonetizeData } from '@/interfaces/MonetizeData';
//import { AnalitycsData } from '@/interfaces/AnalityscsData';

export interface ProfileData {
    uuid?: string;
    user_uuid?: string;
    profile_uuid?: string;
    linksDefault: RSLinkConfigDataProps[];
    styleDefault: InfoProfileEditDataProps;
    isAprobed?: boolean;
    monetize?: MonetizeData;
}
