export type DataRsLoginInputs = 'google' | 'facebook';

export type DataRsLogin<V = string> = {
    [id in DataRsLoginInputs]?: V;
};
