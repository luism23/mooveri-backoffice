export type DataLoginPaymentInputs = 'email' | 'password' | 'type';

export type DataLoginPayment<V = string> = {
    [id in DataLoginPaymentInputs]?: V;
};
