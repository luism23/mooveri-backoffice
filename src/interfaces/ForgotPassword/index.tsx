export type DataForgotPasswordInputs = 'email' | 'type';

export type DataForgotPassword<V = string> = {
    [id in DataForgotPasswordInputs]?: V;
};
