export const AnimationsListConst = [
    'none',
    'beat',
    'bounce',
    'fade',
    'beat-fade',
    'flip',
    'shake',
    'spin',
    'pulse',
] as const;

export const AnimationsList = [...AnimationsListConst];

export type AnimationsListType = (typeof AnimationsListConst)[number];
