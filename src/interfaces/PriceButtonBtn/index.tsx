export interface parseIntPriceToNumberOptions {
    notDecimals?: boolean;
    noUse?: boolean;
}
export interface ContadoresPropsItem {
    number: string | number | undefined;
    text: string;
    link?: string;
    money?: boolean;
    query?: {
        [id: string]: string;
    };
    isText?: boolean;
    isDate?: boolean;
    parseIntPriceToNumberOptions?: parseIntPriceToNumberOptions;
    icon?: any;
    iconPos?: 'l' | 'r';
}

export type ContadoresPropsItems = ContadoresPropsItem | ContadoresPropsItem[];
