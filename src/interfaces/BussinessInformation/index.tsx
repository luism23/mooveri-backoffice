export type DataBussinessInformationInputs =
    | 'uuid'
    | 'img'
    | 'company'
    | 'legalName'
    | 'yearFounded'
    | 'contactName'
    | 'tel'
    | 'alternativeTel';

export type DataBussinessInformation<V = string> = {
    [id in DataBussinessInformationInputs]?: V;
};
