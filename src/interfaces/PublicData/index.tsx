import { InfoProfileEditDataProps } from '@/components/InfoProfileEdit/Base';

import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';

export interface PublicData {
    uuid?: string;
    name?: string;
    links: RSLinkConfigDataProps[];
    style: InfoProfileEditDataProps;
    onClickBtn?: (button?: RSLinkConfigDataProps) => void;
    userLinksBuy?: string[];
}
