import { DataCategory } from '@/interfaces/Category';

export interface DataNameCategory {
    name?: string;
    categories?: DataCategory[];
    token?: string;
}
