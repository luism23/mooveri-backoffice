import { CompanyProps } from '../Company';
import { TransactionProps } from '../Transaction';

export type UserStatusType = 'verify' | 'no-verify' | 'delete';

export interface UserAddressProps {
    address?: string;
    apt?: string;
    city?: string;
    name_location?: string;
    zip_code?: string;
}

export interface UserProps {
    id?: string;
    name?: string;
    phone?: string;
    email?: string;
    status?: UserStatusType;
    dateCreate?: Date;

    first_name?: string;
    imagen?: string;
    last_name?: string;
    status_confirm?: boolean;
    type_login?: string;
    created_at?: Date;
    password?: string;

    address?: UserAddressProps;

    transactions?: TransactionProps[];
    companies?: CompanyProps[];
}

export type DataUserInputs =
    | 'id'
    | 'name'
    | 'phone'
    | 'email'
    | 'status'
    | 'dateCreate'
    | 'first_name'
    | 'imagen'
    | 'last_name'
    | 'status_confirm'
    | 'type_login'
    | 'created_at';

export type DataUser<V = string> = {
    [id in DataUserInputs]?: V;
};

export type DataUserProps = DataUser<string | Date | boolean>;
