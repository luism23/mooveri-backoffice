export type InformationSubmittedStatusType =
    | 'In_Review'
    | 'Verified'
    | 'Appeal'
    | 'Without_Sending';
