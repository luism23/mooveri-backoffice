import { AnimationsListType } from '../Animations';
import { BackgroundDataObjectTypeProps } from '../Background';
import { BorderConfig } from '../Border';
import { BoxShadowConfig } from '../BoxShadow';
import { ConfigText } from '../ConfigText';

export const SizeButtonConst = ['slim', 'regular', 'strong'] as const;

export const SizeButton = [...SizeButtonConst];

export type SizeButtonType = (typeof SizeButtonConst)[number];

export type SizeButtonValuesType = { [id in SizeButtonType]?: string };

export const SizeButtonValues: SizeButtonValuesType = {
    slim: `
        p-5
    `,
    regular: `
        p-10
    `,
    strong: `
        p-15
    `,
};
export const SizeButtonContentImgValues: SizeButtonValuesType = {
    slim: `
        p-5
    `,
    regular: `
        p-10
    `,
    strong: `
        p-10
    `,
};
export const SizeButtonImgValues: SizeButtonValuesType = {
    slim: `
        height-20
        width-20
        object-fit-contain
        object-pocition-center
    `,
    regular: `
        height-25
        width-25
        object-fit-contain
        object-pocition-center
    `,
    strong: `
        height-30
        width-30
        object-fit-contain
        object-pocition-center
    `,
};
export const SizeButtonImgValue = `
    height-p-100
    aspect-ratio-1-1
    object-fit-contain
`;

export const BorderRadiusButtonConst = [
    'no-rounding',
    'semi-rounded',
    'rounded',
] as const;

export const BorderRadiusButton = [...BorderRadiusButtonConst];

export type BorderRadiusButtonType = (typeof BorderRadiusButtonConst)[number];

export type BorderRadiusButtonValuesType = {
    [id in BorderRadiusButtonType]?: string;
};

export const BorderRadiusButtonValues: BorderRadiusButtonValuesType = {
    'no-rounding': `
        border-radius-0
    `,
    'semi-rounded': `
        border-radius-10
    `,
    rounded: `
        border-radius-100
    `,
};

export const IconButtonConst = ['sin', 'con'] as const;

export const IconButton = [...IconButtonConst];

export type IconButtonType = (typeof IconButtonConst)[number];

export type IconButtonValuesType = {
    [id in IconButtonType]?: string;
};

export const IconButtonValues: IconButtonValuesType = {
    sin: `
        d-none
    `,
    con: `
        left-15   
    `,
};

export interface ButtonIconConfig {
    borderRadius?: BorderRadiusButtonType;
    background?: BackgroundDataObjectTypeProps;
    border?: BorderConfig;
    padding?: number;
    size?: number;
}

export type ButtonPrincipalConfigType =
    | 'size'
    | 'padding'
    | 'border'
    | 'icon'
    | 'bg';

export interface ButtonPrincipalConfig extends ButtonIconConfig {
    color?: string;
}

export type ButtonConfigType =
    | 'type'
    | 'box-shadow'
    | 'border'
    | 'bg'
    | 'text'
    | 'icon';

export interface ButtonConfig {
    size?: SizeButtonType;
    borderRadius?: BorderRadiusButtonType;
    background?: BackgroundDataObjectTypeProps;
    text?: ConfigText;
    icon?: IconButtonType;
    border?: BorderConfig;
    boxShadow?: BoxShadowConfig;
    iconConfig?: ButtonIconConfig;
    className?: string;
}

export const ButtonAnimationsConfigConst = [
    'on-page-load',
    'infinite',
    'hover',
] as const;
export const ButtonAnimationsConfig = [...ButtonAnimationsConfigConst];
export type ButtonAnimationsConfigType =
    (typeof ButtonAnimationsConfigConst)[number];

export type ButtonAnimationsConfig = {
    [id in ButtonAnimationsConfigType]?: AnimationsListType;
};
