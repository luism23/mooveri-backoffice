export type DataPersonalEditInputs =
    | 'firstName'
    | 'lastName'
    | 'phone'
    | 'email'
    | 'token'
    | 'type';

export type DataPersonalEdit<V = string> = {
    [id in DataPersonalEditInputs]?: V;
};
