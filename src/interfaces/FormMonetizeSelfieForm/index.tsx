export type DataFormMonetizeSelfieFormInputs = 'avatar';

export type DataFormMonetizeSelfieForm<V = any> = {
    [id in DataFormMonetizeSelfieFormInputs]?: V;
};
