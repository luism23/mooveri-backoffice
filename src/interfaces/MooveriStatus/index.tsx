export enum TYPE_LOGIN {
    EMAIL,
    OAUTH,
}
export enum DAY {
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday,
    Sunday,
}
export enum ROLE {
    ADMIN,
    REVIEWER,
    BACKOFFICE,
}
export enum VEREFY_STATUS {
    VERIFIED,
    NOT_VERIFIED,
}
export enum PROFILE_STATUS {
    ACTIVE,
    HIDDEN,
    BLOCKED,
}
export enum TYPE_USER_TRANSACTION {
    GUEST,
    REGISTERED,
}
export enum TIPE_PRECIO {
    STANDAR,
    OVERTIME,
    WEEKEND_RATE,
}
export enum STATUS {
    CREATE = 'CREATE',
    CANCELLED = 'CANCELLED',
    APPROVED = 'APPROVED',
}
export enum MOVE_STATUS {
    CREATE,
    DONE,
    CANCELLED,
}
