import { BackgroundDataObjectTypeProps } from '../Background';
import { BorderRadiusButtonType } from '../Button';

export interface ConfigImgProfile {
    useLogoTolinkme?: boolean;
    bg?: BackgroundDataObjectTypeProps;
    borderSize?: number;
    borderType?: BorderRadiusButtonType;
}
