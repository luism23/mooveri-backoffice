export type TypeDate = 'date' | 'month' | 'week' | 'time';

export const DaysConst = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday',
] as const;

export const Days = [...DaysConst];

export type TypeDay = (typeof DaysConst)[number];
