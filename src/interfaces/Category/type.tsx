import { CategoryType } from '@/data/components/Categories';

export interface DataCategoryType extends CategoryType {
    active?: boolean;
}
