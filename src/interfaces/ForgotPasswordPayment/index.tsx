export type DataForgotPasswordPaymentInputs = 'email' | 'type';

export type DataForgotPasswordPayment<V = string> = {
    [id in DataForgotPasswordPaymentInputs]?: V;
};
