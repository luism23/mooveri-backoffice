export type DataRegisterInputs =
    | 'type'
    | 'userName'
    | 'email'
    | 'name'
    | 'firstName'
    | 'lastName'
    | 'phone'
    | 'password'
    | 'repeatPassword';

export type DataRegister<V = string> = {
    [id in DataRegisterInputs]?: V;
};
