export type DataFormPayInputs = 'paymentMethodId';

export type DataFormPay<V = any> = {
    [id in DataFormPayInputs]?: V;
};
