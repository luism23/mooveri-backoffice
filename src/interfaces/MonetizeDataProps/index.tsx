import { InputFileDataProps } from '@/components/Input/File/Base';
import { DataFormMonetizerForm } from '../FormMonetizerForm';
import { DataFormMonetizeSelfieForm } from '../FormMonetizeSelfieForm';
import { DataMonetizeCategory } from '../MonetizeCategory';
import { DataVerifyIdentity } from '../VerifyIdentity';

export interface MonetizeDataProps {
    isCreate?: boolean;
    step1: DataFormMonetizerForm;
    step2: DataVerifyIdentity<InputFileDataProps>;
    step3: DataFormMonetizeSelfieForm;
    step4: DataMonetizeCategory;
}
