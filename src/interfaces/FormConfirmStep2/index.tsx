export type DataFormConfirmStep2Inputs =
    | 'contactName'
    | 'phone'
    | 'phoneAlternative'
    | 'state'
    | 'city'
    | 'search';

export type DataFormConfirmStep2Props<V = string> = {
    [id in DataFormConfirmStep2Inputs]?: V;
};
