export type DataFormConfirmStep3Inputs =
    | 'numberTrucks'
    | 'numberWorkers'
    | 'search';

export type DataFormConfirmStep3Props<V = string> = {
    [id in DataFormConfirmStep3Inputs]?: V;
};
