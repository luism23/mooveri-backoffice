import { Story, Meta } from "@storybook/react";

import { Mouse } from "./index";

export default {
    title: "Svg/Mouse",
    component: Mouse,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Mouse {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
