import { Story, Meta } from "@storybook/react";

import { Desing } from "./index";

export default {
    title: "Svg/Desing",
    component: Desing,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Desing {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
