import { Story, Meta } from "@storybook/react";

import { Upload } from "./index";

export default {
    title: "Svg/Upload",
    component: Upload,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Upload {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
