import { Story, Meta } from "@storybook/react";

import { Png } from "./index";

export default {
    title: "Svg/Png",
    component: Png,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Png {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
