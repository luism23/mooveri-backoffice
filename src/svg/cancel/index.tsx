export const Cancel = ({ size = 16 }: { size?: number }) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width={size / 16 + 'rem'}
        viewBox="0 0 14.744 14.744"
    >
        <path
            data-name="Path 11636"
            d="M131.651 131.651a1.493 1.493 0 0 1-2.093 0l-4.851-4.851-4.851 4.851a1.48 1.48 0 0 1-2.093-2.093l4.851-4.851-4.851-4.851a1.48 1.48 0 0 1 2.093-2.093l4.851 4.851 4.851-4.851a1.48 1.48 0 0 1 2.093 2.093l-4.851 4.851 4.851 4.851a1.493 1.493 0 0 1 0 2.093z"
            transform="translate(-117.334 -117.334)"
            fill="currentColor"
        ></path>
    </svg>
);
export default Cancel;
