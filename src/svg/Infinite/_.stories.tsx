import { Story, Meta } from "@storybook/react";

import { Infinite } from "./index";

export default {
    title: "Svg/Infinite",
    component: Infinite,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Infinite {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
