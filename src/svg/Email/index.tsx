export const Email = ({ size = 16 }: { size?: number }) => (
    <svg
        width={size / 16 + 'rem'}
        viewBox="0 0 650 650"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
    >
        <path
            d="M117 133C90.5 133 69 154.5 69 181C69 196.1 76.1 210.3 88.2 219.4L305.8 382.6C317.2 391.1 332.8 391.1 344.2 382.6L561.8 219.4C573.9 210.3 581 196.1 581 181C581 154.5 559.5 133 533 133H117ZM69 245V453C69 488.3 97.7 517 133 517H517C552.3 517 581 488.3 581 453V245L363.4 408.2C340.6 425.3 309.4 425.3 286.6 408.2L69 245Z"
            fill="currentColor"
        />
    </svg>
);
export default Email;
