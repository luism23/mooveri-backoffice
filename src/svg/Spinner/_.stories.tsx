import { Story, Meta } from "@storybook/react";

import { Spinner } from "./index";

export default {
    title: "Svg/Spinner",
    component: Spinner,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Spinner {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
