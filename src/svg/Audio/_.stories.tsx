import { Story, Meta } from "@storybook/react";

import { Audio } from "./index";

export default {
    title: "Svg/Audio",
    component: Audio,
} as Meta;

const TemplateIndex: Story<{ size?: number }> = (args) => (
    <Audio {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
