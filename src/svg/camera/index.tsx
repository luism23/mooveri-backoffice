export const Camera = ({ size = 16 }: { size?: number }) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        width={size / 16 + 'rem'}
        viewBox="0 0 28 22"
    >
        <path
            data-name="Path 11880"
            d="M28 8.922V8a1 1 0 0 0-1-1h-3a1 1 0 0 0-1 1v.191l-1.368-.183A1 1 0 0 0 21.5 8a.5.5 0 0 1-.5-.5A2.5 2.5 0 0 0 18.5 5h-5A2.5 2.5 0 0 0 11 7.5a.5.5 0 0 1-.5.5.978.978 0 0 0-.132.009l-5.79.773A2.977 2.977 0 0 0 2 11.725V24.03A2.973 2.973 0 0 0 4.97 27h22.06A2.973 2.973 0 0 0 30 24.03V11.725a2.977 2.977 0 0 0-2-2.8zM15 6h2a1 1 0 0 1 0 2h-2a1 1 0 0 1 0-2zm-6 8a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-2a1 1 0 0 1 1-1h3a1 1 0 0 1 1 1zm7 9a6 6 0 1 1 6-6 6.007 6.007 0 0 1-6 6z"
            fill="currentColor"
            transform="translate(-2 -5)"
        ></path>
        <path
            data-name="Path 11881"
            d="M16 13a4 4 0 1 0 4 4 4 4 0 0 0-4-4zm0 7a1 1 0 0 1 0-2 1 1 0 0 0 1-1 1 1 0 0 1 2 0 3 3 0 0 1-3 3z"
            fill="currentColor"
            transform="translate(-2 -5)"
        ></path>
    </svg>
);
export default Camera;
