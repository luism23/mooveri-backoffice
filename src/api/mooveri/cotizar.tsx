import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';
import { URL } from '@/api/mooveri/_';
import { request } from '@/api/request';
import { DataCotizar } from '@/interfaces/Cotizar';

export type CotizarProps = (
    data: DataCotizar
) => Promise<SubmitResult> | SubmitResult;

export const Cotizar: CotizarProps = async (data: DataCotizar) => {
    log('Cotizar Data', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'SMS Send',
        };
    }
    const respond = await request({
        method: 'post',
        url: `${URL}/get_book`,
        data,
    });
    log('respond Cotizar', respond, 'aqua');
    if (respond.type == 'ok') {
        return {
            status: 'ok',
            message: 'Cotizado',
        };
    }

    return {
        status: 'ok',
        message: 'Cotizado',
    };
};
