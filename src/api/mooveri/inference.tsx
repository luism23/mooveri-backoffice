import log from '@/functions/log';
import { URLINFERENCIA } from '@/api/mooveri/_';
import { request } from '@/api/request';

import { ItemScanProps, ScanItemScanProps } from '@/interfaces/Scan/ItemScan';

export const Inference: ScanItemScanProps = async (data: ItemScanProps) => {
    log('Inference Data', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return [data];
    }

    // return [
    //     {
    //         bbox: [34, 520, 92, 685],
    //         image: "",
    //         label: "couch",
    //     },
    // ];
    const respond = await request({
        method: 'post',
        url: `${URLINFERENCIA}/inference`,
        data: {
            image: data.fileData.split(';base64,')[1],
        },
    });
    // alert(JSON.stringify(respond));
    log('respond Inference', respond, 'aqua');
    if (respond.type == 'ok') {
        if (respond?.result?.error) {
            return [];
        }
        return (respond?.result ?? [])?.map((r: any) => ({
            text: r.label,
            bbox: r.bbox,
            fileData: `data:image/png;base64,${r.image}`,
            name: r.label,
        }));
    }

    return [];
};
