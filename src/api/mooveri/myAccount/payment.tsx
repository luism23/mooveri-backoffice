import { URL } from '@/api/mooveri/_';
import log from '@/functions/log';
import { PaymentProps } from '@/interfaces/Payment';
import { CardData, onDeleteCardType, onSaveCardType } from '@/interfaces/Card';
import { UserLoginProps } from '@/hook/useUser';
import { request } from '@/api/request';

export type onLoadPaymentsProps = (
    user?: UserLoginProps
) => Promise<PaymentProps[]> | PaymentProps[];

export const onLoadPayments: onLoadPaymentsProps = async (
    user?: UserLoginProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return [
            {
                id: '1',
                card: {
                    type: 'visa',
                    number: 'xxxx xxxx xxxx 1111',
                },
            },
            {
                id: '2',
                card: {
                    type: 'mastercard',
                    number: 'xxxx xxxx xxxx 1111',
                },
            },
            {
                id: '3',
                card: {
                    type: 'amex',
                    number: 'xxxx xxxx xxxx 1111',
                },
            },
        ];
    }
    const respond = await request({
        method: 'get',
        url: `${URL}/client?uuid=${user?.id}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('respond onLoadPayments', respond, 'aqua');
    if (respond.type == 'error') {
        throw {
            message: '',
        };
    }
    const userR = respond.result?.data[0];
    if (!userR) {
        throw {
            message: '',
        };
    }
    const stripe_id = userR?.client_payment_details?.[0]?.stripe_id ?? null;

    if (!stripe_id) {
        return [];
    }

    const respondPayment = await request({
        method: 'get',
        url: `${URL}/stripe/cards?id=${stripe_id}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('respondPayment onLoadPayments', respondPayment, 'aqua');

    if (respondPayment.type == 'error') {
        throw {
            message: '',
        };
    }

    return (respondPayment.result?.cards ?? []).map((card: any) => {
        const payment: PaymentProps = {
            id: card?.id,
            card: {
                number: `xxxx xxx xxxx ${card?.card?.last4}`,
                type: card?.card?.brand,
            },
        };
        return payment;
    });
};

export const onSaveCard: onSaveCardType = async (
    data: CardData,
    user?: UserLoginProps
) => {
    log('Data onSaveInput', {
        data,
        user,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Save Card Ok',
        };
    }
    const respond = await request({
        method: 'post',
        url: `${URL}/stripe/saveCard`,
        headers: {
            authorization: user?.token,
        },
        data: {
            payment_method: data.id,
            customer_id: user?.stripe_id ?? null,
            uuid: user?.id,
        },
    });
    log('respond onSaveCard', respond, 'aqua');
    if (respond.type == 'error') {
        return {
            status: 'error',
            message:
                respond?.error?.response?.data?.error ??
                respond?.error?.response?.data?.message ??
                '',
        };
    }
    return {
        status: 'ok',
        message: 'Save Card Ok',
    };
};

export const onDeletePayment: onDeleteCardType = async ({
    id,
    user,
}: {
    user?: UserLoginProps;
    id?: string;
}) => {
    log('Data onDeletePayment', {
        id,
        user,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Delete Ok',
        };
    }
    const respond = await request({
        method: 'delete',
        url: `${URL}/stripe/deleteCard?payment_method=${id}`,
        headers: {
            authorization: user?.token,
        },
    });
    if (respond.type == 'error') {
        return {
            status: 'error',
            message:
                respond?.error?.response?.data?.error ??
                respond?.error?.response?.data?.message ??
                '',
        };
    }
    log('respond onDeletePayment', respond, 'aqua');
    return {
        status: 'ok',
        message: 'Delete Ok',
    };
};
