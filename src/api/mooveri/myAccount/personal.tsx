import { URL } from '@/api/mooveri/_';
import { request } from '@/api/request';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';
import {
    DataPersonalEdit,
    DataPersonalEditInputs,
} from '@/interfaces/PersonalEdit';

export interface onSaveInputDataProps {
    id: DataPersonalEditInputs;
    value: string;
    user?: UserLoginProps;
}

export type onSaveInputProps = (
    data: onSaveInputDataProps
) => Promise<SubmitResult> | SubmitResult;

export const onSaveInput: onSaveInputProps = async (
    data: onSaveInputDataProps
) => {
    log('Data onSaveInput', {
        data,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Save ok',
        };
        return r;
    }

    const parseId: DataPersonalEdit = {
        email: 'email',
        firstName: 'first_name',
        lastName: 'last_name',
        phone: 'phone',
    };

    const id = parseId[data.id];

    const dataSend = {
        [id ?? '']: data.value,
    };

    const responde = await request({
        method: 'put',
        url: `${URL}/client/${data?.user?.id}`,
        data: dataSend,
        headers: {
            Authorization: data?.user?.token,
            'Content-Type': 'application/json',
        },
    });
    log('responde put client', responde);

    if (responde.type == 'error') {
        if (responde?.error?.response?.data?.code == 401) {
            throw {
                code: 401,
                message: 'Sesion Expirada',
            };
        }
        throw {
            message:
                responde?.error?.response?.data?.message ??
                'Upps, Ocurrion un Error',
        };
    }

    const r: SubmitResult = {
        status: 'ok',
        message: 'Informacion Guardada Exitosamente',
    };
    return r;
};

export type onLoadDataProps = (
    user?: UserLoginProps
) => Promise<DataPersonalEdit<string>> | DataPersonalEdit<string>;

export const onLoadData: onLoadDataProps = async (user?: UserLoginProps) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            email: 'Email.test@gmail.com',
            firstName: 'Fist Name',
            lastName: 'Last Name',
            phone: '+57-1112233',
            type: '',
        };
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/client?uuid=${user?.id}`,
    });
    log('responde get client', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }

    const userR = responde.result?.data?.[0];

    if (!userR) {
        throw {
            code: 401,
            message: 'Error',
        };
    }

    return {
        email: userR?.email,
        firstName: userR?.first_name,
        lastName: userR?.last_name,
        phone: userR?.phone,
        type: user?.role,
    };
};
