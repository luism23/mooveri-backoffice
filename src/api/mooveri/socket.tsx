import io from 'socket.io-client';
import { URLSOCKET } from '@/api/mooveri/_';

export const Socket = io(URLSOCKET, { transports: ['websocket'] });
