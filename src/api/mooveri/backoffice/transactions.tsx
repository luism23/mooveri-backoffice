import { URL } from '@/api/mooveri/_';
import { request } from '@/api/request';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';

import { TransactionProps } from '@/interfaces/Transaction';
import { parseUserMooveri } from './users';
import { parseCompanyMooveri } from './companies';

export const parseTransactionMooveri = (data: any): TransactionProps => {
    try {
        const move_details = data?.move_details?.[0] ?? {};

        const r: TransactionProps = {
            id: data?.uuid,
            address: move_details?.from_zip_code ?? '',
            dateCreate: new Date(data.created_at),
            price: data?.total_amount / 100,
            user: {
                id: data?.client?.uuid ?? '',
                name: data?.client?.first_name ?? '',
            },

            chat_room_id: data?.chat_room_id,
            ...(data?.client ? { client: parseUserMooveri(data?.client) } : {}),

            client_uuid: data?.client_uuid,
            company_uuid: data?.company_uuid,
            created_at: data?.created_at,
            payment_intent_id: data?.payment_intent_id,
            status: data?.status,
            total_amount: data?.total_amount / 100,
            type: data?.type,
            type_payment: data?.type_payment,
            type_user_transaction: data?.type_user_transaction,
            update_at: data?.update_at,
            uuid: data?.uuid,

            ...(data?.company
                ? { company: parseCompanyMooveri(data?.company) }
                : {}),
        };
        return r;
    } catch (error) {
        log('error parseTransactionMooveri', error, 'red');
        throw 'Error parseTransactionMooveri';
    }
};

export type onLoadTransactionProps = (
    user?: UserLoginProps
) => Promise<TransactionProps[]> | TransactionProps[];

export const onLoadTransaction: onLoadTransactionProps = async (
    user?: UserLoginProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return [
            {
                id: '1',
                user: {
                    id: '1',
                    name: 'Name',
                },
                address: 'address',
                dateCreate: new Date(),
                price: 500,
            },
            {
                id: '1',
                user: {
                    id: '1',
                    name: 'Name',
                },
                address: 'address',
                dateCreate: new Date(),
                price: 500,
            },
            {
                id: '1',
                user: {
                    id: '1',
                    name: 'Name',
                },
                address: 'address',
                dateCreate: new Date(),
                price: 500,
            },
        ];
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/transaction`,
        headers: {
            authorization: user?.token,
        },
    });
    log('responde get company', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }
    const transactions = responde.result;

    return transactions.map(parseTransactionMooveri);
};

export type onLoadTransactionByIdProps = (
    user?: UserLoginProps,
    uuid?: string
) => Promise<TransactionProps> | TransactionProps;

export const onLoadTransactionById: onLoadTransactionByIdProps = async (
    user?: UserLoginProps,
    uuid?: string
) => {
    log('Data onSaveInput', {
        user,
        URL,
        uuid,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            id: '1',
            user: {
                id: '1',
                name: 'Name',
            },
            address: 'address',
            dateCreate: new Date(),
            price: 500,
        };
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/transaction/${uuid}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('responde get transaction', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }
    const transactions = responde.result;

    return parseTransactionMooveri(transactions);
};
