import { URL } from '@/api/mooveri/_';
import { request } from '@/api/request';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';
import { parseHorariosMooveri } from '@/functions/parseHorariosMooveri';
import { UserLoginProps } from '@/hook/useUser';

import {
    CompanyHorariosProps,
    CompanyImportProps,
    CompanyPricesProps,
    CompanyProps,
    CompanySchedulesHours,
} from '@/interfaces/Company';
import { TransactionProps } from '@/interfaces/Transaction';
import { UserProps } from '@/interfaces/User';
import { parseTransactionMooveri } from './transactions';

export const parseCompanyMooveri = (company: any) => {
    try {
        const transactions: TransactionProps[] = (
            company?.transaction ?? []
        )?.map(parseTransactionMooveri);

        const customersObject: {
            [id: string]: UserProps;
        } = {};

        transactions.forEach((t: TransactionProps) => {
            if (t?.client && !customersObject[t?.client?.id ?? '']) {
                customersObject[t?.client?.id ?? ''] = t?.client;
            }
        });

        const customers = Object.values(customersObject);

        const company_precios = company.company_precios ?? [];

        const company_prices: CompanyPricesProps = {};

        company_precios.forEach((item: any) => {
            if (item.type == 'OVERTIME') {
                company_prices.overtime_uuid = item.uuid;
                company_prices.overtime_value = item.value;
            } else if (item.type == 'STANDAR') {
                company_prices.standar_uuid = item.uuid;
                company_prices.standar_value = item.value;
            } else if (item.type == 'WEEKEND_RATE') {
                company_prices.weekend_rate_uuid = item.uuid;
                company_prices.weekend_rate_value = item.value;
            }
        });

        const company_horarios: CompanyHorariosProps[] = (
            company?.company_horarios ?? []
        ).map((h: any) => {
            const r: CompanyHorariosProps = {
                uuid: h?.uuid,
                day: h?.day,
                active: h?.active,
                schedules: (h?.company_schedules_hours ?? [])?.map((s: any) => {
                    const r: CompanySchedulesHours = {
                        uuid: s?.uuid,
                        start: s?.start,
                        end: s?.end,
                    };
                    return r;
                }),
            };
            return r;
        });

        const r: CompanyProps = {
            id: company.uuid,
            name: company.name,
            address: company?.city,

            moovings: (company?.transaction ?? []).length,
            dateCreate: new Date(company.created_at),

            legal_name: company.legal_name,
            phone_1: company.phone_1,
            phone_2: company.phone_2,
            city: company.city,
            description: company.description,
            state: company.state,
            zip_code: company.zip_code,
            ssn: company.ssn,
            ein: company.ein,
            imagen: company.imagen,
            imagen_banner: company.imagen_banner,
            profile_status: company.profile_status,

            company_details: company?.company_details?.[0] ?? undefined,

            company_verification:
                company?.company_verification?.[0] ?? undefined,

            user_company: company?.user_company ?? undefined,

            transactions,

            customers,

            company_prices,

            company_horarios,
        };
        return r;
    } catch (error) {
        log('error parseCompanyMooveri', error, 'red');
        throw 'Error parseCompanyMooveri';
    }
};

export type onLoadCompanyProps = (
    user?: UserLoginProps
) => Promise<CompanyProps[]> | CompanyProps[];

export const onLoadCompany: onLoadCompanyProps = async (
    user?: UserLoginProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return [
            {
                id: '1',
                name: 'Name',
                address: 'address',
                moovings: 1,
                dateCreate: new Date('1-1-1'),
            },
            {
                id: '1',
                name: 'Name',
                address: 'address',
                moovings: 1,
                dateCreate: new Date('1-1-1'),
            },
            {
                id: '1',
                name: 'Name',
                address: 'address',
                moovings: 1,
                dateCreate: new Date('1-1-1'),
            },
        ];
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/company`,
        headers: {
            authorization: user?.token,
        },
    });
    log('responde get company', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }
    const companies = responde.result?.data;

    return companies.map(parseCompanyMooveri);
};

export type onLoadCompanyByIdProps = (
    user?: UserLoginProps,
    uuid?: string
) => Promise<CompanyProps> | CompanyProps;

export const onLoadCompanyById: onLoadCompanyByIdProps = async (
    user?: UserLoginProps,
    uuid?: string
) => {
    log('Data onSaveInput', {
        user,
        URL,
        uuid,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            id: '1',
            name: 'Name',
            address: 'address',
            moovings: 1,
            dateCreate: new Date('1-1-1'),
        };
    }
    const responde = await request({
        method: 'get',
        url: `${URL}/company?uuid=${uuid}`,
        headers: {
            authorization: user?.token,
        },
    });
    log('responde get company', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }
    const companies = responde.result?.data;

    return companies.map(parseCompanyMooveri)[0];
};

export type onUpdateCompanyByIdProps = (
    user?: UserLoginProps,
    company?: CompanyProps
) => Promise<SubmitResult> | SubmitResult;

export const onUpdateCompanyById: onUpdateCompanyByIdProps = async (
    user?: UserLoginProps,
    company?: CompanyProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
        company,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Company Update Ok',
        };
    }
    const responde = await request({
        method: 'put',
        url: `${URL}/company/${company?.id}`,
        headers: {
            authorization: user?.token,
        },
        data: {
            name: company?.name,
            legal_name: company?.legal_name,
            phone_1: company?.phone_1,
            phone_2: company?.phone_2,
            city: company?.city,
            description: company?.description,
            state: company?.state,
            zip_code: company?.zip_code,
            ssn: company?.ssn,
            ein: company?.ein,
            imagen: company?.imagen,
            imagen_banner: company?.imagen_banner,
        },
    });
    log('responde put company', responde);

    if (responde.type == 'error') {
        throw {
            code: 401,
            message: 'Error',
        };
    }
    const respondeDeatails = await request({
        method: 'put',
        url: `${URL}/company/details/${company?.id}`,
        headers: {
            authorization: user?.token,
        },
        data: {
            contact_name: company?.company_details?.contact_name,
            year_founded: company?.company_details?.year_founded,
            facebook_page_link: company?.company_details?.facebook_page_link,
            instagram_page_link: company?.company_details?.instagram_page_link,
            linkedin_link: company?.company_details?.linkedin_link,
            website_link: company?.company_details?.website_link,
            trucks_have: parseInt(
                `${company?.company_details?.trucks_have ?? 0}`
            ),
            workers_have: parseInt(
                `${company?.company_details?.workers_have ?? 0}`
            ),
        },
    });
    log('respondeDeatails put company', respondeDeatails);
    const respondeVerifycation = await request({
        method: 'put',
        url: `${URL}/company/verification/${company?.company_verification?.uuid}`,
        headers: {
            authorization: user?.token,
        },
        data: {
            local_license_link:
                company?.company_verification?.local_license_link,
            dot_license_link: company?.company_verification?.dot_license_link,
            cdl_link: company?.company_verification?.cdl_link,

            local_license_status:
                company?.company_verification?.local_license_status,
            dot_license_status:
                company?.company_verification?.dot_license_status,
            cdl_status: company?.company_verification?.cdl_status,
        },
    });
    log('respondeVerifycation put company', respondeVerifycation);

    //UPDATE PRICE
    if (company?.company_prices?.overtime_value) {
        if (company?.company_prices?.overtime_uuid) {
            const respondeUpateOvertime = await request({
                method: 'put',
                url: `${URL.split('/v1').join(
                    ''
                )}/backoffice/v1/company/values/${
                    company?.company_prices?.overtime_uuid
                }`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    value: `${company?.company_prices?.overtime_value ?? '0'}`,
                },
            });
            log('respondeUpateOvertime put company', respondeUpateOvertime);
        } else {
            const respondeCreateOvertime = await request({
                method: 'post',
                url: `${URL.split('/v1').join(
                    ''
                )}/backoffice/v1/company/values`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    company_uuid: company.id,
                    type: 'OVERTIME',
                    value: `${company?.company_prices?.overtime_value ?? '0'}`,
                },
            });
            log('respondeCreateOvertime put company', respondeCreateOvertime);
        }
    }
    if (company?.company_prices?.standar_value) {
        if (company?.company_prices?.standar_uuid) {
            const respondeUpatestandar = await request({
                method: 'put',
                url: `${URL.split('/v1').join(
                    ''
                )}/backoffice/v1/company/values/${
                    company?.company_prices?.standar_uuid
                }`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    value: `${company?.company_prices?.standar_value ?? '0'}`,
                },
            });
            log('respondeUpatestandar put company', respondeUpatestandar);
        } else {
            const respondeCreatestandar = await request({
                method: 'post',
                url: `${URL.split('/v1').join(
                    ''
                )}/backoffice/v1/company/values`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    company_uuid: company.id,
                    type: 'STANDAR',
                    value: `${company?.company_prices?.standar_value ?? '0'}`,
                },
            });
            log('respondeCreatestandar put company', respondeCreatestandar);
        }
    }
    if (company?.company_prices?.weekend_rate_value) {
        if (company?.company_prices?.weekend_rate_uuid) {
            const respondeUpateweekend_rate = await request({
                method: 'put',
                url: `${URL.split('/v1').join(
                    ''
                )}/backoffice/v1/company/values/${
                    company?.company_prices?.weekend_rate_uuid
                }`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    value: `${company?.company_prices?.standar_value ?? '0'}`,
                },
            });
            log(
                'respondeUpateweekend_rate put company',
                respondeUpateweekend_rate
            );
        } else {
            const respondeCreateweekend_rate = await request({
                method: 'post',
                url: `${URL.split('/v1').join(
                    ''
                )}/backoffice/v1/company/values`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    company_uuid: company.id,
                    type: 'WEEKEND_RATE',
                    value: `${company?.company_prices?.standar_value ?? '0'}`,
                },
            });
            log(
                'respondeCreateweekend_rate put company',
                respondeCreateweekend_rate
            );
        }
    }

    //UPDATE HORARIOS

    const horarios = company?.company_horarios ?? [];

    for (let i = 0; i < horarios.length; i++) {
        const horario = horarios[i];
        let horario_uuid: string | undefined = horario.uuid;
        if (horario.uuid) {
            ///update
            const respontUpdateHours = await request({
                method: 'put',
                url: `${URL.split('/v1').join('')}/backoffice/v1/schedules/${
                    horario.uuid
                }`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    active: horario.active ?? false,
                },
            });
            log('respontUpdateHours put company', respontUpdateHours);
        } else {
            ///create
            horario_uuid = undefined;
            const respontCreateHours = await request({
                method: 'post',
                url: `${URL.split('/v1').join('')}/backoffice/v1/schedules`,
                headers: {
                    authorization: user?.token,
                },
                data: {
                    active: horario.active ?? false,
                    type: horario.day,
                },
            });
            log('respontCreateHours post company', respontCreateHours);
        }
        const schedules = horario.schedules ?? [];
        for (let j = 0; j < schedules.length; j++) {
            const schedule = schedules[j];
            if (schedule.uuid) {
                if (schedule.delete) {
                    //delete
                    const respontDeleteSchedule = await request({
                        method: 'delete',
                        url: `${URL.split('/v1').join(
                            ''
                        )}/backoffice/v1/hours/${schedule.uuid}`,
                        headers: {
                            authorization: user?.token,
                        },
                    });
                    log(
                        'respontDeleteSchedule put company',
                        respontDeleteSchedule
                    );
                } else {
                    //update
                    const respontUpdateSchedule = await request({
                        method: 'put',
                        url: `${URL.split('/v1').join(
                            ''
                        )}/backoffice/v1/hours/${schedule.uuid}`,
                        headers: {
                            authorization: user?.token,
                        },
                        data: {
                            start: schedule.start,
                            end: schedule.end,
                        },
                    });
                    log(
                        'respontUpdateSchedule put company',
                        respontUpdateSchedule
                    );
                }
            } else {
                if (horario_uuid) {
                    //create
                    const respontCreateSchedule = await request({
                        method: 'post',
                        url: `${URL.split('/v1').join('')}/backoffice/v1/hours`,
                        headers: {
                            authorization: user?.token,
                        },
                        data: {
                            start: schedule.start,
                            end: schedule.end,
                            company_schedules_uuid: horario_uuid,
                        },
                    });
                    log(
                        'respontCreateSchedule put company',
                        respontCreateSchedule
                    );
                }
            }
        }
    }

    return {
        status: 'ok',
        message: 'Company Update Ok',
    };
};

export type onImportCompanyProps = (
    user?: UserLoginProps,
    companies?: CompanyImportProps[],
    setProgress?: (e: number) => void
) => Promise<SubmitResult> | SubmitResult;

export const onImportCompany: onImportCompanyProps = async (
    user?: UserLoginProps,
    companies?: CompanyImportProps[],
    setProgress?: (e: number) => void
) => {
    log('Data onSaveInput', {
        user,
        URL,
        companies,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Import Ok',
        };
    }

    const newCompanies = (companies ?? [])?.map((c) => ({
        first_name: c.first_name,
        last_name: c.last_name,
        phone: c.phone,
        email: c.email,
        password: c.password,
        company: {
            name: c.company_name,
            legal_name: c.company_legal_name,
            phone_1: c.company_phone_1,
            phone_2: c.company_phone_2,
            city: c.company_city,
            description: c.company_description,
            state: c.company_state,
            zip_code: c.company_zip_code,
            ssn: c.company_ssn,
            ein: c.company_ein,
            imagen: c.company_imagen,
            imagen_banner: c.company_imagen_banner,

            company_details: {
                create: {
                    contact_name: c?.company_contact_name,
                    year_founded: c?.company_year_founded,
                    facebook_page_link: c?.company_facebook_page_link,
                    instagram_page_link: c?.company_instagram_page_link,
                    linkedin_link: c?.company_linkedin_link,
                    website_link: c?.company_website_link,
                    trucks_have: parseInt(`${c?.company_trucks_have ?? 0}`),
                    workers_have: parseInt(`${c?.company_workers_have ?? 0}`),
                },
            },
            company_values: [
                {
                    type: 'OVERTIME',
                    value: c.company_value_overtime,
                },
                {
                    type: 'STANDAR',
                    value: c.company_value_standar,
                },
                {
                    type: 'WEEKEND_RATE',
                    value: c.company_value_weekend_rate,
                },
            ],
            company_horarios: [
                {
                    day: 'Monday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_monday ?? '00:00-00:00'
                        ),
                    },
                },
                {
                    day: 'Tuesday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_tuesday ?? '00:00-00:00'
                        ),
                    },
                },
                {
                    day: 'Wednesday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_wednesday ?? '00:00-00:00'
                        ),
                    },
                },
                {
                    day: 'Thursday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_thursday ?? '00:00-00:00'
                        ),
                    },
                },
                {
                    day: 'Friday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_friday ?? '00:00-00:00'
                        ),
                    },
                },
                {
                    day: 'Saturday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_saturday ?? '00:00-00:00'
                        ),
                    },
                },
                {
                    day: 'Sunday',
                    active: true,
                    company_schedules_hours: {
                        create: parseHorariosMooveri(
                            c.company_horario_sunday ?? '00:00-00:00'
                        ),
                    },
                },
            ],
        },
    }));

    const total = newCompanies.length;
    const results = [];
    for (let i = 0; i < total; i++) {
        const company = newCompanies[i];
        const responde = await request({
            method: 'post',
            url: `${URL.split('/v1').join('')}/backoffice/v1/bulkCompany`,
            headers: {
                authorization: user?.token,
            },
            data: company,
        });
        results.push({
            responde,
            company,
        });
        if (setProgress) {
            setProgress(((i + 1) / total) * 100);
        }
    }

    return {
        status: 'ok',
        message: 'Import Ok',
        data: results,
    };
};

export const onCreateCompany: onUpdateCompanyByIdProps = async (
    user?: UserLoginProps,
    company?: CompanyProps
) => {
    log('Data onSaveInput', {
        user,
        URL,
        company,
    });
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'Company Create Ok',
        };
    }

    const company_send = {
        first_name: company?.user_company?.first_name,
        last_name: company?.user_company?.last_name,
        phone: company?.user_company?.phone,
        email: company?.user_company?.email,
        password: company?.user_company?.password,
        company: {
            name: company?.name,
            legal_name: company?.legal_name,
            phone_1: company?.phone_1,
            phone_2: company?.phone_2,
            city: company?.city,
            description: company?.description,
            state: company?.state,
            zip_code: company?.zip_code,
            ssn: company?.ssn,
            ein: company?.ein,
            imagen: company?.imagen,
            imagen_banner: company?.imagen_banner,

            company_details: {
                create: {
                    contact_name: company?.company_details?.contact_name,
                    year_founded: company?.company_details?.year_founded,
                    facebook_page_link:
                        company?.company_details?.facebook_page_link,
                    instagram_page_link:
                        company?.company_details?.instagram_page_link,
                    linkedin_link: company?.company_details?.linkedin_link,
                    website_link: company?.company_details?.website_link,
                    trucks_have: parseInt(
                        `${company?.company_details?.trucks_have ?? 0}`
                    ),
                    workers_have: parseInt(
                        `${company?.company_details?.workers_have ?? 0}`
                    ),
                },
            },
            company_verification: {
                create: {
                    local_license_link:
                        company?.company_verification?.local_license_link,
                    dot_license_link:
                        company?.company_verification?.dot_license_link,
                    cdl_link: company?.company_verification?.cdl_link,

                    local_license_status:
                        company?.company_verification?.local_license_status,
                    dot_license_status:
                        company?.company_verification?.dot_license_status,
                    cdl_status: company?.company_verification?.cdl_status,
                },
            },

            company_values: [
                {
                    type: 'OVERTIME',
                    value: `${company?.company_prices?.overtime_value ?? 0}`,
                },
                {
                    type: 'STANDAR',
                    value: `${company?.company_prices?.standar_value ?? 0}`,
                },
                {
                    type: 'WEEKEND_RATE',
                    value: `${
                        company?.company_prices?.weekend_rate_value ?? 0
                    }`,
                },
            ],
            company_horarios: (company?.company_horarios ?? [])?.map((e) => {
                const schedules = e.schedules;
                const schedulesCreate = schedules
                    ?.filter((e) => !e.delete)
                    ?.map((s) => {
                        return {
                            start: s.start ?? '0:0',
                            end: s.end ?? '0:0',
                        };
                    });
                return {
                    day: e.day,
                    active: e.active ?? false,
                    ...(schedulesCreate?.length
                        ? {
                              company_schedules_hours: {
                                  create: schedulesCreate,
                              },
                          }
                        : {}),
                };
            }),
        },
    };

    const responde = await request({
        method: 'post',
        url: `${URL.split('/v1').join('')}/backoffice/v1/bulkCompany`,
        headers: {
            authorization: user?.token,
        },
        data: company_send,
    });

    log('responde create company', responde);
    if (responde.error) {
        return {
            status: 'error',
            message: `${responde.error}`,
        };
    }
    return {
        status: 'ok',
        message: 'Company Update Ok',
    };
};
