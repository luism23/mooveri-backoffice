import jwt_decode from 'jwt-decode';

import { onSubmintLogin } from '@/components/Form/Login/Base';
import log from '@/functions/log';

import { DataLogin } from '@/interfaces/Login';
import { request } from '@/api/request';

import { URL } from '@/api/mooveri/_';
import { UserRoles } from '@/hook/useUser';

export const Login: onSubmintLogin = async (data: DataLogin) => {
    log('DataLogin', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: '😎 Successfully logged in Mooveri',
            data: {
                id: '1',
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
                name: 'test',
                img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
                role: data.type as UserRoles,
            },
        };
    }

    const respond = await request({
        method: 'post',
        url: `${URL}/auth`,
        data: {
            type: data.type,
            email: data.email,
            password: data.password,
        },
    });
    log('respond login', respond, 'aqua');

    if (respond.type == 'error') {
        throw {
            message: respond.error?.response?.data?.message ?? '',
        };
    }
    const token = respond?.result?.data;

    try {
        const user: any = jwt_decode(token);
        log('respond user decode', user, 'aqua');

        let stripe_id = '';
        if (data.type == 'company') {
            stripe_id = '';
        } else if (data.type == 'backoffice') {
            stripe_id = '';
        } else {
            const respond = await request({
                method: 'get',
                url: `${URL}/client?uuid=${user?.uuid}`,
                headers: {
                    authorization: token,
                },
            });
            log('respond onLoadPayments', respond, 'aqua');
            const userR = respond.result?.data[0];
            if (!userR) {
                throw {
                    message: 'User not found',
                };
            }
            stripe_id = userR?.client_payment_details?.[0]?.stripe_id ?? null;
        }

        return {
            status: 'ok',
            message: '😎 Successfully logged in Mooveri',
            data: {
                id: user?.uuid ?? '',
                token,
                name: user?.first_name,
                img: user?.imagen,
                email: user?.email,
                stripe_id,
                role: data.type as UserRoles,
            },
        };
    } catch (error: any) {
        throw {
            message: `${error?.message ?? error}`,
        };
    }
};
