import axios from 'axios';
import log from '@/functions/log';

export interface requestProps {
    method?: 'get' | 'post' | 'put' | 'delete';
    url: string;
    data?: {
        [key: string]: any;
    };
    headers?: {
        [key: string]: any;
    };
    params?: {
        [key: string]: any; 
    };
}

export type requestResultStatus = 'ok' | 'error';

export interface requestResult {
    type: requestResultStatus;
    result?: any;
    error?: any;
}

export type requestFuntion = (config: requestProps) => Promise<requestResult>;

export const request: requestFuntion = async (config: requestProps) => {
    try {
        const response = await axios(config);
        log('RESPOND REQUEST', response, 'green');
        return {
            type: 'ok',
            result: response.data,
        };
    } catch (error: any) {
        log('ERROR REQUEST', error, 'red');
        return {
            type: 'error',
            error,
        };
    }
};
