import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';

import { onSubmintLogin } from '@/components/Form/Login/Base';
import log from '@/functions/log';

import { DataLogin } from '@/interfaces/Login';

export const Login: onSubmintLogin = async (data: DataLogin) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: '😎 Successfully logged in',
            data: {
                id: '1',
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
                name: 'test',
                img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
            },
        };
    }
    const result = await request({
        method: 'post',
        url: `${URL}/auth/signin`,
        data: {
            ...data,
            email: data.email?.toLocaleLowerCase(),
        },
    });
    log('result login', result, 'aqua');
    if (result.type == 'error') {
        throw {
            message:
                result?.error?.response?.data?.data?.message ??
                result?.error?.response?.data?.data,
        };
    }
    if (result.result.status == 'error') {
        throw {
            message: result.result.data,
        };
    }
    const user = result.result.data.user;
    const token = result.result.data.token;
    const resultProfile = await request({
        url: `${URL}/model-profile/username?username=${user.username}`,
    });
    const user_ModelProfile =
        resultProfile?.result?.data?.user?.user_ModelProfile;
    const profile = user_ModelProfile?.profile ?? {};
    return {
        status: 'ok',
        message: '😎 Successfully logged in',
        data: {
            id: user.uuid,
            token,
            name: user.username,
            img: profile?.profile_img ?? '',
        },
    };
};
