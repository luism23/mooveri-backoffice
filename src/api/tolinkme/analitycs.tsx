import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';
import { SubmitResult } from '@/components/Form/Base';
import log from '@/functions/log';

export interface DATACLICKBTN {
    buttonUuid?: string;
    ip?: string;
    userAgent?: string;
}

export type CLICKBTNINTERFACE = (data: DATACLICKBTN) => Promise<SubmitResult>;

export const CLICKBTN: CLICKBTNINTERFACE = async (data: DATACLICKBTN) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
        };
        return r;
    }
    const result = await request({
        method: 'post',
        url: `${URL}/buttons/hit`,
        data,
        headers: {
            'Content-Type': 'application/json',
        },
    });
    log('post result CLICK BTN', result, 'aqua');
    return {
        status: 'ok',
    };
};

export interface DATAHITPROFILE {
    user_uuid?: string;
    ip?: string;
    'user-agent'?: string;
}

export type HITPROFILEINTERFACE = (
    data: DATAHITPROFILE
) => Promise<SubmitResult>;

export const HITPROFILE: HITPROFILEINTERFACE = async (data: DATAHITPROFILE) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
        };
        return r;
    }
    const result = await request({
        method: 'post',
        url: `${URL}/model-profile/profile-hit`,
        data: {
            user_uuid: data.user_uuid,
            ip: data.ip,
            'user-agent': data['user-agent'],
        },
        headers: {
            'Content-Type': 'application/json',
        },
    });
    log('post result HIT PROFILE', result, 'aqua');
    return {
        status: 'ok',
    };
};
