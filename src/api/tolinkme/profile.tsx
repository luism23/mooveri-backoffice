import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';
import { SubmitResult } from '@/components/Form/Base';
import { ImgRsCountBaseProps } from '@/components/ImgRsCount/Base';

import { ProfileData as ProfileDataProps } from '@/interfaces/ProfileData';
import { AnalitycsData as ContentAnalitycsBaseProps } from '@/interfaces/AnalityscsData';
// import { PageProfilePublicProps } from "@/components/Pages/ProfilePublic";
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';

import { idData as TolinkmeProps } from '@/interfaces/Id';
import { AnimationsListType } from '@/interfaces/Animations';
import { parseIntPriceToNumber } from '@/functions/moneyPay';

export type GETANALITYCSINTERFACE = (data: {
    user_id: string;
    profile_id: string;
    start: string;
    end: string;
}) => Promise<ContentAnalitycsBaseProps>;

export const GETANALITYCS: GETANALITYCSINTERFACE = async ({
    profile_id,
    user_id,
    start,
    end,
}) => {
    const resultAnalitycs = await request({
        url: `${URL}/analytics/${user_id}/${profile_id}?start=${start}&end=${end}`,
    });
    if (resultAnalitycs.type != 'ok') {
        throw resultAnalitycs;
    }
    log('resultAnalitycs request', resultAnalitycs);
    const dataAnalitycs = resultAnalitycs?.result?.data;
    const analitycs: ContentAnalitycsBaseProps = {
        porcentClicks: dataAnalitycs?.percentClicks ?? 0,
        totalButtons: dataAnalitycs?.totalButtons ?? 0,
        totalClicks: dataAnalitycs?.totalClicks ?? 0,
        views: dataAnalitycs?.views ?? 0,
        countryClicks: Object.keys(dataAnalitycs?.countryClicks ?? {}).map(
            (key) => ({
                country: key,
                count: dataAnalitycs?.countryClicks?.[key],
            })
        ),
        countryVisitors: Object.keys(dataAnalitycs?.countryVisitors ?? {}).map(
            (key) => ({
                country: key,
                count: dataAnalitycs?.countryVisitors?.[key],
            })
        ),
        listClicksButtons: dataAnalitycs?.listClicksButtons
            ?.map((btn: any) => ({
                id: btn?.title,
                count: btn?.ButtonHit?.length ?? 0,
                customImg: btn?.customImg,
                customTitle: btn?.customTitle,
            }))
            .sort(
                (a: ImgRsCountBaseProps, b: ImgRsCountBaseProps) =>
                    b.count - a.count
            )
            .filter((btn: ImgRsCountBaseProps) => btn.count > 0),
    };
    return analitycs;
};

export type GETINTERFACE = (data: {
    username?: string;
}) => Promise<ProfileDataProps | 401>;

export const GET: GETINTERFACE = async ({ username = '' }) => {
    log('username', username);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: ProfileDataProps = {
            linksDefault: [
                {
                    uuid: '1',
                    rs: 'custom',
                    active: true,
                    customTitle: 'Podcast',
                    customImg:
                        'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2FPodcast-tools%201.png?alt=media&token=6e861e3f-f809-434a-a465-07fda8816b6f',
                    url: 'https://vancamps.com.co/experiencias/listas/podcast',
                    monetizeData: {
                        amount: '1',
                        recurrent: 'Flat_Rate',
                        type: 'Gift',
                    },
                    monetize: true,
                },
                {
                    uuid: '1',
                    rs: 'bongacams',
                    active: true,
                    url: 'https://es.bongacams.com/',
                },
                {
                    uuid: '1',
                    rs: 'camsoda',
                    active: true,
                    url: 'https://www.camsoda.com/',
                },
                {
                    uuid: '1',
                    rs: 'facebook',
                    active: true,
                    url: 'https://www.facebook.com/',
                    isPrincipal: true,
                },
                {
                    uuid: '1',
                    rs: 'whatsapp',
                    active: true,
                    url: 'https://www.whatsapp.com/',
                },
                {
                    uuid: '1',
                    rs: 'onlyfans',
                    active: true,
                    url: 'https://onlyfans.com/',
                },
                {
                    uuid: '1',
                    rs: 'telegram',
                    active: true,
                    url: 'https://web.telegram.org/',
                    isPrincipal: true,
                },
                {
                    uuid: '1',
                    rs: 'tiktok',
                    active: true,
                    url: 'https://www.tiktok.com/es/',
                },
            ],
            styleDefault: {
                name: 'JuanDavid',
                description: `Product Design Engineer #techentrepreneur - #geek - #digitalmarketing - #developer - #blockchaintechnology #web3 Founder @startscoinc & Byte4bit`,
                web: 'https://startscoinc.com/',
                avatar: 'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F110227607_1165145000509177_4569972509399988220_n.jpg?alt=media&token=2874d280-aa9b-4baf-aa02-b30a247ac8df',

                //TODO: eliminar esto
                bgDesktop:
                    'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F279386084_761400515018237_4116278363919223995_n.jpg?alt=media&token=abdafdbd-b8a6-414e-83cf-9e1f1a26cdba',
                bgMovil:
                    'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F279386084_761400515018237_4116278363919223995_n.jpg?alt=media&token=abdafdbd-b8a6-414e-83cf-9e1f1a26cdba',
                fontColor: '#fffffffff',
                btnBgColor: '#fffffffff',
                btnBorderColor: '#fffffffff',
                btnColor: '',
                buttonHeight: 'regular',
                buttonRound: 'semi-rounded',
                capaColor: '#000000',
                showIconButton: true,
                tolinkmeLogo: true,

                img: {
                    borderSize: 0,
                    borderType: 'rounded',
                    useLogoTolinkme: true,
                },
                bg: {
                    bg: {
                        img: {
                            fileData:
                                'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2F805180.jpg?alt=media&token=1dd56bb2-94e8-414c-aa21-675134b2a1ea',
                            text: '',
                        },
                        video: {
                            fileData:
                                'https://joy1.videvo.net/videvo_files/video/free/video0485/large_watermarked/_import_61c03301db44a1.11720954_preview.mp4',
                            text: '',
                        },
                        color: '#ffffff',
                        type: 'video',
                        opacity: 100,
                    },
                    useLayer: true,
                    bgLayer: {
                        type: 'color',
                        color: '#000',
                        opacity: 75,
                    },
                },
                info: {
                    name: {
                        color: '#ffffff',
                        fontFamily: 'nunito',
                        fontSize: 40,
                        fontWeight: 'black',
                        lineHeight: 10,
                    },
                    description: {
                        color: '#ffffff',
                        fontFamily: 'nunito',
                        fontSize: 20,
                        fontWeight: 'medium',
                        lineHeight: 10,
                    },
                    web: {
                        color: '#6a4af4',
                        fontFamily: 'nunito',
                        fontSize: 25,
                        fontWeight: 'bold',
                        lineHeight: 10,
                    },
                },
                btn: {
                    background: {
                        gradient: {
                            color1: '#3ebeee',
                            deg: 90,
                            color2: '#6a4af4',
                        },
                        type: 'gradient',
                    },
                    borderRadius: 'rounded',
                    icon: 'con',
                    size: 'regular',
                    text: {
                        color: '#ffffff',
                        fontFamily: 'nunito',
                        fontSize: 18,
                        fontWeight: 'black',
                        lineHeight: 10,
                    },
                    iconConfig: {
                        background: {
                            gradient: {
                                color1: '#04506c',
                                deg: 130,
                                color2: '#9d016e',
                            },
                            type: 'gradient',
                        },
                        padding: 0,
                        size: 15,
                        borderRadius: 'rounded',
                        border: {
                            color: 'white',
                            size: 0,
                            type: 'solid',
                        },
                    },
                    boxShadow: {
                        type: 'inset',
                        blur: 0,
                        size: 0,
                        x: 0,
                        y: 0,
                        color: '#fff',
                    },
                },
                btnAnimation: {
                    'on-page-load': 'fade',
                    hover: 'beat-fade',
                    infinite: 'bounce',
                },
            },
            isAprobed: true,
        };
        return r;
    }
    const result = await request({
        url: `${URL}/model-profile/username?username=${username}`,
    });
    log('get result profile', result, 'aqua');
    if (result.type == 'ok') {
        if (result.result.code == 200) {
            const user_ModelProfile =
                result?.result?.data?.user?.user_ModelProfile;
            const profile = user_ModelProfile?.profile ?? {};
            const user = user_ModelProfile?.user ?? {};

            const r: ProfileDataProps = {
                uuid: profile.uuid,
                user_uuid: user.uuid,
                profile_uuid: profile.uuid,
                linksDefault: (profile?.buttons_model_profile ?? [])
                    .map(({ button }: any) => {
                        const MDTB = button?.ButtonPriceDetails?.type;
                        const MDPB = button?.ButtonPriceDetails?.period;
                        const MDRB = button?.ButtonPriceDetails?.recurrence;
                        let monetizeDataRecurrence = '';
                        let monetizeDataType = '';
                        let monetizeDataPeriod = '';

                        if (MDRB == 'RECURRENT') {
                            monetizeDataRecurrence = 'Recurrent';
                        } else if (MDRB == 'FLAT_RATE') {
                            monetizeDataRecurrence = 'Flat_Rate';
                        }

                        if (MDTB == 'DONATION') {
                            monetizeDataType = 'Donation';
                        } else if (MDTB == 'GIFT') {
                            monetizeDataType = 'Gift';
                        } else if (MDTB == 'TIP') {
                            monetizeDataType = 'Tip';
                        } else if (MDTB == 'FREELANCE_SERVICE') {
                            monetizeDataType == 'Freelance_Service';
                        }

                        if (MDPB == 'MONTHLY') {
                            monetizeDataPeriod = 'Monthly';
                        } else if (MDPB == 'QUARTERLY') {
                            monetizeDataPeriod = 'Quarterly';
                        } else if (MDPB == 'SEMIANNUAL') {
                            monetizeDataPeriod = 'Semi Annual';
                        } else if (MDPB == 'ANNUALLY') {
                            monetizeDataPeriod = 'Annually';
                        }

                        const monetizeData: RSLinkConfigDataProps['monetizeData'] =
                            button?.ButtonPriceDetails?.basePrice
                                ? {
                                      amount: `${parseIntPriceToNumber(
                                          button?.ButtonPriceDetails?.basePrice
                                      )}`,
                                      period: monetizeDataPeriod,
                                      type: monetizeDataType,
                                      recurrent: monetizeDataRecurrence,
                                      description:
                                          button?.ButtonPriceDetails
                                              ?.description,
                                  }
                                : undefined;

                        const r: RSLinkConfigDataProps = {
                            ...button,
                            uuid: button.uuid,
                            rs: button.title?.toLocaleLowerCase(),
                            active: !button.private,
                            url: button.url,
                            order: button?.order ?? 0,
                            customImg: button?.customImg ?? '',
                            customTitle: button?.customTitle ?? '',
                            isPrincipal: button?.isPrincipal ?? false,
                            monetizeData,
                        };
                        return r;
                    })
                    .sort(
                        (a: RSLinkConfigDataProps, b: RSLinkConfigDataProps) =>
                            (a?.order ?? 0) - (b?.order ?? 0)
                    ),
                styleDefault: {
                    avatar: profile?.profile_img ?? '',
                    name: user?.username ?? '',
                    description: profile?.Bio ?? '',
                    web: profile?.website ?? '',
                    username_title: profile?.username_title ?? undefined,

                    btnBorderColor: profile?.btnBorderColor,
                    //------
                    btnBgColor: profile?.btnBgColor,
                    btnColor: profile?.btnColor,
                    buttonHeight: profile?.buttonHeight ?? 'regular',
                    buttonRound: profile?.buttonRound ?? 'semi-rounded',
                    showIconButton: profile?.showIconButton ?? true,
                    fontColor: profile?.text_color ?? '',
                    bgDesktop: profile?.banner ?? '',
                    capaColor: profile?.capaColor ?? '',
                    bgMovil: profile?.banner_mobile ?? '',
                    tolinkmeLogo: profile?.tolinkmeLogo ?? false,

                    btn: {
                        size: profile?.buttonHeight ?? 'regular',

                        borderRadius: profile?.buttonRound ?? 'semi-rounded',

                        background: {
                            type: profile?.btnBgType ?? 'color',
                            color: profile?.btnBgColor ?? '#ffffff',
                            gradient: {
                                color1:
                                    profile?.btnBgGradientColor1 ?? '#ffffff',
                                color2:
                                    profile?.btnBgGradientColor2 ?? '#ffffff',
                                deg: profile?.btnBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.btnBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.btnBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.btnBgOpacity ?? 100,
                        },
                        border: {
                            size: profile?.btnBorderSize ?? 0,
                            color: profile?.btnBorderColor ?? '#ffffff',
                            type: profile?.btnBorderType ?? 'solid',
                        },
                        text: {
                            fontFamily: profile?.btnTextFontFamily ?? 'nunito',
                            fontWeight: profile?.btnTextFontWeight ?? 'black',
                            fontSize: profile?.btnTextFontSize ?? 17,
                            color: profile?.btnColor ?? '#8d8d8d',
                            lineHeight: profile?.btnTextLineHeight ?? 10,
                        },
                        icon: profile?.showIconButton ?? true ? 'con' : 'sin',
                        iconConfig: {
                            size: Math.min(
                                profile?.btnIconConfigSize ?? 27,
                                40
                            ),
                            padding: profile?.btnIconConfigPadding ?? 0,

                            borderRadius:
                                profile?.btnIconConfigRound ?? 'no-rounding',
                            border: {
                                size: profile?.btnIconConfigBorderSize ?? 0,
                                color:
                                    profile?.btnIconConfigBorderColor ??
                                    '#ffffff',
                                type:
                                    profile?.btnIconConfigBorderType ?? 'solid',
                            },

                            background: {
                                type:
                                    profile?.btnIconConfigBgType ?? 'gradient',
                                color:
                                    profile?.btnIconConfigBgColor ?? '#ffffff',
                                gradient: {
                                    color1:
                                        profile?.btnIconConfigBgGradientColor1 ??
                                        '#04506c',
                                    color2:
                                        profile?.btnIconConfigBgGradientColor2 ??
                                        '#9d016e',
                                    deg:
                                        profile?.btnIconConfigBgGradientDeg ??
                                        130,
                                },
                                img: {
                                    fileData: profile?.btnIconConfigBgImg ?? '',
                                    text: '',
                                },
                                video: {
                                    fileData:
                                        profile?.btnIconConfigBgVideo ?? '',
                                    text: '',
                                },
                                opacity: profile?.btnIconConfigBgOpacity ?? 0,
                            },
                        },
                        boxShadow: {
                            type: profile?.btnBoxShadowType ?? 'normal',
                            blur: profile?.btnBoxShadowBlur ?? 0,
                            size: profile?.btnBoxShadowSize ?? 0,
                            x: profile?.btnBoxShadowX ?? 0,
                            y: profile?.btnBoxShadowY ?? 0,
                            color: profile?.btnBoxShadowColor ?? '#0c0c0c',
                        },
                    },
                    info: {
                        name: {
                            fontFamily:
                                profile?.infoNameTextFontFamily ?? 'nunito',
                            fontWeight:
                                profile?.infoNameTextFontWeight ?? 'black',
                            fontSize: profile?.infoNameTextFontSize ?? 20,
                            color:
                                profile?.infoNameColor ??
                                profile?.text_color ??
                                '#ffffff',
                            lineHeight: profile?.infoNameLineHeight ?? 10,
                        },
                        description: {
                            fontFamily:
                                profile?.infoDescriptionTextFontFamily ??
                                'nunito',
                            fontWeight:
                                profile?.infoDescriptionTextFontWeight ??
                                'regular',
                            fontSize:
                                profile?.infoDescriptionTextFontSize ?? 13,
                            color:
                                profile?.infoDescriptionColor ??
                                profile?.text_color ??
                                '#ffffff',
                            lineHeight:
                                profile?.infoDescriptionLineHeight ?? 10,
                        },
                        web: {
                            fontFamily:
                                profile?.infoWebTextFontFamily ?? 'nunito',
                            fontWeight:
                                profile?.infoWebTextFontWeight ?? 'regular',
                            fontSize: profile?.infoWebTextFontSize ?? 13,
                            color:
                                profile?.infoWebColor ??
                                profile?.text_color ??
                                '#ffffff',
                            lineHeight: profile?.infoWebLineHeight ?? 10,
                        },
                    },
                    bg: {
                        bg: {
                            type: profile?.bgDesktopBgType ?? 'img',
                            color: profile?.bgDesktopBgColor ?? '',
                            gradient: {
                                color1:
                                    profile?.bgDesktopBgGradientColor1 ?? '',
                                color2:
                                    profile?.bgDesktopBgGradientColor2 ?? '',
                                deg: profile?.bgDesktopBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData:
                                    profile?.bgDesktopBgImg ??
                                    profile?.banner ??
                                    '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgDesktopBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgDesktopBgOpacity ?? 100,
                        },
                        useLayer: profile?.useLayer ?? false,
                        bgLayer: {
                            type: profile?.bgDesktopLayerBgType ?? 'color',
                            color:
                                profile?.bgDesktopLayerBgColor ??
                                profile?.capaColor ??
                                '',
                            gradient: {
                                color1:
                                    profile?.bgDesktopLayerBgGradientColor1 ??
                                    '',
                                color2:
                                    profile?.bgDesktopLayerBgGradientColor2 ??
                                    '',
                                deg: profile?.bgDesktopLayerBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.bgDesktopLayerBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgDesktopLayerBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgDesktopLayerBgOpacity ?? 100,
                        },
                        bgMovil: {
                            type: profile?.bgMovilBgType ?? 'img',
                            color: profile?.bgMovilBgColor ?? '',
                            gradient: {
                                color1: profile?.bgMovilBgGradientColor1 ?? '',
                                color2: profile?.bgMovilBgGradientColor2 ?? '',
                                deg: profile?.bgMovilBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData:
                                    profile?.bgMovilBgImg ??
                                    profile?.banner_mobile ??
                                    '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgMovilBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgMovilBgOpacity ?? 100,
                        },
                        useLayerMovil: profile?.useLayerMovil ?? false,
                        bgMovilLayer: {
                            type: profile?.bgMovilLayerBgType ?? 'color',
                            color:
                                profile?.bgMovilLayerBgColor ??
                                profile?.capaColor ??
                                '',
                            gradient: {
                                color1:
                                    profile?.bgMovilLayerBgGradientColor1 ?? '',
                                color2:
                                    profile?.bgMovilLayerBgGradientColor2 ?? '',
                                deg: profile?.bgMovilLayerBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.bgMovilLayerBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgMovilLayerBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgMovilLayerBgOpacity ?? 100,
                        },
                    },
                    img: {
                        useLogoTolinkme: profile?.tolinkmeLogo ?? false,
                        bg: {
                            type: profile?.imgBgType ?? 'color',
                            color: profile?.imgBgColor ?? '',
                            gradient: {
                                color1: profile?.imgBgGradientColor1 ?? '',
                                color2: profile?.imgBgGradientColor2 ?? '',
                                deg: profile?.imgBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.imgBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.imgBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.imgBgOpacity ?? 100,
                        },
                        borderSize: profile?.imgBorderSize ?? 0,
                        borderType: profile?.imgBorderType ?? 'rounded',
                    },
                    btnPrincipal: {
                        size: Math.max(profile?.btnPrincipalSize ?? 10, 15),
                        padding: profile?.btnPrincipalPadding ?? 10,
                        color: profile?.btnPrincipalColor ?? '#ffffff',

                        borderRadius: profile?.btnPrincipalRound ?? 'rounded',
                        border: {
                            size: profile?.btnPrincipalBorderSize ?? 0,
                            color:
                                profile?.btnPrincipalBorderColor ?? '#ffffff',
                            type: profile?.btnPrincipalBorderType ?? 'solid',
                        },

                        background: {
                            type: profile?.btnPrincipalBgType ?? 'gradient',
                            color: profile?.btnPrincipalBgColor ?? '#ffffff',
                            gradient: {
                                color1:
                                    profile?.btnPrincipalBgGradientColor1 ??
                                    '#04506c',
                                color2:
                                    profile?.btnPrincipalBgGradientColor2 ??
                                    '#9d016e',
                                deg: profile?.btnPrincipalBgGradientDeg ?? 130,
                            },
                            img: {
                                fileData: profile?.btnPrincipalBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.btnPrincipalBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.btnPrincipalBgOpacity ?? 100,
                        },
                    },
                    btnAnimation: {
                        'on-page-load': `${
                            profile?.btnAnimationOnPageLoad ?? 'none'
                        }` as AnimationsListType,
                        hover: `${
                            profile?.btnAnimationHover ?? 'none'
                        }` as AnimationsListType,
                        infinite: `${
                            profile?.btnAnimationInfinite ?? 'none'
                        }` as AnimationsListType,
                    },
                },
                isAprobed:
                    user?.PhoneStatus == 'VERIFY' ||
                    user?.EmailStatus == 'VERIFY',
            };
            return r;
        }
    }

    return 401;
};

export type GET_PUBLICINTERFACE = (data: {
    username?: string;
}) => Promise<'404' | TolinkmeProps>;

export const GET_PUBLIC: GET_PUBLICINTERFACE = async ({
    username = '',
}: {
    username?: string;
}) => {
    log('username', username);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: TolinkmeProps = {
            links: [
                {
                    rs: 'custom',
                    active: true,
                    customTitle: 'exclusive content',
                    customImg:
                        'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2FPodcast-tools%201.png?alt=media&token=6e861e3f-f809-434a-a465-07fda8816b6f',
                    url: 'https://vancamps.com.co/experiencias/listas/podcast',
                    uuid: '1111',
                    monetize: true,
                    monetizeData: {
                        amount: '2',
                        recurrent: 'Flat_Rate',
                        type: 'Gift',
                    },
                },
                {
                    rs: 'facebook',
                    active: true,
                    url: 'https://www.facebook.com/',
                    isPrincipal: true,
                },
                {
                    rs: 'whatsapp',
                    active: true,
                    url: 'https://www.whatsapp.com/',
                },
                {
                    rs: 'telegram',
                    active: true,
                    url: 'https://web.telegram.org/',
                    isPrincipal: true,
                },
                {
                    rs: 'tiktok',
                    active: true,
                    url: 'https://www.tiktok.com/es/',
                },
            ],
            style: {
                name: 'JuanDavid',
                description: `Product Design Engineer #techentrepreneur - #geek - #digitalmarketing - #developer - #blockchaintechnology #web3 Founder @startscoinc & Byte4bit`,
                web: 'https://startscoinc.com/',
                avatar: 'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F110227607_1165145000509177_4569972509399988220_n.jpg?alt=media&token=2874d280-aa9b-4baf-aa02-b30a247ac8df',

                //TODO: eliminar esto
                bgDesktop:
                    'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F279386084_761400515018237_4116278363919223995_n.jpg?alt=media&token=abdafdbd-b8a6-414e-83cf-9e1f1a26cdba',
                bgMovil:
                    'https://firebasestorage.googleapis.com/v0/b/fenextjs.appspot.com/o/localhost%2Ffiles%2F279386084_761400515018237_4116278363919223995_n.jpg?alt=media&token=abdafdbd-b8a6-414e-83cf-9e1f1a26cdba',
                fontColor: '#fffffffff',
                btnBgColor: '#fffffffff',
                btnBorderColor: '#fffffffff',
                btnColor: '',
                buttonHeight: 'regular',
                buttonRound: 'semi-rounded',
                capaColor: '#000000',
                showIconButton: true,
                tolinkmeLogo: true,

                img: {
                    borderSize: 0,
                    borderType: 'rounded',
                    useLogoTolinkme: true,
                },
                bg: {
                    bg: {
                        img: {
                            fileData:
                                'https://firebasestorage.googleapis.com/v0/b/linkme-f37f5.appspot.com/o/localhost%2Ffiles%2F805180.jpg?alt=media&token=1dd56bb2-94e8-414c-aa21-675134b2a1ea',
                        },
                        video: {
                            fileData:
                                'https://joy1.videvo.net/videvo_files/video/free/video0485/large_watermarked/_import_61c03301db44a1.11720954_preview.mp4',
                        },
                        color: '#ffffff',
                        type: 'video',
                    },
                    useLayer: true,
                    bgLayer: {
                        type: 'color',
                        color: '#000',
                        opacity: 75,
                    },
                },
                info: {
                    name: {
                        color: '#ffffff',
                        fontFamily: 'nunito',
                        fontSize: 40,
                        fontWeight: 'black',
                        lineHeight: 10,
                    },
                    description: {
                        color: '#ffffff',
                        fontFamily: 'nunito',
                        fontSize: 20,
                        fontWeight: 'medium',
                        lineHeight: 10,
                    },
                    web: {
                        color: '#6a4af4',
                        fontFamily: 'nunito',
                        fontSize: 25,
                        fontWeight: 'bold',
                        lineHeight: 10,
                    },
                },
                btn: {
                    background: {
                        gradient: {
                            color1: '#3ebeee',
                            deg: 90,
                            color2: '#6a4af4',
                        },
                        type: 'gradient',
                    },
                    borderRadius: 'rounded',
                    icon: 'con',
                    size: 'regular',
                    text: {
                        color: '#ffffff',
                        fontFamily: 'nunito',
                        fontSize: 18,
                        fontWeight: 'black',
                        lineHeight: 10,
                    },
                    iconConfig: {
                        background: {
                            gradient: {
                                color1: '#04506c',
                                deg: 130,
                                color2: '#9d016e',
                            },
                            type: 'gradient',
                        },
                        padding: 0,
                        size: 15,
                        borderRadius: 'rounded',
                        border: {
                            color: 'white',
                            size: 0,
                            type: 'solid',
                        },
                    },
                    boxShadow: {
                        type: 'inset',
                        blur: 0,
                        size: 0,
                        x: 0,
                        y: 0,
                        color: '#fff',
                    },
                },
                btnAnimation: {
                    'on-page-load': 'none',
                    hover: 'none',
                    infinite: 'none',
                },
            },
            isAprobed: true,
        };
        return r;
    }
    const result = await request({
        url: `${URL}/model-profile/username?username=${username}`,
    });
    log('get result profile', result, 'aqua');
    console.log(result);
    if (result.type == 'ok') {
        if (result.result.code == 200) {
            const user_ModelProfile =
                result?.result?.data?.user?.user_ModelProfile;

            const profile = user_ModelProfile?.profile ?? {};

            const user_ = user_ModelProfile?.user ?? {};

            const isAprobed =
                user_?.PhoneStatus == 'VERIFY' ||
                user_?.EmailStatus == 'VERIFY';
            if (!isAprobed) {
                return '404';
            }

            const user = user_ModelProfile?.user ?? {};
            const r: TolinkmeProps = {
                uuid: user.uuid ?? '',
                name: user_?.username ?? '',
                links: (profile?.buttons_model_profile ?? [])
                    .map(({ button }: any) => {
                        const MDTB = button?.ButtonPriceDetails?.type;
                        const MDPB = button?.ButtonPriceDetails?.period;
                        const MDRB = button?.ButtonPriceDetails?.recurrence;
                        let monetizeDataRecurrence = '';
                        let monetizeDataType = '';
                        let monetizeDataPeriod = '';

                        if (MDRB == 'RECURRENT') {
                            monetizeDataRecurrence = 'Recurrent';
                        } else if (MDRB == 'FLAT_RATE') {
                            monetizeDataRecurrence = 'Flat_Rate';
                        }

                        if (MDTB == 'DONATION') {
                            monetizeDataType = 'Donation';
                        } else if (MDTB == 'GIFT') {
                            monetizeDataType = 'Gift';
                        } else if (MDTB == 'TIP') {
                            monetizeDataType = 'Tip';
                        } else if (MDTB == 'FREELANCE_SERVICE') {
                            monetizeDataType == 'Freelance_Service';
                        }

                        if (MDPB == 'MONTHLY') {
                            monetizeDataPeriod = 'Monthly';
                        } else if (MDPB == 'QUARTERLY') {
                            monetizeDataPeriod = 'Quarterly';
                        } else if (MDPB == 'SEMIANNUAL') {
                            monetizeDataPeriod = 'Semi Annual';
                        } else if (MDPB == 'ANNUALLY') {
                            monetizeDataPeriod = 'Annually';
                        } else if (MDPB == 'JUST_ONE') {
                            monetizeDataPeriod = 'Just one';
                        }

                        const monetizeData: RSLinkConfigDataProps['monetizeData'] =
                            button?.ButtonPriceDetails
                                ? {
                                      amount: `${parseIntPriceToNumber(
                                          button?.ButtonPriceDetails?.basePrice
                                      )}`,
                                      period: monetizeDataPeriod,
                                      type: monetizeDataType,
                                      recurrent: monetizeDataRecurrence,
                                      description:
                                          button?.ButtonPriceDetails
                                              ?.description ?? '',
                                  }
                                : undefined;

                        const r: RSLinkConfigDataProps = {
                            ...button,
                            uuid: button.uuid,
                            rs: button.title?.toLocaleLowerCase(),
                            active: !button.private,
                            url: button.url,
                            order: button?.order ?? 0,
                            customImg: button?.customImg ?? '',
                            customTitle: button?.customTitle ?? '',
                            title: button?.title,
                            isPrincipal: button?.isPrincipal ?? false,
                            monetizeData,
                        };
                        return r;
                    })
                    .sort(
                        (a: RSLinkConfigDataProps, b: RSLinkConfigDataProps) =>
                            (a?.order ?? 0) - (b?.order ?? 0)
                    ),
                style: {
                    avatar: profile?.profile_img ?? '',
                    name: user?.username ?? '',
                    username_title: profile?.username_title ?? undefined,
                    description: profile?.Bio ?? '',
                    web: profile?.website ?? '',

                    btnBorderColor: profile?.btnBorderColor,
                    //------
                    btnBgColor: profile?.btnBgColor,
                    btnColor: profile?.btnColor,
                    buttonHeight: profile?.buttonHeight ?? 'regular',
                    buttonRound: profile?.buttonRound ?? 'semi-rounded',
                    showIconButton: profile?.showIconButton ?? true,
                    fontColor: profile?.text_color ?? '',
                    bgDesktop: profile?.banner ?? '',
                    capaColor: profile?.capaColor ?? '',
                    bgMovil: profile?.banner_mobile ?? '',
                    tolinkmeLogo: profile?.tolinkmeLogo ?? false,

                    btn: {
                        size: profile?.buttonHeight ?? 'regular',

                        borderRadius: profile?.buttonRound ?? 'semi-rounded',

                        background: {
                            type: profile?.btnBgType ?? 'color',
                            color: profile?.btnBgColor ?? '#ffffff',
                            gradient: {
                                color1:
                                    profile?.btnBgGradientColor1 ?? '#ffffff',
                                color2:
                                    profile?.btnBgGradientColor2 ?? '#ffffff',
                                deg: profile?.btnBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.btnBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.btnBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.btnBgOpacity ?? 100,
                        },
                        border: {
                            size: profile?.btnBorderSize ?? 0,
                            color: profile?.btnBorderColor ?? '#ffffff',
                            type: profile?.btnBorderType ?? 'solid',
                        },
                        text: {
                            fontFamily: profile?.btnTextFontFamily ?? 'nunito',
                            fontWeight: profile?.btnTextFontWeight ?? 'black',
                            fontSize: profile?.btnTextFontSize ?? 17,
                            lineHeight: profile?.btnTextLineHeight ?? 10,
                            color: profile?.btnColor ?? '#8d8d8d',
                        },
                        icon: profile?.showIconButton ?? true ? 'con' : 'sin',
                        iconConfig: {
                            size: Math.min(
                                profile?.btnIconConfigSize ?? 27,
                                40
                            ),
                            padding: profile?.btnIconConfigPadding ?? 0,

                            borderRadius:
                                profile?.btnIconConfigRound ?? 'no-rounding',
                            border: {
                                size: profile?.btnIconConfigBorderSize ?? 0,
                                color:
                                    profile?.btnIconConfigBorderColor ??
                                    '#ffffff',
                                type:
                                    profile?.btnIconConfigBorderType ?? 'solid',
                            },

                            background: {
                                type:
                                    profile?.btnIconConfigBgType ?? 'gradient',
                                color:
                                    profile?.btnIconConfigBgColor ?? '#ffffff',
                                gradient: {
                                    color1:
                                        profile?.btnIconConfigBgGradientColor1 ??
                                        '#04506c',
                                    color2:
                                        profile?.btnIconConfigBgGradientColor2 ??
                                        '#9d016e',
                                    deg:
                                        profile?.btnIconConfigBgGradientDeg ??
                                        130,
                                },
                                img: {
                                    fileData: profile?.btnIconConfigBgImg ?? '',
                                    text: '',
                                },
                                video: {
                                    fileData:
                                        profile?.btnIconConfigBgVideo ?? '',
                                    text: '',
                                },
                                opacity: profile?.btnIconConfigBgOpacity ?? 0,
                            },
                        },

                        boxShadow: {
                            type: profile?.btnBoxShadowType ?? 'normal',
                            blur: profile?.btnBoxShadowBlur ?? 0,
                            size: profile?.btnBoxShadowSize ?? 0,
                            x: profile?.btnBoxShadowX ?? 0,
                            y: profile?.btnBoxShadowY ?? 0,
                            color: profile?.btnBoxShadowColor ?? '#0c0c0c',
                        },
                    },
                    info: {
                        name: {
                            fontFamily:
                                profile?.infoNameTextFontFamily ?? 'nunito',
                            fontWeight:
                                profile?.infoNameTextFontWeight ?? 'black',
                            fontSize: profile?.infoNameTextFontSize ?? 20,
                            lineHeight: profile?.infoNameLineHeight ?? 10,
                            color:
                                profile?.infoNameColor ??
                                profile?.text_color ??
                                '#ffffff',
                        },
                        description: {
                            fontFamily:
                                profile?.infoDescriptionTextFontFamily ??
                                'nunito',
                            fontWeight:
                                profile?.infoDescriptionTextFontWeight ??
                                'regular',
                            fontSize:
                                profile?.infoDescriptionTextFontSize ?? 13,
                            lineHeight:
                                profile?.infoDescriptionLineHeight ?? 10,
                            color:
                                profile?.infoDescriptionColor ??
                                profile?.text_color ??
                                '#ffffff',
                        },
                        web: {
                            fontFamily:
                                profile?.infoWebTextFontFamily ?? 'nunito',
                            fontWeight:
                                profile?.infoWebTextFontWeight ?? 'regular',
                            fontSize: profile?.infoWebTextFontSize ?? 13,
                            lineHeight: profile?.infoWebLineHeight ?? 10,
                            color:
                                profile?.infoWebColor ??
                                profile?.text_color ??
                                '#ffffff',
                        },
                    },
                    bg: {
                        bg: {
                            type: profile?.bgDesktopBgType ?? 'img',
                            color: profile?.bgDesktopBgColor ?? '',
                            gradient: {
                                color1:
                                    profile?.bgDesktopBgGradientColor1 ?? '',
                                color2:
                                    profile?.bgDesktopBgGradientColor2 ?? '',
                                deg: profile?.bgDesktopBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData:
                                    profile?.bgDesktopBgImg ??
                                    profile?.banner ??
                                    '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgDesktopBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgDesktopBgOpacity ?? 100,
                        },
                        useLayer: profile?.useLayer ?? false,
                        bgLayer: {
                            type: profile?.bgDesktopLayerBgType ?? 'color',
                            color:
                                profile?.bgDesktopLayerBgColor ??
                                profile?.capaColor ??
                                '',
                            gradient: {
                                color1:
                                    profile?.bgDesktopLayerBgGradientColor1 ??
                                    '',
                                color2:
                                    profile?.bgDesktopLayerBgGradientColor2 ??
                                    '',
                                deg: profile?.bgDesktopLayerBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.bgDesktopLayerBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgDesktopLayerBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgDesktopLayerBgOpacity ?? 100,
                        },
                        bgMovil: {
                            type: profile?.bgMovilBgType ?? 'img',
                            color: profile?.bgMovilBgColor ?? '',
                            gradient: {
                                color1: profile?.bgMovilBgGradientColor1 ?? '',
                                color2: profile?.bgMovilBgGradientColor2 ?? '',
                                deg: profile?.bgMovilBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData:
                                    profile?.bgMovilBgImg ??
                                    profile?.banner_mobile ??
                                    '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgMovilBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgMovilBgOpacity ?? 100,
                        },
                        useLayerMovil: profile?.useLayerMovil ?? false,
                        bgMovilLayer: {
                            type: profile?.bgMovilLayerBgType ?? 'color',
                            color:
                                profile?.bgMovilLayerBgColor ??
                                profile?.capaColor ??
                                '',
                            gradient: {
                                color1:
                                    profile?.bgMovilLayerBgGradientColor1 ?? '',
                                color2:
                                    profile?.bgMovilLayerBgGradientColor2 ?? '',
                                deg: profile?.bgMovilLayerBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.bgMovilLayerBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.bgMovilLayerBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.bgMovilLayerBgOpacity ?? 100,
                        },
                    },
                    img: {
                        useLogoTolinkme: profile?.tolinkmeLogo ?? false,
                        bg: {
                            type: profile?.imgBgType ?? 'color',
                            color: profile?.imgBgColor ?? '',
                            gradient: {
                                color1: profile?.imgBgGradientColor1 ?? '',
                                color2: profile?.imgBgGradientColor2 ?? '',
                                deg: profile?.imgBgGradientDeg ?? 0,
                            },
                            img: {
                                fileData: profile?.imgBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.imgBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.imgBgOpacity ?? 100,
                        },
                        borderSize: profile?.imgBorderSize ?? 0,
                        borderType: profile?.imgBorderType ?? 'rounded',
                    },
                    btnPrincipal: {
                        size: Math.max(profile?.btnPrincipalSize ?? 10, 15),
                        padding: profile?.btnPrincipalPadding ?? 10,
                        color: profile?.btnPrincipalColor ?? '#ffffff',

                        borderRadius: profile?.btnPrincipalRound ?? 'rounded',
                        border: {
                            size: profile?.btnPrincipalBorderSize ?? 0,
                            color:
                                profile?.btnPrincipalBorderColor ?? '#ffffff',
                            type: profile?.btnPrincipalBorderType ?? 'solid',
                        },

                        background: {
                            type: profile?.btnPrincipalBgType ?? 'gradient',
                            color: profile?.btnPrincipalBgColor ?? '#ffffff',
                            gradient: {
                                color1:
                                    profile?.btnPrincipalBgGradientColor1 ??
                                    '#04506c',
                                color2:
                                    profile?.btnPrincipalBgGradientColor2 ??
                                    '#9d016e',
                                deg: profile?.btnPrincipalBgGradientDeg ?? 130,
                            },
                            img: {
                                fileData: profile?.btnPrincipalBgImg ?? '',
                                text: '',
                            },
                            video: {
                                fileData: profile?.btnPrincipalBgVideo ?? '',
                                text: '',
                            },
                            opacity: profile?.btnPrincipalBgOpacity ?? 100,
                        },
                    },
                    btnAnimation: {
                        'on-page-load': `${
                            profile?.btnAnimationOnPageLoad ?? 'none'
                        }` as AnimationsListType,
                        hover: `${
                            profile?.btnAnimationHover ?? 'none'
                        }` as AnimationsListType,
                        infinite: `${
                            profile?.btnAnimationInfinite ?? 'none'
                        }` as AnimationsListType,
                    },
                },
                isAprobed:
                    user?.PhoneStatus == 'VERIFY' ||
                    user?.EmailStatus == 'VERIFY',
            };
            return r;
        }
    }

    return '404';
};

export type PUTINTERFACE = (
    data: ProfileDataProps,
    user: UserLoginProps | undefined
) => Promise<SubmitResult>;
export const PUT: PUTINTERFACE = async (
    data: ProfileDataProps,
    user: UserLoginProps | undefined
) => {
    log('put data profile', data, 'aqua');
    if (!user) {
        const r: SubmitResult = {
            status: 'error',
            message: 'User is not found',
        };
        return r;
    }
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Profile Publish',
        };
        return r;
    }

    if (!data.isAprobed) {
        return {
            status: 'error',
            message: 'Confirma tu cuenta para poder publicar',
        };
    }

    const result = await request({
        method: 'put',
        url: `${URL}/model-profile/${data.uuid}`,
        data: {
            Bio: data.styleDefault.description,
            website: data.styleDefault.web == '' ? null : data.styleDefault.web,
            profile_img: data.styleDefault.avatar,
            banner: data.styleDefault.bgDesktop,
            banner_mobile: data.styleDefault.bgMovil,
            text_color: data.styleDefault.fontColor,
            capaColor: data.styleDefault.capaColor,

            username_title: data.styleDefault.username_title,

            // btnBgColor: data.styleDefault.btnBgColor,
            // btnColor: data.styleDefault.btnColor,
            // tolinkmeLogo: data.styleDefault.tolinkmeLogo,

            buttonHeight: data.styleDefault.btn?.size ?? 'regular',
            buttonRound: data.styleDefault.btn?.borderRadius ?? 'semi-rounded',

            btnBgType: data.styleDefault.btn?.background?.type ?? 'color',
            btnBgColor: data.styleDefault.btn?.background?.color,
            btnBgGradientColor1:
                data.styleDefault.btn?.background?.gradient?.color1,
            btnBgGradientColor2:
                data.styleDefault.btn?.background?.gradient?.color2,
            btnBgGradientDeg: parseFloat(
                `${data.styleDefault.btn?.background?.gradient?.deg}`
            ),
            btnBgImg: data.styleDefault.btn?.background?.img?.fileData,
            btnBgVideo: data.styleDefault.btn?.background?.video?.fileData,
            btnBgOpacity: parseInt(
                `${data.styleDefault.btn?.background?.opacity}`
            ),

            btnBorderSize: parseInt(`${data.styleDefault.btn?.border?.size}`),
            btnBorderColor: data.styleDefault.btn?.border?.color,
            btnBorderType: data.styleDefault.btn?.border?.type,

            btnTextFontFamily: data.styleDefault.btn?.text?.fontFamily,
            btnTextFontWeight: data.styleDefault.btn?.text?.fontWeight,
            btnTextFontSize: parseFloat(
                `${data.styleDefault.btn?.text?.fontSize}`
            ),
            btnTextLineHeight: parseFloat(
                `${data.styleDefault.btn?.text?.lineHeight}`
            ),
            btnIconConfigSize: parseFloat(
                `${data.styleDefault.btn?.iconConfig?.size ?? 27}`
            ),
            btnIconConfigPadding: parseFloat(
                `${data.styleDefault.btn?.iconConfig?.padding ?? 27}`
            ),
            btnIconConfigRound:
                data.styleDefault.btn?.iconConfig?.borderRadius ?? 'no-rounded',
            btnIconConfigBorderSize: parseFloat(
                `${data.styleDefault.btn?.iconConfig?.border?.size ?? 0}`
            ),
            btnIconConfigBorderColor:
                data.styleDefault.btn?.iconConfig?.border?.color ?? '#fff',
            btnIconConfigBorderType:
                data.styleDefault.btn?.iconConfig?.border?.type ?? 'solid',
            btnIconConfigBgType:
                data.styleDefault.btn?.iconConfig?.background?.type ?? 'color',
            btnIconConfigBgColor:
                data.styleDefault.btn?.iconConfig?.background?.color ?? '#fff',
            btnIconConfigBgGradientColor1:
                data.styleDefault.btn?.iconConfig?.background?.gradient
                    ?.color1 ?? '#04506c',
            btnIconConfigBgGradientColor2:
                data.styleDefault.btn?.iconConfig?.background?.gradient
                    ?.color2 ?? '#9d016e',
            btnIconConfigBgGradientDeg: parseFloat(
                `${
                    data.styleDefault.btn?.iconConfig?.background?.gradient
                        ?.deg ?? 130
                }`
            ),
            btnIconConfigBgImg:
                data.styleDefault.btn?.iconConfig?.background?.img?.fileData ??
                '',
            btnIconConfigBgVideo:
                data.styleDefault.btn?.iconConfig?.background?.video
                    ?.fileData ?? '',
            btnIconConfigBgOpacity: parseFloat(
                `${
                    data.styleDefault.btn?.iconConfig?.background?.opacity ??
                    100
                }`
            ),

            btnBoxShadowType:
                data.styleDefault.btn?.boxShadow?.type ?? 'normal',
            btnBoxShadowBlur: parseFloat(
                `${data.styleDefault.btn?.boxShadow?.blur ?? 0}`
            ),
            btnBoxShadowSize: parseFloat(
                `${data.styleDefault.btn?.boxShadow?.size ?? 0}`
            ),
            btnBoxShadowX: parseFloat(
                `${data.styleDefault.btn?.boxShadow?.x ?? 0}`
            ),
            btnBoxShadowY: parseFloat(
                `${data.styleDefault.btn?.boxShadow?.y ?? 0}`
            ),
            btnBoxShadowColor:
                data.styleDefault.btn?.boxShadow?.color ?? '#0c0c0c',

            btnColor: data.styleDefault.btn?.text?.color,

            showIconButton: data.styleDefault.btn?.icon == 'con' ?? true,

            infoNameTextFontFamily: data?.styleDefault?.info?.name?.fontFamily,
            infoNameTextFontWeight: data?.styleDefault?.info?.name?.fontWeight,
            infoNameTextFontSize: parseFloat(
                `${data?.styleDefault?.info?.name?.fontSize}`
            ),
            infoNameLineHeight: parseFloat(
                `${data?.styleDefault?.info?.name?.lineHeight}`
            ),
            infoNameColor: data?.styleDefault?.info?.name?.color,

            infoDescriptionTextFontFamily:
                data?.styleDefault?.info?.description?.fontFamily,
            infoDescriptionTextFontWeight:
                data?.styleDefault?.info?.description?.fontWeight,
            infoDescriptionTextFontSize: parseFloat(
                `${data?.styleDefault?.info?.description?.fontSize}`
            ),
            infoDescriptionLineHeight: parseFloat(
                `${data?.styleDefault?.info?.description?.lineHeight}`
            ),
            infoDescriptionColor: data?.styleDefault?.info?.description?.color,

            infoWebTextFontFamily: data?.styleDefault?.info?.web?.fontFamily,
            infoWebTextFontWeight: data?.styleDefault?.info?.web?.fontWeight,
            infoWebTextFontSize: parseFloat(
                `${data?.styleDefault?.info?.web?.fontSize}`
            ),
            infoWebLineHeight: parseFloat(
                `${data?.styleDefault?.info?.web?.lineHeight}`
            ),
            infoWebColor: data?.styleDefault?.info?.web?.color,

            bgDesktopBgType: data.styleDefault.bg?.bg?.type,
            bgDesktopBgColor: data.styleDefault.bg?.bg?.color,
            bgDesktopBgGradientColor1:
                data.styleDefault.bg?.bg?.gradient?.color1,
            bgDesktopBgGradientColor2:
                data.styleDefault.bg?.bg?.gradient?.color2,
            bgDesktopBgGradientDeg: parseFloat(
                `${data.styleDefault.bg?.bg?.gradient?.deg}`
            ),
            bgDesktopBgImg: data.styleDefault.bg?.bg?.img?.fileData,
            bgDesktopBgVideo: data.styleDefault.bg?.bg?.video?.fileData,
            bgDesktopBgOpacity: parseFloat(
                `${data.styleDefault.bg?.bg?.opacity}`
            ),

            useLayer: data.styleDefault.bg?.useLayer ?? false,

            bgDesktopLayerBgType: data.styleDefault.bg?.bgLayer?.type,
            bgDesktopLayerBgColor: data.styleDefault.bg?.bgLayer?.color,
            bgDesktopLayerBgGradientColor1:
                data.styleDefault.bg?.bgLayer?.gradient?.color1,
            bgDesktopLayerBgGradientColor2:
                data.styleDefault.bg?.bgLayer?.gradient?.color2,
            bgDesktopLayerBgGradientDeg: parseFloat(
                `${data.styleDefault.bg?.bgLayer?.gradient?.deg}`
            ),
            bgDesktopLayerBgImg: data.styleDefault.bg?.bgLayer?.img?.fileData,
            bgDesktopLayerBgVideo:
                data.styleDefault.bg?.bgLayer?.video?.fileData,
            bgDesktopLayerBgOpacity: parseFloat(
                `${data.styleDefault.bg?.bgLayer?.opacity}`
            ),

            bgMovilBgType: data.styleDefault.bg?.bgMovil?.type,
            bgMovilBgColor: data.styleDefault.bg?.bgMovil?.color,
            bgMovilBgGradientColor1:
                data.styleDefault.bg?.bgMovil?.gradient?.color1,
            bgMovilBgGradientColor2:
                data.styleDefault.bg?.bgMovil?.gradient?.color2,
            bgMovilBgGradientDeg: parseFloat(
                `${data.styleDefault.bg?.bgMovil?.gradient?.deg}`
            ),
            bgMovilBgImg: data.styleDefault.bg?.bgMovil?.img?.fileData,
            bgMovilBgVideo: data.styleDefault.bg?.bgMovil?.video?.fileData,
            bgMovilBgOpacity: parseFloat(
                `${data.styleDefault.bg?.bgMovil?.opacity}`
            ),

            useLayerMovil: data.styleDefault.bg?.useLayerMovil ?? false,

            bgMovilLayerBgType: data.styleDefault.bg?.bgMovilLayer?.type,
            bgMovilLayerBgColor: data.styleDefault.bg?.bgMovilLayer?.color,
            bgMovilLayerBgGradientColor1:
                data.styleDefault.bg?.bgMovilLayer?.gradient?.color1,
            bgMovilLayerBgGradientColor2:
                data.styleDefault.bg?.bgMovilLayer?.gradient?.color2,
            bgMovilLayerBgGradientDeg: parseFloat(
                `${data.styleDefault.bg?.bgMovilLayer?.gradient?.deg}`
            ),
            bgMovilLayerBgImg:
                data.styleDefault.bg?.bgMovilLayer?.img?.fileData,
            bgMovilLayerBgVideo:
                data.styleDefault.bg?.bgMovilLayer?.video?.fileData,
            bgMovilLayerBgOpacity: parseFloat(
                `${data.styleDefault.bg?.bgMovilLayer?.opacity}`
            ),

            imgBgType: data.styleDefault.img?.bg?.type,
            imgBgColor: data.styleDefault.img?.bg?.color,
            imgBgGradientColor1: data.styleDefault.img?.bg?.gradient?.color1,
            imgBgGradientColor2: data.styleDefault.img?.bg?.gradient?.color2,
            imgBgGradientDeg: parseFloat(
                `${data.styleDefault.img?.bg?.gradient?.deg}`
            ),
            imgBgImg: data.styleDefault.img?.bg?.img?.fileData,
            imgBgVideo: data.styleDefault.img?.bg?.video?.fileData,
            imgBgOpacity: parseFloat(`${data.styleDefault.img?.bg?.opacity}`),

            imgBorderSize: parseInt(
                `${data.styleDefault.img?.borderSize ?? 0}`
            ),
            imgBorderType: data.styleDefault.img?.borderType,

            tolinkmeLogo: data.styleDefault.img?.useLogoTolinkme ?? true,

            btnPrincipalSize: parseInt(
                `${data?.styleDefault?.btnPrincipal?.size ?? 15}`
            ),
            btnPrincipalPadding: parseInt(
                `${data?.styleDefault?.btnPrincipal?.padding ?? 10}`
            ),
            btnPrincipalColor:
                data?.styleDefault?.btnPrincipal?.color ?? '#ffffff',
            btnPrincipalRound:
                data?.styleDefault?.btnPrincipal?.borderRadius ?? 'rounded',

            btnPrincipalBorderSize: parseInt(
                `${data?.styleDefault?.btnPrincipal?.border?.size ?? 0}`
            ),
            btnPrincipalBorderColor:
                data?.styleDefault?.btnPrincipal?.border?.color ?? '#ffffff',
            btnPrincipalBorderType:
                data?.styleDefault?.btnPrincipal?.border?.type ?? 'solid',

            btnPrincipalBgType:
                data?.styleDefault?.btnPrincipal?.background?.type ??
                'gradient',
            btnPrincipalBgColor:
                data?.styleDefault?.btnPrincipal?.background?.color ??
                '#ffffff',
            btnPrincipalBgGradientColor1:
                data?.styleDefault?.btnPrincipal?.background?.gradient
                    ?.color1 ?? '#04506c',
            btnPrincipalBgGradientColor2:
                data?.styleDefault?.btnPrincipal?.background?.gradient
                    ?.color2 ?? '#9d016e',
            btnPrincipalBgGradientDeg: parseInt(
                `${
                    data?.styleDefault?.btnPrincipal?.background?.gradient
                        ?.deg ?? 130
                }`
            ),
            btnPrincipalBgImg:
                data?.styleDefault?.btnPrincipal?.background?.img?.fileData ??
                '',
            btnPrincipalBgVideo:
                data?.styleDefault?.btnPrincipal?.background?.video?.fileData ??
                '',
            btnPrincipalBgOpacity: parseInt(
                `${
                    data?.styleDefault?.btnPrincipal?.background?.opacity ?? 100
                }`
            ),
            btnAnimationOnPageLoad: `${data.styleDefault.btnAnimation?.['on-page-load']}`,
            btnAnimationHover: `${data.styleDefault.btnAnimation?.hover}`,
            btnAnimationInfinite: `${data.styleDefault.btnAnimation?.infinite}`,
        },
        headers: {
            Authorization: user.token,
            'Content-Type': 'application/json',
        },
    });

    log('put result profile', result, 'aqua');

    if (result.type == 'ok') {
        for (let i = 0; i < data.linksDefault.length; i++) {
            const link = data.linksDefault[i];
            // const styles = {
            //     btnPrincipalSize: parseInt(`0`),
            //     btnPrincipalPadding: parseInt(`0`),
            //     btnPrincipalColor: ``,
            //     btnPrincipalRound: ``,
            //     btnPrincipalBorderSize: parseInt(`0`),
            //     btnPrincipalBorderColor: ``,
            //     btnPrincipalBorderType: ``,
            //     btnPrincipalBgType: ``,
            //     btnPrincipalBgColor: ``,
            //     btnPrincipalBgGradientColor1: ``,
            //     btnPrincipalBgGradientColor2: ``,
            //     btnPrincipalBgGradientDeg: parseInt(`0`),
            //     btnPrincipalBgImg: ``,
            //     btnPrincipalBgVideo: ``,
            //     btnPrincipalBgOpacity: parseInt(`0`),
            //     btnBorderSize: parseInt(`0`),
            //     btnBorderType: ``,
            //     btnBgOpacity: parseInt(`0`),
            //     btnTextLineHeight: parseInt(`0`),
            //     btnIconConfigSize: parseInt(`0`),
            //     btnIconConfigPadding: parseInt(`0`),
            //     btnIconConfigRound: ``,
            //     btnIconConfigBorderSize: parseInt(`0`),
            //     btnIconConfigBorderColor: ``,
            //     btnIconConfigBorderType: ``,
            //     btnIconConfigBgType: ``,
            //     btnIconConfigBgColor: ``,
            //     btnIconConfigBgGradientColor1: ``,
            //     btnIconConfigBgGradientColor2: ``,
            //     btnIconConfigBgGradientDeg: parseInt(`0`),
            //     btnIconConfigBgImg: ``,
            //     btnIconConfigBgVideo: ``,
            //     btnIconConfigBgOpacity: parseInt(`0`),
            //     btnBoxShadowType: ``,
            //     btnBoxShadowBlur: parseInt(`0`),
            //     btnBoxShadowSize: parseInt(`0`),
            //     btnBoxShadowX: parseInt(`0`),
            //     btnBoxShadowY: parseInt(`0`),
            //     btnBoxShadowColor: ``,
            //     btnAnimationOnPageLoad: ``,
            //     btnAnimationHover: ``,
            //     btnAnimationInfinite: ``,
            // };

            const MDTB = link?.monetizeData?.type;
            const MDPB = link?.monetizeData?.period;
            const MDRB = link?.monetizeData?.recurrent;
            let monetizeDataRecurrence = '';
            let monetizeDataType = '';
            let monetizeDataPeriod = '';

            if (MDRB == 'Recurrent') {
                monetizeDataRecurrence = 'RECURRENT';
            } else if (MDRB == 'Flat_Rate') {
                monetizeDataRecurrence = 'FLAT_RATE';
            }

            if (MDTB == 'Donation') {
                monetizeDataType = 'DONATION';
            } else if (MDTB == 'Gift') {
                monetizeDataType = 'GIFT';
            } else if (MDTB == 'Tip') {
                monetizeDataType = 'TIP';
            } else if (MDTB == 'Freelance_Service') {
                monetizeDataType == 'FREELANCE_SERVICE';
            }

            if (MDPB == 'Monthly') {
                monetizeDataPeriod = 'MONTHLY';
            } else if (MDPB == 'Quarterly') {
                monetizeDataPeriod = 'QUARTERLY';
            } else if (MDPB == 'Semi Annual') {
                monetizeDataPeriod = 'SEMIANNUAL';
            } else if (MDPB == 'Annually') {
                monetizeDataPeriod = 'ANNUALLY';
            } else if (MDPB == 'Just one') {
                monetizeDataPeriod = 'JUST_ONE';
            }

            const monetizeData: RSLinkConfigDataProps['monetizeData'] =
                link?.monetizeData
                    ? {
                          amount: `${link?.monetizeData?.amount ?? 0}`,
                          period: monetizeDataPeriod,
                          type: monetizeDataType,
                          recurrent: monetizeDataRecurrence,
                          description: link?.monetizeData.description ?? '',
                      }
                    : undefined;

            if (link.delete) {
                const resultDeleteBtn = await request({
                    method: 'delete',
                    url: `${URL}/buttons/${link.uuid}`,
                    headers: {
                        Authorization: user.token,
                        'Content-Type': 'application/json',
                    },
                });
                log('delete result delete Btn', resultDeleteBtn, 'crimson');
            } else if (link.uuid) {
                const resultUpdateBtn = await request({
                    method: 'put',
                    url: `${URL}/buttons/${link?.uuid}`,
                    data: {
                        title: link.rs,
                        url: link?.url ?? '',
                        logo: `/image/tolinkme/rs/${link.rs}.png`,
                        color: '#000',
                        private: !link.active,
                        order: i,
                        customImg: link.customImg,
                        customTitle: link.customTitle,
                        isPrincipal: link?.isPrincipal ?? false,
                        monetizeData,
                        // "profile_uuid": data.uuid
                    },
                    headers: {
                        Authorization: user.token,
                        'Content-Type': 'application/json',
                    },
                });
                log('put result Update Btn', resultUpdateBtn, 'aqua');
            } else {
                const resultCreateBtn = await request({
                    method: 'post',
                    url: `${URL}/buttons`,
                    data: {
                        title: link.rs,
                        url: link?.url ?? '',
                        logo: `/image/tolinkme/rs/${link.rs}.png`,
                        color: '#000',
                        private: !link.active,
                        profile_uuid: data?.uuid,
                        order: i,
                        customImg: link?.customImg,
                        customTitle: link?.customTitle,
                        isPrincipal: link?.isPrincipal ?? false,
                        monetizeData,
                    },
                    headers: {
                        Authorization: user.token,
                        'Content-Type': 'application/json',
                    },
                });
                log('put result Create Btn', resultCreateBtn, 'aqua');
            }
        }

        const resultUpdateUserName = await request({
            method: 'put',
            url: `${URL}/users/${user?.id}`,
            data: {
                username: data.styleDefault.name,
            },
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
        });
        log('result PUT resultUpdateUserName', resultUpdateUserName);
    } else {
        const r: SubmitResult = {
            status: 'error',
            message: `${result.error}`,
        };
        return r;
    }

    // throw {
    //     message: JSON.stringify(result),
    // };

    const r: SubmitResult = {
        status: 'ok',
        message: 'Publish Ok',
    };
    return r;
};
