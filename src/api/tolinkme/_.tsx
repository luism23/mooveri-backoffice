export const URLPRO = 'https://backends.tolnk.me/api/v1';

export const URLDEV = 'https://backends.tolnk.me/api/v1';

export const URL = process.env['NEXT_PUBLIC_MODE'] == 'DEV' ? URLDEV : URLPRO;
