import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';

import { onSubmintForgotPassword } from '@/components/Form/ForgotPassword/Base';
import log from '@/functions/log';

import { DataForgotPassword } from '@/interfaces/ForgotPassword';

export const ForgotPassword: onSubmintForgotPassword = async (
    data: DataForgotPassword
) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'ForgotPassword ok',
        };
    }
    const result = await request({
        method: 'post',
        url: `${URL}/auth/forgot-password`,
        data: {
            ...data,
            email: data.email?.toLocaleLowerCase(),
        },
    });

    log('Send forgot password', result, 'yellow');

    if (result.type == 'error') {
        throw {
            message: result.error.response.data.data,
        };
    }
    if (result.result.status == 'error') {
        throw {
            message: result.result.data,
        };
    }

    return {
        status: 'ok',
        message: 'Email Send',
    };
};
