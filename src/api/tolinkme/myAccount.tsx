import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';
import { SubmitResult } from '@/components/Form/Base';
import { CategoriesId } from '@/data/components/Categories';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';

import { DataMyAccount } from '@/interfaces/MyAccount';

export type GETINTERFACE = (data: {
    user: UserLoginProps | undefined;
}) => Promise<DataMyAccount>;

export const GET: GETINTERFACE = async ({ user }) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            profile_uuid: '',
            name: 'Test',
            email: 'test@sas.asd',
            emailVerify: false,
            tel: {
                code: '',
                tel: '',
            },
            telVerify: true,
            password: '',
            repeatPassword: '',
            categories: ['adult-content', 'beauty'],
        };
    }
    const result = await request({
        method: 'get',
        url: `${URL}/me`,
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
    });
    log('result get myaccount', result);
    if (result.type == 'ok') {
        const user = result?.result?.data?.user;
        const phone = user?.phone;
        const profile = user?.user_ModelProfile?.profile;
        const categories: CategoriesId[] = profile?.CategoryModelProfile.map(
            (c: any) => c?.category?.categoryType
        );

        let t = {
            code: '',
            tel: '',
        };
        try {
            const i = phone.indexOf('-');
            const tel = [phone.slice(0, i), phone.slice(i + 1)];
            t = {
                code: tel[0],
                tel: tel[1],
            };
        } catch (error) {
            log('result error phone', error, 'red');
        }
        return {
            profile_uuid: profile?.uuid,
            email: user?.email,
            emailVerify: user?.EmailStatus == 'VERIFY',
            tel: t,
            telVerify: user?.PhoneStatus == 'VERIFY',
            password: '',
            repeatPassword: '',
            categories,
            name: user?.fullName ?? '',
        };
    }
    return {
        name: '',
        email: '',
        emailVerify: false,
        tel: {
            code: '',
            tel: '',
        },
        telVerify: false,
        password: '',
        categories: [],
    };
};

export interface PUTPROPS {
    user: UserLoginProps | undefined;
    data: DataMyAccount;
}

export type PUTINTERFACE = (props: PUTPROPS) => Promise<SubmitResult>;
export const PUT: PUTINTERFACE = async ({ data, user }: PUTPROPS) => {
    log('data PUT myaccount', data);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));

        const r: SubmitResult = {
            status: 'ok',
            message: 'My Account Update',
        };
        return r;
    }

    const result = await request({
        method: 'put',
        url: `${URL}/users/${user?.id}`,
        data: {
            email: data.email,
            phone: `${data.tel.code}-${data.tel.tel}`,
            fullName: data.name,
            ...(data.password != ''
                ? {
                      password: data.password,
                  }
                : {}),
            //TODO: pendiente
            // categories
        },
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
    });
    log('result PUT myaccount', result);
    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'My Account Update',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'error',
        message: 'Upps Ocurrio un error',
    };
    return r;
};

export interface DELETEPROPS {
    user: UserLoginProps | undefined;
}

export type DELETEINTERFACE = (props: DELETEPROPS) => Promise<SubmitResult>;
export const DELETE: DELETEINTERFACE = async ({ user }: DELETEPROPS) => {
    log('user DELETE myaccount', user);
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));

        const r: SubmitResult = {
            status: 'ok',
            message: 'My Account Delete',
        };
        return r;
    }

    const result = await request({
        method: 'put',
        url: `${URL}/users/${user?.id}`,
        data: {
            status: 'DELETED',
        },
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
    });
    log('result DELETE myaccount', result, 'crimson');
    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'My Account Delete',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'error',
        message: 'Upps Ocurrio un error',
    };
    return r;
};
