import { SubmitResult } from '@/components/Form/Base';
import { UserLoginProps } from '@/hook/useUser';
import { DataFormStripe } from '@/interfaces/FormStripe';
import { request } from '@/api/request';
import log from '@/functions/log';
import { DataFormPayment } from '@/interfaces/FormPayment';
import { URL } from '@/api/tolinkme/_';
import {
    MethodCardDataProps,
    PaymentMethodCardDataProps,
} from '@/components/Form/Pay/Base';
import { DataFormAddPayment } from '@/interfaces/FormAddPayment';
import { DataFormPay } from '@/interfaces/FormPay';

export type POST_STRIPE_CARD_INTERFACE = (data: {
    data: DataFormStripe;
    user: UserLoginProps | undefined;
}) => Promise<SubmitResult>;

export const POST_STRIPE_CARD: POST_STRIPE_CARD_INTERFACE = async ({
    data,
    user,
}) => {
    log('put data profile', data, 'aqua');
    if (!user) {
        const r: SubmitResult = {
            status: 'error',
            message: 'User is not found',
        };
        return r;
    }
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Card Created',
            data: {
                firstName: 'Luis Miguel',
                LastName: 'Miguel',
                MiddleName: 'Narvaez',
                country: {
                    text: 'test',
                },
                state: {
                    text: 'test',
                },
                city: {
                    text: 'test',
                },
                phone: {
                    tel: '3223173104',
                    code: '+57',
                },
                stripe_before: {
                    elementType: 'card',
                    value: {
                        postalCode: '',
                    },
                    empty: false,
                    complete: true,
                    brand: 'visa',
                },
                stripe_after: {
                    paymentMethod: {
                        id: 'pm_1NECmzF4HKeYDTvLhGQ1gMEA',
                        object: 'payment_method',
                        billing_details: {
                            address: {
                                city: null,
                                country: null,
                                line1: null,
                                line2: null,
                                postal_code: null,
                                state: null,
                            },
                            email: null,
                            name: null,
                            phone: null,
                        },
                        card: {
                            brand: 'visa',
                            checks: {
                                address_line1_check: null,
                                address_postal_code_check: null,
                                cvc_check: null,
                            },
                            country: 'CO',
                            exp_month: 11,
                            exp_year: 2025,
                            funding: 'credit',
                            generated_from: null,
                            last4: '3634',
                            networks: {
                                available: ['visa'],
                                preferred: null,
                            },
                            three_d_secure_usage: {
                                supported: true,
                            },
                            wallet: null,
                        },
                        created: 1685630866,
                        customer: null,
                        livemode: false,
                        type: 'card',
                    },
                },
            },
        };
        return r;
    }

    const result = await request({
        method: 'post',
        url: `${URL}/monetize/stripe/${user?.id}`,

        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
        data: {
            firstName: data.firstName,
            LastName: data.LastName,
            MiddleName: data.MiddleName,
            country: {
                text: data.country,
            },
            state: {
                text: data.state,
            },
            city: {
                text: data.city,
            },
            phone: {
                code: data.phone.code,
                tel: data.phone.tel,
            },
            stripe_before: data.stripe_before,
            stripe_after: data.stripe_after,
        },
    });

    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'Card Created',
        };
        return r;
    }
    const r: SubmitResult = {
        status: 'ok',
        message: 'Card Created',
    };
    return r;
};

export type POST_STRIPE_CARD_USER_INTERFACE = (data: {
    data: DataFormPayment;
    user: UserLoginProps | undefined;
    btn_uuid: string;
}) => Promise<SubmitResult>;

export const POST_STRIPE_CARD_USER: POST_STRIPE_CARD_USER_INTERFACE = async ({
    data,
    user,
    btn_uuid,
}) => {
    log('put data profile', data, 'aqua');
    if (!user) {
        const r: SubmitResult = {
            status: 'error',
            message: 'User is not found',
        };
        return r;
    }
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: SubmitResult = {
            status: 'ok',
            message: 'Card Created',
            data: {
                street: 'Narvaez',
                country: {
                    text: 'test',
                },
                state: {
                    text: 'test',
                },
                city: {
                    text: 'test',
                },
                email: 'eee@gmail.com',
                zipCode: '2222',
                nameOnTheCard: '2222223',
                stripe_before: {
                    elementType: 'card',
                    value: {
                        postalCode: '',
                    },
                    empty: false,
                    complete: true,
                    brand: 'visa',
                },
                stripe_after: {
                    paymentMethod: {
                        id: 'pm_1NECmzF4HKeYDTvLhGQ1gMEA',
                        object: 'payment_method',
                        billing_details: {
                            address: {
                                city: null,
                                country: null,
                                line1: null,
                                line2: null,
                                postal_code: null,
                                state: null,
                            },
                            email: null,
                            name: null,
                            phone: null,
                        },
                        card: {
                            brand: 'visa',
                            checks: {
                                address_line1_check: null,
                                address_postal_code_check: null,
                                cvc_check: null,
                            },
                            country: 'CO',
                            exp_month: 11,
                            exp_year: 2025,
                            funding: 'credit',
                            generated_from: null,
                            last4: '3634',
                            networks: {
                                available: ['visa'],
                                preferred: null,
                            },
                            three_d_secure_usage: {
                                supported: true,
                            },
                            wallet: null,
                        },
                        created: 1685630866,
                        customer: null,
                        livemode: false,
                        type: 'card',
                    },
                },
            },
        };
        return r;
    }

    const result = await request({
        method: 'post',
        url: `${URL}/subscription`,

        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
        data: {
            button_uuid: btn_uuid,
            user_uuid: user.id,
            paymentMethodId: data.stripe_after.paymentMethod.id,
            confirm18: data.confirm18,
        },
    });

    if (result.type == 'ok') {
        const r: SubmitResult = {
            status: 'ok',
            message: 'Card created and successful payment',
        };
        return r;
    }
    const DATA = result.error?.response?.data?.data;

    if (DATA.decline_code == 'generic_decline') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Generic Decline',
        };
        return r;
    }
    if (DATA.decline_code == 'insufficient_funds') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Insufficient funds',
        };
        return r;
    }

    if (DATA.decline_code == 'lost_card') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Lost Card',
        };
        return r;
    }
    if (DATA.decline_code == 'stolen_card') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Stolen card decline',
        };
        return r;
    }
    if (DATA.code == 'expired_card') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Expired Card',
        };
        return r;
    }
    if (DATA.code == 'incorrect_cvc') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Incorrect cvc',
        };
        return r;
    }
    if (DATA.code == 'processing_error') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Processing Error',
        };
        return r;
    }

    if (result.type == 'error') {
        const r: SubmitResult = {
            status: 'error',
            message: 'Button already registered',
        };
        return r;
    }

    const r: SubmitResult = {
        status: 'ok',
        message: 'Card Created',
    };
    return r;
};

export type POST_STRIPE_ADD_CARD_USER_INTERFACE = (data: {
    data: DataFormAddPayment;
    user: UserLoginProps | undefined;
}) => Promise<SubmitResult>;

export const POST_STRIPE_ADD_CARD_USER: POST_STRIPE_ADD_CARD_USER_INTERFACE =
    async ({ data, user }) => {
        log('put data profile', data, 'aqua');
        if (!user) {
            const r: SubmitResult = {
                status: 'error',
                message: 'User is not found',
            };
            return r;
        }
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: SubmitResult = {
                status: 'ok',
                message: 'Card Created',
                data: {
                    street: 'Narvaez',
                    country: {
                        text: 'test',
                    },
                    state: {
                        text: 'test',
                    },
                    city: {
                        text: 'test',
                    },
                    email: 'eee@gmail.com',
                    zipCode: '2222',
                    nameOnTheCard: '2222223',
                    stripe_before: {
                        elementType: 'card',
                        value: {
                            postalCode: '',
                        },
                        empty: false,
                        complete: true,
                        brand: 'visa',
                    },
                    stripe_after: {
                        paymentMethod: {
                            id: 'pm_1NECmzF4HKeYDTvLhGQ1gMEA',
                            object: 'payment_method',
                            billing_details: {
                                address: {
                                    city: null,
                                    country: null,
                                    line1: null,
                                    line2: null,
                                    postal_code: null,
                                    state: null,
                                },
                                email: null,
                                name: null,
                                phone: null,
                            },
                            card: {
                                brand: 'visa',
                                checks: {
                                    address_line1_check: null,
                                    address_postal_code_check: null,
                                    cvc_check: null,
                                },
                                country: 'CO',
                                exp_month: 11,
                                exp_year: 2025,
                                funding: 'credit',
                                generated_from: null,
                                last4: '3634',
                                networks: {
                                    available: ['visa'],
                                    preferred: null,
                                },
                                three_d_secure_usage: {
                                    supported: true,
                                },
                                wallet: null,
                            },
                            created: 1685630866,
                            customer: null,
                            livemode: false,
                            type: 'card',
                        },
                    },
                },
            };
            return r;
        }

        const result = await request({
            method: 'post',
            url: `${URL}/subscription/pm/${user.id}`,

            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
            data: {
                paymentMethodId: data.stripe_after.paymentMethod.id,
            },
        });

        if (result.type == 'ok') {
            const r: SubmitResult = {
                status: 'ok',
                message: 'Card created successfully',
            };
            return r;
        }

        const DATA = result.error?.response?.data?.data;

        if (DATA.decline_code == 'generic_decline') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Generic Decline',
            };
            return r;
        }
        if (DATA.decline_code == 'insufficient_funds') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Insufficient funds',
            };
            return r;
        }

        if (DATA.decline_code == 'lost_card') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Lost Card',
            };
            return r;
        }
        if (DATA.decline_code == 'stolen_card') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Stolen card decline',
            };
            return r;
        }
        if (DATA.code == 'expired_card') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Expired Card',
            };
            return r;
        }
        if (DATA.code == 'incorrect_cvc') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Incorrect cvc',
            };
            return r;
        }
        if (DATA.code == 'processing_error') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Processing Error',
            };
            return r;
        }
        const r: SubmitResult = {
            status: 'ok',
            message: 'Card Created',
        };

        return r;
    };

//Get cards
export type GET_STRIPE_CARD_USER_INTERFACE = (data: {
    user: UserLoginProps | undefined;
}) => Promise<SubmitResult<PaymentMethodCardDataProps>>;

export const GET_STRIPE_CARD_USER: GET_STRIPE_CARD_USER_INTERFACE = async ({
    user,
}) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        const r: PaymentMethodCardDataProps = {};
        return {
            status: 'ok',
            message: '',
            data: r,
        };
    }
    const result = await request({
        url: `${URL}/subscription/pm/customer/${user?.id}`,
        method: 'get',
        headers: {
            Authorization: user?.token,
            'Content-Type': 'application/json',
        },
    });
    if (result.type == 'ok') {
        const r: PaymentMethodCardDataProps = {
            PaymentMethodCard:
                result?.result?.data?.paymentMethods?.data?.map((pay: any) => {
                    const r: MethodCardDataProps = {
                        id: pay?.id,
                        card: {
                            brand: pay?.card?.brand ?? '',
                            exp_month: pay.card?.exp_month ?? null,
                            exp_year: pay.card?.exp_year ?? null,
                            funding: pay.card?.funding ?? '',
                            last4: pay.card?.last4 ?? '',
                        },
                    };
                    return r;
                }) ?? [],
        };

        return {
            status: 'ok',
            message: 'Data Payment Card User',
            data: r,
        };
    }
    return {
        status: 'error',
        message: 'Error Get',
    };
};

//DELETE CARD

export type DELETE_STRIPE_CARD_USER_INTERFACE = (data: {
    user: UserLoginProps | undefined;
    paymentMethodId: string;
}) => Promise<SubmitResult<DataFormAddPayment>>;

export const DELETE_STRIPE_CARD_USER: DELETE_STRIPE_CARD_USER_INTERFACE =
    async ({ user, paymentMethodId }) => {
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: DataFormAddPayment = {};
            return {
                status: 'ok',
                message: '',
                data: r,
            };
        }
        const result = await request({
            url: `${URL}/subscription/pm/`,
            method: 'delete',
            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
            data: {
                paymentMethodId,
            },
        });
        if (result.type == 'ok') {
            const r: PaymentMethodCardDataProps = {};
            log('', r);

            return {
                status: 'ok',
                message: 'Delete Card',
            };
        }
        return {
            status: 'error',
            message: 'Error delete',
        };
    };

//POST Card user que ya esta agregada la taarjeta
export type POST_STRIPE_AVAILABLE_CARD_USER_INTERFACE = (data: {
    data: DataFormPay;
    user: UserLoginProps | undefined;
    btn_uuid: string;
}) => Promise<SubmitResult>;

export const POST_STRIPE_AVAILABLE_CARD_USER: POST_STRIPE_AVAILABLE_CARD_USER_INTERFACE =
    async ({ data, user, btn_uuid }) => {
        log('put data profile', data, 'aqua');
        if (!user) {
            const r: SubmitResult = {
                status: 'error',
                message: 'User is not found',
            };
            return r;
        }
        if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
            await new Promise((r) => setTimeout(r, 2000));
            const r: SubmitResult = {
                status: 'ok',
                message: 'Card Created',
                data: {},
            };
            return r;
        }

        const result = await request({
            method: 'post',
            url: `${URL}/subscription`,

            headers: {
                Authorization: user?.token,
                'Content-Type': 'application/json',
            },
            data: {
                button_uuid: btn_uuid,
                user_uuid: user.id,
                paymentMethodId: data.paymentMethodId,
            },
        });
        if (result.type == 'ok') {
            const r: SubmitResult = {
                status: 'ok',
                message: 'Bottom bought',
            };
            return r;
        }
        if (result.type == 'error') {
            const r: SubmitResult = {
                status: 'error',
                message: 'Button already registered',
            };
            return r;
        }
        if (
            result.type == 'StripeCardError' &&
            result.result?.data?.decline_code === 'insufficient_funds'
        ) {
            const r: SubmitResult = {
                status: 'error',
                message: 'Insufficient funds',
            };
            return r;
        }
        const r: SubmitResult = {
            status: 'ok',
            message: 'Card Created',
        };
        return r;
    };
