import { request } from '@/api/request';
import { URL } from '@/api/tolinkme/_';

import {
    onSubmintRegister,
    onValidateUsername,
} from '@/components/Form/Register/Base';
import log from '@/functions/log';

import { DataRegister } from '@/interfaces/Register';

export const Register: onSubmintRegister = async (data: DataRegister) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        return {
            status: 'ok',
            message: 'register ok',
            data: {
                id: '1',
                token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
                name: 'test',
                img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
            },
        };
    }
    const result = await request({
        method: 'post',
        url: `${URL}/users`,
        data: {
            email: data.email?.toLocaleLowerCase(),
            username: data.userName?.toLocaleLowerCase(),
            password: data.password,
        },
    });
    log('result register', result, 'aqua');
    if (result.type == 'error') {
        const targets =
            result?.error?.response?.data?.data?.meta?.target ?? null;
        if (targets) {
            throw {
                status: 'error',
                message: `Oops the following fields are invalid: [${targets.join()}]`,
            };
        }
    }
    if (result.result.status == 'success') {
        return {
            status: 'ok',
            message: '😎 Successfully logged in',
            data: {
                id: result?.result?.data?.user?.uuid,
                token: result?.result?.data?.token,
                name: result?.result?.data?.user?.username,
                img: result?.result?.data?.user?.img ?? '',
            },
        };
    }

    return {
        status: 'ok',
        message: 'register ok',
        data: {
            id: '1',
            token: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c',
            name: 'test',
            img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
        },
    };
};

export const RegisterValidateName: onValidateUsername = async (
    username: string
) => {
    if (process.env['NEXT_PUBLIC_MOCKDATA'] == 'TRUE') {
        await new Promise((r) => setTimeout(r, 2000));
        if (username == 'error') {
            throw {
                message: 'username invalid',
            };
        }
        return;
    }
    const result = await request({
        url: `${URL}/users/verify?username=${username?.toLocaleLowerCase()}`,
    });
    log('result RegisterValidateName', result, 'aqua');
    if (result.type == 'error') {
        throw {
            message: `${result.error}`,
        };
    }
    if (result.result.data == 'unavailable') {
        throw {
            message: 'User Name Taken',
        };
    }
};
