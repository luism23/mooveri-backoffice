import feliz from '@/public/lottie/feliz.json';
import ImageLottie from '@/components/ImageLottie';

export const UnicornHappy = () => <ImageLottie img={feliz} />;
export default UnicornHappy;
