import { Story, Meta } from "@storybook/react";

import { UnicornWealthy } from "./index";

export default {
    title: "Lottie/UnicornWealthy",
    component: UnicornWealthy,
} as Meta;

const TemplateIndex: Story = (args) => (
    <UnicornWealthy {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
