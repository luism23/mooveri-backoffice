import triste from '@/public/lottie/triste.json';
import ImageLottie from '@/components/ImageLottie';

export const UnicornSad = () => <ImageLottie img={triste} />;
export default UnicornSad;
