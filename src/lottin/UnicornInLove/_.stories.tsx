import { Story, Meta } from "@storybook/react";

import { UnicornInLove } from "./index";

export default {
    title: "Lottie/UnicornInLove",
    component: UnicornInLove,
} as Meta;

const TemplateIndex: Story = (args) => (
    <UnicornInLove {...args}/>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
