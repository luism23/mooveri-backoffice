import enamorado from '@/public/lottie/enamorado.json';
import ImageLottie from '@/components/ImageLottie';

export const UnicornInLove = () => <ImageLottie img={enamorado} />;
export default UnicornInLove;
