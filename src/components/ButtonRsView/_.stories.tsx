import { Story, Meta } from "@storybook/react";

import { ButtonRsViewProps, ButtonRsView } from "./index";

export default {
    title: "ButtonRsView/ButtonRsView",
    component: ButtonRsView,
} as Meta;

const ButtonRsViewIndex: Story<ButtonRsViewProps> = (args) => (
    <ButtonRsView {...args}>Test Children</ButtonRsView>
);

export const Index = ButtonRsViewIndex.bind({});
Index.args = {};
