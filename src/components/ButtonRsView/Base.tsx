import CSS from 'csstype';

import {
    BorderRadiusButtonValues,
    ButtonPrincipalConfig,
} from '@/interfaces/Button';
import Settings from '@/svg/Settings';
import { useMemo } from 'react';
import { getCorrectColor } from '@/functions/getCorrectColor';

export interface ButtonRsViewClassProps {}

export interface ButtonRsViewBaseProps {
    config?: ButtonPrincipalConfig;
    Icon?: any;
    img?: boolean;
}

export interface ButtonRsViewProps
    extends ButtonRsViewClassProps,
        ButtonRsViewBaseProps {}

export const ButtonRsViewBase = ({
    config = {},
    Icon = Settings,
    img = false,
}: ButtonRsViewProps) => {
    const className = useMemo(
        () => `
        ${BorderRadiusButtonValues[config?.borderRadius ?? 'rounded']}
    `,
        [config?.borderRadius]
    );

    const bgStyle = useMemo<CSS.Properties>(() => {
        let bg = '';
        if (config?.background?.type == 'color') {
            bg =
                config?.background?.color ??
                getCorrectColor(config.color ?? '#ffffff');
        }
        if (config?.background?.type == 'gradient') {
            bg = `linear-gradient(${config?.background?.gradient?.deg}deg, ${config?.background?.gradient?.color1}, ${config?.background?.gradient?.color2})`;
        }
        return {
            background: bg,
            opacity: (config?.background?.opacity ?? 100) / 100,
        };
    }, [config?.background]);

    const style = useMemo<CSS.Properties>(
        () => ({
            color: config?.color ?? '#fff',
            padding: `${(config.padding ?? 0) / 16}rem`,
            borderWidth: `${config?.border?.size ?? 0}px`,
            borderStyle: `${config?.border?.type ?? 'solid'}`,
            borderColor: `${config?.border?.color ?? 'transparent'}`,
        }),
        [config]
    );

    const Icon_ = useMemo(
        () => (
            <div
                style={style}
                className={`flex flex-align-center pos-r overflow-hidden ${className}`}
            >
                <div
                    className="pos-a inset-0 width-p-100 height-p-100"
                    style={bgStyle}
                ></div>
                <span className="pos-r flex flex-align-center ">
                    {img ? Icon : <Icon size={config?.size ?? 20} />}
                </span>
            </div>
        ),
        [style, config, className, Icon, img]
    );

    return <>{Icon_}</>;
};
export default ButtonRsViewBase;
