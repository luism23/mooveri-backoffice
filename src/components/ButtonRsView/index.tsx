import { useMemo } from 'react';

import * as styles from '@/components/ButtonRsView/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonRsViewBaseProps,
    ButtonRsViewBase,
} from '@/components/ButtonRsView/Base';

export const ButtonRsViewStyle = { ...styles } as const;

export type ButtonRsViewStyles = keyof typeof ButtonRsViewStyle;

export interface ButtonRsViewProps extends ButtonRsViewBaseProps {
    styleTemplate?: ButtonRsViewStyles | ThemesType;
}

export const ButtonRsView = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonRsViewProps) => {
    const Style = useMemo(
        () =>
            ButtonRsViewStyle[styleTemplate as ButtonRsViewStyles] ??
            ButtonRsViewStyle._default,
        [styleTemplate]
    );

    return <ButtonRsViewBase {...Style} {...props} />;
};
export default ButtonRsView;
