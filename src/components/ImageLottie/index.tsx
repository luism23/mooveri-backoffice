import { useMemo } from 'react';

import * as styles from '@/components/ImageLottie/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ImageLottieBaseProps,
    ImageLottieBase,
} from '@/components/ImageLottie/Base';

export const ImageLottieStyle = { ...styles } as const;

export type ImageLottieStyles = keyof typeof ImageLottieStyle;

export interface ImageLottieProps extends ImageLottieBaseProps {
    styleTemplate?: ImageLottieStyles | ThemesType;
}

export const ImageLottie = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ImageLottieProps) => {
    const Style = useMemo(
        () =>
            ImageLottieStyle[styleTemplate as ImageLottieStyles] ??
            ImageLottieStyle._default,
        [styleTemplate]
    );

    return <ImageLottieBase {...Style} {...props} />;
};
export default ImageLottie;
