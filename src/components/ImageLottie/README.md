# ImageLottie

## Dependencies

[ImageLottie](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ImageLottie)

```js
import { ImageLottie } from '@/components/ImageLottie';
```

## Import

```js
import { ImageLottie, ImageLottieStyles } from '@/components/ImageLottie';
```

## Props

```tsx
interface ImageLottieProps {}
```

## Use

```js
<ImageLottie>ImageLottie</ImageLottie>
```
