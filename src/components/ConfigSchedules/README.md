# ConfigSchedules

## Dependencies

[ConfigSchedules](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigSchedules)

```js
import { ConfigSchedules } from '@/components/ConfigSchedules';
```

## Import

```js
import {
    ConfigSchedules,
    ConfigSchedulesStyles,
} from '@/components/ConfigSchedules';
```

## Props

```tsx
interface ConfigSchedulesProps {}
```

## Use

```js
<ConfigSchedules>ConfigSchedules</ConfigSchedules>
```
