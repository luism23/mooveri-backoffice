import { useMemo } from 'react';

import * as styles from '@/components/Footer/styles';

import { Theme, ThemesType } from '@/config/theme';

import { FooterBaseProps, FooterBase } from '@/components/Footer/Base';

export const FooterStyle = { ...styles } as const;

export type FooterStyles = keyof typeof FooterStyle;

export interface FooterProps extends FooterBaseProps {
    styleTemplate?: FooterStyles | ThemesType;
}

export const Footer = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FooterProps) => {
    const Style = useMemo(
        () =>
            FooterStyle[styleTemplate as FooterStyles] ?? FooterStyle._default,
        [styleTemplate]
    );

    return <FooterBase {...Style} {...props} />;
};
export default Footer;
