import { Story, Meta } from "@storybook/react";

import { FooterProps, Footer } from "./index";

export default {
    title: "Global/Footer",
    component: Footer,
} as Meta;

const Template: Story<FooterProps> = (args) => (
    <Footer {...args}>Test Children</Footer>
);

export const Index = Template.bind({});
Index.args = {};
