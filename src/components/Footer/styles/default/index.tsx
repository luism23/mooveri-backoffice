import { FooterClassProps } from '@/components/Footer/Base';

export const _default: FooterClassProps = {
    classNameContent: `
        p-v-20
        m-t-auto
    `,
    classNameContainer: `
        container
        p-h-15
        flex
    `,
    classNameCols: `
        flex-4
    `,
};
