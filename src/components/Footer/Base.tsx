import { useEffect, useRef } from 'react';

import { DATA } from '@/data/components/Footer';

import Title from '@/components/Title';
import Link from '@/components/Link';
import Image from '@/components/Image';
import url from '@/data/routes';

export interface FooterClassProps {
    classNameContent?: string;
    classNameContainer?: string;
    classNameCols?: string;
}

export interface FooterBaseProps {}

export interface FooterProps extends FooterClassProps, FooterBaseProps {}

export const FooterBase = ({
    classNameContent = '',
    classNameContainer = '',
    classNameCols = '',
}: FooterProps) => {
    const footer = useRef<any>(null);

    const loadHeightTopPaddingBody = () => {
        const heightFooter = footer.current.offsetHeight;
        document.body.style.setProperty('--sizeFooter', `${heightFooter}px`);
    };
    const loadFooter = () => {
        loadHeightTopPaddingBody();
    };
    useEffect(() => {
        if (footer) {
            loadFooter();
        }
    }, [footer]);
    useEffect(() => {
        return () => {
            document.body.style.setProperty('--sizeFooter', `0px`);
        };
    }, []);
    return (
        <>
            <footer className={classNameContent} ref={footer}>
                <div className={classNameContainer}>
                    <div className={classNameCols}>
                        <Link href={url.home}>
                            <Image src={`${DATA.logo}`} />
                        </Link>
                    </div>
                    <div className={classNameCols}></div>
                    <div className={classNameCols}>
                        <Title>Developers</Title>
                        {DATA.developers.map((e, i) => {
                            return (
                                <Link href={e?.link} key={i}>
                                    {e?.text}
                                </Link>
                            );
                        })}
                    </div>
                </div>
            </footer>
        </>
    );
};
export default FooterBase;
