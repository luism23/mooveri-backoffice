# ConfigHorarios

## Dependencies

[ConfigHorarios](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigHorarios)

```js
import { ConfigHorarios } from '@/components/ConfigHorarios';
```

## Import

```js
import {
    ConfigHorarios,
    ConfigHorariosStyles,
} from '@/components/ConfigHorarios';
```

## Props

```tsx
interface ConfigHorariosProps {}
```

## Use

```js
<ConfigHorarios>ConfigHorarios</ConfigHorarios>
```
