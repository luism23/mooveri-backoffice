import { useMemo } from 'react';

import * as styles from '@/components/ConfigHorarios/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigHorariosBaseProps,
    ConfigHorariosBase,
} from '@/components/ConfigHorarios/Base';

export const ConfigHorariosStyle = { ...styles } as const;

export type ConfigHorariosStyles = keyof typeof ConfigHorariosStyle;

export interface ConfigHorariosProps extends ConfigHorariosBaseProps {
    styleTemplate?: ConfigHorariosStyles | ThemesType;
}

export const ConfigHorarios = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigHorariosProps) => {
    const Style = useMemo(
        () =>
            ConfigHorariosStyle[styleTemplate as ConfigHorariosStyles] ??
            ConfigHorariosStyle._default,
        [styleTemplate]
    );

    return <ConfigHorariosBase {...Style} {...props} />;
};
export default ConfigHorarios;
