import { ConfigHorariosClassProps } from '@/components/ConfigHorarios/Base';

export const mooveri: ConfigHorariosClassProps = {};

export const mooveriBackoffice: ConfigHorariosClassProps = {
    classNameContentDays: `
        grid
        grid-columns-7
        column-gap-10
        row-gap-10
        m-b-20
    `,
    classNameContentHorario: `
        grid-
    `,
    styleTemplateConfigHorario: 'mooveriBackoffice',
};
