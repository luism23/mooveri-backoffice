import { MenuClassProps } from '@/components/Menu/Base';
import SVGBars from '@/svg/bars';
import SVGClose from '@/svg/close';

export const mooveri: MenuClassProps = {
    classNameContent: `
        pos-r-
    `,
    classNameButton: `
        bg-transparent
        border-0
        color-greenish-cyan
        color-metallicBlueDark-hover
        p-0
    `,
    iconBar: <SVGBars size={20} />,
    iconClose: <SVGClose size={17} />,
    classNameContentUl: `
        pos-a
        top-p-100
        right-0
        overflow-auto
        z-index-9
        m-0
        transition-5
        transform
        transform-translate-Y-10


        bg-white
        box-shadow
        box-shadow-b-10
        box-shadow-black-16
        p-h-0
        p-v-16
        border-radius-15
    `,
    styleContentUl: {
        width: 'min(80vh,280px)',
        maxHeight: 'calc(100vh - var(--sizeHeader,0px))',
        transformOrigin: 'top',
    },
    classNameContentUlActive: `
        transform-scale-Y-p-100
    `,
    classNameContentUlInactive: `
        transform-scale-Y-0
    `,
    classNameUl: `
        flex flex-justify-left
        flex-column flex-nowrap flex-align-start
        
        p-0 
        m-0
    `,
    classNameLi: `
        font-14
        line-h-10
        font-w-600
        color-warmGreyTwo
        p-h-18
        p-v-5
        bg-white
        bg-pinkishGreyFour-hover
        flex
        flex-align-center
        width-p-100
    `,
    classNameLiActive: `
    `,
    classNameLiInactive: ``,
    classNameLiStrong: `
    
    `,
    classNameA: `
        flex
        flex-align-center
        color-currentColor
        width-p-100
    `,
    classNameSeparator: `
        width-p-100
        m-v-10
        height-1
        bg-warmGreyTwo
    `,
};

export const mooveriBackoffice: MenuClassProps = {
    classNameContent: `
        pos-r-
    `,
    classNameButton: `
        bg-transparent
        border-0
        color-greenish-cyan
        color-metallicBlueDark-hover
        p-0
    `,
    iconBar: <SVGBars size={20} />,
    iconClose: <SVGClose size={17} />,
    classNameContentUl: `
        pos-f
        left-0
        overflow-auto
        z-index-9
        m-0
        transition-5
        transform

        bg-dark-jungle-green
        
        opacity-7

        p-h-0
        p-v-16
    `,
    styleContentUl: {
        width: 'min(80vh,280px)',
        height: 'calc(100vh - var(--sizeHeader,0px))',
        top: '106px',
    },
    classNameContentUlActive: `
        transform-translate-X-0
    `,
    classNameContentUlInactive: `
        transform-translate-X-p--100
    `,
    classNameUl: `
        flex flex-justify-left
        flex-column flex-nowrap flex-align-start
        
        p-0 
        m-0
    `,
    classNameLi: `
        font-18
        line-h-10
        font-w-900
        color-white
        p-h-18
        p-v-5
        flex
        flex-align-center
        width-p-100
    `,
    classNameLiActive: `
    `,
    classNameLiInactive: ``,
    classNameLiStrong: `
    
    `,
    classNameA: `
        flex
        flex-align-center
        color-currentColor
        width-p-100
    `,
    classNameSeparator: `
        width-p-100
        m-v-10
        height-1
        bg-white
    `,
    useLoadEnventBody: false,
    clickHiddenMenu: false,
    defaultActive: true,
};
