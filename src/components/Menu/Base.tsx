import Link from 'next/link';
import CSS from 'csstype';

import { useState, useEffect, useRef } from 'react';

import SVGBars from '@/svg/bars';
import SVGClose from '@/svg/close';
import { useLang } from '@/lang/translate';

export const BaseStyle = {
    classNameContent: `
        pos-r
    `,
    classNameButton: `
        border-0
        bg-transparent
        color-black color-blue-hover
    `,
    classNameUl: `
        p-0
        m-0
        z-index-9
    `,
    classNameUlActive: `
    `,
    classNameLi: `
    `,
    classNameLiActive: `
    `,
    classNameA: `
    `,
};

export interface MenuItem {
    link: string;
    text: any;
    type?: 'strong' | unknown;
    separator?: boolean;
}

export interface MenuClassProps {
    classNameContent?: string;
    classNameButton?: string;

    classNameContentUl?: string;
    styleContentUl?: CSS.Properties;
    classNameContentUlActive?: string;
    classNameContentUlInactive?: string;

    classNameUl?: string;
    styleUl?: CSS.Properties;
    classNameLi?: string;
    classNameLiStrong?: string;
    classNameLiActive?: string;
    classNameLiInactive?: string;
    classNameA?: string;
    classNameSeparator?: string;

    useLoadEnventBody?: boolean;
    clickHiddenMenu?: boolean;
    defaultActive?: boolean;

    extraTop?: any;
    extraDown?: any;

    iconBar?: any;
    iconClose?: any;
}
export interface MenuBaseProps {
    items: MenuItem[];
}
export interface MenuProps extends MenuBaseProps, MenuClassProps {}
export const MenuBase = ({
    items = [],
    classNameContent = '',
    classNameButton = '',
    classNameContentUl = '',
    styleContentUl = {},
    classNameContentUlActive = '',
    classNameContentUlInactive = '',
    classNameUl = '',
    styleUl = {},
    classNameLi = '',
    classNameLiStrong = '',
    classNameA = '',
    classNameSeparator = '',

    useLoadEnventBody = true,
    clickHiddenMenu = true,
    defaultActive = false,

    extraTop = '',
    extraDown = '',

    iconBar = <SVGBars size={26} />,
    iconClose = <SVGClose size={24} />,
}: MenuProps) => {
    const [active, setActive] = useState(defaultActive);
    const menu = useRef<any>(null);
    const menuUl = useRef<any>(null);
    const _t = useLang();

    const toggleMenu = () => {
        setActive(!active);
    };
    const loadSizePaddingBody = () => {
        const heightMenu = active ? menuUl?.current?.offsetHeight ?? 0 : 0;
        const whidthMenu = active ? menuUl?.current?.offsetWidth ?? 0 : 0;
        document.body.style.setProperty('--sizeMenu', `${heightMenu}px`);
        document.body.style.setProperty('--hMenu', `${heightMenu}px`);
        document.body.style.setProperty('--wMenu', `${whidthMenu}px`);
    };
    const loadEnventBody = () => {
        if (!useLoadEnventBody) {
            return;
        }
        document.addEventListener('click', function (event) {
            if (menu?.current) {
                const isClickInsideElement = menu.current.contains(
                    event.target
                );
                if (!isClickInsideElement) {
                    setActive(false);
                }
            }
        });
    };
    useEffect(() => {
        loadEnventBody();
    }, []);
    useEffect(() => {
        loadSizePaddingBody();
    }, [active]);
    return (
        <>
            <div ref={menu} className={classNameContent}>
                <button className={classNameButton} onClick={toggleMenu}>
                    <span className={`${active ? 'd-none' : ''}`}>
                        {iconBar}
                    </span>
                    <span className={`${!active ? 'd-none' : ''}`}>
                        {iconClose}
                    </span>
                </button>
                <div
                    style={styleContentUl}
                    className={`${classNameContentUl}  ${
                        active
                            ? classNameContentUlActive
                            : classNameContentUlInactive
                    }`}
                >
                    {extraTop}
                    <ul ref={menuUl} style={styleUl} className={classNameUl}>
                        {items.map((e, i) => {
                            return (
                                <>
                                    <li
                                        key={i}
                                        className={`${classNameLi} ${
                                            e.type == 'strong'
                                                ? classNameLiStrong
                                                : ''
                                        }`}
                                    >
                                        <Link href={e.link}>
                                            <a
                                                className={classNameA}
                                                onClick={() => {
                                                    if (clickHiddenMenu) {
                                                        setActive(false);
                                                    }
                                                }}
                                            >
                                                {_t(e.text)}
                                            </a>
                                        </Link>
                                    </li>
                                    {e.separator ? (
                                        <div className={classNameSeparator} />
                                    ) : (
                                        <></>
                                    )}
                                </>
                            );
                        })}
                    </ul>
                    {extraDown}
                </div>
            </div>
        </>
    );
};
export default MenuBase;
