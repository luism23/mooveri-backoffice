# ConfigSchedule

## Dependencies

[ConfigSchedule](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigSchedule)

```js
import { ConfigSchedule } from '@/components/ConfigSchedule';
```

## Import

```js
import {
    ConfigSchedule,
    ConfigScheduleStyles,
} from '@/components/ConfigSchedule';
```

## Props

```tsx
interface ConfigScheduleProps {}
```

## Use

```js
<ConfigSchedule>ConfigSchedule</ConfigSchedule>
```
