import { ConfigScheduleClassProps } from '@/components/ConfigSchedule/Base';

export const mooveri: ConfigScheduleClassProps = {};

export const mooveriBackoffice: ConfigScheduleClassProps = {
    classNameContentStartEnd: `
        flex
        column-gap-10
        flex-nowrap
        flex-align-end
    `,
    styleTemplateInputDate: 'mooveriBackofficeDate',
    classNameContentDelete: `
        cursor-pointer
        color-white
        color-red-hover
        opacity-7
    `,
};
