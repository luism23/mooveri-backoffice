import { useEffect, useMemo, useState } from 'react';

import { useLang } from '@/lang/translate';

import {
    InputCheckbox,
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';
import { ContentWidth } from '@/components/ContentWidth';

import { categories } from '@/data/components/Categories';
import { DataCategory } from '@/interfaces/Category';
import Theme, { ThemesType } from '@/config/theme';

export interface CategoriesClassProps {
    classNameContent?: string;
    sizeContent?: number;
    styleTemplateCheck?: InputCheckboxStyles | ThemesType;
}

export interface CategoriesBaseProps {
    defaultCategories?: DataCategory[];
    onChange?: (data: DataCategory[]) => void;
    disabled?: boolean;
}

export interface CategoriesProps
    extends CategoriesClassProps,
        CategoriesBaseProps {}

export const CategoriesBase = ({
    classNameContent = '',
    sizeContent = 350,
    styleTemplateCheck = Theme?.styleTemplate ?? '_default',
    defaultCategories = [],
    onChange,
    disabled = false,
}: CategoriesProps) => {
    const [categoriesActive, setCategoriesActive] =
        useState<DataCategory[]>(defaultCategories);

    const toggleCategory = (category: DataCategory) => (value: boolean) => {
        setCategoriesActive((pre) => {
            if (value) {
                return [
                    ...pre,
                    {
                        ...category,
                        active: true,
                    },
                ];
            } else {
                return pre.filter((c) => c.id != category.id);
            }
        });
    };
    useEffect(() => {
        onChange?.(categoriesActive);
    }, [categoriesActive]);
    const _t = useLang();
    const AllCategories = useMemo(
        () =>
            categories.map((c, i) => (
                <InputCheckbox
                    key={i}
                    defaultValue={
                        defaultCategories.find((e) => e.id == c.id) != undefined
                    }
                    label={_t(c.name)}
                    onChange={toggleCategory(c)}
                    disabled={disabled}
                    styleTemplate={styleTemplateCheck}
                />
            )),
        [categories, disabled]
    );

    return (
        <ContentWidth size={sizeContent} className={classNameContent}>
            {AllCategories}
        </ContentWidth>
    );
};
export default CategoriesBase;
