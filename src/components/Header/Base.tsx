import { useEffect, useRef } from 'react';
import CSS from 'csstype';
import { useRouter } from 'next/router';

export interface HeaderClassProps {
    classNameHeader?: string;
    styleHeader?: CSS.Properties;
    classNameContent?: string;
    content?: any;
}

export interface HeaderBaseProps {}

export interface HeaderProps extends HeaderClassProps, HeaderBaseProps {}

export const HeaderBase = ({
    classNameHeader = '',
    styleHeader = {},
    classNameContent = '',
    content = <></>,
}: HeaderProps) => {
    const router = useRouter();
    const header = useRef<any>(null);

    const loadHeightTopPaddingBody = () => {
        const heightMenu = header?.current?.offsetHeight ?? 0;
        const whidthMenu = header?.current?.offsetWidth ?? 0;
        document.body.style.setProperty('--sizeHeader', `${heightMenu}px`);
        document.body.style.setProperty('--hHeader', `${heightMenu}px`);
        document.body.style.setProperty('--wHeader', `${whidthMenu}px`);
    };
    const loadHeader = () => {
        setTimeout(loadHeightTopPaddingBody, 100);
    };
    useEffect(() => {
        if (header && router.isReady) {
            loadHeader();
        }
    }, [header, router.isReady]);
    useEffect(() => {
        return () => {
            document.body.style.setProperty('--sizeHeader', `0px`);
        };
    }, []);

    return (
        <>
            <header
                ref={header}
                className={classNameHeader}
                style={styleHeader}
            >
                <div className={classNameContent}>
                    {router.isReady ? content : ''}
                </div>
            </header>
        </>
    );
};
export default HeaderBase;
