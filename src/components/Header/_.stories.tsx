import { Story, Meta } from "@storybook/react";

import { HeaderProps, Header } from "./index";

export default {
    title: "Global/Header",
    component: Header,
} as Meta;

const Template: Story<HeaderProps> = (args) => (
    <Header {...args}>Test Children</Header>
);

export const Index = Template.bind({});
Index.args = {};
