import { HeaderClassProps } from '@/components/Header/Base';

export const _default: HeaderClassProps = {
    classNameHeader: `
        header 
        pos-f top-0 left-0 
        width-p-100 
        flex flex-align-center
        p-v-15 
        z-index-9
        bg-white
        box-shadow box-shadow-black box-shadow-b-1
    `,
    classNameContent: `
        container
        p-h-15
        flex flex-align-center
    `,
};
