# ImgRsCount

## Dependences

[TypeRS](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/data/components/RS.tsx)

```js
import { TypeRS } from '@/data/components/RS';
```

## Import

```js
import { ImgRsCount, ImgRsCountStyles } from '@/components/ImgRsCount';
```

## Props

```ts
interface ImgRsCountProps {
    styleTemplate?: ImgRsCountStyles;
    id: TypeRS;
    count: number;
}
```

## Use

```js
<ImgRsCount id={'instagram'} count={10} />
```
