import { useMemo } from 'react';

import * as styles from '@/components/ImgRsCount/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ImgRsCountBaseProps,
    ImgRsCountBase,
} from '@/components/ImgRsCount/Base';

export const ImgRsCountStyle = { ...styles } as const;

export type ImgRsCountStyles = keyof typeof ImgRsCountStyle;

export interface ImgRsCountProps extends ImgRsCountBaseProps {
    styleTemplate?: ImgRsCountStyles | ThemesType;
}

export const ImgRsCount = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ImgRsCountProps) => {
    const Style = useMemo(
        () =>
            ImgRsCountStyle[styleTemplate as ImgRsCountStyles] ??
            ImgRsCountStyle._default,
        [styleTemplate]
    );

    return <ImgRsCountBase {...Style} {...props} />;
};
export default ImgRsCount;
