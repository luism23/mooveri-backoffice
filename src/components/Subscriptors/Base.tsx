import { PropsWithChildren } from 'react';
import ContentWidth from '@/components/ContentWidth';
import money from '@/functions/money';
import Text, { TextStyles } from '../Text';
import Theme, { ThemesType } from '@/config/theme';

export interface SubscriptorsClassProps {
    classNameText?: string;
    classNamePrice?: string;
    classNamecontent?: string;
    classNamecontent2?: string;
    classNamecontentPrice?: string;
    classNameContentSubscriptors?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateText2?: TextStyles | ThemesType;
    styleTemplateText3?: TextStyles | ThemesType;
}

export interface SubscriptorsBaseProps extends PropsWithChildren {
    price?: number;
    text?: string;
    text2?: string;
    text3?: string;
    Numbertext?: number;
}

export interface SubscriptorsProps
    extends SubscriptorsClassProps,
        SubscriptorsBaseProps {}

export const SubscriptorsBase = ({
    text = '/Month',
    text2 = '',
    text3 = '',
    price = 0.0,
    Numbertext,
    classNameText = '',
    classNamePrice = '',
    classNamecontent = '',
    classNamecontent2 = '',
    classNamecontentPrice = '',
    classNameContentSubscriptors = '',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    styleTemplateText2 = Theme?.styleTemplate ?? '_default',
    styleTemplateText3 = Theme?.styleTemplate ?? '_default',
}: SubscriptorsProps) => {
    return (
        <>
            <ContentWidth className={`${classNameContentSubscriptors}`}>
                <div className={classNamecontent}>
                    <div className={classNamecontentPrice}>
                        <div className={classNamePrice}>{money(price)}</div>
                        <Text
                            className={classNameText}
                            styleTemplate={styleTemplateText}
                        >
                            {text}
                        </Text>
                    </div>
                    <Text styleTemplate={styleTemplateText2}>{text2}</Text>
                </div>
                <div className={`${classNamecontent} ,${classNamecontent2}`}>
                    <Text styleTemplate={styleTemplateText3}>{Numbertext}</Text>
                    <Text styleTemplate={styleTemplateText2}>{text3}</Text>
                </div>
            </ContentWidth>
        </>
    );
};
export default SubscriptorsBase;
