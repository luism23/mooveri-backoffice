import { Story, Meta } from '@storybook/react';

import { SubscriptorsProps, Subscriptors } from './index';

export default {
    title: 'Subscriptors/Subscriptors',
    component: Subscriptors,
} as Meta;

const SubscriptorsIndex: Story<SubscriptorsProps> = (args) => (
    <Subscriptors {...args}>Test Children</Subscriptors>
);

export const Index = SubscriptorsIndex.bind({});
Index.args = {
    
    text2: '3 Buttons',
    text3: 'Subscriptors',
    Numbertext: 100,

};
