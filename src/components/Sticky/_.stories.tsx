import { Story, Meta } from "@storybook/react";

import { StickyProps, Sticky } from "./index";

export default {
    title: "Sticky/Sticky",
    component: Sticky,
} as Meta;

const Template: Story<StickyProps> = (args) => (
    <Sticky {...args}>Test Children</Sticky>
);

export const Index = Template.bind({});
Index.args = {};
