import { useMemo } from 'react';

import * as styles from '@/components/BoxLink/styles';

import { Theme, ThemesType } from '@/config/theme';

import { BoxLinkBaseProps, BoxLinkBase } from '@/components/BoxLink/Base';

export const BoxLinkStyle = { ...styles } as const;

export type BoxLinkStyles = keyof typeof BoxLinkStyle;

export interface BoxLinkProps extends BoxLinkBaseProps {
    styleTemplate?: BoxLinkStyles | ThemesType;
}

export const BoxLink = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BoxLinkProps) => {
    const Style = useMemo(
        () =>
            BoxLinkStyle[styleTemplate as BoxLinkStyles] ??
            BoxLinkStyle._default,
        [styleTemplate]
    );

    return <BoxLinkBase {...Style} {...props} />;
};
export default BoxLink;
