import { BoxLink, BoxLinkProps } from '@/components/BoxLink';
import url from '@/data/routes';
import Payment from '@/svg/payment';

export const BoxLinkPayment = ({ ...props }: BoxLinkProps) => {
    return (
        <BoxLink
            {...props}
            text="Payment Information"
            icon={<Payment size={29} />}
            link={url.myAccount.payments.index}
        />
    );
};
export default BoxLink;
