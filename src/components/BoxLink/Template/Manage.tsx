import { BoxLink, BoxLinkProps } from '@/components/BoxLink';
import url from '@/data/routes';
import Manage from '@/svg/manageAddresses';
export const BoxLinkManage = ({ ...props }: BoxLinkProps) => {
    return (
        <BoxLink
            {...props}
            text="Manage Addresses"
            icon={<Manage size={22} />}
            link={url.myAccount.addresses.index}
        />
    );
};
export default BoxLink;
