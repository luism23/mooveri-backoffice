import { BoxLink, BoxLinkProps } from '@/components/BoxLink';
import url from '@/data/routes';
import { UserAccount3 } from '@/svg/userAccount';

export const BoxLinkLegalInformation = ({ ...props }: BoxLinkProps) => {
    return (
        <BoxLink
            {...props}
            text="Legal Information"
            icon={<UserAccount3 size={22} />}
            link={url.myAccount.legal.index}
        />
    );
};
export default BoxLink;
