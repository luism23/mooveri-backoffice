import { BoxLink, BoxLinkProps } from '@/components/BoxLink';
import url from '@/data/routes';
import UserAccount from '@/svg/userAccount';

export const BoxLinkPersonal = ({ ...props }: BoxLinkProps) => {
    return (
        <BoxLink
            {...props}
            text="Personal Information"
            icon={<UserAccount size={22} />}
            link={url.myAccount.personal}
        />
    );
};
export default BoxLink;
