import { IconTextStyles } from '@/components/IconText';
import Box, { BoxStyles } from '@/components/Box';
import IconText from '@/components/IconText';
import Theme, { ThemesType } from '@/config/theme';
import Link, { LinkStyles } from '@/components/Link';

export interface BoxLinkClassProps {
    classNameContentLink?: string;

    styleTemplateIconText?: IconTextStyles | ThemesType;
    styleTemplateBox?: BoxStyles | ThemesType;
    styleTemplateLink?: LinkStyles | ThemesType;

    classNameContentTop?: string;
}

export interface BoxLinkBaseProps {
    text?: string;
    icon?: any;
    link?: string;
    top?: any;
}

export interface BoxLinkProps extends BoxLinkClassProps, BoxLinkBaseProps {}

export const BoxLinkBase = ({
    classNameContentLink = '',
    styleTemplateIconText = Theme.styleTemplate ?? '_default',
    styleTemplateBox = Theme.styleTemplate ?? '_default',
    styleTemplateLink = Theme.styleTemplate ?? '_default',
    classNameContentTop = '',
    text = '',
    icon = '',
    link = '#',
    top = null,
}: BoxLinkProps) => {
    return (
        <>
            <Link
                styleTemplate={styleTemplateLink}
                href={link}
                className={classNameContentLink}
            >
                <Box styleTemplate={styleTemplateBox}>
                    <IconText
                        text={text}
                        icon={icon}
                        styleTemplate={styleTemplateIconText}
                    />
                    {top ? (
                        <div className={classNameContentTop}>{top}</div>
                    ) : (
                        <></>
                    )}
                </Box>
            </Link>
        </>
    );
};
export default BoxLinkBase;
