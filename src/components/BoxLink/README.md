# BoxLink

## Dependencies

[Link](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Link)
[Box](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Box)
[IconText](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/IconText)

```js
import { Link } from '@/components/Link';
import { Box } from '@/components/Box';
import { IconText } from '@/components/IconText';
```

## Import

```js
import { BoxLink, BoxLinkStyles } from '@/components/BoxLink';
```

## Props

```tsx
interface BoxLinkProps {}
```

## Use

```js
<BoxLink>BoxLink</BoxLink>
```
