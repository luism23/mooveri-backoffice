import { useMemo } from 'react';

import * as styles from '@/components/404/styles';

import { Theme, ThemesType } from '@/config/theme';

import { C404BaseProps, C404Base } from '@/components/404/Base';

export const C404Style = { ...styles } as const;

export type C404Styles = keyof typeof C404Style;

export interface C404Props extends C404BaseProps {
    styleTemplate?: C404Styles | ThemesType;
}

export const C404 = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: C404Props) => {
    const Style = useMemo(
        () => C404Style[styleTemplate as C404Styles] ?? C404Style._default,
        [styleTemplate]
    );

    return <C404Base {...Style} {...props} />;
};
export default C404;
