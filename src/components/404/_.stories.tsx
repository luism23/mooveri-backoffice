import { Story, Meta } from "@storybook/react";

import { C404Props, C404 } from "./index";

export default {
    title: "404/404",
    component: C404,
} as Meta;

const C404Index: Story<C404Props> = (args) => (
    <C404 {...args}>Test Children</C404>
);

export const Index = C404Index.bind({});
Index.args = {};
