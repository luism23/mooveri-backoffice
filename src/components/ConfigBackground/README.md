# ConfigBackground

## Dependencies

[ConfigBackground](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigBackground)

```js
import { ConfigBackground } from '@/components/ConfigBackground';
```

## Import

```js
import {
    ConfigBackground,
    ConfigBackgroundStyles,
} from '@/components/ConfigBackground';
```

## Props

```tsx
interface ConfigBackgroundProps {}
```

## Use

```js
<ConfigBackground>ConfigBackground</ConfigBackground>
```
