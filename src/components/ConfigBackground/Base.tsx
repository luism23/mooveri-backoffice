import { Popup, PopupStyles } from '../Popup';
import InputSelectBackground from '../Input/SelectBackground';
import { Collapse, CollapseStyles } from '../Collapse';
import Desktop, { DesktopLayer } from '@/svg/Desktop';
import Movil, { MovilLayer } from '@/svg/Movil';
import { Theme, ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';
import {
    InputCheckbox,
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';

import { useLang } from '@/lang/translate';
import { useData } from 'fenextjs-hook/cjs/useData';
import {
    ConfigBackground,
    ConfigBackgroundType,
} from '@/interfaces/ConfigBackground';
import { useMemo, useState } from 'react';
import Reload from '@/svg/Reload';
import LoaderPage from '../Loader/LoaderPage';

export interface ConfigBackgroundClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    classNameContentSw?: string;

    styleTemplateTitleBg?: TextStyles | ThemesType;

    styleTemplateCheckbox?: InputCheckboxStyles | ThemesType;

    classNameReload?: string;
}

export interface ConfigBackgroundBaseProps {
    defaultValue?: ConfigBackground;
    onChange?: (data: ConfigBackground) => void;
}

export interface ConfigBackgroundProps
    extends ConfigBackgroundClassProps,
        ConfigBackgroundBaseProps {}

export const ConfigBackgroundBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    classNameContentSw = '',

    styleTemplateTitleBg = Theme?.styleTemplate ?? '_default',

    styleTemplateCheckbox = Theme?.styleTemplate ?? '_default',

    classNameReload = '',

    defaultValue = {
        useLayer: false,
        useLayerMovil: false,
        bg: {},
        bgLayer: {},
        bgMovil: {},
        bgMovilLayer: {},
    },
    onChange,
}: ConfigBackgroundProps) => {
    const _t = useLang();
    const [loader, setLoader] = useState(false);
    const [isActive, setIsActive] = useState<ConfigBackgroundType | ''>('');
    const defaultValue_ = useMemo(() => defaultValue, []);
    const { data, onChangeData, onRestart } = useData<ConfigBackground>(
        defaultValue_,
        {
            onChangeDataAfter: (data: ConfigBackground) => {
                onChange?.(data);
            },
        }
    );

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }
        return (
            <>
                <Collapse
                    header={
                        <>
                            <Desktop size={15} />
                            {_t('Background Desktop')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'bg'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'bg' : '');
                    }}
                >
                    <InputSelectBackground
                        title={null}
                        onChange={onChangeData('bg')}
                        defaultValue={data.bg}
                    />

                    <div className={classNameContentSw}>
                        <Text styleTemplate={styleTemplateTitleBg}>
                            {_t('Use Layer')}
                        </Text>
                        <InputCheckbox
                            styleTemplate={styleTemplateCheckbox}
                            defaultValue={data.useLayer}
                            onChange={onChangeData('useLayer')}
                        />
                    </div>
                    <Space size={20} />
                    {data.useLayer ? (
                        <>
                            <InputSelectBackground
                                title={
                                    <>
                                        <DesktopLayer size={15} />
                                        {_t('Background Layer Desktop')}
                                    </>
                                }
                                onChange={onChangeData('bgLayer')}
                                defaultValue={data.bgLayer}
                            />
                        </>
                    ) : (
                        <></>
                    )}
                </Collapse>

                <Collapse
                    header={
                        <>
                            <Movil size={15} />
                            {_t('Background Movil')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'bgMovil'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'bgMovil' : '');
                    }}
                >
                    <InputSelectBackground
                        title={null}
                        onChange={onChangeData('bgMovil')}
                        defaultValue={data.bgMovil}
                    />

                    <div className={classNameContentSw}>
                        <Text styleTemplate={styleTemplateTitleBg}>
                            {_t('Use Layer')}
                        </Text>
                        <InputCheckbox
                            styleTemplate={styleTemplateCheckbox}
                            defaultValue={data.useLayerMovil}
                            onChange={onChangeData('useLayerMovil')}
                        />
                    </div>
                    <Space size={20} />
                    {data.useLayerMovil ? (
                        <>
                            <InputSelectBackground
                                title={
                                    <>
                                        <MovilLayer size={15} />
                                        {_t('Background Layer Movil')}
                                    </>
                                }
                                onChange={onChangeData('bgMovilLayer')}
                                defaultValue={data.bgMovilLayer}
                            />
                        </>
                    ) : (
                        <></>
                    )}
                </Collapse>
            </>
        );
    }, [data, isActive, loader, _t]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };
    return (
        <>
            <Popup
                btn={_t('Background')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigBackgroundBase;
