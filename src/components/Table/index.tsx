import { useMemo } from 'react';

import * as styles from '@/components/Table/styles';

import { Theme, ThemesType } from '@/config/theme';

import { TableBaseProps, TableBase } from '@/components/Table/Base';

export const TableStyle = { ...styles } as const;

export type TableStyles = keyof typeof TableStyle;

export interface TableProps<T> extends TableBaseProps<T> {
    styleTemplate?: TableStyles | ThemesType;
}

export const Table = <T,>({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: TableProps<T>) => {
    const Style = useMemo(
        () => TableStyle[styleTemplate as TableStyles] ?? TableStyle._default,
        [styleTemplate]
    );

    return <TableBase {...Style} {...props} />;
};
export default Table;
