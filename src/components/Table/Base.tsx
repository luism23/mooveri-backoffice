import { useMemo } from 'react';

import {
    Pagination,
    PaginationProps,
    PaginationStyles,
} from '@/components/Pagination';
import { LoaderTable } from '@/components/Loader/LoaderTable';
import Theme, { ThemesType } from '@/config/theme';
import { useLang } from '@/lang/translate';

export interface TableClassProps {
    classNameContent?: string;

    classNameContentTable?: string;
    classNameTable?: string;
    classNameTHead?: string;
    classNameTBody?: string;
    classNameThr?: string;
    classNameTr?: string;
    classNameTh?: string;
    classNameTd?: string;

    classNameContentPagination?: string;
    styleTemplatePagination?: PaginationStyles | ThemesType;
}

export type TableHeader<T> = {
    id: keyof T;
    th: string;
    parse?: (data: T) => any;
}[];

export interface TableBaseProps<T> {
    items: T[];
    header: TableHeader<T>;
    pagination?: PaginationProps;
    loader?: boolean;
}

export interface TableProps<T> extends TableClassProps, TableBaseProps<T> {}

export const TableBase = <T,>({
    classNameContent = '',

    classNameContentTable = '',
    classNameTable = '',
    classNameTHead = '',
    classNameTBody = '',
    classNameThr = '',
    classNameTr = '',
    classNameTh = '',
    classNameTd = '',

    classNameContentPagination = '',
    styleTemplatePagination = Theme.styleTemplate ?? '_default',

    items,
    header,

    pagination,
    loader = false,
}: TableProps<T>) => {
    const _t = useLang();
    const CONTENT = useMemo(() => {
        if (loader) {
            return (
                <tr>
                    <td colSpan={header.length}>
                        <LoaderTable />
                    </td>
                </tr>
            );
        }
        if (items.length == 0) {
            return (
                <tr className={classNameTr}>
                    <td className={classNameTd} colSpan={header.length}>
                        <div>{_t('There is not results')}</div>
                    </td>
                </tr>
            );
        }
        return items.map((item, i) => (
            <tr key={i} className={classNameTr}>
                {header.map((h, j) => (
                    <td key={`${i}-${j}`} className={classNameTd}>
                        {h?.parse?.(item) ?? item[h.id] ?? ''}
                    </td>
                ))}
            </tr>
        ));
    }, [items, header, loader, classNameTr, classNameTd]);

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentTable}>
                    <table className={classNameTable}>
                        <thead className={classNameTHead}>
                            <tr className={classNameThr}>
                                {header.map((h, i) => (
                                    <th key={i} className={classNameTh}>
                                        {h.th}
                                    </th>
                                ))}
                            </tr>
                        </thead>
                        <tbody className={classNameTBody}>{CONTENT}</tbody>
                    </table>
                </div>
                {pagination && (
                    <div className={classNameContentPagination}>
                        <Pagination
                            styleTemplate={styleTemplatePagination}
                            {...pagination}
                            disabled={loader}
                        />
                    </div>
                )}
            </div>
        </>
    );
};
export default TableBase;
