import { TableClassProps } from '@/components/Table/Base';

export const mooveri: TableClassProps = {
    classNameContent: `

    `,
    classNameContentTable: `
        p-b-20  
    `,
    classNameTable: `
        border-1
        border-style-solid   
        border-collapse-collapse
        color-white
        bg-independence
        width-p-100
    `,
    classNameTBody: `
        border-1
        border-style-solid   
    `,
    classNameTr: `
        bg-dark-jungle-green-hover  
    `,
    classNameTh: `
        border-1
        border-style-solid   
        p-v-5 p-h-15
    `,
    classNameTd: `
        border-1
        border-style-solid    
        p-v-5 p-h-15
    `,
    classNameContentPagination: `
        flex
        flex-justify-right  
    `,
    styleTemplatePagination: 'mooveri',
};
