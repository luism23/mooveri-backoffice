import { Table, TableProps } from '@/components/Table';
import { TableHeader } from '@/components/Table/Base';
import url from '@/data/routes';
import { parseStatusUser } from '@/functions/parseStatusUser';
import { UserProps } from '@/interfaces/User';

export interface TableItemUserProps extends UserProps {}

export const TableHeaderUser: TableHeader<TableItemUserProps> = [
    {
        id: 'id',
        th: 'ID',
        parse: (data: TableItemUserProps) => {
            return (
                <a
                    href={`${url.users.single}${data.id}`}
                    className={`color-currentColor`}
                >
                    {data.id}
                </a>
            );
        },
    },
    {
        id: 'name',
        th: 'Name',
        parse: (data: TableItemUserProps) => {
            return (
                <a
                    href={`${url.users.single}${data.id}`}
                    className={`color-currentColor`}
                >
                    {data.name}
                </a>
            );
        },
    },
    {
        id: 'phone',
        th: 'Phone',
        parse: (data: TableItemUserProps) => {
            return (
                <a href={`tel:${data.phone}`} className={`color-currentColor`}>
                    {data.phone}
                </a>
            );
        },
    },
    {
        id: 'email',
        th: 'Email',
        parse: (data: TableItemUserProps) => {
            return (
                <a
                    href={`mailto:${data.email}`}
                    className={`color-currentColor`}
                >
                    {data.email}
                </a>
            );
        },
    },
    {
        id: 'status',
        th: 'Status',
        parse: (data: TableItemUserProps) => {
            return parseStatusUser(data.status ?? 'no-verify');
        },
    },
    {
        id: 'dateCreate',
        th: 'Date',
        parse: (data: TableItemUserProps) => {
            return data?.dateCreate?.toDateString();
        },
    },
];

export interface TableUserProps
    extends Omit<TableProps<TableItemUserProps>, 'header'> {
    header?: null;
}

export const TableUser = ({ ...props }: TableUserProps) => {
    return <Table {...props} header={TableHeaderUser} />;
};
export default TableUser;
