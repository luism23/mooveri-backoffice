import { Table, TableProps } from '@/components/Table';
import { TableHeader } from '@/components/Table/Base';
import money from '@/functions/money';
import { printStringArray } from '@/functions/printStringArray';
import { CompanyImportProps } from '@/interfaces/Company';

export interface TableItemCompanyImportProps extends CompanyImportProps {}

export const TableHeaderCompanyImport: TableHeader<TableItemCompanyImportProps> =
    [
        {
            id: 'first_name',
            th: 'first_name',
        },
        {
            id: 'last_name',
            th: 'last_name',
        },
        {
            id: 'phone',
            th: 'phone',
        },
        {
            id: 'email',
            th: 'email',
        },
        {
            id: 'password',
            th: 'password',
        },
        {
            id: 'imagen',
            th: 'imagen',
            parse: (data: TableItemCompanyImportProps) => {
                return (
                    <img
                        src={data.imagen}
                        className={`width-50 aspect-ratio-1-1`}
                    />
                );
            },
        },
        {
            id: 'company_name',
            th: 'company_name',
        },
        {
            id: 'company_legal_name',
            th: 'company_legal_name',
        },
        {
            id: 'company_phone_1',
            th: 'company_phone_1',
        },
        {
            id: 'company_city',
            th: 'company_city',
        },
        {
            id: 'company_description',
            th: 'company_description',
        },
        {
            id: 'company_state',
            th: 'company_state',
        },
        {
            id: 'company_zip_code',
            th: 'company_zip_code',
        },
        {
            id: 'company_ssn',
            th: 'company_ssn',
        },
        {
            id: 'company_ein',
            th: 'company_ein',
        },
        {
            id: 'company_imagen',
            th: 'company_imagen',
            parse: (data: TableItemCompanyImportProps) => {
                return (
                    <img
                        src={data.company_imagen}
                        className={`width-50 aspect-ratio-1-1`}
                    />
                );
            },
        },
        {
            id: 'company_imagen_banner',
            th: 'company_imagen_banner',
            parse: (data: TableItemCompanyImportProps) => {
                return (
                    <img
                        src={data.company_imagen_banner}
                        className={`width-50 aspect-ratio-1-1`}
                    />
                );
            },
        },
        {
            id: 'company_contact_name',
            th: 'company_contact_name',
        },
        {
            id: 'company_year_founded',
            th: 'company_year_founded',
        },
        {
            id: 'company_facebook_page_link',
            th: 'company_facebook_page_link',
        },
        {
            id: 'company_instagram_page_link',
            th: 'company_instagram_page_link',
        },
        {
            id: 'company_linkedin_link',
            th: 'company_linkedin_link',
        },
        {
            id: 'company_website_link',
            th: 'company_website_link',
        },
        {
            id: 'company_trucks_have',
            th: 'company_trucks_have',
        },
        {
            id: 'company_workers_have',
            th: 'company_workers_have',
        },
        {
            id: 'company_value_overtime',
            th: 'company_value_overtime',
            parse: (data: TableItemCompanyImportProps) => {
                return money(parseFloat(`${data.company_value_overtime ?? 0}`));
            },
        },
        {
            id: 'company_value_standar',
            th: 'company_value_standar',
            parse: (data: TableItemCompanyImportProps) => {
                return money(parseFloat(`${data.company_value_standar ?? 0}`));
            },
        },
        {
            id: 'company_value_weekend_rate',
            th: 'company_value_weekend_rate',
            parse: (data: TableItemCompanyImportProps) => {
                return money(
                    parseFloat(`${data.company_value_weekend_rate ?? 0}`)
                );
            },
        },
        {
            id: 'company_horario_monday',
            th: 'company_horario_monday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_monday ?? '');
            },
        },
        {
            id: 'company_horario_tuesday',
            th: 'company_horario_tuesday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_tuesday ?? '');
            },
        },
        {
            id: 'company_horario_wednesday',
            th: 'company_horario_wednesday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_wednesday ?? '');
            },
        },
        {
            id: 'company_horario_thursday',
            th: 'company_horario_thursday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_thursday ?? '');
            },
        },
        {
            id: 'company_horario_friday',
            th: 'company_horario_friday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_friday ?? '');
            },
        },
        {
            id: 'company_horario_saturday',
            th: 'company_horario_saturday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_saturday ?? '');
            },
        },
        {
            id: 'company_horario_sunday',
            th: 'company_horario_sunday',
            parse: (data: TableItemCompanyImportProps) => {
                return printStringArray(data.company_horario_sunday ?? '');
            },
        },
    ];

export interface TableCompanyImportProps
    extends Omit<TableProps<TableItemCompanyImportProps>, 'header'> {
    header?: null;
}

export const TableCompanyImport = ({ ...props }: TableCompanyImportProps) => {
    return <Table {...props} header={TableHeaderCompanyImport} />;
};
export default TableCompanyImport;
