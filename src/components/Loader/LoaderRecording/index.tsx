import styled from 'styled-components';

export const LoaderRecordingAnimation = styled.span`
    position: relative;
    display: inline-flex;
    align-items: center;
    column-gap: 5px;
    .box {
        display: inline-block;
        width: 80px;
        height: 80px;
        background: currentColor;
        border-radius: 10px;
    }
    .capture {
        display: inline-block;
        width: 20px;
        height: 60px;
        border-radius: 5px;
        border: 10px solid transparent;
        border-left: 0;
        border-right: 20px solid currentColor;
    }
    &:before {
        content: '';
        position: absolute;
        left: calc(100% + 10px);
        top: 0px;
        height: 70px;
        width: 70px;
        background-image: linear-gradient(currentColor 45px, transparent 0),
            linear-gradient(currentColor 45px, transparent 0),
            linear-gradient(currentColor 45px, transparent 0);
        background-repeat: no-repeat;
        background-size: 30px 4px;
        background-position: 0px 11px, 8px 35px, 0px 60px;
        animation: lineDropping 0.75s linear infinite;
    }
    @keyframes lineDropping {
        0% {
            background-position: 100px 11px, 115px 35px, 105px 60px;
            opacity: 1;
        }
        50% {
            background-position: 0px 11px, 20px 35px, 5px 60px;
        }
        60% {
            background-position: -30px 11px, 0px 35px, -10px 60px;
        }
        75%,
        100% {
            background-position: -30px 11px, -30px 35px, -30px 60px;
            opacity: 0;
        }
    }
`;
export const LoaderRecording = () => (
    <LoaderRecordingAnimation>
        <span className="box"></span>
        <span className="capture"></span>
    </LoaderRecordingAnimation>
);
