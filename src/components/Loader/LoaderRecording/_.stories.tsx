import { Story, Meta } from "@storybook/react";

import { LoaderRecording } from "./index";

export default {
    title: "Loader/LoaderRecording",
    component: LoaderRecording,
} as Meta;

const Template: Story = () => (
    <LoaderRecording />
);

export const Index = Template.bind({});
