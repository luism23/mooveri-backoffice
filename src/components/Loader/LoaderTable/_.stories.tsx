import { Story, Meta } from "@storybook/react";

import { LoaderTableProps, LoaderTable } from "./index";

export default {
    title: "Loader/LoaderTable",
    component: LoaderTable,
} as Meta;

const Template: Story<LoaderTableProps> = (args) => (
    <LoaderTable {...args}>Test Children</LoaderTable>
);

export const Index = Template.bind({});
Index.args = {};
