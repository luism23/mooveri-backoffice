import { Story, Meta } from "@storybook/react";

import { LoaderLineProps, LoaderLine } from "./index";

export default {
    title: "Loader/LoaderLine",
    component: LoaderLine,
} as Meta;

const Template: Story<LoaderLineProps> = (args) => (
    <LoaderLine {...args}>Test Children</LoaderLine>
);

export const Index = Template.bind({});
Index.args = {};
