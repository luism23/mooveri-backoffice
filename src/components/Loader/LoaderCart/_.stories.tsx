import { Story, Meta } from "@storybook/react";

import { LoaderCart } from "./index";

export default {
    title: "Loader/LoaderCart",
    component: LoaderCart,
} as Meta;

const Template: Story = (args) => (
    <LoaderCart {...args}>Test Children</LoaderCart>
);

export const Index = Template.bind({});
Index.args = {};
