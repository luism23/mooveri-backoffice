import CSS from 'csstype';
import { useMemo, PropsWithChildren } from 'react';

export interface ContentColClassProps {}

export interface ContentColBaseProps extends PropsWithChildren {
    size?: number;
    className?: string;
    columnGap?: number;
    rowGap?: number;
}

export interface ContentColProps
    extends ContentColClassProps,
        ContentColBaseProps {}

export const ContentColBase = ({
    size = 10000000,
    className = '',
    columnGap = 0,
    rowGap = 0,
    children,
}: ContentColProps) => {
    const style: CSS.Properties = useMemo(() => {
        const s: CSS.Properties = {
            display: 'grid',
            gridTemplateColumns: `repeat(auto-fit, minmax(${
                size / 16
            }rem,1fr))`,
            columnGap: `${columnGap / 16}rem`,
            rowGap: `${rowGap / 16}rem`,
        };
        return s;
    }, [size]);

    return (
        <>
            <div className={`ContentColBase ${className}`} style={style}>
                {children}
            </div>
        </>
    );
};
export default ContentColBase;
