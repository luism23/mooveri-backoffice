import { Story, Meta } from "@storybook/react";

import { ContentColProps, ContentCol } from "./index";

export default {
    title: "Content/ContentCol",
    component: ContentCol,
} as Meta;

const ContentColIndex: Story<ContentColProps> = (args) => (
    <ContentCol {...args}/>
);

export const Index = ContentColIndex.bind({});
Index.args = {
    size:200,
    columnGap:20,
    rowGap:10,
    children: (
        <>
            <div className="bg-red">col1</div>
            <div className="bg-blue">col2</div>
            <div className="bg-green">col3</div>
            <div className="bg-yellow">col4</div>
        </>
    ),
};
