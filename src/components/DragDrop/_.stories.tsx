import { Story, Meta } from "@storybook/react";

import { Button,ButtonProps} from "@/components/Button";

import { DragDropProps, DragDrop } from "./index";

export default {
    title: "DragDrop/DragDrop",
    component: DragDrop,
} as Meta;

const DragDropIndex: Story<DragDropProps<ButtonProps>> = (args) => (
    <DragDrop {...args}>Test Children</DragDrop>
);

export const Index = DragDropIndex.bind({});
Index.args = {
    component:Button,
    items:[
        {
            id:1,
            data:{
                icon:"test1",
                styleTemplate:"tolinkme"
            }
        },
        {
            id:2,
            data:{
                icon:"test2",
                styleTemplate:"tolinkme"
            }
        },
    ]
};
