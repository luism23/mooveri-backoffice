import React, { useEffect, useState } from 'react';
import { ReactSortable } from 'react-sortablejs';

export interface DragDropClassProps {}

export interface DragDropItemProps<T> {
    id: number;
    data: T | any;
}

export interface DragDropBaseProps<T> {
    id: string;
    component: any;
    items: DragDropItemProps<T>[];

    onChangeState?: (data: T[]) => void;

    classNameContent?: string;
    classNameItem?: string;
    classNameHandle?: string;
}

export interface DragDropProps<T>
    extends DragDropClassProps,
        DragDropBaseProps<T> {}

export const DragDropBase = <T,>({
    id,
    items,
    component,

    classNameContent = '',
    classNameItem = '',
    classNameHandle = 'move',

    ...props
}: DragDropProps<T>) => {
    const [state, setState] = useState<DragDropItemProps<T>[]>(items);
    const CustomTag = component;
    useEffect(() => {
        props?.onChangeState?.(state.map((s) => s.data));
    }, [state]);

    return (
        <>
            <ReactSortable
                id={id + items.length}
                list={state}
                setList={setState}
                className={classNameContent}
                handle={`.${classNameHandle}`}
                group={id}
                ghostClass="ReactSortable-ghost"
                chosenClass={'ReactSortable-chosen'}
                dragClass={'ReactSortable-drag'}
                swapClass="ReactSortable-swap"
            >
                {state.map((item, i) => {
                    if (item.data.delete) {
                        return <></>;
                    }
                    return (
                        <div
                            key={item.id + id + items.length}
                            id={item.id + id + items.length}
                            className={classNameItem + ' ' + i}
                        >
                            <CustomTag
                                {...item.data}
                                onDelete={(d: any) => {
                                    item?.data?.onDelete?.(d);
                                    // setState((pre) => [
                                    //     ...pre.filter((s, j) => j != i),
                                    // ]);
                                }}
                            />
                        </div>
                    );
                })}
            </ReactSortable>
        </>
    );
};
export default DragDropBase;
