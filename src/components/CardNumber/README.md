# CardNumber

## Dependencies

[CardNumber](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/CardNumber)

```js
import { CardNumber } from '@/components/CardNumber';
```

## Import

```js
import { CardNumber, CardNumberStyles } from '@/components/CardNumber';
```

## Props

```tsx
interface CardNumberProps {}
```

## Use

```js
<CardNumber>CardNumber</CardNumber>
```
