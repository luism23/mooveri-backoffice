import { CardNumberClassProps } from '@/components/CardNumber/Base';

export const mooveri: CardNumberClassProps = {
    classNameContent: `
        
    `,
    TitleProps: {
        className: `
            m-b-6    
        `,
        styleTemplate: 'mooveri6',
    },
    classNameContentCard: `
        flex
        flex-align-center
        flex-gap-column-5
    `,
    classNameContentImg: `
        width-25
        flex
        flex-align-center
    `,
    classNameImg: `

    `,
    classNameContentNumber: `
        m-r-auto
    `,
    NumberProps: {
        styleTemplate: 'mooveri7',
    },
    classNameContentDelete: `
        flex flex-gap-column-5
        color-peachyPink
        color-grayHexa-hover
        font-circular
        font-11 font-sm-14
        font-w-900
        pointer
        cancelEditField  
        flex-nowrap
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
        text-center
        m-h-auto 
    `,
};
