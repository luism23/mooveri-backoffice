import { useMemo } from 'react';

import * as styles from '@/components/CardNumber/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CardNumberBaseProps,
    CardNumberBase,
} from '@/components/CardNumber/Base';

export const CardNumberStyle = { ...styles } as const;

export type CardNumberStyles = keyof typeof CardNumberStyle;

export interface CardNumberProps extends CardNumberBaseProps {
    styleTemplate?: CardNumberStyles | ThemesType;
}

export const CardNumber = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CardNumberProps) => {
    const Style = useMemo(
        () =>
            CardNumberStyle[styleTemplate as CardNumberStyles] ??
            CardNumberStyle._default,
        [styleTemplate]
    );

    return <CardNumberBase {...Style} {...props} />;
};
export default CardNumber;
