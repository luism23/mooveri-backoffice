import { CardProps, onDeleteCardType } from '@/interfaces/Card';
import { Text, TextProps } from '@/components/Text';
import { useLang } from '@/lang/translate';
import Image from '@/components/Image';
import Cancel from '@/svg/cancel';
import { SubmitResult } from '../Form/Base';
import { useState } from 'react';
import { useNotification } from '@/hook/useNotification';
import { useUser } from '@/hook/useUser';

export interface CardNumberClassProps {
    classNameContent?: string;
    TitleProps?: TextProps;
    classNameContentCard?: string;
    classNameContentImg?: string;
    classNameImg?: string;
    classNameContentNumber?: string;
    NumberProps?: TextProps;
    classNameContentDelete?: string;
    classNameLoader?: string;
}

export interface CardNumberBaseProps extends CardProps {
    id?: string;
    onDelete?: onDeleteCardType;
    onDeleteOk?: () => void;
}

export interface CardNumberProps
    extends CardNumberClassProps,
        CardNumberBaseProps {}

export const CardNumberBase = ({
    classNameContent = '',
    TitleProps = {},
    classNameContentCard = '',
    classNameContentImg = '',
    classNameImg = '',
    classNameContentNumber = '',
    NumberProps = {},
    classNameContentDelete = '',
    classNameLoader = '',

    id,
    type,
    number,
    ...props
}: CardNumberProps) => {
    const _t = useLang();
    const { user } = useUser();
    const { pop } = useNotification();
    const [loader, setLoader] = useState(false);

    const onDelete = async () => {
        setLoader(true);

        try {
            const result: SubmitResult | undefined = await props?.onDelete?.({
                id,
                user,
            });
            if (result) {
                pop({
                    type: result.status,
                    message: result.message ?? '',
                });
                props?.onDeleteOk?.();
            } else {
                throw {
                    message: 'onDelete undefined',
                };
            }
        } catch (error: any) {
            pop({
                type: 'error',
                message: `${error.message ?? error}`,
            });
        }

        setLoader(false);
    };

    return (
        <div className={classNameContent}>
            <Text {...TitleProps}>{_t('Card Number')}</Text>
            <div className={classNameContentCard}>
                <div className={classNameContentImg}>
                    <Image
                        styleTemplate="_default"
                        src={`card-type/${type}.png`}
                        className={classNameImg}
                    />
                </div>
                <div className={classNameContentNumber}>
                    <Text {...NumberProps}>{number}</Text>
                </div>
                {loader ? (
                    <>
                        <div className={classNameLoader} />
                    </>
                ) : (
                    <>
                        <div
                            className={classNameContentDelete}
                            onClick={onDelete}
                        >
                            <Cancel size={8} />
                            {_t('Delete')}
                        </div>
                    </>
                )}
            </div>
        </div>
    );
};
export default CardNumberBase;
