import { Story, Meta } from "@storybook/react";

import { InformationSubmittedProps, InformationSubmitted } from "./index";

export default {
    title: "InformationSubmitted/InformationSubmitted",
    component: InformationSubmitted,
} as Meta;

const InformationSubmittedIndex: Story<InformationSubmittedProps> = (args) => (
    <InformationSubmitted {...args}>Test Children</InformationSubmitted>
);

export const Index = InformationSubmittedIndex.bind({});
Index.args = {
    number:1,
    text:"Tell us about your activity"
};
