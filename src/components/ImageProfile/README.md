# ImageProfile

## Dependencies

[ImageProfile](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ImageProfile)

```js
import { ImageProfile } from '@/components/ImageProfile';
```

## Import

```js
import { ImageProfile, ImageProfileStyles } from '@/components/ImageProfile';
```

## Props

```tsx
interface ImageProfileProps {}
```

## Use

```js
<ImageProfile>ImageProfile</ImageProfile>
```
