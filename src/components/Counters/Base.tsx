import money from '@/functions/money';
import { numberCount } from '@/functions/numberCount';
import Link from 'next/link';
import { useMemo } from 'react';

export interface CounterProps {
    title: any;
    count: number;
    money?: boolean;
    link?: string;
}

export interface CountersClassProps {
    classNameContent?: string;
    classNameContentCounter?: string;
    classNameContentTitle?: string;
    classNameContentCount?: string;
    classNameLink?: string;
}

export interface CountersBaseProps {
    counters?: CounterProps[];
}

export interface CountersProps extends CountersClassProps, CountersBaseProps {}

export const CountersBase = ({
    classNameContent = '',
    classNameContentCounter = '',
    classNameContentTitle = '',
    classNameContentCount = '',
    classNameLink = '',

    counters = [],
}: CountersProps) => {
    const Counters = useMemo(
        () =>
            counters.map((counter, i) => {
                const C = (
                    <div className={classNameContentCounter} key={i}>
                        <div className={classNameContentTitle}>
                            {counter.title}
                        </div>
                        <div className={classNameContentCount}>
                            {counter.money
                                ? money(counter.count)
                                : numberCount(counter.count)}
                        </div>
                    </div>
                );

                if (counter?.link) {
                    return (
                        <Link href={counter.link} key={i}>
                            <a className={classNameLink}>{C}</a>
                        </Link>
                    );
                }
                return C;
            }),
        [counters]
    );

    return (
        <>
            <div className={classNameContent}>{Counters}</div>
        </>
    );
};
export default CountersBase;
