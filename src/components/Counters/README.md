# Counters

## Dependencies

[Counters](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Counters)

```js
import { Counters } from '@/components/Counters';
```

## Import

```js
import { Counters, CountersStyles } from '@/components/Counters';
```

## Props

```tsx
interface CountersProps {}
```

## Use

```js
<Counters>Counters</Counters>
```
