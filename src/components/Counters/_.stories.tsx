import { Story, Meta } from "@storybook/react";

import { CountersProps, Counters } from "./index";

export default {
    title: "Counters/Counters",
    component: Counters,
} as Meta;

const CountersIndex: Story<CountersProps> = (args) => (
    <Counters {...args}>Test Children</Counters>
);

export const Index = CountersIndex.bind({});
Index.args = {
    counters: [
        {
            title: "Title",
            count: 2321245,
        },
        {
            title: "Title",
            count: 2321245,
        },
        {
            title: "Money",
            count: 2321245,
            money: true,
        },
        {
            title: "Link",
            count: 2321245,
            link:"/"
        },
    ],
};
