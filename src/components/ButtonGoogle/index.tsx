import { useMemo } from 'react';

import * as styles from '@/components/ButtonGoogle/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonGoogleBaseProps,
    ButtonGoogleBase,
} from '@/components/ButtonGoogle/Base';

export const ButtonGoogleStyle = { ...styles } as const;

export type ButtonGoogleStyles = keyof typeof ButtonGoogleStyle;

export interface ButtonGoogleProps extends ButtonGoogleBaseProps {
    styleTemplate?: ButtonGoogleStyles | ThemesType;
}

export const ButtonGoogle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    children,
    ...props
}: ButtonGoogleProps) => {
    const Style = useMemo(
        () =>
            ButtonGoogleStyle[styleTemplate as ButtonGoogleStyles] ??
            ButtonGoogleStyle._default,
        [styleTemplate]
    );

    return (
        <ButtonGoogleBase {...Style} {...props}>
            {children}
            <div></div>
        </ButtonGoogleBase>
    );
};
export default ButtonGoogle;
