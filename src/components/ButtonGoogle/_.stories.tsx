import { Story, Meta } from "@storybook/react";

import { ButtonGoogleProps, ButtonGoogle } from "./index";

export default {
    title: "Button/ButtonGoogle",
    component: ButtonGoogle,
} as Meta;

const ButtonGoogleIndex: Story<ButtonGoogleProps> = (args) => (
    <ButtonGoogle {...args}>Test Children</ButtonGoogle>
);

export const Index = ButtonGoogleIndex.bind({});
Index.args = {};
