# ButtonGoogle

## Dependencies

[Button](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Button)

```js
import { ButtonGoogle } from '@/components/ButtonGoogle';
```

## Import

```js
import { ButtonGoogle, ButtonGoogleStyles } from '@/components/ButtonGoogle';
```

## Props

```tsx
interface ButtonGoogleProps {}
```

## Use

```js
<ButtonGoogle>ButtonGoogle</ButtonGoogle>
```
