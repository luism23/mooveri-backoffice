import Button, { ButtonStyles } from '@/components/Button';
import { ButtonBaseProps } from '@/components/Button/Base';
import Theme, { ThemesType } from '@/config/theme';
import { Google } from '@/svg/Google';
import { LoginGoogle } from '@/firebase';

export interface ButtonGoogleClassProps {
    styleTemplateBtn?: ButtonStyles | ThemesType;
    icon?: any;
    position?: 'left' | 'right';
}

export interface ButtonGoogleBaseProps extends ButtonBaseProps {}

export interface ButtonGoogleProps
    extends ButtonGoogleClassProps,
        ButtonGoogleBaseProps {}

export const ButtonGoogleBase = ({
    styleTemplateBtn = Theme.styleTemplate ?? '_default',
    icon = <Google size={25} />,
    position = 'left',
    ...props
}: ButtonGoogleProps) => {
    return (
        <>
            <Button
                {...props}
                styleTemplate={styleTemplateBtn}
                iconPosition={position}
                icon={icon}
                onClick={LoginGoogle}
            />
        </>
    );
};
export default ButtonGoogleBase;
