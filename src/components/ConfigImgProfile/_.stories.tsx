import { Story, Meta } from "@storybook/react";

import { ConfigImgProfileProps, ConfigImgProfile } from "./index";

export default {
    title: "ConfigImgProfile/ConfigImgProfile",
    component: ConfigImgProfile,
} as Meta;

const ConfigImgProfileIndex: Story<ConfigImgProfileProps> = (args) => (
    <ConfigImgProfile {...args}>Test Children</ConfigImgProfile>
);

export const Index = ConfigImgProfileIndex.bind({});
Index.args = {};
