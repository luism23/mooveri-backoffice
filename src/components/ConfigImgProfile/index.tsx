import { useMemo } from 'react';

import * as styles from '@/components/ConfigImgProfile/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigImgProfileBaseProps,
    ConfigImgProfileBase,
} from '@/components/ConfigImgProfile/Base';

export const ConfigImgProfileStyle = { ...styles } as const;

export type ConfigImgProfileStyles = keyof typeof ConfigImgProfileStyle;

export interface ConfigImgProfileProps extends ConfigImgProfileBaseProps {
    styleTemplate?: ConfigImgProfileStyles | ThemesType;
}

export const ConfigImgProfile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigImgProfileProps) => {
    const Style = useMemo(
        () =>
            ConfigImgProfileStyle[styleTemplate as ConfigImgProfileStyles] ??
            ConfigImgProfileStyle._default,
        [styleTemplate]
    );

    return <ConfigImgProfileBase {...Style} {...props} />;
};
export default ConfigImgProfile;
