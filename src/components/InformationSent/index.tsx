import { useMemo } from 'react';

import * as styles from '@/components/InformationSent/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InformationSentBaseProps,
    InformationSentBase,
} from '@/components/InformationSent/Base';

export const InformationSentStyle = { ...styles } as const;

export type InformationSentStyles = keyof typeof InformationSentStyle;

export interface InformationSentProps extends InformationSentBaseProps {
    styleTemplate?: InformationSentStyles | ThemesType;
}

export const InformationSent = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InformationSentProps) => {
    const Style = useMemo(
        () =>
            InformationSentStyle[styleTemplate as InformationSentStyles] ??
            InformationSentStyle._default,
        [styleTemplate]
    );

    return <InformationSentBase {...Style} {...props} />;
};
export default InformationSent;
