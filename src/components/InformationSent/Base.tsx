import EarringWatch from '@/svg/EarringWatch';
import Link, { LinkStyles } from '../Link';
import Theme, { ThemesType } from '@/config/theme';
import InformationSubmitted from '../InformationSubmitted';
import { useLang } from '@/lang/translate';
import ContentWidth from '../ContentWidth';
import { InformationSentStyles } from '.';
import { InformationSubmittedStatusType } from '@/interfaces/InformationSubmittedStatusType';

export interface InformationSentClassProps {
    classNameContent?: string;
    classNameContentLink?: string;
    classNameContentstepss?: string;
    styleTemplateLink?: LinkStyles | ThemesType;
    styleTemplateTitleStep?: InformationSentStyles | ThemesType;
}

export interface InformationSentBaseProps {
    setStep: (a: number) => void;
    setShowSteps: (a: boolean) => void;
    statusSteps: {
        step1: InformationSubmittedStatusType;
        step2: InformationSubmittedStatusType;
        step3: InformationSubmittedStatusType;
    };
}

export interface InformationSentProps
    extends InformationSentClassProps,
        InformationSentBaseProps {}

export const InformationSentBase = ({
    classNameContentstepss = '',
    classNameContentLink = '',
    classNameContent = '',
    styleTemplateLink = Theme?.styleTemplate ?? '_default',
    styleTemplateTitleStep = Theme?.styleTemplate ?? '_default',
    statusSteps,
    setStep,
    setShowSteps,
}: InformationSentProps) => {
    const _t = useLang();
    return (
        <>
            <ContentWidth className={classNameContentstepss}>
                <div className={classNameContentLink}>
                    <EarringWatch size={15} />
                    <Link styleTemplate={styleTemplateLink}>
                        {_t('Monetize Information submitted')}
                    </Link>
                </div>
                <div className={classNameContent}>
                    <InformationSubmitted
                        styleTemplate={styleTemplateTitleStep}
                        text={_t('Personal Details')}
                        number={1}
                        onClick={() => {
                            setStep(1);
                            setShowSteps(false);
                        }}
                        status={statusSteps.step1}
                    />
                </div>
                <div className={classNameContent}>
                    <InformationSubmitted
                        text={_t('Verify your Identity')}
                        number={2}
                        onClick={() => {
                            setStep(2);
                            setShowSteps(false);
                        }}
                        status={statusSteps.step2}
                    />
                </div>
                <div className={classNameContent}>
                    <InformationSubmitted
                        text={_t('Send Us a Selfie')}
                        number={3}
                        onClick={() => {
                            setStep(3);
                            setShowSteps(false);
                        }}
                        status={statusSteps.step3}
                    />
                </div>
            </ContentWidth>
        </>
    );
};
export default InformationSentBase;
