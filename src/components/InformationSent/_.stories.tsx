import { Story, Meta } from "@storybook/react";

import { InformationSentProps, InformationSent } from "./index";

export default {
    title: "InformationSent/InformationSent",
    component: InformationSent,
} as Meta;

const InformationSentIndex: Story<InformationSentProps> = (args) => (
    <InformationSent {...args}>Test Children</InformationSent>
);

export const Index = InformationSentIndex.bind({});
Index.args = {};
