# Title

## Import

```js
import { Title, TitleStyles } from '@/components/Title';
```

## Props

```tsx
interface TitleProps {
    styleTemplate?: TitleStyles;
    typeStyle?: TitleStylesType;
    type?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
    className?: string;
    children?: any;
}
```

## Use

```js
<Title>Title</Title>
```
