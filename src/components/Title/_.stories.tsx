import { Story, Meta } from "@storybook/react";

import { TitleProps, Title } from "./index";

export default {
    title: "Title/Title",
    component: Title,
} as Meta;

const Template: Story<TitleProps> = (args) => (
    <Title {...args}>Test Children</Title>
);

export const Index = Template.bind({});
Index.args = {};
