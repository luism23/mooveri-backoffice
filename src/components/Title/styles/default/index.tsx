export const h1 = `
    color-black
    font-25 font-montserrat font-w-700
`;
export const h11 = `
    color-black
    font-25 font-montserrat font-w-700
`;
export const h2 = `
    color-black
    font-22 font-montserrat font-w-700
`;
export const h3 = `
    color-black
    font-21 font-montserrat font-w-700
`;
export const h4 = `
    color-black
    font-20 font-montserrat font-w-600
`;
export const h5 = `
    color-black
    font-19 font-montserrat font-w-500
`;
export const h6 = `
    color-black
    font-18 font-montserrat font-w-400
`;
export const h7 = `
    font-10
    color-dark-jungle-green
    font-nunito
    font-w-400
`;
