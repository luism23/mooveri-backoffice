import { PropsWithChildren } from 'react';

import * as styles from '@/components/Title/styles';

import { Theme, ThemesType } from '@/config/theme';

export const TitleStyle = { ...styles } as const;

export type TitleStyles = keyof typeof TitleStyle;
export type TitleStylesType = keyof (typeof styles)[TitleStyles];

export interface TitleProps {
    styleTemplate?: TitleStyles | ThemesType;
    typeStyle?: TitleStylesType;
    type?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6';
    className?: string;
}
export const Title = ({
    type = 'h1',
    typeStyle = 'h1',
    styleTemplate = Theme?.styleTemplate ?? '_default',
    className = '',
    children,
}: PropsWithChildren<TitleProps>) => {
    const CustomTag = type;
    return (
        <CustomTag
            className={`title ${className} ${
                (TitleStyle[styleTemplate as TitleStyles] ??
                    TitleStyle._default)[typeStyle] ??
                TitleStyle._default[typeStyle] ??
                ''
            }`}
        >
            {children}
        </CustomTag>
    );
};
export default Title;
