import Theme, { ThemesType } from '@/config/theme';
import Text, { TextStyles } from '../Text';
import { useLang } from '@/lang/translate';
import Button, { ButtonStyles } from '../Button';
import Space from '../Space';
import ContentWidth from '../ContentWidth';
export interface MonetizeClassProps {
    classNameContent?: string;
    classNameContenttext?: string;
    styleTemplateTitle?: TextStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    classNametext?: string;
    classNametextEasy?: string;
    sizeButton?: number;
    classNameContentIcon?: string;
    classNameContentBtn?: string;
}

export interface MonetizeBaseProps {
    Next?: string;
    title?: string;
    text?: string;
    easy?: string;
    textBtn2?: string;
    icon?: any;
    Contitle?: boolean | null;
    Context?: boolean | null;
    ContentBtn2?: boolean | null;

    onClick?: (Next?: string) => void;
}

export interface MonetizeProps extends MonetizeClassProps, MonetizeBaseProps {}

export const MonetizeBase = ({
    Next,
    classNameContent = '',
    title = '',
    text = '',
    easy = '',
    textBtn2 = '',
    icon = null,
    Contitle = true,
    Context = true,
    ContentBtn2 = true,
    classNameContenttext = '',
    classNametext = '',
    classNametextEasy = '',
    sizeButton = 1,
    classNameContentIcon = '',
    classNameContentBtn = '',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    onClick,
}: MonetizeProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent}>
                {Contitle ? (
                    <>
                        <div className={classNameContenttext}>
                            <Text styleTemplate={styleTemplateTitle}>
                                {_t(title)}{' '}
                            </Text>
                        </div>
                    </>
                ) : (
                    <></>
                )}
                <div className={classNameContentIcon}>{icon}</div>
                {Context ? (
                    <>
                        <div
                            className={`${classNameContenttext} ${classNametext}`}
                        >
                            <Text styleTemplate={styleTemplateText}>
                                {_t(text)}
                                <Space size={1} />
                                <span className={classNametextEasy}>
                                    {_t(easy)}
                                </span>
                            </Text>
                        </div>
                    </>
                ) : (
                    <></>
                )}
                {ContentBtn2 ? (
                    <>
                        <ContentWidth
                            className={classNameContentBtn}
                            size={sizeButton}
                        >
                            <Button
                                onClick={() => {
                                    onClick?.(Next);
                                }}
                                styleTemplate={'tolinkme6'}
                            >
                                {_t(textBtn2)}
                            </Button>
                        </ContentWidth>
                    </>
                ) : (
                    <></>
                )}
            </div>
        </>
    );
};
export default MonetizeBase;
