# Monetize

## Dependencies

[Monetize](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Monetize)

```js
import { Monetize } from '@/components/Monetize';
```

## Import

```js
import { Monetize, MonetizeStyles } from '@/components/Monetize';
```

## Props

```tsx
interface MonetizeProps {}
```

## Use

```js
<Monetize>Monetize</Monetize>
```
