import { useMemo } from 'react';

import * as styles from '@/components/Monetize/styles';

import { Theme, ThemesType } from '@/config/theme';

import { MonetizeBaseProps, MonetizeBase } from '@/components/Monetize/Base';

export const MonetizeStyle = { ...styles } as const;

export type MonetizeStyles = keyof typeof MonetizeStyle;

export interface MonetizeProps extends MonetizeBaseProps {
    styleTemplate?: MonetizeStyles | ThemesType;
}

export const Monetize = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: MonetizeProps) => {
    const Style = useMemo(
        () =>
            MonetizeStyle[styleTemplate as MonetizeStyles] ??
            MonetizeStyle._default,
        [styleTemplate]
    );

    return <MonetizeBase {...Style} {...props} />;
};
export default Monetize;
