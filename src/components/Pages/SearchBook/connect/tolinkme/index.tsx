import { PageSearchBookConnectProps } from '@/components/Pages/SearchBook/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/SearchBook/content/tolinkme';

export const tolinkme: PageSearchBookConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
