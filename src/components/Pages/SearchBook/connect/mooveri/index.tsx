import { PageSearchBookConnectProps } from '@/components/Pages/SearchBook/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/SearchBook/content/mooveri';
import * as Ipapi from '@/api/ipapi';

export const mooveri: PageSearchBookConnectProps<MooveriProps> = {
    onLoadData: async () => {
        const ipInfo: Ipapi.IpInfo | null = await Ipapi.GET();
        return {
            ip: ipInfo?.ip,
        };
    },
    onLoadContent: (props) => {
        return <Mooveri {...props} styleTemplateSearchBook="mooveri" />;
    },
};

export const mooveri2: PageSearchBookConnectProps<MooveriProps> = {
    onLoadData: async () => {
        const ipInfo: Ipapi.IpInfo | null = await Ipapi.GET();
        return {
            ip: ipInfo?.ip,
        };
    },
    onLoadContent: (props) => {
        return <Mooveri {...props} styleTemplateSearchBook={'mooveri2'} />;
    },
};

export const mooveri3: PageSearchBookConnectProps<MooveriProps> = {
    onLoadData: async () => {
        const ipInfo: Ipapi.IpInfo | null = await Ipapi.GET();
        return {
            ip: ipInfo?.ip,
        };
    },
    onLoadContent: (props) => {
        return <Mooveri {...props} styleTemplateSearchBook={'mooveri3'} />;
    },
};

export const mooveriBackoffice: PageSearchBookConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
