import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageServicePoliciesProps, PageServicePolicies } from "./index";

export default {
    title: "Page/PageServicePolicies",
    component: PageServicePolicies,
} as Meta;

const Template: Story<PropsWithChildren<PageServicePoliciesProps>> = (args) => (
    <PageServicePolicies {...args}>Test Children</PageServicePolicies>
);

export const Index = Template.bind({});
Index.args = {
   
};
