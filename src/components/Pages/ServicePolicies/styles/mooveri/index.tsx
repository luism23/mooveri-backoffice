import { PageServicePoliciesClassProps } from '@/components/Pages/ServicePolicies/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageServicePoliciesClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Terms Conditions',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    ContentWidthProps: {
        size: 500,
        className: `
            text-center
            m-h-auto
        `,
    },
    TextProps: {
        styleTemplate: 'mooveri',
    },
};
export const mooveriBackoffice = mooveri;
