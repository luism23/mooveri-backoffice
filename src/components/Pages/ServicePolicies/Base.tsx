import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { Text, TextProps } from '@/components/Text';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

export interface PageServicePoliciesConnectProps extends RenderLoadPageProps {
    term?: string;
}

export interface PageServicePoliciesClassProps {
    render?: RenderLoaderProps;
    TextProps?: TextProps;
    ContentWidthProps?: ContentWidthProps;
}

export interface PageServicePoliciesBaseProps {}

export interface PageServicePoliciesProps
    extends PageServicePoliciesBaseProps,
        PageServicePoliciesClassProps,
        PageServicePoliciesConnectProps {}

export const PageServicePoliciesBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Terms Conditions',
    },

    TextProps = {},
    ContentWidthProps = {},

    term = '',
}: PageServicePoliciesProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <ContentWidth {...ContentWidthProps}>
                    <Text {...TextProps}>{term}</Text>
                </ContentWidth>
            </RenderLoader>
        </>
    );
};
export default PageServicePoliciesBase;
