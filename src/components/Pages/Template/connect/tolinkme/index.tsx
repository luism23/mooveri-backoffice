import { PageTemplateConnectProps } from '@/components/Pages/Template/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/Template/content/tolinkme';

export const tolinkme: PageTemplateConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
