import { PageTemplateConnectProps } from '@/components/Pages/Template/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/Template/content/default';

export const _default: PageTemplateConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
