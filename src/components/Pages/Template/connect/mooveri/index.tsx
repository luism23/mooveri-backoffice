import { PageTemplateConnectProps } from '@/components/Pages/Template/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Template/content/mooveri';

export const mooveri: PageTemplateConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageTemplateConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
