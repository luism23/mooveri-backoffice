import { useMemo } from 'react';

import * as styles from '@/components/Pages/NameCategory/styles';
import * as connect from '@/components/Pages/NameCategory/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageNameCategoryBaseProps,
    PageNameCategoryBase,
} from '@/components/Pages/NameCategory/Base';

export const PageNameCategoryStyle = { ...styles } as const;
export const PageNameCategoryConnect = { ...connect } as const;

export type PageNameCategoryStyles = keyof typeof PageNameCategoryStyle;
export type PageNameCategoryConnects = keyof typeof PageNameCategoryConnect;

export interface PageNameCategoryProps extends PageNameCategoryBaseProps {
    styleTemplate?: PageNameCategoryStyles | ThemesType;
    company?: boolean;
}

export const PageNameCategory = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageNameCategoryProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageNameCategoryStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);
    const Style = useMemo(
        () =>
            PageNameCategoryStyle[
                styleTemplateSelected as PageNameCategoryStyles
            ] ?? PageNameCategoryStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageNameCategoryConnect[
                styleTemplateSelected as PageNameCategoryConnects
            ] ?? PageNameCategoryConnect._default,
        [styleTemplateSelected]
    );

    return <PageNameCategoryBase {...Style} {...Connect} {...props} />;
};
export default PageNameCategory;
