import { onSubmintNameCategory } from '@/components/Form/NameCategory/Base';
import { PageNameCategoryConnectProps } from '@/components/Pages/NameCategory/Base';
import log from '@/functions/log';
import { DataNameCategory } from '@/interfaces/NameCategory';

const onSubmit: onSubmintNameCategory = async (data: DataNameCategory) => {
    log('DataNameCategory', data);
    return {
        status: 'ok',
        message: '_default',
    };
};

export const _default: PageNameCategoryConnectProps = {
    onSubmit,
};
