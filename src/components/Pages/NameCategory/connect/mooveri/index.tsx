import { onSubmintNameCategory } from '@/components/Form/NameCategory/Base';
import { PageNameCategoryConnectProps } from '@/components/Pages/NameCategory/Base';
import { DataNameCategory } from '@/interfaces/NameCategory';
import url from '@/data/routes';
import log from '@/functions/log';

const onSubmit: onSubmintNameCategory = async (data: DataNameCategory) => {
    log('DataNameCategory', data);
    return {
        status: 'ok',
        message: 'pending',
    };
};

export const mooveri: PageNameCategoryConnectProps = {
    onSubmit,
    urlRedirect: url.profile,
};
