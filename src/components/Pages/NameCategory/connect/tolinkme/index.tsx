import { onSubmintNameCategory } from '@/components/Form/NameCategory/Base';
import { PageNameCategoryConnectProps } from '@/components/Pages/NameCategory/Base';
import { DataNameCategory } from '@/interfaces/NameCategory';
import { NameCategory } from '@/api/tolinkme/nameCategory';
import url from '@/data/routes';

const onSubmit: onSubmintNameCategory = async (data: DataNameCategory) => {
    const result = await NameCategory(data);
    return result;
};

export const tolinkme: PageNameCategoryConnectProps = {
    onSubmit,
    urlRedirect: url.profile,
};
