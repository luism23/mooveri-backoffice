import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import FormNameCategory, {
    FormNameCategoryStyles,
} from '@/components/Form/NameCategory';
import { FormNameCategoryBaseProps } from '@/components/Form/NameCategory/Base';
import Theme, { ThemesType } from '@/config/theme';
import { useState } from 'react';

export interface PageNameCategoryConnectProps
    extends RenderLoadPageProps,
        FormNameCategoryBaseProps {}

export interface PageNameCategoryClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate: FormNameCategoryStyles | ThemesType;
    };
}

export interface PageNameCategoryBaseProps {}

export interface PageNameCategoryProps
    extends PageNameCategoryBaseProps,
        PageNameCategoryClassProps,
        PageNameCategoryConnectProps {}

export const PageNameCategoryBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Name Category',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageNameCategoryProps) => {
    const [token, setToken] = useState('');
    const onLoadPage = async ({ query }: RenderDataLoadPage) => {
        const token = query?.id;

        setToken(token);

        const r: LoadPageFunctionRespond = 'ok';
        return r;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={onLoadPage}>
                <FormNameCategory {...props} {...form} token={token} />
            </RenderLoader>
        </>
    );
};
export default PageNameCategoryBase;
