import { onLoadTransactionById } from '@/api/mooveri/backoffice/transactions';
import { PageTransactionDetalleConnectProps } from '@/components/Pages/TransactionDetalle/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/TransactionDetalle/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';
import { CompanyProps } from '@/interfaces/Company';
import { UserProps } from '@/interfaces/User';

export const mooveri: PageTransactionDetalleConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageTransactionDetalleConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const transactions = await onLoadTransactionById(
                props?.user,
                props?.query?.id
            );
            const propsComponent: MooveriBackofficeProps = {
                transacions: transactions,
                user: transactions.client as UserProps,
                companies: transactions.company as CompanyProps,
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
