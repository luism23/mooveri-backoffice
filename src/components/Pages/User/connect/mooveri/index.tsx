import { PageUserConnectProps } from '@/components/Pages/User/Base';
import {
    Mooveri as C,
    mooveriProps,
    MooveriBackoffice as Cb,
    mooveriBackofficeProps,
} from '@/components/Pages/User/content/mooveri';

import { onLoadUser } from '@/api/mooveri/backoffice/users';
import { RenderDataLoadPage } from '@/components/Render';
import { Months, MonthsType } from '@/data/components/Month';

export const mooveri: PageUserConnectProps<mooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};

export const mooveriBackoffice: PageUserConnectProps<mooveriBackofficeProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        const users = await onLoadUser(props?.user);

        const usersByMonth: any = {};

        Months.forEach((month: MonthsType, i) => {
            usersByMonth[month] =
                users?.filter((user) => user?.dateCreate?.getMonth() == i)
                    .length ?? 0;
        });

        const userByStatesEEUU: {
            [id: string]: number;
        } = {};

        users.forEach((user) => {
            const l = user?.address?.name_location ?? 'Without Address';
            userByStatesEEUU[l] = (userByStatesEEUU[l] ?? 0) + 1;
        });

        const propsComponent: mooveriBackofficeProps = {
            usersByMonth,
            userByStatesEEUU,
            users: users,
        };
        return propsComponent;
    },
    onLoadContent: (props: mooveriBackofficeProps) => {
        return <Cb {...props} />;
    },
};
