import { PageUserConnectProps } from '@/components/Pages/User/Base';
import { _default as C } from '@/components/Pages/User/content/default';

export interface _defaultDataProps {}

export const _default: PageUserConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
