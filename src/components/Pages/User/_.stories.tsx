import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageUserProps, PageUser } from "./index";

export default {
    title: "Page/PageUser",
    component: PageUser,
} as Meta;

const User: Story<PropsWithChildren<PageUserProps>> = (args) => (
    <PageUser {...args}>Test Children</PageUser>
);

export const Index = User.bind({});
Index.args = {
   
};
