import { FormLogin, FormLoginStyles } from '@/components/Form/Login';
import { LayoutLogin } from '@/layout/Login';
import RenderLoader, { RenderLoaderProps } from '@/components/Render';
import { FormLoginBaseProps } from '@/components/Form/Login/Base';
import Theme, { ThemesType } from '@/config/theme';

export interface PageLoginConnectProps extends FormLoginBaseProps {}

export interface PageLoginClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate?: FormLoginStyles | ThemesType;
    };
}

export interface PageLoginBaseProps {}

export interface PageLoginProps
    extends PageLoginBaseProps,
        PageLoginClassProps,
        PageLoginConnectProps {}

export const PageLoginBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Login',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageLoginProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <FormLogin {...props} {...form} />
            </RenderLoader>
        </>
    );
};
export default PageLoginBase;
