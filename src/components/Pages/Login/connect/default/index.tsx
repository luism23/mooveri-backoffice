import { PageLoginConnectProps } from '@/components/Pages/Login/Base';

import { onSubmintLogin } from '@/components/Form/Login/Base';
import { DataLogin } from '@/interfaces/Login';

import { Login as LoginApi } from '@/api/tolinkme/login';
import url from '@/data/routes';

const onSubmit: onSubmintLogin = async (data: DataLogin) => {
    const result = await LoginApi(data);
    return result;
};

export const _default: PageLoginConnectProps = {
    onSubmit,
    urlRedirect: url.profile,
};
