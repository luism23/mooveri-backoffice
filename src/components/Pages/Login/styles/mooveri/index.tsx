import { PageLoginClassProps } from '@/components/Pages/Login/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageLoginClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Login',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
};

export const mooveriCustomer: PageLoginClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Login',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageLoginClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Login',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    form: {
        styleTemplate: 'mooveriCompany',
    },
};

export const mooveriBackoffice: PageLoginClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Login',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
            showGoBack: false,
            showLogo: true,
            showLink: false,
        },
    },
    form: {
        styleTemplate: 'mooveriBackoffice',
    },
};
