import { PageHomeConnectProps } from '@/components/Pages/Home/Base';

import {
    Mooveri,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Home/content/mooveri';

import { onLoadCompany } from '@/api/mooveri/backoffice/companies';
import { onLoadUser } from '@/api/mooveri/backoffice/users';
import { RenderDataLoadPage } from '@/components/Render';
import { Months, MonthsType } from '@/data/components/Month';
import { onLoadTransaction } from '@/api/mooveri/backoffice/transactions';

export interface mooveriDataProps {}

export const mooveri: PageHomeConnectProps<mooveriDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: () => <Mooveri />,
    useLoadData: false,
};

export const mooveriBackoffice: PageHomeConnectProps<MooveriBackofficeProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        const users = await onLoadUser(props?.user);

        const usersByMonth: any = {};

        Months.forEach((month: MonthsType, i) => {
            usersByMonth[month] =
                users?.filter((user) => user?.dateCreate?.getMonth() == i)
                    .length ?? 0;
        });

        const companies = await onLoadCompany(props?.user);

        const companiesByMonths: any = {};

        Months.forEach((month: MonthsType, i) => {
            companiesByMonths[month] =
                companies?.filter(
                    (company) => company.dateCreate.getMonth() == i
                ).length ?? 0;
        });

        const transactions = await onLoadTransaction(props?.user);

        const transactionsByMonthsCount: any = {};
        const transactionsByMonthsTotal: any = {};
        let transactionsTotal = 0;
        let transactionsCount = 0;

        Months.forEach((month: MonthsType, i) => {
            const t = transactions?.filter(
                (transaction) => transaction.dateCreate.getMonth() == i
            );
            const total = t?.reduce((a, b) => a + b.price, 0);
            transactionsByMonthsCount[month] = t.length ?? 0;
            transactionsByMonthsTotal[month] = total;
            transactionsTotal += total;
            transactionsCount += t.length ?? 0;
        });
        const propsComponent: MooveriBackofficeProps = {
            counts: {
                companies: companies.length,
                clients: users.length,
                //TODO: pendiente transactions
                transactions: transactionsCount,
                total: transactionsTotal,
            },
            users: usersByMonth,
            companies: companiesByMonths,
            transactions: transactionsByMonthsCount,
            total: transactionsByMonthsTotal,
        };
        return propsComponent;
    },
    onLoadContent: (props: MooveriBackofficeProps) => (
        <MooveriBackoffice {...props} />
    ),
    useLoadData: true,
};
