import { PageHomeConnectProps } from '@/components/Pages/Home/Base';
import { _default as C } from '@/components/Pages/Home/content/default';

export interface _defaultDataProps {}

export const _default: PageHomeConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
