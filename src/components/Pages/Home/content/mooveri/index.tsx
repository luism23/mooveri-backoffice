import Counters from '@/components/Counters';
import { LineByMonth } from '@/components/Graf/Line/template/LineByMonth';
import Text from '@/components/Text';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import { useRouter } from 'next/router';
import { MonthsConst, MonthsType } from '@/data/components/Month';

export const Mooveri = () => {
    const router = useRouter();
    if (router.isReady) {
        router.push(url.myAccount.index);
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    counts: {
        companies: number;
        clients: number;
        transactions: number;
        total: number;
    };
    users: {
        [id in MonthsType]: number;
    };
    companies: {
        [id in MonthsType]: number;
    };
    transactions: {
        [id in MonthsType]: number;
    };
    total: {
        [id in MonthsType]: number;
    };
}

export const MooveriBackoffice = ({ ...props }: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <Counters
                counters={[
                    {
                        title: 'Companies',
                        count: props.counts.companies,
                        link: url.companies.index,
                    },
                    {
                        title: 'Clients',
                        count: props.counts.clients,
                        link: url.users.index,
                    },
                    {
                        title: 'Transactions',
                        count: props.counts.transactions,
                        link: url.transactions.index,
                    },
                    {
                        title: 'Total',
                        count: props.counts.total,
                        money: true,
                    },
                ]}
                styleTemplate="mooveri"
            />
            <div className="flex p-t-50 flex-nowrap flex-gap-40 flex-justify-between width-p-100">
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">{_t('Users')}</Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Users',
                                data: MonthsConst.map((m) => {
                                    return props?.users?.[m] ?? 0;
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">{_t('Companies')}</Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Companies',
                                data: MonthsConst.map((m) => {
                                    return props?.companies?.[m] ?? 0;
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
            </div>
            <div className="flex p-t-50 flex-nowrap flex-gap-40 flex-justify-between width-p-100">
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">{_t('Transactions')}</Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Transactions',
                                data: MonthsConst.map((m) => {
                                    return props?.transactions?.[m] ?? 0;
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
                <div className="flex-6 text-center">
                    <Text styleTemplate="mooveri8">{_t('Total')}</Text>
                    <LineByMonth
                        datasets={[
                            {
                                label: 'Total',
                                data: MonthsConst.map((m) => {
                                    return props?.total?.[m] ?? 0;
                                }),
                                borderColor: '#19a576',
                                backgroundColor: '#2cf6b3',
                            },
                        ]}
                        styleTemplate="mooveri"
                    />
                </div>
            </div>
        </>
    );
};
