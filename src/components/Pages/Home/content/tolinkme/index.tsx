import url from '@/data/routes';
import { useRouter } from 'next/router';

export const Tolinkme = () => {
    const router = useRouter();
    if (router.isReady) {
        router.push(url.profile);
    }
    return <></>;
};
