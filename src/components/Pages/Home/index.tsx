import { useMemo } from 'react';

import * as styles from '@/components/Pages/Home/styles';
import * as connect from '@/components/Pages/Home/connect';

import { Theme, ThemesType } from '@/config/theme';

import { PageHomeBaseProps, PageHomeBase } from '@/components/Pages/Home/Base';

export const PageHomeStyle = { ...styles } as const;
export const PageHomeConnect = { ...connect } as const;

export type PageHomeStyles = keyof typeof PageHomeStyle;
export type PageHomeConnects = keyof typeof PageHomeConnect;

export interface PageHomeProps extends PageHomeBaseProps {
    styleTemplate?: PageHomeStyles | ThemesType;
    company?: boolean;
}

export const PageHome = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageHomeProps) => {
    const Style = useMemo(
        () =>
            PageHomeStyle[styleTemplate as PageHomeStyles] ??
            PageHomeStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageHomeConnect[styleTemplate as PageHomeConnects] ??
            PageHomeConnect._default,
        [styleTemplate]
    );

    return <PageHomeBase<any> {...Style} {...Connect} {...props} />;
};
export default PageHome;
