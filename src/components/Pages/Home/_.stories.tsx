import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageHomeProps, PageHome } from "./index";

export default {
    title: "Page/PageHome",
    component: PageHome,
} as Meta;

const Home: Story<PropsWithChildren<PageHomeProps>> = (args) => (
    <PageHome {...args}>Test Children</PageHome>
);

export const Index = Home.bind({});
Index.args = {
   
};
