import { PageTransactionsConnectProps } from '@/components/Pages/Transactions/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Transactions/content/mooveri';
import { onLoadTransaction } from '@/api/mooveri/backoffice/transactions';
import { RenderDataLoadPage } from '@/components/Render';
import { Months, MonthsType } from '@/data/components/Month';

export const mooveri: PageTransactionsConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageTransactionsConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const transactions = await onLoadTransaction(props?.user);

            const transactionsByMonths: any = {};

            Months.forEach((month: MonthsType, i) => {
                const t = transactions?.filter(
                    (transaction) => transaction.dateCreate.getMonth() == i
                );
                transactionsByMonths[month] = {
                    total: t?.reduce((a, b) => a + b.price, 0),
                    count: t.length ?? 0,
                };
            });
            const propsComponent: MooveriBackofficeProps = {
                transactions,
                transactionsByMonths,
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
