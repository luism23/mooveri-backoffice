import { PageTransactionsConnectProps } from '@/components/Pages/Transactions/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/Transactions/content/tolinkme';

export const tolinkme: PageTransactionsConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
