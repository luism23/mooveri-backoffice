import { PageTransactionsConnectProps } from '@/components/Pages/Transactions/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/Transactions/content/default';

export const _default: PageTransactionsConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
