import { PageTransactionsClassProps } from '@/components/Pages/Transactions/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageTransactionsClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Transactions',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageTransactionsClassProps = {
    render: {
        Layout: Layout,
        title: 'Transactions',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
