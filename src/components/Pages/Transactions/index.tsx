import { useMemo } from 'react';

import * as styles from '@/components/Pages/Transactions/styles';
import * as connect from '@/components/Pages/Transactions/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageTransactionsBaseProps,
    PageTransactionsBase,
} from '@/components/Pages/Transactions/Base';

export const PageTransactionsStyle = { ...styles } as const;
export const PageTransactionsConnect = { ...connect } as const;

export type PageTransactionsStyles = keyof typeof PageTransactionsStyle;
export type PageTransactionsConnects = keyof typeof PageTransactionsConnect;

export interface PageTransactionsProps extends PageTransactionsBaseProps {
    styleTemplate?: PageTransactionsStyles | ThemesType;
    company?: boolean;
}

export const PageTransactions = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageTransactionsProps) => {
    const Style = useMemo(
        () =>
            PageTransactionsStyle[styleTemplate as PageTransactionsStyles] ??
            PageTransactionsStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageTransactionsConnect[
                styleTemplate as PageTransactionsConnects
            ] ?? PageTransactionsConnect._default,
        [styleTemplate]
    );

    return <PageTransactionsBase<any> {...Style} {...Connect} {...props} />;
};
export default PageTransactions;
