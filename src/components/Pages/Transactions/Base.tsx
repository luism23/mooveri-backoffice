import { LayoutBase as LayoutTransactions } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageTransactionsConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageTransactionsClassProps {
    render?: RenderLoaderProps;
}

export interface PageTransactionsBaseProps {}

export interface PageTransactionsProps<P = any>
    extends PageTransactionsBaseProps,
        PageTransactionsClassProps,
        PageTransactionsConnectProps<P> {}

export const PageTransactionsBase = <P,>({
    render = {
        Layout: LayoutTransactions,
        title: 'Transactions',
    },

    useLoadData = true,

    ...props
}: PageTransactionsProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageTransactionsBase;
