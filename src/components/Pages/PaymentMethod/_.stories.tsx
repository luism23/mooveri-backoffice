import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PagePaymentMethodProps, PagePaymentMethod } from "./index";

export default {
    title: "Page/PagePaymentMethod",
    component: PagePaymentMethod,
} as Meta;

const PaymentMethod: Story<PropsWithChildren<PagePaymentMethodProps>> = (args) => (
    <PagePaymentMethod {...args}>Test Children</PagePaymentMethod>
);

export const Index = PaymentMethod.bind({});
Index.args = {
   
};
