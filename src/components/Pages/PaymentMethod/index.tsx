import { useMemo } from 'react';

import * as styles from '@/components/Pages/PaymentMethod/styles';
import * as connect from '@/components/Pages/PaymentMethod/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PagePaymentMethodBaseProps,
    PagePaymentMethodBase,
} from '@/components/Pages/PaymentMethod/Base';

export const PagePaymentMethodStyle = { ...styles } as const;
export const PagePaymentMethodConnect = { ...connect } as const;

export type PagePaymentMethodStyles = keyof typeof PagePaymentMethodStyle;
export type PagePaymentMethodConnects = keyof typeof PagePaymentMethodConnect;

export interface PagePaymentMethodProps extends PagePaymentMethodBaseProps {
    styleTemplate?: PagePaymentMethodStyles | ThemesType;
    company?: boolean;
}

export const PagePaymentMethod = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PagePaymentMethodProps) => {
    const Style = useMemo(
        () =>
            PagePaymentMethodStyle[styleTemplate as PagePaymentMethodStyles] ??
            PagePaymentMethodStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PagePaymentMethodConnect[
                styleTemplate as PagePaymentMethodConnects
            ] ?? PagePaymentMethodConnect._default,
        [styleTemplate]
    );

    return <PagePaymentMethodBase<any> {...Style} {...Connect} {...props} />;
};
export default PagePaymentMethod;
