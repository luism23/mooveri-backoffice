import { PagePaymentMethodConnectProps } from '@/components/Pages/PaymentMethod/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/PaymentMethod/content/tolinkme';

export const tolinkme: PagePaymentMethodConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
