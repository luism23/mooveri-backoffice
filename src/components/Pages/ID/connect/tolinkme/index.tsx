import { PageIDConnectProps } from '@/components/Pages/ID/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/ID/content/tolinkme';
import { RenderDataLoadPage } from '@/components/Render';
import * as ProfileApi from '@/api/tolinkme/profile';
import * as AnalitycsApi from '@/api/tolinkme/analitycs';
import * as Ipapi from '@/api/ipapi';
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import { TolinkmeProps___ } from '@/interfaces/Id';
import * as ButtonMonetizeApi from '@/api/tolinkme/CustomerSuscriptions';
const onClickBtn =
    (ipInfo?: Ipapi.IpInfo | null, profile?: TolinkmeProps___) =>
    (button?: RSLinkConfigDataProps) => {
        AnalitycsApi.CLICKBTN({
            buttonUuid: button?.uuid,
            userAgent: window.navigator.userAgent,
            ip: ipInfo?.ip,
        });

        const BtnType = (
            ['custom'].includes(`${button?.rs}`)
                ? button?.customTitle
                : button?.rs
        )
            ?.toLowerCase()
            .replaceAll(' ', '_');

        const w: any = window;
        if (w?.dataLayer?.push) {
            w.dataLayer?.push?.({
                event: `btn-profile-click`,
                BtnType,
                ProfileUser: profile?.name,
                Country: ipInfo?.country,
                City: ipInfo?.city,
            });
        }
    };

const onHit = (ipInfo?: Ipapi.IpInfo | null) => (profile?: TolinkmeProps) => {
    AnalitycsApi.HITPROFILE({
        'user-agent': window.navigator.userAgent,
        ip: ipInfo?.ip,
        user_uuid: profile?.uuid,
    });

    const w: any = window;
    if (w?.dataLayer?.push) {
        w.dataLayer?.push?.({
            event: `load-profile-page`,
            ProfileUser: profile?.name,
            Country: ipInfo?.country,
            City: ipInfo?.city,
        });
    }
};

export const tolinkme: PageIDConnectProps<TolinkmeProps> = {
    onLoadData: async ({ query, user }: RenderDataLoadPage) => {
        const profileResult: '404' | TolinkmeProps =
            await ProfileApi.GET_PUBLIC({
                username: query?.id ?? '',
            });

        if (profileResult == '404') {
            throw {
                code: '404',
            };
        }
        const ipInfo: Ipapi.IpInfo | null = await Ipapi.GET();
        onHit(ipInfo)(profileResult);
        if (!user) {
            return {
                ...profileResult,
                ipInfo,
                userLinksBuy: [],
            };
        }

        const s = await ButtonMonetizeApi.GET_CUSTOMER_SUBCRIPTION({
            user,
        });
        let sList: string[] = [];
        if (s != 401) {
            sList =
                s.data?.btns?.map?.(
                    (e) => e.TransactionSubscriptionButtons?.Button?.uuid ?? ''
                ) ?? [];
        }

        const c = await ButtonMonetizeApi.GET_CUSTOMER_SHOPPING({
            user,
        });
        let cList: string[] = [];
        if (c != 401) {
            cList =
                c.data?.uniquerPurchase?.map?.((e) => e?.Button?.uuid ?? '') ??
                [];
        }

        return {
            ...profileResult,
            ipInfo,
            userLinksBuy: [...sList, ...cList],
        };
    },
    onLoadContent: (props) => {
        return <Tolinkme {...props} onClickBtn={onClickBtn} />;
    },
};
