import { PageIDConnectProps } from '@/components/Pages/ID/Base';
import { Defualt, DefualtProps } from '@/components/Pages/ID/content/default';

export const _default: PageIDConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
