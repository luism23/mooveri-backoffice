import { PageIDConnectProps } from '@/components/Pages/ID/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/ID/content/mooveri';

export const mooveri: PageIDConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageIDConnectProps<MooveriBackofficeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <MooveriBackoffice {...props} />;
    },
};
