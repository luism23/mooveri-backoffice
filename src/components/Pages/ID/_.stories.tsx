import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageIDProps, PageID } from "./index";

export default {
    title: "Page/PageID",
    component: PageID,
} as Meta;

const ID: Story<PropsWithChildren<PageIDProps>> = (args) => (
    <PageID {...args}>Test Children</PageID>
);

export const Index = ID.bind({});
Index.args = {
   
};
