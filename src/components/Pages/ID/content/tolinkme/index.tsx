import ProfilePublic from '@/components/Profile/Public';
import IsMore18 from '@/components/IsMore18';
import { RS_AdultoConst } from '@/data/components/RS';
import { idData } from '@/interfaces/Id';
import { PublicData } from '@/interfaces/PublicData';

export interface TolinkmeProps extends Omit<PublicData, 'onClickBtn'>, idData {}

export const Tolinkme = ({
    uuid,
    name,
    links,
    style,
    userLinksBuy,
    onClickBtn,
    ipInfo,
}: TolinkmeProps) => {
    const isMore18: string[] = [
        '😈',
        'exclusive content',
        'private content',
        'adult content',
        'sexy',
        'contenido exclusivo',
        'contenido privado',
        'contenido adulto',
        'only',
        'only fans',
    ];
    return (
        <>
            <IsMore18
                validate={links.some(
                    (e) =>
                        RS_AdultoConst?.find(
                            (r) =>
                                r.id == e.rs ||
                                r.id.toLowerCase() ==
                                    e.customTitle?.toLocaleLowerCase()
                        ) ||
                        isMore18.includes(
                            e.customTitle?.toLocaleLowerCase() ?? ''
                        )
                )}
            >
                <>
                    <ProfilePublic
                        links={links.map((link) => {
                            if (userLinksBuy?.includes(link.uuid ?? '')) {
                                return {
                                    ...link,
                                    monetize: false,
                                    monetizeData: undefined,
                                    useMonetize: false,
                                };
                            }
                            return {
                                ...link,
                            };
                        })}
                        style={style}
                        onClickBtn={onClickBtn?.(ipInfo, {
                            name,
                            uuid,
                            links,
                            style,
                        })}
                    />
                </>
            </IsMore18>
        </>
    );
};
