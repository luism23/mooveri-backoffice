import { useMemo } from 'react';

import * as styles from '@/components/Pages/ID/styles';
import * as connect from '@/components/Pages/ID/connect';

import { Theme, ThemesType } from '@/config/theme';

import { PageIDBaseProps, PageIDBase } from '@/components/Pages/ID/Base';

export const PageIDStyle = { ...styles } as const;
export const PageIDConnect = { ...connect } as const;

export type PageIDStyles = keyof typeof PageIDStyle;
export type PageIDConnects = keyof typeof PageIDConnect;

export interface PageIDProps extends PageIDBaseProps {
    styleTemplate?: PageIDStyles | ThemesType;
    company?: boolean;
}

export const PageID = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageIDProps) => {
    const Style = useMemo(
        () =>
            PageIDStyle[styleTemplate as PageIDStyles] ?? PageIDStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageIDConnect[styleTemplate as PageIDConnects] ??
            PageIDConnect._default,
        [styleTemplate]
    );

    return <PageIDBase<any> {...Style} {...Connect} {...props} />;
};
export default PageID;
