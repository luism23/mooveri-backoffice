import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageUserDetalleProps, PageUserDetalle } from "./index";

export default {
    title: "Page/PageUserDetalle",
    component: PageUserDetalle,
} as Meta;

const UserDetalle: Story<PropsWithChildren<PageUserDetalleProps>> = (args) => (
    <PageUserDetalle {...args}>Test Children</PageUserDetalle>
);

export const Index = UserDetalle.bind({});
Index.args = {
   
};
