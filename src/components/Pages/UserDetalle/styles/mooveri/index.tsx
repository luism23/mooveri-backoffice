import { PageUserDetalleClassProps } from '@/components/Pages/UserDetalle/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageUserDetalleClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'UserDetalle',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageUserDetalleClassProps = {
    render: {
        Layout: Layout,
        title: 'UserDetalle',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
