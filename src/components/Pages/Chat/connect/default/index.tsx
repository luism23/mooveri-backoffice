import { PageChatConnectProps } from '@/components/Pages/Chat/Base';
import { Defualt, DefualtProps } from '@/components/Pages/Chat/content/default';

export const _default: PageChatConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
