import { PageChatConnectProps } from '@/components/Pages/Chat/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Chat/content/mooveri';

export const mooveri: PageChatConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageChatConnectProps<MooveriBackofficeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <MooveriBackoffice {...props} />;
    },
};
