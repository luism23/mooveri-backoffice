import { PageChatClassProps } from '@/components/Pages/Chat/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageChatClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Chat',
        LayoutProps: {
            styleTemplate: 'mooveri',
            style: {
                padding: 0,
                width: '100%',
                maxWidth: '100%',
            },
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageChatClassProps = {
    render: {
        Layout: Layout,
        title: 'Chat',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
