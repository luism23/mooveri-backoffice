import { useMemo } from 'react';

import * as styles from '@/components/Pages/Chat/styles';
import * as connect from '@/components/Pages/Chat/connect';

import { Theme, ThemesType } from '@/config/theme';

import { PageChatBaseProps, PageChatBase } from '@/components/Pages/Chat/Base';

export const PageChatStyle = { ...styles } as const;
export const PageChatConnect = { ...connect } as const;

export type PageChatStyles = keyof typeof PageChatStyle;
export type PageChatConnects = keyof typeof PageChatConnect;

export interface PageChatProps extends PageChatBaseProps {
    styleTemplate?: PageChatStyles | ThemesType;
    company?: boolean;
}

export const PageChat = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageChatProps) => {
    const Style = useMemo(
        () =>
            PageChatStyle[styleTemplate as PageChatStyles] ??
            PageChatStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageChatConnect[styleTemplate as PageChatConnects] ??
            PageChatConnect._default,
        [styleTemplate]
    );

    return <PageChatBase<any> {...Style} {...Connect} {...props} />;
};
export default PageChat;
