import { useRouter } from 'next/router';

import { Chat } from '@/components/Chat';
import { ChatItemProps } from '@/components/Chat/Base';
import {
    DataTypeFormChat,
    onSubmintChatMesseges,
} from '@/components/Form/ChatMesseges/Base';
import { UserLoginProps } from '@/hook/useUser';
import { DataChatMesseges } from '@/interfaces/ChatMesseges';
import log from '@/functions/log';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const listChat: ChatItemProps[] = [
        {
            id: '1',
            user: 'User Name',
            img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/01.jpg',
            msj: 'Hola',
            statusMsj: 'send',
            date: new Date('1-1-1'),
            DateOfLastMsj: new Date(),
            messages: [
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                    my: true,
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                    my: true,
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                    my: true,
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                    my: true,
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                    my: true,
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                    my: true,
                },
                {
                    id: '1',
                    dateSend: new Date(),
                    dateView: new Date(),
                    content: 'Hola',
                },
            ],
        },
        {
            id: '2',
            user: 'Mooveri',
            img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/02.jpg',
            msj: 'Hola',
            statusMsj: 'send',
            date: new Date('1-1-1'),
            DateOfLastMsj: new Date(),
            messages: [],
        },
        {
            id: '3',
            user: 'Otra Empresa',
            img: 'https://www.aerocivil.gov.co/Style%20Library/CEA/img/03.jpg',
            msj: 'Hola',
            statusMsj: 'view',
            date: new Date('1-1-1'),
            DateOfLastMsj: new Date(),
            messages: [],
        },
    ];

    const sendMessage: onSubmintChatMesseges =
        (user: UserLoginProps) =>
        async (data: DataChatMesseges<DataTypeFormChat>) => {
            log(
                'send Message',
                {
                    user,
                    data,
                },
                'aqua'
            );
            return {
                status: 'ok',
            };
        };

    return (
        <>
            <Chat
                styleTemplate="mooveri"
                listChat={listChat}
                sendMessage={sendMessage}
            />
        </>
    );
};

export interface MooveriBackofficeProps {}

export const MooveriBackoffice = ({}: MooveriBackofficeProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
