import { useMemo } from 'react';

import * as styles from '@/components/Pages/ConfirmEmail/styles';
import * as connect from '@/components/Pages/ConfirmEmail/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageConfirmEmailBaseProps,
    PageConfirmEmailBase,
} from '@/components/Pages/ConfirmEmail/Base';

export const PageConfirmEmailStyle = { ...styles } as const;
export const PageConfirmEmailConnect = { ...connect } as const;

export type PageConfirmEmailStyles = keyof typeof PageConfirmEmailStyle;
export type PageConfirmEmailConnects = keyof typeof PageConfirmEmailConnect;

export interface PageConfirmEmailProps extends PageConfirmEmailBaseProps {
    styleTemplate?: PageConfirmEmailStyles | ThemesType;
    company?: boolean;
}

export const PageConfirmEmail = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageConfirmEmailProps) => {
    const Style = useMemo(
        () =>
            PageConfirmEmailStyle[styleTemplate as PageConfirmEmailStyles] ??
            PageConfirmEmailStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageConfirmEmailConnect[
                styleTemplate as PageConfirmEmailConnects
            ] ?? PageConfirmEmailConnect._default,
        [styleTemplate]
    );

    return <PageConfirmEmailBase<any> {...Style} {...Connect} {...props} />;
};
export default PageConfirmEmail;
