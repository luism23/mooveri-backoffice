import { LayoutBase as LayoutConfirmEmail } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageConfirmEmailConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageConfirmEmailClassProps {
    render?: RenderLoaderProps;
}

export interface PageConfirmEmailBaseProps {}

export interface PageConfirmEmailProps<P = any>
    extends PageConfirmEmailBaseProps,
        PageConfirmEmailClassProps,
        PageConfirmEmailConnectProps<P> {}

export const PageConfirmEmailBase = <P,>({
    render = {
        Layout: LayoutConfirmEmail,
        title: 'ConfirmEmail',
    },

    useLoadData = true,

    ...props
}: PageConfirmEmailProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageConfirmEmailBase;
