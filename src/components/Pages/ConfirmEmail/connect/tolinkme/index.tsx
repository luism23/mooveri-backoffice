import { PageConfirmEmailConnectProps } from '@/components/Pages/ConfirmEmail/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/ConfirmEmail/content/tolinkme';

export const tolinkme: PageConfirmEmailConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({
        onSubmintConfirmEmail: async () => ({ status: 'ok' }),
    }),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
