import { PageConfirmEmailConnectProps } from '@/components/Pages/ConfirmEmail/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/ConfirmEmail/content/default';

export const _default: PageConfirmEmailConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
