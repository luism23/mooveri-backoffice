import { useMemo } from 'react';

import * as styles from '@/components/Pages/SendEmail/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageSendEmailBaseProps,
    PageSendEmailBase,
} from '@/components/Pages/SendEmail/Base';

export const PageSendEmailStyle = { ...styles } as const;

export type PageSendEmailStyles = keyof typeof PageSendEmailStyle;

export interface PageSendEmailProps extends PageSendEmailBaseProps {
    styleTemplate?: PageSendEmailStyles | ThemesType;
    company?: boolean;
}

export const PageSendEmail = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageSendEmailProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageSendEmailStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageSendEmailStyle[styleTemplateSelected as PageSendEmailStyles] ??
            PageSendEmailStyle._default,
        [styleTemplateSelected]
    );
    return <PageSendEmailBase {...Style} {...props} />;
};
export default PageSendEmail;
