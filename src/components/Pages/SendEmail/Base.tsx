import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import ContentWidth from '@/components/ContentWidth';
import Image from '@/components/Image';
import Space from '@/components/Space';
import Title, { TitleProps } from '@/components/Title';
import Text from '@/components/Text';
import Buttons, { ButtonStyles } from '@/components/Button';
import { Link } from '@/components/Link';
import { useLang } from '@/lang/translate';
import url from '@/data/routes';
import Theme, { ThemesType } from '@/config/theme';

export interface PageSendEmailConnectProps extends RenderLoadPageProps {}

export interface PageSendEmailClassProps {
    render?: RenderLoaderProps;
    sizeContentWidth?: number;
    classNameContentWidth?: string;
    TitleProps?: TitleProps;
    classNameText?: string;
    sizeContentWidthImg?: number;
    classNameContentWidthImg?: string;
    classNameLink?: string;
    styleTemplateImg?: ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;

    img?: string;
    link?: string;
}

export interface PageSendEmailBaseProps {
    title?: string;
    text?: string;
    btnText?: string;
}

export interface PageSendEmailProps
    extends PageSendEmailBaseProps,
        PageSendEmailClassProps,
        PageSendEmailConnectProps {}

export const PageSendEmailBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Change Password',
    },

    sizeContentWidth = 300,
    classNameContentWidth = '',
    TitleProps = {},
    classNameText = '',

    sizeContentWidthImg = 100,
    classNameContentWidthImg = '',
    img = 'sendEmail.png',
    styleTemplateImg = Theme.styleTemplate ?? '_default',

    classNameLink = '',
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    title = 'Succesfully Register',
    text = 'Please check your email to confirm',
    btnText = 'Done',
    link = url.home,
}: PageSendEmailProps) => {
    const _t = useLang();
    return (
        <>
            <RenderLoader {...render}>
                <ContentWidth
                    size={sizeContentWidth}
                    className={classNameContentWidth}
                >
                    <Title {...TitleProps}>{_t(title)}</Title>
                    <Space size={28} />
                    <Text className={classNameText}>{_t(text)}</Text>
                    <Space size={28} />
                    <ContentWidth
                        size={sizeContentWidthImg}
                        className={classNameContentWidthImg}
                    >
                        <Image src={img} styleTemplate={styleTemplateImg} />
                    </ContentWidth>
                    <Space size={28} />
                    <Link href={link} className={classNameLink}>
                        <Buttons styleTemplate={styleTemplateButton}>
                            {_t(btnText)}
                        </Buttons>
                    </Link>
                </ContentWidth>
            </RenderLoader>
        </>
    );
};
export default PageSendEmailBase;
