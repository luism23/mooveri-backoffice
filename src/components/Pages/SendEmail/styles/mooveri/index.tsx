import { PageSendEmailClassProps } from '@/components/Pages/SendEmail/Base';
import url from '@/data/routes';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageSendEmailClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Succesfully',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
    sizeContentWidth: 300,
    classNameContentWidth: `
        m-h-auto
    `,
    TitleProps: {
        className: `
            text-center
            color-sea
        `,
        styleTemplate: 'mooveri',
        typeStyle: 'h7',
    },
    classNameText: `
        text-center
        color-black
    `,
    sizeContentWidthImg: 100,
    classNameContentWidthImg: `
        m-h-auto
    `,
    img: 'sendEmail.png',
    styleTemplateImg: 'mooveri',
    classNameLink: `
        text-decoration-none-link
    `,
    styleTemplateButton: 'mooveri',
};

export const mooveriCompany: PageSendEmailClassProps = {
    ...mooveri,
    render: {
        Layout: TwoColumns,
        title: 'Succesfully',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    link: url.company.login,
};

export const mooveriCustomer: PageSendEmailClassProps = {
    ...mooveri,
    render: {
        Layout: TwoColumns,
        title: 'Succesfully',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    link: url.login,
};
