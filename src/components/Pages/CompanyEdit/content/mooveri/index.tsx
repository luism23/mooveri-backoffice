import { useRouter } from 'next/router';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { CompanyProps } from '@/interfaces/Company';
import FormCompanyEdit from '@/components/Form/CompanyEdit';
import { onUpdateCompanyById } from '@/api/mooveri/backoffice/companies';
import { UserLoginProps } from '@/hook/useUser';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    user: UserLoginProps;
    companies: CompanyProps;
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('Company')}</Text>
                <br />
                <FormCompanyEdit
                    defaultData={props.companies}
                    styleTemplate={'mooveri'}
                    onSubmit={(e) => {
                        return onUpdateCompanyById(props?.user, e);
                    }}
                />
            </div>
        </>
    );
};
