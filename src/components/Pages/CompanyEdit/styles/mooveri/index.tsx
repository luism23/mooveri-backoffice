import { PageCompanyEditClassProps } from '@/components/Pages/CompanyEdit/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageCompanyEditClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'CompanyEdit',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageCompanyEditClassProps = {
    render: {
        Layout: Layout,
        title: 'CompanyEdit',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
