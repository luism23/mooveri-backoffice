import { PageCompanyEditConnectProps } from '@/components/Pages/CompanyEdit/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/CompanyEdit/content/default';

export const _default: PageCompanyEditConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
