import { PageCompanyEditConnectProps } from '@/components/Pages/CompanyEdit/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/CompanyEdit/content/tolinkme';

export const tolinkme: PageCompanyEditConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
