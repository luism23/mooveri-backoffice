import { PageVerificationCodeConnectProps } from '@/components/Pages/VerificationCode/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/VerificationCode/content/default';

export const _default: PageVerificationCodeConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
