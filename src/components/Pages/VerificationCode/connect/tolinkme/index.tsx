import { PageVerificationCodeConnectProps } from '@/components/Pages/VerificationCode/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/VerificationCode/content/tolinkme';
import { RenderDataLoadPage } from '@/components/Render';

export const tolinkme: PageVerificationCodeConnectProps<TolinkmeProps> = {
    onLoadData: (props: RenderDataLoadPage) => ({
        user: {
            id: props.user?.id,
        },
    }),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
