import { PageVerificationCodeClassProps } from '@/components/Pages/VerificationCode/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageVerificationCodeClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'VerificationCode',
        LayoutProps: {
            styleVerificationCode: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageVerificationCodeClassProps = {
    render: {
        Layout: Layout,
        title: 'VerificationCode',
        LayoutProps: {
            styleVerificationCode: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
