import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageVerificationCodeProps, PageVerificationCode } from "./index";

export default {
    title: "Page/PageVerificationCode",
    component: PageVerificationCode,
} as Meta;

const VerificationCode: Story<PropsWithChildren<PageVerificationCodeProps>> = (args) => (
    <PageVerificationCode {...args}>Test Children</PageVerificationCode>
);

export const Index = VerificationCode.bind({});
Index.args = {
   
};
