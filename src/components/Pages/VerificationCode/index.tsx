import { useMemo } from 'react';

import * as styles from '@/components/Pages/VerificationCode/styles';
import * as connect from '@/components/Pages/VerificationCode/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageVerificationCodeBaseProps,
    PageVerificationCodeBase,
} from '@/components/Pages/VerificationCode/Base';

export const PageVerificationCodeStyle = { ...styles } as const;
export const PageVerificationCodeConnect = { ...connect } as const;

export type PageVerificationCodeStyles = keyof typeof PageVerificationCodeStyle;
export type PageVerificationCodeConnects =
    keyof typeof PageVerificationCodeConnect;

export interface PageVerificationCodeProps
    extends PageVerificationCodeBaseProps {
    styleTemplate?: PageVerificationCodeStyles | ThemesType;
    company?: boolean;
}

export const PageVerificationCode = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageVerificationCodeProps) => {
    const Style = useMemo(
        () =>
            PageVerificationCodeStyle[
                styleTemplate as PageVerificationCodeStyles
            ] ?? PageVerificationCodeStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageVerificationCodeConnect[
                styleTemplate as PageVerificationCodeConnects
            ] ?? PageVerificationCodeConnect._default,
        [styleTemplate]
    );

    return <PageVerificationCodeBase<any> {...Style} {...Connect} {...props} />;
};
export default PageVerificationCode;
