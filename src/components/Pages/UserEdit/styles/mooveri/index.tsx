import { PageUserEditClassProps } from '@/components/Pages/UserEdit/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageUserEditClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'UserEdit',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageUserEditClassProps = {
    render: {
        Layout: Layout,
        title: 'UserEdit',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
