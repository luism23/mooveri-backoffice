import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageUserEditProps, PageUserEdit } from "./index";

export default {
    title: "Page/PageUserEdit",
    component: PageUserEdit,
} as Meta;

const UserEdit: Story<PropsWithChildren<PageUserEditProps>> = (args) => (
    <PageUserEdit {...args}>Test Children</PageUserEdit>
);

export const Index = UserEdit.bind({});
Index.args = {
   
};
