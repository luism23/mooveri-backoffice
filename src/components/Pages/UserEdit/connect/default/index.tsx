import { PageUserEditConnectProps } from '@/components/Pages/UserEdit/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/UserEdit/content/default';

export const _default: PageUserEditConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
