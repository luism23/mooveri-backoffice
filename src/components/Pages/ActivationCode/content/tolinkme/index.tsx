import { onUpdateUser } from '@/api/mooveri/backoffice/users';
import FormConfirmPhoneCode from '@/components/Form/ConfirmPhoneCode';
import ProfileAndSubscriptions from '@/components/ProfileAndSubscriptions';
import { UserProps } from '@/interfaces/User';

export interface TolinkmeProps {
    user: UserProps;
}

export const Tolinkme = ({ ...props }: TolinkmeProps) => {
    return (
        <>
            <div>
                <ProfileAndSubscriptions
                    NameUser="@michellegiraldo"
                    text={'Confirm your Phone'}
                    img="https://i.blogs.es/e91376/650_1000_we-love-your-genes-img-models-instagram_-3-/450_1000.jpg"
                    btn={{
                        monetize: true,
                        active: true,
                    }}
                    style={{
                        description: '',
                        name: '',
                        web: '',
                    }}
                />
                <div className="p-t-30 p-h-10 p-b-50">
                    <FormConfirmPhoneCode
                        onSubmit={(e) => {
                            return onUpdateUser(props?.user, e);
                        }}
                    />
                </div>
            </div>
        </>
    );
};
