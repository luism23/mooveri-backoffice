import { PageActivationCodeConnectProps } from '@/components/Pages/ActivationCode/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/ActivationCode/content/tolinkme';
import { RenderDataLoadPage } from '@/components/Render';

export const tolinkme: PageActivationCodeConnectProps<TolinkmeProps> = {
    onLoadData: (props: RenderDataLoadPage) => ({
        user: {
            id: props.user?.id,
        },
    }),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
