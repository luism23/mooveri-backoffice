import { PageActivationCodeConnectProps } from '@/components/Pages/ActivationCode/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/ActivationCode/content/default';

export const _default: PageActivationCodeConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
