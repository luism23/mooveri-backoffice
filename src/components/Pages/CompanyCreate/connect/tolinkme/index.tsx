import { PageCompanyCreateConnectProps } from '@/components/Pages/CompanyCreate/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/CompanyCreate/content/tolinkme';

export const tolinkme: PageCompanyCreateConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
