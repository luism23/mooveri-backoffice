import { PageCompanyCreateConnectProps } from '@/components/Pages/CompanyCreate/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/CompanyCreate/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';

export const mooveri: PageCompanyCreateConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageCompanyCreateConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const propsComponent: MooveriBackofficeProps = {
                user: props?.user ?? {},
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
