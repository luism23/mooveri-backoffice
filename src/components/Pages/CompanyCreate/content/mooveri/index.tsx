import { useRouter } from 'next/router';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import FormCompanyCreate from '@/components/Form/CompanyEdit';
import { onCreateCompany } from '@/api/mooveri/backoffice/companies';
import { UserLoginProps } from '@/hook/useUser';
import { Days } from '@/interfaces/Date';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    user: UserLoginProps;
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    return (
        <>
            <div className="m-b-50">
                <Text styleTemplate="mooveri8">{_t('Company')}</Text>
                <br />
                <FormCompanyCreate
                    defaultData={{
                        id: '',
                        address: '',
                        dateCreate: new Date(),
                        moovings: 0,
                        name: '',

                        company_horarios: Days.map((day) => {
                            return {
                                day,
                                active: false,
                                schedules: [],
                            };
                        }),
                    }}
                    styleTemplate={'mooveri'}
                    onSubmit={(e) => {
                        return onCreateCompany(props?.user, e);
                    }}
                    showPassword={true}
                />
            </div>
        </>
    );
};
