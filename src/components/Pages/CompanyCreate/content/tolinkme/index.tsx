import { useRouter } from 'next/router';

export interface TolinkmeProps {}

export const Tolinkme = ({}: TolinkmeProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
