import { LayoutBase as LayoutCompanyCreate } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageCompanyCreateConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageCompanyCreateClassProps {
    render?: RenderLoaderProps;
}

export interface PageCompanyCreateBaseProps {}

export interface PageCompanyCreateProps<P = any>
    extends PageCompanyCreateBaseProps,
        PageCompanyCreateClassProps,
        PageCompanyCreateConnectProps<P> {}

export const PageCompanyCreateBase = <P,>({
    render = {
        Layout: LayoutCompanyCreate,
        title: 'CompanyCreate',
    },

    useLoadData = true,

    ...props
}: PageCompanyCreateProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageCompanyCreateBase;
