import { useMemo } from 'react';

import * as styles from '@/components/Pages/ForgotPassword/styles';
import * as connect from '@/components/Pages/ForgotPassword/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageForgotPasswordBaseProps,
    PageForgotPasswordBase,
} from '@/components/Pages/ForgotPassword/Base';

export const PageForgotPasswordStyle = { ...styles } as const;
export const PageForgotPasswordConnect = { ...connect } as const;

export type PageForgotPasswordStyles = keyof typeof PageForgotPasswordStyle;
export type PageForgotPasswordConnects = keyof typeof PageForgotPasswordConnect;

export interface PageForgotPasswordProps extends PageForgotPasswordBaseProps {
    styleTemplate?: PageForgotPasswordStyles | ThemesType;
    company?: boolean;
}

export const PageForgotPassword = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageForgotPasswordProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageForgotPasswordStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageForgotPasswordStyle[
                styleTemplateSelected as PageForgotPasswordStyles
            ] ?? PageForgotPasswordStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageForgotPasswordConnect[
                styleTemplateSelected as PageForgotPasswordConnects
            ] ?? PageForgotPasswordConnect._default,
        [styleTemplateSelected]
    );

    return <PageForgotPasswordBase {...Style} {...Connect} {...props} />;
};
export default PageForgotPassword;
