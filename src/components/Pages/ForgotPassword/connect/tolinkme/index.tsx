import { onSubmintForgotPassword } from '@/components/Form/ForgotPassword/Base';
import { PageForgotPasswordConnectProps } from '@/components/Pages/ForgotPassword/Base';
import { DataForgotPassword } from '@/interfaces/ForgotPassword';

import { ForgotPassword as ForgotPasswordApi } from '@/api/tolinkme/forgotPassword';
import url from '@/data/routes';

const onSubmit: onSubmintForgotPassword = async (data: DataForgotPassword) => {
    const result = await ForgotPasswordApi(data);
    return result;
};

export const tolinkme: PageForgotPasswordConnectProps = {
    onSubmit,
    urlRedirect: url?.forgotPasswordOk,
};
