import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageMySubscriptionsProps, PageMySubscriptions } from "./index";

export default {
    title: "Page/MySubscriptions",
    component: PageMySubscriptions,
} as Meta;

const MySubscriptions: Story<PropsWithChildren<PageMySubscriptionsProps>> = (args) => (
    <PageMySubscriptions {...args}>Test Children</PageMySubscriptions>
);

export const Index = MySubscriptions.bind({});
Index.args = {
    
};
