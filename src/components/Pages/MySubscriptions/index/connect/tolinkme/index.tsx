import { PageMySubscriptionsConnectProps } from '@/components/Pages/MySubscriptions/index/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/MySubscriptions/index/content/tolinkme';
import * as ButtonMonetizeApi from '@/api/tolinkme/CustomerSuscriptions';

export const tolinkme: PageMySubscriptionsConnectProps<TolinkmeProps> = {
    onLoadData: async ({ user }) => {
        if (!user) {
            throw 401;
        }
        const customerSubscriptions =
            await ButtonMonetizeApi.GET_CUSTOMER_SUBCRIPTION({
                user,
            });
        if (
            customerSubscriptions == 401 ||
            customerSubscriptions == undefined
        ) {
            throw {
                code: 401,
            };
        }
        const btns = customerSubscriptions?.data?.btns || [];

        if (!user) {
            throw 401;
        }
        const customerShopping = await ButtonMonetizeApi.GET_CUSTOMER_SHOPPING({
            user,
        });
        if (customerShopping == 401 || customerShopping == undefined) {
            throw {
                code: 401,
            };
        }
        const uniquerPurchase = customerShopping?.data?.uniquerPurchase || [];

        return {
            btns,
            uniquerPurchase,
            user,
        };
    },
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
