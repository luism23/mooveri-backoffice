import { PageMyAccountConnectProps } from '@/components/Pages/MyAccount/index/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/MyAccount/index/content/mooveri';

export const mooveri: PageMyAccountConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
    useLoadData: false,
};

export const mooveriBackoffice: PageMyAccountConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
        useLoadData: false,
    };
