import { useMemo } from 'react';

import * as styles from '@/components/Pages/MySubscriptions/index/styles';
import * as connect from '@/components/Pages/MySubscriptions/index/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageMySubscriptionsBaseProps,
    PageMySubscriptionsBase,
} from '@/components/Pages/MySubscriptions/index/Base';

export const PageMySubscriptionsStyle = { ...styles } as const;
export const PageMySubscriptionsConnect = { ...connect } as const;

export type PageMySubscriptionsStyles = keyof typeof PageMySubscriptionsStyle;
export type PageMySubscriptionsConnects =
    keyof typeof PageMySubscriptionsConnect;

export interface PageMySubscriptionsProps extends PageMySubscriptionsBaseProps {
    styleTemplate?: PageMySubscriptionsStyles | ThemesType;
}

export const PageMySubscriptions = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageMySubscriptionsProps) => {
    const Style = useMemo(
        () =>
            PageMySubscriptionsStyle[
                styleTemplate as PageMySubscriptionsStyles
            ] ?? PageMySubscriptionsStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageMySubscriptionsConnect[
                styleTemplate as PageMySubscriptionsConnects
            ] ?? PageMySubscriptionsConnect._default,
        [styleTemplate]
    );

    return <PageMySubscriptionsBase<any> {...Style} {...Connect} {...props} />;
};
export default PageMySubscriptions;
