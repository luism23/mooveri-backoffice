import { LayoutBase as LayoutMySubscriptions } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageMySubscriptionsConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageMySubscriptionsClassProps {
    render?: RenderLoaderProps;
}

export interface PageMySubscriptionsBaseProps {}

export interface PageMySubscriptionsProps<P = any>
    extends PageMySubscriptionsBaseProps,
        PageMySubscriptionsClassProps,
        PageMySubscriptionsConnectProps<P> {}

export const PageMySubscriptionsBase = <P,>({
    render = {
        Layout: LayoutMySubscriptions,
        title: 'MySubscriptions',
    },

    useLoadData = true,

    ...props
}: PageMySubscriptionsProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageMySubscriptionsBase;
