import ButtonUserSubcriptions from '@/components/ButtonUserSubcriptions';
import Link from '@/components/Link';
import Text from '@/components/Text';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import ArrowGoBack from '@/svg/arrowGoBack';
import Trash from '@/svg/trash';

export interface ButtonsPurchaseDataProps {
    Amount?: any;
    Button?: {
        url?: any;
        uuid?: string;
        logo?: string;
        title?: string;
        buttons_model_profile?: {
            model_profile?: {
                user_ModelProfile?: {
                    user?: {
                        username?: string;
                    };
                };
                profile_img?: string;
            };
            logo?: string;
        };
        ButtonPriceDetails?: {
            basePrice?: any;
            period?: string;
        };
    };
}

export interface ButtonsDataProps {
    TransactionSubscriptionButtons?: {
        Button?: {
            url?: string;
            uuid?: string;
            logo?: string;
            title?: string;
            buttons_model_profile?: {
                model_profile?: {
                    user_ModelProfile?: {
                        user?: {
                            username?: string;
                        };
                    };
                    profile_img?: string;
                };
                logo?: string;
            };
            ButtonPriceDetails?: {
                basePrice?: any;
                period?: string;
            };
        };
        uuid?: any;
        Status?: string;
    };
    uuid?: any;
}

export interface TolikmeButtonCustomerDataProps {
    btns?: ButtonsDataProps[];
    uniquerPurchase?: ButtonsPurchaseDataProps[];
    numberSubscriptions?: any;
}

export interface TolinkmeProps extends TolikmeButtonCustomerDataProps {}

export const Tolinkme = ({ btns, uniquerPurchase }: TolinkmeProps) => {
    const _t = useLang();
    return (
        <>
            <Link
                styleTemplate="tolinkme14"
                href={url.home}
                className="flex color-teal font-nunito m-v-40 p-10"
            >
                <span className="m-r-5">
                    <ArrowGoBack size={20} />
                </span>
                {_t('Back')}
            </Link>
            <div className="flex flex-justify-between p-10">
                <div
                    className={`text-left width-p-40 colum-suscriptions m-b-20`}
                >
                    <Text className="m-b-20" styleTemplate="tolinkme11">
                        <span className="">{btns?.length ?? 0}</span>{' '}
                        {_t('Subscriptions')}
                    </Text>

                    {btns?.map((btn, i) => {
                        return (
                            <>
                                <ButtonUserSubcriptions
                                    //idButton={btn.TransactionSubscriptionButtons?.Button?.uuid}
                                    statusCancel={
                                        btn.TransactionSubscriptionButtons
                                            ?.Status
                                    }
                                    uuid_transation={
                                        btn?.TransactionSubscriptionButtons
                                            ?.uuid
                                    }
                                    Subscribed={'Subscribed'}
                                    url={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.url
                                    }
                                    key={i}
                                    title={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.title
                                    }
                                    ImgUserSubscriptions={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.buttons_model_profile
                                            ?.model_profile?.profile_img
                                    }
                                    userSubscriptions={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.buttons_model_profile
                                            ?.model_profile?.user_ModelProfile
                                            ?.user?.username
                                    }
                                    icon={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.logo
                                    }
                                    money={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.ButtonPriceDetails
                                            ?.basePrice
                                    }
                                    Month={
                                        btn.TransactionSubscriptionButtons
                                            ?.Button?.ButtonPriceDetails?.period
                                    }
                                    Unsuscribe={'Unsuscribe'}
                                    trash={<Trash size={13} />}
                                />
                            </>
                        );
                    })}
                </div>
                <div className="text-left width-p-40  colum-suscriptions">
                    <Text className="m-b-20" styleTemplate="tolinkme11">
                        <span className="">{uniquerPurchase?.length ?? 0}</span>{' '}
                        {_t('Shopping')}
                    </Text>

                    {uniquerPurchase?.map((uniquer, i) => {
                        return (
                            <>
                                <ButtonUserSubcriptions
                                    Subscribed={'Bought'}
                                    url={uniquer.Button?.url}
                                    key={i}
                                    title={uniquer.Button?.title}
                                    ImgUserSubscriptions={
                                        uniquer.Button?.buttons_model_profile
                                            ?.model_profile?.profile_img
                                    }
                                    userSubscriptions={
                                        uniquer.Button?.buttons_model_profile
                                            ?.model_profile?.user_ModelProfile
                                            ?.user?.username
                                    }
                                    icon={uniquer.Button?.logo}
                                    money={
                                        uniquer.Button?.ButtonPriceDetails
                                            ?.basePrice
                                    }
                                    Month={'Bought'}
                                />
                            </>
                        );
                    })}
                </div>
            </div>
        </>
    );
};
