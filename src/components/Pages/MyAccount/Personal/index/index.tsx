import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/Personal/index/styles';
import * as connect from '@/components/Pages/MyAccount/Personal/index/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PagePersonalBaseProps,
    PagePersonalBase,
} from '@/components/Pages/MyAccount/Personal/index/Base';

export const PagePersonalStyle = { ...styles } as const;
export const PagePersonalConnect = { ...connect } as const;

export type PagePersonalStyles = keyof typeof PagePersonalStyle;
export type PagePersonalConnects = keyof typeof PagePersonalConnect;

export interface PagePersonalProps extends PagePersonalBaseProps {
    styleTemplate?: PagePersonalStyles | ThemesType;
    company?: boolean;
}

export const PagePersonal = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PagePersonalProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PagePersonalStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PagePersonalStyle[styleTemplateSelected as PagePersonalStyles] ??
            PagePersonalStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PagePersonalConnect[
                styleTemplateSelected as PagePersonalConnects
            ] ?? PagePersonalConnect._default,
        [styleTemplateSelected]
    );

    return <PagePersonalBase {...Style} {...Connect} {...props} />;
};
export default PagePersonal;
