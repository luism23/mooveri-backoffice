import FormPersonalEdit from '@/components/Form/PersonalEdit';
import GoBack from '@/components/GoBack';
import { movery } from '@/components/Pages/MyAccount/Personal/index/connect/mooveri';
import { DataPersonalEdit } from '@/interfaces/PersonalEdit';

export const Cmooveri = ({ ...props }: DataPersonalEdit<string>) => {
    return (
        <>
            <div className="p-h-15 p-sm-h-0">
                <GoBack text={'Back / My Account'} styleTemplate="mooveri2" />
                <FormPersonalEdit
                    styleTemplate="mooveri"
                    onSaveInput={movery?.onSaveInput}
                    defaultData={props}
                />
            </div>
        </>
    );
};
export const mooveri = () => <></>;
