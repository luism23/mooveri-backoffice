import { PagePersonalClassProps } from '@/components/Pages/MyAccount/Personal/index/Base';
import { mooveri as ContentMooveri } from '@/components/Pages/MyAccount/Personal/index/content/mooveri';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PagePersonalClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'My Account',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriCustomer: PagePersonalClassProps = {
    ...mooveri,
    content: <ContentMooveri />,
};

export const mooveriCompany: PagePersonalClassProps = {
    ...mooveri,
    content: <ContentMooveri />,
};
