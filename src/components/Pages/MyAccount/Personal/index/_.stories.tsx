import { Story, Meta } from "@storybook/react";

import { PagePersonalProps, PagePersonal } from "./index";

export default {
    title: "Page/MyAccount/Personal",
    component: PagePersonal,
} as Meta;

const Template: Story<PagePersonalProps> = (args) => (
    <PagePersonal {...args}>Test Children</PagePersonal>
);

export const Index = Template.bind({});
Index.args = {
};
