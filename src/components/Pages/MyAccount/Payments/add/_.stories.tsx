import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PagePaymentsAddProps, PagePaymentsAdd } from "./index";

export default {
    title: "Page/MyAccount/Payments/Add",
    component: PagePaymentsAdd,
} as Meta;

const PaymentsAdd: Story<PropsWithChildren<PagePaymentsAddProps>> = (args) => (
    <PagePaymentsAdd {...args}>Test Children</PagePaymentsAdd>
);

export const Index = PaymentsAdd.bind({});
Index.args = {
   
};
