import { LayoutBase as LayoutPaymentsAdd } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PagePaymentsAddConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PagePaymentsAddClassProps {
    render?: RenderLoaderProps;
}

export interface PagePaymentsAddBaseProps {}

export interface PagePaymentsAddProps<P = any>
    extends PagePaymentsAddBaseProps,
        PagePaymentsAddClassProps,
        PagePaymentsAddConnectProps<P> {}

export const PagePaymentsAddBase = <P,>({
    render = {
        Layout: LayoutPaymentsAdd,
        title: 'PaymentsAdd',
    },

    useLoadData = true,

    ...props
}: PagePaymentsAddProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (!p.user && !(process?.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE')) {
            return 401;
        }
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PagePaymentsAddBase;
