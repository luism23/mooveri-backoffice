import GoBack from '@/components/GoBack';
import Space from '@/components/Space';
import Text from '@/components/Text';
import { Payments } from '@/components/List/Payments';
import { PaymentsBaseProps } from '@/components/List/Payments/Base';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';

export interface MooveriProps extends PaymentsBaseProps {}

export const Mooveri = (props: MooveriProps) => {
    const _t = useLang();
    return (
        <div className="p-h-15 p-sm-h-0">
            <GoBack
                text={_t('Back / My Account')}
                href={url.myAccount.index}
                styleTemplate="mooveri2"
            />
            <Space size={10} />
            <Text styleTemplate="mooveri3">{_t('Payment Information')}</Text>
            <Space size={10} />
            <Text styleTemplate="mooveri4">
                {_t('Manage different payment methods.')}
            </Text>
            <Space size={26} />
            <Payments
                payments={props.payments}
                styleTemplate="mooveri"
                onDelete={props.onDelete}
            />
            <Space size={26} />
        </div>
    );
};
