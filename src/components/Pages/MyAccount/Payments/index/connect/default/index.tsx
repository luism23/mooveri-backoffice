import { PagePaymentsConnectProps } from '@/components/Pages/MyAccount/Payments/index/Base';
import { _default as C } from '@/components/Pages/MyAccount/Payments/index/content/default';

export interface _defaultDataProps {}

export const _default: PagePaymentsConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
