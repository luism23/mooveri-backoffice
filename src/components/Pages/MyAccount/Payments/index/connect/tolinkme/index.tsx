import { PagePaymentsConnectProps } from '@/components/Pages/MyAccount/Payments/index/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/MyAccount/Payments/index/content/tolinkme';

export const tolinkme: PagePaymentsConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
    useLoadData: false,
};
