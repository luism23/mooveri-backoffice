import { onSaveAddress } from '@/api/mooveri/myAccount/address';
import { PageManageAddressesAddConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/add/Base';
import {
    Mooveri as C,
    MooveriProps,
} from '@/components/Pages/MyAccount/ManageAddresses/add/content/mooveri';

import { RenderDataLoadPage } from '@/components/Render';
import log from '@/functions/log';

export interface mooveriDataProps extends MooveriProps {}

export const mooveri: PageManageAddressesAddConnectProps<mooveriDataProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        log('RenderDataLoadPage', props);
        return {};
    },
    onLoadContent: (props) => {
        return <C {...props} onSubmit={onSaveAddress} />;
    },
};
