import { PageManageAddressesAddConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/add/Base';
import { _default as C } from '@/components/Pages/MyAccount/ManageAddresses/add/content/default';

export interface _defaultDataProps {}

export const _default: PageManageAddressesAddConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
