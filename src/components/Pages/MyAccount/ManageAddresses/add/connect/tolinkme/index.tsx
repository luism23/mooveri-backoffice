import { PageManageAddressesAddConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/add/Base';
import { tolinkme as C } from '@/components/Pages/MyAccount/ManageAddresses/add/content/tolinkme';

export interface tolinkmeDataProps {}

export const tolinkme: PageManageAddressesAddConnectProps<tolinkmeDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
