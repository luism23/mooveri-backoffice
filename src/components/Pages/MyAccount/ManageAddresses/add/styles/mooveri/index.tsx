import { PageManageAddressesAddClassProps } from '@/components/Pages/MyAccount/ManageAddresses/add/Base';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageManageAddressesAddClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'ManageAddressesAdd',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};
