import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageManageAddressesEditProps, PageManageAddressesEdit } from "./index";

export default {
    title: "Page/PageManageAddressesEdit",
    component: PageManageAddressesEdit,
} as Meta;

const ManageAddressesEdit: Story<PropsWithChildren<PageManageAddressesEditProps>> = (args) => (
    <PageManageAddressesEdit {...args}>Test Children</PageManageAddressesEdit>
);

export const Index = ManageAddressesEdit.bind({});
Index.args = {
   
};
