import { LayoutBase as LayoutManageAddressesEdit } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageManageAddressesEditConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageManageAddressesEditClassProps {
    render?: RenderLoaderProps;
}

export interface PageManageAddressesEditBaseProps {}

export interface PageManageAddressesEditProps<P = any>
    extends PageManageAddressesEditBaseProps,
        PageManageAddressesEditClassProps,
        PageManageAddressesEditConnectProps<P> {}

export const PageManageAddressesEditBase = <P,>({
    render = {
        Layout: LayoutManageAddressesEdit,
        title: 'ManageAddressesEdit',
    },

    useLoadData = true,

    ...props
}: PageManageAddressesEditProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (!p.user) {
            return 401;
        }
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageManageAddressesEditBase;
