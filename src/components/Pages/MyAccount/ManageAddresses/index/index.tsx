import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/ManageAddresses/index/styles';
import * as connect from '@/components/Pages/MyAccount/ManageAddresses/index/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageManageAddressesBaseProps,
    PageManageAddressesBase,
} from '@/components/Pages/MyAccount/ManageAddresses/index/Base';

export const PageManageAddressesStyle = { ...styles } as const;
export const PageManageAddressesConnect = { ...connect } as const;

export type PageManageAddressesStyles = keyof typeof PageManageAddressesStyle;
export type PageManageAddressesConnects =
    keyof typeof PageManageAddressesConnect;

export interface PageManageAddressesProps extends PageManageAddressesBaseProps {
    styleTemplate?: PageManageAddressesStyles | ThemesType;
    company?: boolean;
}

export const PageManageAddresses = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageManageAddressesProps) => {
    const Style = useMemo(
        () =>
            PageManageAddressesStyle[
                styleTemplate as PageManageAddressesStyles
            ] ?? PageManageAddressesStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageManageAddressesConnect[
                styleTemplate as PageManageAddressesConnects
            ] ?? PageManageAddressesConnect._default,
        [styleTemplate]
    );

    return <PageManageAddressesBase {...Style} {...Connect} {...props} />;
};
export default PageManageAddresses;
