import ContentWidth from '@/components/ContentWidth';
import GoBack from '@/components/GoBack';
import Addresses from '@/components/List/Addresses';
import Space from '@/components/Space';
import Text from '@/components/Text';
import url from '@/data/routes';
import { DataManageAddress } from '@/interfaces/ManageAddress';
import { useLang } from '@/lang/translate';

export interface MooveriProps {
    addresses?: DataManageAddress[];
}

export const Mooveri = (props: MooveriProps) => {
    const _t = useLang();
    return (
        <div className="p-h-15 p-sm-h-0">
            <GoBack
                text={_t('Back / My Account')}
                href={url.myAccount.index}
                styleTemplate="mooveri2"
            />
            <Space size={10} />
            <Text styleTemplate="mooveri3">{_t('Manage Addresses')}</Text>
            <Space size={10} />
            <ContentWidth size={323}>
                <Text styleTemplate="mooveri4">
                    {_t(
                        'Here you can manage different addresses, your home, your family or friend place and so on, customize it according to your needs.'
                    )}
                </Text>
            </ContentWidth>
            <Space size={26} />
            <Addresses addresses={props?.addresses} styleTemplate="mooveri" />
            <Space size={26} />
        </div>
    );
};
