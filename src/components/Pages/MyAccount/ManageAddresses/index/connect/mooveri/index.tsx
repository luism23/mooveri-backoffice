import { onLoadAddress } from '@/api/mooveri/myAccount/address';
import { PageManageAddressesConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/index/Base';
import {
    Mooveri as C,
    MooveriProps,
} from '@/components/Pages/MyAccount/ManageAddresses/index/content/mooveri';

import { RenderDataLoadPage } from '@/components/Render';

export interface mooveriDataProps extends MooveriProps {}

export const mooveri: PageManageAddressesConnectProps<mooveriDataProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        const addresses = await onLoadAddress(props?.user);
        return {
            addresses,
        };
    },
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
