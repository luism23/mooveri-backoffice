import { PageManageAddressesConnectProps } from '@/components/Pages/MyAccount/ManageAddresses/index/Base';
import { _default as C } from '@/components/Pages/MyAccount/ManageAddresses/index/content/default';

export interface _defaultDataProps {}

export const _default: PageManageAddressesConnectProps<_defaultDataProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <C {...props} />;
    },
};
