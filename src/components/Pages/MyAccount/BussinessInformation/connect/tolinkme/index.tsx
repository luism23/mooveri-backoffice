import { PageBussinessInformationConnectProps } from '@/components/Pages/MyAccount/BussinessInformation/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/MyAccount/BussinessInformation/content/tolinkme';

export const tolinkme: PageBussinessInformationConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
