import { PageBussinessInformationConnectProps } from '@/components/Pages/MyAccount/BussinessInformation/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/MyAccount/BussinessInformation/content/mooveri';
import {
    onLoadBussines,
    onSaveBussines,
} from '@/api/mooveri/myAccount/bussiness';

export const mooveri: PageBussinessInformationConnectProps<MooveriProps> = {
    onLoadData: async (pross) => {
        const defaultData = await onLoadBussines(pross?.user);

        return {
            defaultData,
        };
    },
    onLoadContent: (props) => {
        return <Mooveri {...props} onSubmit={onSaveBussines} />;
    },
    useLoadData: true,
};

export const mooveriBackoffice: PageBussinessInformationConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async () => ({}),
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
