import { PageBussinessInformationConnectProps } from '@/components/Pages/MyAccount/BussinessInformation/Base';
import {
    Defualt,
    DefualtProps,
} from '@/components/Pages/MyAccount/BussinessInformation/content/default';

export const _default: PageBussinessInformationConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
