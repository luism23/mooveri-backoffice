import { LayoutBase as LayoutBussinessInformation } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageBussinessInformationConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageBussinessInformationClassProps {
    render?: RenderLoaderProps;
}

export interface PageBussinessInformationBaseProps {}

export interface PageBussinessInformationProps<P = any>
    extends PageBussinessInformationBaseProps,
        PageBussinessInformationClassProps,
        PageBussinessInformationConnectProps<P> {}

export const PageBussinessInformationBase = <P,>({
    render = {
        Layout: LayoutBussinessInformation,
        title: 'BussinessInformation',
    },

    useLoadData = true,

    ...props
}: PageBussinessInformationProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageBussinessInformationBase;
