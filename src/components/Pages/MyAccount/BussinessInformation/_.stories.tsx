import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageBussinessInformationProps, PageBussinessInformation } from "./index";

export default {
    title: "Page/MyAccount/BussinessInformation",
    component: PageBussinessInformation,
} as Meta;

const BussinessInformation: Story<PropsWithChildren<PageBussinessInformationProps>> = (args) => (
    <PageBussinessInformation {...args}>Test Children</PageBussinessInformation>
);

export const Index = BussinessInformation.bind({});
Index.args = {
   
};
