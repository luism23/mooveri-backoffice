import { useRouter } from 'next/router';

import ContentWidth from '@/components/ContentWidth';
import GoBack from '@/components/GoBack';
import Space from '@/components/Space';
import Text from '@/components/Text';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import {
    FormBussinessInformation,
    FormBussinessInformationProps,
} from '@/components/Form/BussinessInformation';

export interface MooveriProps extends FormBussinessInformationProps {}

export const Mooveri = ({ ...props }: MooveriProps) => {
    const _t = useLang();
    return (
        <>
            <div className="p-h-15 p-sm-h-0">
                <GoBack
                    text={_t('Back / My Account')}
                    href={url.myAccount.index}
                    styleTemplate="mooveri2"
                />
                <Space size={10} />
                <Text styleTemplate="mooveri3">
                    {_t('Bussiness Information')}
                </Text>
                <Space size={10} />
                <ContentWidth size={323}>
                    <Text styleTemplate="mooveri4">
                        {_t('Help us to understand your company.')}
                    </Text>
                </ContentWidth>
                <Space size={26} />
                <ContentWidth size={800}>
                    <FormBussinessInformation
                        {...props}
                        styleTemplate="mooveri"
                        urlRedirect={url.myAccount.index}
                    />
                </ContentWidth>
                <Space size={26} />
            </div>
        </>
    );
};

export interface MooveriBackofficeProps {}

export const MooveriBackoffice = ({}: MooveriBackofficeProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
