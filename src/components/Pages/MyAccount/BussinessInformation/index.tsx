import { useMemo } from 'react';

import * as styles from '@/components/Pages/MyAccount/BussinessInformation/styles';
import * as connect from '@/components/Pages/MyAccount/BussinessInformation/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageBussinessInformationBaseProps,
    PageBussinessInformationBase,
} from '@/components/Pages/MyAccount/BussinessInformation/Base';

export const PageBussinessInformationStyle = { ...styles } as const;
export const PageBussinessInformationConnect = { ...connect } as const;

export type PageBussinessInformationStyles =
    keyof typeof PageBussinessInformationStyle;
export type PageBussinessInformationConnects =
    keyof typeof PageBussinessInformationConnect;

export interface PageBussinessInformationProps
    extends PageBussinessInformationBaseProps {
    styleTemplate?: PageBussinessInformationStyles | ThemesType;
    company?: boolean;
}

export const PageBussinessInformation = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageBussinessInformationProps) => {
    const Style = useMemo(
        () =>
            PageBussinessInformationStyle[
                styleTemplate as PageBussinessInformationStyles
            ] ?? PageBussinessInformationStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageBussinessInformationConnect[
                styleTemplate as PageBussinessInformationConnects
            ] ?? PageBussinessInformationConnect._default,
        [styleTemplate]
    );

    return (
        <PageBussinessInformationBase<any> {...Style} {...Connect} {...props} />
    );
};
export default PageBussinessInformation;
