import { useRouter } from 'next/router';

export interface DefualtProps {}

export const Defualt = ({}: DefualtProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
