import { useRouter } from 'next/router';
import ContentWidth from '@/components/ContentWidth';
import Space from '@/components/Space';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import { BoxLinkManage } from '@/components/BoxLink/Template/Manage';
import { BoxLinkPayment } from '@/components/BoxLink/Template/Payment';
import { BoxLinkPersonal } from '@/components/BoxLink/Template/Personal';
import { useUser } from '@/hook/useUser';
import { BoxLinkBusiness } from '@/components/BoxLink/Template/Business';
import { useNotification } from '@/hook/useNotification';
import { useMemo } from 'react';
import Status from '@/components/Status';
import { BoxLinkLegalInformation } from '@/components/BoxLink/Template/LegalInformation';
import { BoxLinkProfile } from '@/components/BoxLink/Template/Profile';

export interface MooveriProps {}

export const Mooveri = (props: MooveriProps) => {
    const { user } = useUser();
    const { pop } = useNotification();
    if (user?.notification) {
        pop(user.notification);
    }
    if (user?.role == 'company') {
        return <MooveriCompany {...props} />;
    }
    return <MooveriClient {...props} />;
};

export const MooveriClient = ({}: MooveriProps) => {
    const _t = useLang();
    return (
        <div className="p-h-15 p-sm-h-0">
            <Text styleTemplate="mooveri3">{_t('My Account')}</Text>
            <Space size={10} />
            <ContentWidth size={323}>
                <Text styleTemplate="mooveri4">
                    {_t(
                        'You control your profile and can limit what is shown on search engines and other off-MOT services.'
                    )}
                </Text>
            </ContentWidth>
            <Space size={10} />
            <div
                className={`
                    flex
                    flex-gap-column-20
                    flex-gap-row-15
                    flex-justify-between
                    flex-lg-nowrap
                    p-b-15
                `}
            >
                <BoxLinkPersonal styleTemplate="mooveri" />
                <BoxLinkPayment styleTemplate="mooveri" />
                <BoxLinkManage styleTemplate="mooveri" />
            </div>
        </div>
    );
};
export const MooveriCompany = ({}: MooveriProps) => {
    const _t = useLang();
    const { user } = useUser();
    const showBox = useMemo(() => {
        return {
            business: true,
            legal: user?.verify?.company ?? false,
        };
    }, []);
    return (
        <>
            <div className="p-h-15 p-sm-h-0">
                <Text styleTemplate="mooveri3">{_t('My Account')}</Text>
                <Space size={10} />
                <ContentWidth size={323}>
                    <Text styleTemplate="mooveri4">
                        {_t(
                            'You control your profile and can limit what is shown on search engines and other off-MOT services.'
                        )}
                    </Text>
                </ContentWidth>
                <Space size={10} />
                <div
                    className={`
                    flex
                    flex-gap-column-20
                    flex-gap-row-15
                    flex-justify-between
                    flex-lg-nowrap
                    p-b-15
                `}
                >
                    {showBox.business ? (
                        <>
                            <BoxLinkBusiness
                                styleTemplate="mooveri"
                                top={
                                    !showBox.legal ? (
                                        <Status
                                            styleTemplate="mooveri"
                                            text="Missing Information"
                                            status="error"
                                        />
                                    ) : (
                                        <div></div>
                                    )
                                }
                            />
                        </>
                    ) : (
                        <></>
                    )}
                    {showBox.legal ? (
                        <>
                            <BoxLinkLegalInformation styleTemplate="mooveri" />
                            <BoxLinkProfile styleTemplate="mooveri" />
                        </>
                    ) : (
                        <>
                            <div className="width-p-100"></div>
                            <div className="width-p-100"></div>
                        </>
                    )}
                </div>
            </div>
        </>
    );
};

export interface MooveriBackofficeProps {}

export const MooveriBackoffice = ({}: MooveriBackofficeProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};
