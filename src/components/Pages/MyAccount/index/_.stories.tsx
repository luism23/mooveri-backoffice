import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageMyAccountProps, PageMyAccount } from "./index";

export default {
    title: "Page/MyAccount",
    component: PageMyAccount,
} as Meta;

const MyAccount: Story<PropsWithChildren<PageMyAccountProps>> = (args) => (
    <PageMyAccount {...args}>Test Children</PageMyAccount>
);

export const Index = MyAccount.bind({});
Index.args = {
    
};
