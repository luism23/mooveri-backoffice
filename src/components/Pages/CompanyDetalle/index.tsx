import { useMemo } from 'react';

import * as styles from '@/components/Pages/CompanyDetalle/styles';
import * as connect from '@/components/Pages/CompanyDetalle/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageCompanyDetalleBaseProps,
    PageCompanyDetalleBase,
} from '@/components/Pages/CompanyDetalle/Base';

export const PageCompanyDetalleStyle = { ...styles } as const;
export const PageCompanyDetalleConnect = { ...connect } as const;

export type PageCompanyDetalleStyles = keyof typeof PageCompanyDetalleStyle;
export type PageCompanyDetalleConnects = keyof typeof PageCompanyDetalleConnect;

export interface PageCompanyDetalleProps extends PageCompanyDetalleBaseProps {
    styleTemplate?: PageCompanyDetalleStyles | ThemesType;
    company?: boolean;
}

export const PageCompanyDetalle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageCompanyDetalleProps) => {
    const Style = useMemo(
        () =>
            PageCompanyDetalleStyle[
                styleTemplate as PageCompanyDetalleStyles
            ] ?? PageCompanyDetalleStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageCompanyDetalleConnect[
                styleTemplate as PageCompanyDetalleConnects
            ] ?? PageCompanyDetalleConnect._default,
        [styleTemplate]
    );

    return <PageCompanyDetalleBase<any> {...Style} {...Connect} {...props} />;
};
export default PageCompanyDetalle;
