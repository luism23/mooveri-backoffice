import { onLoadCompanyById } from '@/api/mooveri/backoffice/companies';
import { PageCompanyDetalleConnectProps } from '@/components/Pages/CompanyDetalle/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/CompanyDetalle/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';
import { Months, MonthsType } from '@/data/components/Month';

export const mooveri: PageCompanyDetalleConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageCompanyDetalleConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const companies = await onLoadCompanyById(
                props?.user,
                props?.query?.id
            );

            const transactions = companies.transactions ?? [];

            const transactionsByMonths: any = {};

            Months.forEach((month: MonthsType, i) => {
                const t = transactions?.filter(
                    (transaction) => transaction.dateCreate.getMonth() == i
                );
                transactionsByMonths[month] = {
                    total: t?.reduce((a, b) => a + b.price, 0),
                    count: t.length ?? 0,
                };
            });

            const propsComponent: MooveriBackofficeProps = {
                companies,
                transactions,

                transactionsByMonths,

                user: companies?.customers ?? [],
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
