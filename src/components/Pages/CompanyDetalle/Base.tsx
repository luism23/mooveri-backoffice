import { LayoutBase as LayoutCompanyDetalle } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PageCompanyDetalleConnectProps<P = any>
    extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PageCompanyDetalleClassProps {
    render?: RenderLoaderProps;
}

export interface PageCompanyDetalleBaseProps {}

export interface PageCompanyDetalleProps<P = any>
    extends PageCompanyDetalleBaseProps,
        PageCompanyDetalleClassProps,
        PageCompanyDetalleConnectProps<P> {}

export const PageCompanyDetalleBase = <P,>({
    render = {
        Layout: LayoutCompanyDetalle,
        title: 'CompanyDetalle',
    },

    useLoadData = true,

    ...props
}: PageCompanyDetalleProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PageCompanyDetalleBase;
