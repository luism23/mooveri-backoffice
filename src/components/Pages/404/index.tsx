import { useMemo } from 'react';

import * as styles from '@/components/Pages/404/styles';
import * as connect from '@/components/Pages/404/connect';

import { Theme, ThemesType } from '@/config/theme';

import { Page404BaseProps, Page404Base } from '@/components/Pages/404/Base';

export const Page404Style = { ...styles } as const;
export const Page404Connect = { ...connect } as const;

export type Page404Styles = keyof typeof Page404Style;
export type Page404Connects = keyof typeof Page404Connect;

export interface Page404Props extends Page404BaseProps {
    styleTemplate?: Page404Styles | ThemesType;
    company?: boolean;
}

export const Page404 = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: Page404Props) => {
    const Style = useMemo(
        () =>
            Page404Style[styleTemplate as Page404Styles] ??
            Page404Style._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            Page404Connect[styleTemplate as Page404Connects] ??
            Page404Connect._default,
        [styleTemplate]
    );

    return <Page404Base<any> {...Style} {...Connect} {...props} />;
};
export default Page404;
