import { Page404ClassProps } from '@/components/Pages/404/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: Page404ClassProps = {
    render: {
        Layout: LayoutBase,
        title: '404',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: false,
    },
};

export const mooveriBackoffice: Page404ClassProps = {
    render: {
        Layout: Layout,
        title: '404',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: false,
    },
};
