import { C404 } from '@/components/404';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    return <C404 styleTemplate="mooveri" />;
};

export interface MooveriBackofficeProps {}

export const MooveriBackoffice = ({}: MooveriBackofficeProps) => {
    return <C404 styleTemplate="mooveriBackoffice" />;
};
