import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { Page404Props, Page404 } from "./index";

export default {
    title: "Page/Page404",
    component: Page404,
} as Meta;

const T: Story<PropsWithChildren<Page404Props>> = (args) => (
    <Page404 {...args}>Test Children</Page404>
);

export const Index = T.bind({});
Index.args = {
   
};
