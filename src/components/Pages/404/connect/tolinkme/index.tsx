import { Page404ConnectProps } from '@/components/Pages/404/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/404/content/tolinkme';

export const tolinkme: Page404ConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
