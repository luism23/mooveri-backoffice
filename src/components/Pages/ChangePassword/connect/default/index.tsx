import { onSubmintChangePassword } from '@/components/Form/ChangePassword/Base';
import { PageChangePasswordConnectProps } from '@/components/Pages/ChangePassword/Base';
import log from '@/functions/log';
import { DataChangePassword } from '@/interfaces/ChangePassword';

const onSubmit: onSubmintChangePassword = async (data: DataChangePassword) => {
    log('DataChangePassword', data);
    return {
        status: 'ok',
        message: '_default',
    };
};

export const _default: PageChangePasswordConnectProps = {
    onSubmit,
};
