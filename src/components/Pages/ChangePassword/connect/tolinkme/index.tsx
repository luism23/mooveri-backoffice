import { onSubmintChangePassword } from '@/components/Form/ChangePassword/Base';
import { PageChangePasswordConnectProps } from '@/components/Pages/ChangePassword/Base';
import { DataChangePassword } from '@/interfaces/ChangePassword';
import { ChangePassword as ChangePasswordApi } from '@/api/tolinkme/changePassword';
import url from '@/data/routes';

const onSubmit: onSubmintChangePassword = async (data: DataChangePassword) => {
    const result = await ChangePasswordApi(data);
    return result;
};

export const tolinkme: PageChangePasswordConnectProps = {
    onSubmit,
    urlRedirect: url.login,
};
