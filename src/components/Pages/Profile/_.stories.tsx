import { PropsWithChildren } from "react";
import { Story, Meta } from "@storybook/react";

import { PageProfileProps, PageProfile } from "./index";

export default {
    title: "Page/PageProfile",
    component: PageProfile,
} as Meta;

const Profile: Story<PropsWithChildren<PageProfileProps>> = (args) => (
    <PageProfile {...args}>Test Children</PageProfile>
);

export const Index = Profile.bind({});
Index.args = {
   
};
