import { UserLoginProps } from '@/hook/useUser';
import { ProfileDataProps } from '@/components/Profile/Base';
import Profile from '@/components/Profile';
import * as ProfileApi from '@/api/tolinkme/profile';

export interface TolinkmeProps {
    data: ProfileDataProps;
    user: UserLoginProps;
}

export const Tolinkme = ({ user, data }: TolinkmeProps) => {
    const onSubmit = async (data: ProfileDataProps) => {
        const result = await ProfileApi.PUT(data, user);
        return result;
    };

    /*
    const onChangeDate = (data: DateTypeValue | [Date, Date]) => {
        log('onChangeDate', data);
    };
    const { query } = useQuery();
    const { error, result } = useRequest<
        useQuery_QueryProps,
        ContentAnalitycsBaseProps
    >({
        query,
        request: async (query: useQuery_QueryProps) => {
            log('query', query);
            const result = await ProfileApi.GETANALITYCS({
                profile_id: data.profile_uuid ?? '',
                user_id: data.user_uuid ?? '',
                start: `${query.start ?? 0}`,
                end: `${query.end ?? 0}`,
            });
            log('get result Analitycs', result, 'aqua');
            return {
                type: 'ok',
                result,
            };
        },
        autoRequest: true,
    });
    log('get result Analitycs', result, 'aqua');
    log('error result Analitycs', error, 'red');

   */
    return (
        <>
            <Profile
                {...data}
                onSubmit={onSubmit}
                name={user?.name ?? ''}

                /*
               
                analitycs={{
                    // ...data.analitycs,
                    onChangeDate,
                    loader,

                    ...result?.result,
                }}
               */
            />
        </>
    );
};
