import { PageProfileClassProps } from '@/components/Pages/Profile/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageProfileClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Profile',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageProfileClassProps = {
    render: {
        Layout: Layout,
        title: 'Profile',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
