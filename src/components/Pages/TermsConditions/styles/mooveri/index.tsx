import { PageTermsConditionsClassProps } from '@/components/Pages/TermsConditions/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageTermsConditionsClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Terms Conditions',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    ContentWidthProps: {
        size: 500,
        className: `
            text-center
            m-h-auto
        `,
    },
    TextProps: {
        styleTemplate: 'mooveri',
    },
};
export const mooveriBackoffice = mooveri;
