import { useMemo } from 'react';

import * as styles from '@/components/Pages/TermsConditions/styles';
import * as connect from '@/components/Pages/TermsConditions/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageTermsConditionsBaseProps,
    PageTermsConditionsBase,
} from '@/components/Pages/TermsConditions/Base';

export const PageTermsConditionsStyle = { ...styles } as const;
export const PageTermsConditionsConnect = { ...connect } as const;

export type PageTermsConditionsStyles = keyof typeof PageTermsConditionsStyle;
export type PageTermsConditionsConnects =
    keyof typeof PageTermsConditionsConnect;

export interface PageTermsConditionsProps extends PageTermsConditionsBaseProps {
    styleTemplate?: PageTermsConditionsStyles | ThemesType;
    company?: boolean;
}

export const PageTermsConditions = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageTermsConditionsProps) => {
    const Style = useMemo(
        () =>
            PageTermsConditionsStyle[
                styleTemplate as PageTermsConditionsStyles
            ] ?? PageTermsConditionsStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageTermsConditionsConnect[
                styleTemplate as PageTermsConditionsConnects
            ] ?? PageTermsConditionsConnect._default,
        [styleTemplate]
    );

    return <PageTermsConditionsBase {...Style} {...Connect} {...props} />;
};
export default PageTermsConditions;
