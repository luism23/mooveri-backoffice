import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { Text, TextProps } from '@/components/Text';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

export interface PageTermsConditionsConnectProps extends RenderLoadPageProps {
    term?: string;
}

export interface PageTermsConditionsClassProps {
    render?: RenderLoaderProps;
    TextProps?: TextProps;
    ContentWidthProps?: ContentWidthProps;
}

export interface PageTermsConditionsBaseProps {}

export interface PageTermsConditionsProps
    extends PageTermsConditionsBaseProps,
        PageTermsConditionsClassProps,
        PageTermsConditionsConnectProps {}

export const PageTermsConditionsBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Terms Conditions',
    },

    TextProps = {},
    ContentWidthProps = {},

    term = '',
}: PageTermsConditionsProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <ContentWidth {...ContentWidthProps}>
                    <Text {...TextProps}>{term}</Text>
                </ContentWidth>
            </RenderLoader>
        </>
    );
};
export default PageTermsConditionsBase;
