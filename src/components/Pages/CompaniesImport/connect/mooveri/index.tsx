import { PageCompaniesImportConnectProps } from '@/components/Pages/CompaniesImport/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/CompaniesImport/content/mooveri';
import { RenderDataLoadPage } from '@/components/Render';

export const mooveri: PageCompaniesImportConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageCompaniesImportConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const propsComponent: MooveriBackofficeProps = {
                user: props?.user,
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
