import { useRouter } from 'next/router';
import Text from '@/components/Text';
import { useLang } from '@/lang/translate';
import InputUpload from '@/components/Input/Upload';
import log from '@/functions/log';
import { csvToJson } from '@/functions/csv';
import { useState } from 'react';
import TableCompanyImport, {
    TableItemCompanyImportProps,
} from '@/components/Table/template/CompanyImport';
import Button from '@/components/Button';
import { useNotification } from '@/hook/useNotification';
import ProgressLinear from '@/components/Progress/Linear';
import { onImportCompany } from '@/api/mooveri/backoffice/companies';
import { UserLoginProps } from '@/hook/useUser';
import url from '@/data/routes';

export interface MooveriProps {}

export const Mooveri = ({}: MooveriProps) => {
    const router = useRouter();
    if (router.isReady) {
        router.push('/404');
    }
    return <></>;
};

export interface MooveriBackofficeProps {
    user?: UserLoginProps;
}

export const MooveriBackoffice = (props: MooveriBackofficeProps) => {
    const _t = useLang();
    const { pop } = useNotification();
    const router = useRouter();
    const [loader, setLoader] = useState(false);
    const [progress, setProgress] = useState(-1);
    const [csv, setCsv] = useState<TableItemCompanyImportProps[] | null>(null);

    const onImport = async () => {
        if (!csv) {
            return;
        }
        setLoader(true);
        try {
            const result = await onImportCompany(props?.user, csv, setProgress);
            log('result import', result, 'red');
            setProgress(-1);

            pop({
                message: `${result.message}`,
                type: result.status,
            });

            router.push(url.companies.index);
        } catch (error) {
            log('error import', error, 'red');
            pop({
                message: `${error}`,
                type: 'error',
            });
        }
        setLoader(false);
    };

    return (
        <>
            <div className="flex flex-justify-between m-b-50">
                <Text styleTemplate="mooveri8">{_t('Import Companies')}</Text>
                <a href="/template/templateCompanies.csv" download={true}>
                    <Text styleTemplate="mooveri8">
                        {_t('Download Template')}
                    </Text>
                </a>
            </div>
            {progress > 0 && progress < 100 ? (
                <ProgressLinear p={progress} />
            ) : (
                <>
                    <div className="text-center m-b-5">
                        <Text styleTemplate="mooveri8">{_t('Upload CSV')}</Text>
                    </div>
                    <InputUpload
                        styleTemplate="mooveriBackoffice"
                        fileText={true}
                        accept={['csv']}
                        textUpload=""
                        onSubmit={(e) => {
                            const r = csvToJson(e?.fileData);
                            log('csv', r);
                            setCsv(r.data ?? []);
                        }}
                        returnUrl={false}
                    ></InputUpload>
                    <div className="m-v-20 overflow-auto p-b-20">
                        {csv && csv.length > 0 ? (
                            <>
                                <TableCompanyImport items={csv} />
                            </>
                        ) : (
                            <></>
                        )}
                    </div>
                    {csv && csv.length > 0 ? (
                        <>
                            <Button loader={loader} onClick={onImport}>
                                {_t('Import')}
                            </Button>
                        </>
                    ) : (
                        <></>
                    )}
                </>
            )}
        </>
    );
};
