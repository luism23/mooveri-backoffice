import { useMemo } from 'react';

import * as styles from '@/components/Pages/CompaniesImport/styles';
import * as connect from '@/components/Pages/CompaniesImport/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageCompaniesImportBaseProps,
    PageCompaniesImportBase,
} from '@/components/Pages/CompaniesImport/Base';

export const PageCompaniesImportStyle = { ...styles } as const;
export const PageCompaniesImportConnect = { ...connect } as const;

export type PageCompaniesImportStyles = keyof typeof PageCompaniesImportStyle;
export type PageCompaniesImportConnects =
    keyof typeof PageCompaniesImportConnect;

export interface PageCompaniesImportProps extends PageCompaniesImportBaseProps {
    styleTemplate?: PageCompaniesImportStyles | ThemesType;
    company?: boolean;
}

export const PageCompaniesImport = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageCompaniesImportProps) => {
    const Style = useMemo(
        () =>
            PageCompaniesImportStyle[
                styleTemplate as PageCompaniesImportStyles
            ] ?? PageCompaniesImportStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageCompaniesImportConnect[
                styleTemplate as PageCompaniesImportConnects
            ] ?? PageCompaniesImportConnect._default,
        [styleTemplate]
    );

    return <PageCompaniesImportBase<any> {...Style} {...Connect} {...props} />;
};
export default PageCompaniesImport;
