import { PageCompaniesConnectProps } from '@/components/Pages/Companies/Base';
import {
    Tolinkme,
    TolinkmeProps,
} from '@/components/Pages/Companies/content/tolinkme';

export const tolinkme: PageCompaniesConnectProps<TolinkmeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Tolinkme {...props} />;
    },
};
