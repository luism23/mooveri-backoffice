import { PageCompaniesConnectProps } from '@/components/Pages/Companies/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Companies/content/mooveri';
import { onLoadCompany } from '@/api/mooveri/backoffice/companies';
import { RenderDataLoadPage } from '@/components/Render';
import { Months, MonthsType } from '@/data/components/Month';

export const mooveri: PageCompaniesConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PageCompaniesConnectProps<MooveriBackofficeProps> =
    {
        onLoadData: async (props: RenderDataLoadPage) => {
            const companies = await onLoadCompany(props?.user);

            const companiesByMonths: any = {};

            Months.forEach((month: MonthsType, i) => {
                companiesByMonths[month] =
                    companies?.filter(
                        (company) => company.dateCreate.getMonth() == i
                    ).length ?? 0;
            });

            const companiesMoovings: any = {};

            companies.forEach((company) => {
                companiesMoovings[company.name] = company.moovings;
            });

            const propsComponent: MooveriBackofficeProps = {
                companiesByMonths,
                companiesMoovings,
                companies,
            };
            return propsComponent;
        },
        onLoadContent: (props) => {
            return <MooveriBackoffice {...props} />;
        },
    };
