import { useMemo } from 'react';

import * as styles from '@/components/Pages/Companies/styles';
import * as connect from '@/components/Pages/Companies/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageCompaniesBaseProps,
    PageCompaniesBase,
} from '@/components/Pages/Companies/Base';

export const PageCompaniesStyle = { ...styles } as const;
export const PageCompaniesConnect = { ...connect } as const;

export type PageCompaniesStyles = keyof typeof PageCompaniesStyle;
export type PageCompaniesConnects = keyof typeof PageCompaniesConnect;

export interface PageCompaniesProps extends PageCompaniesBaseProps {
    styleTemplate?: PageCompaniesStyles | ThemesType;
    company?: boolean;
}

export const PageCompanies = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PageCompaniesProps) => {
    const Style = useMemo(
        () =>
            PageCompaniesStyle[styleTemplate as PageCompaniesStyles] ??
            PageCompaniesStyle._default,
        [styleTemplate]
    );
    const Connect = useMemo(
        () =>
            PageCompaniesConnect[styleTemplate as PageCompaniesConnects] ??
            PageCompaniesConnect._default,
        [styleTemplate]
    );

    return <PageCompaniesBase<any> {...Style} {...Connect} {...props} />;
};
export default PageCompanies;
