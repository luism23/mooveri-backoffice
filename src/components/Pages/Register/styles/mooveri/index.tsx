import { PageRegisterClassProps } from '@/components/Pages/Register/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageRegisterClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Register',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
};

export const mooveriCustomer: PageRegisterClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Register',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    form: {
        styleTemplate: 'mooveri',
    },
};

export const mooveriCompany: PageRegisterClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Register',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
    form: {
        styleTemplate: 'mooveriCompany',
    },
};
