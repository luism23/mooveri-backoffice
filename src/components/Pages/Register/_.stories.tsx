import { Story, Meta } from "@storybook/react";


import { PageRegisterProps, PageRegister } from "./index";

export default {
    title: "Page/PageRegister",
    component: PageRegister,
} as Meta;

const Template: Story<PageRegisterProps> = (args) => (
    <PageRegister {...args}>Test Children</PageRegister>
);

export const Index = Template.bind({});
Index.args = {
};
