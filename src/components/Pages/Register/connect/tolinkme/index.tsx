import {
    onSubmintRegister,
    onValidateUsername,
} from '@/components/Form/Register/Base';
import { PageRegisterConnectProps } from '@/components/Pages/Register/Base';
import { DataRegister } from '@/interfaces/Register';

import {
    Register as RegisterApi,
    RegisterValidateName,
} from '@/api/tolinkme/register';
import url from '@/data/routes';

const onSubmit: onSubmintRegister = async (data: DataRegister) => {
    const result = await RegisterApi(data);
    return result;
};

const onValidateUsername_: onValidateUsername = async (username: string) => {
    await RegisterValidateName(username);
};
export const tolinkme: PageRegisterConnectProps = {
    onSubmit,
    onValidateUsername: onValidateUsername_,
    urlRediret: url.confirmPhone.index,
};
