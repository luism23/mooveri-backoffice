import { onSubmintRegister } from '@/components/Form/Register/Base';
import { PageRegisterConnectProps } from '@/components/Pages/Register/Base';
import { DataRegister } from '@/interfaces/Register';

import { Register as RegisterApi } from '@/api/mooveri/register';
import { UserRoles } from '@/hook/useUser';
import url from '@/data/routes';

const onSubmit: (type: UserRoles) => onSubmintRegister =
    (type: UserRoles) => async (data: DataRegister) => {
        const result = await RegisterApi({
            type,
            ...data,
        });
        return result;
    };

export const mooveri: PageRegisterConnectProps = {
    onSubmit: onSubmit('client'),
    urlRediret: url.myAccount.index,
};

export const mooveriCustomer: PageRegisterConnectProps = {
    onSubmit: onSubmit('client'),
};

export const mooveriCompany: PageRegisterConnectProps = {
    onSubmit: onSubmit('company'),
};

export const mooveriBackoffice: PageRegisterConnectProps = {
    onSubmit: onSubmit('backoffice'),
};
