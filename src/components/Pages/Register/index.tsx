import { useMemo } from 'react';

import * as styles from '@/components/Pages/Register/styles';
import * as connect from '@/components/Pages/Register/connect';

import { Theme, ThemesType } from '@/config/theme';

import {
    PageRegisterBaseProps,
    PageRegisterBase,
} from '@/components/Pages/Register/Base';

export const PageRegisterStyle = { ...styles } as const;
export const PageRegisterConnect = { ...connect } as const;

export type PageRegisterStyles = keyof typeof PageRegisterStyle;
export type PageRegisterConnects = keyof typeof PageRegisterConnect;

export interface PageRegisterProps extends PageRegisterBaseProps {
    styleTemplate?: PageRegisterStyles | ThemesType;
    company?: boolean;
}

export const PageRegister = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    company = false,
    ...props
}: PageRegisterProps) => {
    const styleTemplateSelected = useMemo(() => {
        const styleTemplateS: PageRegisterStyles | ThemesType =
            styleTemplate == 'mooveri'
                ? company
                    ? 'mooveriCompany'
                    : 'mooveriCustomer'
                : styleTemplate;
        return styleTemplateS;
    }, [styleTemplate, company]);

    const Style = useMemo(
        () =>
            PageRegisterStyle[styleTemplateSelected as PageRegisterStyles] ??
            PageRegisterStyle._default,
        [styleTemplateSelected]
    );
    const Connect = useMemo(
        () =>
            PageRegisterConnect[
                styleTemplateSelected as PageRegisterConnects
            ] ?? PageRegisterConnect._default,
        [styleTemplateSelected]
    );

    return <PageRegisterBase {...Style} {...Connect} {...props} />;
};
export default PageRegister;
