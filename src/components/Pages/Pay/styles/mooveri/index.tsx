import { PagePayClassProps } from '@/components/Pages/Pay/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PagePayClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Pay',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PagePayClassProps = {
    render: {
        Layout: Layout,
        title: 'Pay',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
