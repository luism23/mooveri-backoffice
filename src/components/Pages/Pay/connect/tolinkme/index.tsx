import { GET_BUTTONS_MONETIZE } from '@/api/tolinkme/buttonMonetize';
import { PagePayConnectProps } from '@/components/Pages/Pay/Base';
import {
    TolinkmePay,
    TolinkmePayProps,
} from '@/components/Pages/Pay/content/tolinkme';
import { RenderDataLoadPage } from '@/components/Render';

import * as CardPayment from '@/api/tolinkme/Payment';

export const tolinkme: PagePayConnectProps<TolinkmePayProps> = {
    onLoadData: async (props: RenderDataLoadPage) => {
        const r = await GET_BUTTONS_MONETIZE({
            btn_uuid: props.query?.id,
        });
        if (r == 401) {
            throw '404';
        }

        const user = props.user;
        const Card_Payment = await CardPayment.GET_STRIPE_CARD_USER({
            user,
        });
        return {
            user: {
                id: props.user?.id,
            },
            btn_uuid: props.query?.id,
            amount: r.data?.amount,
            period: r.data?.period,
            recurrence: r.data?.recurrence,
            userImg: r.data?.userImg,
            description: r.data?.description,
            username: r.data?.username,
            style: r.data?.style,
            logoIcon: r.data?.logoIcon,
            PaymentMethodCardDataProps: {
                PaymentMethodCard: Card_Payment.data?.PaymentMethodCard,
            },
        };
    },
    onLoadContent: (props) => {
        return <TolinkmePay {...props} />;
    },
};
