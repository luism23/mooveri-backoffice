import { PagePayConnectProps } from '@/components/Pages/Pay/Base';
import { Defualt, DefualtProps } from '@/components/Pages/Pay/content/default';

export const _default: PagePayConnectProps<DefualtProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Defualt {...props} />;
    },
};
