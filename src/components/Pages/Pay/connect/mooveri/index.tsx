import { PagePayConnectProps } from '@/components/Pages/Pay/Base';
import {
    Mooveri,
    MooveriProps,
    MooveriBackoffice,
    MooveriBackofficeProps,
} from '@/components/Pages/Pay/content/mooveri';

export const mooveri: PagePayConnectProps<MooveriProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <Mooveri {...props} />;
    },
};

export const mooveriBackoffice: PagePayConnectProps<MooveriBackofficeProps> = {
    onLoadData: async () => ({}),
    onLoadContent: (props) => {
        return <MooveriBackoffice {...props} />;
    },
};
