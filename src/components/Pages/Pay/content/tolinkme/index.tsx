import Button from '@/components/Button';
import ContentWidth from '@/components/ContentWidth';
import FormLoginPayment, {
    FormLoginPaymentStyles,
} from '@/components/Form/LoginPayment';
import { onSubmintLoginPayment } from '@/components/Form/LoginPayment/Base';
import FormPay from '@/components/Form/Pay';
import ProfileAndSubscriptions from '@/components/ProfileAndSubscriptions';
import Space from '@/components/Space';
import Theme, { ThemesType } from '@/config/theme';
import { DataLoginPayment } from '@/interfaces/LoginPayment';
import { useLang } from '@/lang/translate';
import { useState } from 'react';
import { Login as LoginApi } from '@/api/tolinkme/login';
import FormRegisterPayment from '@/components/Form/RegisterPayment';
import { onSubmintRegisterPayment } from '@/components/Form/RegisterPayment/Base';
import { DataRegisterPayment } from '@/interfaces/RegisterPayment';
import {
    Register as RegisterApi,
    RegisterValidateName,
} from '@/api/tolinkme/register';
import { onValidateUsername } from '@/components/Form/Register/Base';
import ArrowGoBack from '@/svg/arrowGoBack';
import { DataForgotPasswordPayment } from '@/interfaces/ForgotPasswordPayment';
import { onSubmintForgotPasswordPayment } from '@/components/Form/ForgotPasswordPayment/Base';
import { ForgotPassword as ForgotPasswordApi } from '@/api/tolinkme/forgotPassword';
import FormForgotPasswordPayment from '@/components/Form/ForgotPasswordPayment';
import { InfoProfileEditDataProps } from '@/components/InfoProfileEdit/Base';
import { useUser } from '@/hook/useUser';
import { PaymentMethodCardDataProps } from '@/components/Form/Pay/Base';

export interface TolinkmePayProps {
    form?: {
        styleTemplate?: FormLoginPaymentStyles | ThemesType;
    };
    btn_uuid: string;
    userImg?: string;
    period?: string;
    username?: string;
    recurrence?: string;
    description?: string;
    amount?: string;
    style?: InfoProfileEditDataProps;
    logoIcon?: string;
    PaymentMethodCardDataProps?: PaymentMethodCardDataProps;
}
export interface PageLoginBaseProps {}
export const TolinkmePay = ({
    btn_uuid,
    userImg,
    username,
    amount,
    recurrence,
    period,
    logoIcon,
    description,
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    PaymentMethodCardDataProps,
    ...props
}: TolinkmePayProps) => {
    const { user } = useUser();
    const _t = useLang();
    const [showRegistration, setShowRegistration] = useState(false);
    const [showForgotPassword, setshowForgotPassword] = useState(false);
    const handleShowRegistration = () => {
        setShowRegistration(true);
    };
    const handleGoBackRegister = () => {
        setShowRegistration(false);
    };

    const handleShowForgotPassword = () => {
        setshowForgotPassword(true);
    };
    const handleGoBackShowForgotPassword = () => {
        setshowForgotPassword(false);
    };

    const onSubmitRegister: onSubmintRegisterPayment = async (
        data: DataRegisterPayment
    ) => {
        const result = await RegisterApi(data);
        return result;
    };
    const onValidateUsername_: onValidateUsername = async (
        username: string
    ) => {
        await RegisterValidateName(username);
    };
    const onSubmitLogin: onSubmintLoginPayment = async (
        data: DataLoginPayment
    ) => {
        const result = await LoginApi(data);
        return result;
    };
    const onSubmitForgotPassword: onSubmintForgotPasswordPayment = async (
        data: DataForgotPasswordPayment
    ) => {
        const result = await ForgotPasswordApi(data);
        return result;
    };
    return (
        <>
            <div className="p-b-40">
                <ProfileAndSubscriptions
                    text=""
                    logoIcon={logoIcon}
                    price={amount}
                    recurrence={recurrence}
                    period={period}
                    NameUser={username}
                    text2="Register now and get these subscription:"
                    text3={description}
                    img={userImg}
                    btn={{
                        monetize: true,
                        active: true,
                    }}
                    style={{
                        description: '',
                        name: '',
                        web: '',
                    }}
                />
                {user ? (
                    <>
                        <FormPay
                            PaymentMethodCard={
                                PaymentMethodCardDataProps?.PaymentMethodCard
                            }
                            btn_uuid={btn_uuid}
                        />
                    </>
                ) : (
                    <>
                        {!showForgotPassword ? (
                            <>
                                {!showRegistration ? (
                                    <ContentWidth className="m-auto" size={300}>
                                        <FormRegisterPayment
                                            onSubmit={onSubmitRegister}
                                            onValidateUsername={
                                                onValidateUsername_
                                            }
                                        />
                                        <Space size={15} />
                                        <Button
                                            styleTemplate="tolinkmeDontAccount"
                                            onClick={handleShowRegistration}
                                        >
                                            {_t(`Already Have an Account?`)}
                                        </Button>
                                    </ContentWidth>
                                ) : (
                                    <ContentWidth>
                                        <FormLoginPayment
                                            onSubmit={onSubmitLogin}
                                            {...props}
                                            {...form}
                                        />
                                        <div className="m-auto">
                                            <Button
                                                onClick={
                                                    handleShowForgotPassword
                                                }
                                                styleTemplate="tolinkmeForgotPassword"
                                            >
                                                {_t('Forgot your password?')}
                                            </Button>
                                            <Space size={15} />
                                            <Button
                                                styleTemplate="tolinkmeDontAccount"
                                                onClick={handleGoBackRegister}
                                            >
                                                {_t(`Don't Have an Account?`)}
                                            </Button>
                                        </div>
                                    </ContentWidth>
                                )}
                            </>
                        ) : (
                            <>
                                <ContentWidth size={300} className="m-auto">
                                    <Space size={15} />
                                    <Button
                                        classNameBtn="text-left"
                                        styleTemplate="tolinkmeForgotPassword"
                                        onClick={handleGoBackShowForgotPassword}
                                    >
                                        <span className="p-r-5">
                                            <ArrowGoBack size={20} />
                                        </span>
                                        {_t(`Back`)}
                                    </Button>
                                    <Space size={10} />
                                    <FormForgotPasswordPayment
                                        onSubmit={onSubmitForgotPassword}
                                    />
                                </ContentWidth>
                            </>
                        )}
                    </>
                )}
            </div>
        </>
    );
};
