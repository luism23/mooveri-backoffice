import { LayoutBase as LayoutPay } from '@/layout/Base';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useState } from 'react';

export interface PagePayConnectProps<P = any> extends RenderLoadPageProps {
    useLoadData?: boolean;
    onLoadData?: (props: RenderDataLoadPage) => Promise<P> | P;
    onLoadContent?: (props: P) => any;
}

export interface PagePayClassProps {
    render?: RenderLoaderProps;
}

export interface PagePayBaseProps {}

export interface PagePayProps<P = any>
    extends PagePayBaseProps,
        PagePayClassProps,
        PagePayConnectProps<P> {}

export const PagePayBase = <P,>({
    render = {
        Layout: LayoutPay,
        title: 'Pay',
    },
    useLoadData = true,
    ...props
}: PagePayProps<P>) => {
    const [content, setContent] = useState<any>(<></>);
    const loadPage = async (p: RenderDataLoadPage) => {
        if (useLoadData) {
            if (props?.onLoadData) {
                const propsComponent = await props?.onLoadData(p);
                if (props.onLoadContent) {
                    setContent(props?.onLoadContent(propsComponent));
                }
            }
        } else {
            if (props.onLoadContent) {
                setContent(props?.onLoadContent({} as P));
            }
        }

        const result: LoadPageFunctionRespond = 'ok';
        return result;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={loadPage}>
                {content}
            </RenderLoader>
        </>
    );
};
export default PagePayBase;
