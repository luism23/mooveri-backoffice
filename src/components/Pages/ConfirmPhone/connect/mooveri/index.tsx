import { onSubmintConfirmPhone } from '@/components/Form/ConfirmPhone/Base';
import { PageConfirmPhoneConnectProps } from '@/components/Pages/ConfirmPhone/Base';

import { ConfirmPhone as ConfirmPhoneApi } from '@/api/mooveri/confirmPhone';
import url from '@/data/routes';
import { InputTelValue } from '@/components/Input/Tel/Base';
import { UserLoginProps } from '@/hook/useUser';

const onSubmit: onSubmintConfirmPhone = async (
    tel: InputTelValue,
    user: UserLoginProps
) => {
    const result = await ConfirmPhoneApi({
        tel,
        user,
    });
    return result;
};

export const mooveri: PageConfirmPhoneConnectProps = {
    onSubmit,
    urlRedirect: url?.forgotPasswordOk,
};
