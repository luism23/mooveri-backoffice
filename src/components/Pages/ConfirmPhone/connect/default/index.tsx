import { onSubmintConfirmPhone } from '@/components/Form/ConfirmPhone/Base';
import { PageConfirmPhoneConnectProps } from '@/components/Pages/ConfirmPhone/Base';
import log from '@/functions/log';
import { UserLoginProps } from '@/hook/useUser';
import { InputTelValue } from '@/components/Input/Tel/Base';

const onSubmit: onSubmintConfirmPhone = async (
    tel: InputTelValue,
    user: UserLoginProps
) => {
    log('InputTelValue-UserLoginProps', { tel, user });
    return {
        status: 'ok',
        message: 'default',
    };
};

export const _default: PageConfirmPhoneConnectProps = {
    onSubmit,
};
