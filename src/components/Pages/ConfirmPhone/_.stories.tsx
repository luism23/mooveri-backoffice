import { Story, Meta } from "@storybook/react";


import { PageConfirmPhoneProps, PageConfirmPhone } from "./index";

export default {
    title: "Page/PageConfirmPhone",
    component: PageConfirmPhone,
} as Meta;

const Template: Story<PageConfirmPhoneProps> = (args) => (
    <PageConfirmPhone {...args}>Test Children</PageConfirmPhone>
);

export const Index = Template.bind({});
Index.args = {
};
