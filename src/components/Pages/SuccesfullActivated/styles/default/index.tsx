import { PageSuccesfullActivatedClassProps } from '@/components/Pages/SuccesfullActivated/Base';
import LayoutLogin from '@/layout/Login';

export const _default: PageSuccesfullActivatedClassProps = {
    render: {
        Layout: LayoutLogin,
        title: 'Succesfull Activated',
    },
    styleTemplateFormSuccesfullActivated: 'tolinkme',
};
