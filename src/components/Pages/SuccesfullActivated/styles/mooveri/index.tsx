import { PageSuccesfullActivatedClassProps } from '@/components/Pages/SuccesfullActivated/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageSuccesfullActivatedClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Succesfull Activated',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
    },
    styleTemplateFormSuccesfullActivated: 'mooveri',
};

export const mooveriCompany: PageSuccesfullActivatedClassProps = {
    ...mooveri,
    render: {
        Layout: TwoColumns,
        title: 'Succesfully',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
};

export const mooveriCustomer: PageSuccesfullActivatedClassProps = {
    ...mooveri,
    render: {
        Layout: TwoColumns,
        title: 'Succesfully',
        LayoutProps: {
            styleTemplate: 'mooveriCompany',
        },
    },
};
