import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import Theme, { ThemesType } from '@/config/theme';
import FormSuccesfullActivated, {
    FormSuccesfullActivatedStyles,
} from '@/components/Form/SuccesfullActivated';

export interface PageSuccesfullActivatedConnectProps
    extends RenderLoadPageProps {}

export interface PageSuccesfullActivatedClassProps {
    render?: RenderLoaderProps;
    styleTemplateFormSuccesfullActivated?:
        | FormSuccesfullActivatedStyles
        | ThemesType;
}

export interface PageSuccesfullActivatedBaseProps {}

export interface PageSuccesfullActivatedProps
    extends PageSuccesfullActivatedBaseProps,
        PageSuccesfullActivatedClassProps,
        PageSuccesfullActivatedConnectProps {}

export const PageSuccesfullActivatedBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Succesfull Activated',
    },
    styleTemplateFormSuccesfullActivated = Theme.styleTemplate ?? '_default',
}: PageSuccesfullActivatedProps) => {
    return (
        <>
            <RenderLoader {...render}>
                <FormSuccesfullActivated
                    styleTemplate={styleTemplateFormSuccesfullActivated}
                />
            </RenderLoader>
        </>
    );
};
export default PageSuccesfullActivatedBase;
