import { Story, Meta } from "@storybook/react";


import { PageSuccesfullActivatedProps, PageSuccesfullActivated } from "./index";

export default {
    title: "Page/PageSuccesfullActivated",
    component: PageSuccesfullActivated,
} as Meta;

const Template: Story<PageSuccesfullActivatedProps> = (args) => (
    <PageSuccesfullActivated {...args}>Test Children</PageSuccesfullActivated>
);

export const Index = Template.bind({});
Index.args = {
};
