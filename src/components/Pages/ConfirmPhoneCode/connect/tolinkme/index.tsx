import { onSubmintConfirmPhoneCode } from '@/components/Form/ConfirmPhoneCode/Base';
import { PageConfirmPhoneCodeConnectProps } from '@/components/Pages/ConfirmPhoneCode/Base';

import { ConfirmPhoneCode as ConfirmPhoneCodeApi } from '@/api/tolinkme/confirmPhone';
import url from '@/data/routes';
import { DataCodePhone } from '@/interfaces/CodePhone';
import { UserLoginProps } from '@/hook/useUser';

const onSubmit: onSubmintConfirmPhoneCode = async (
    data: DataCodePhone,
    user: UserLoginProps
) => {
    const result = await ConfirmPhoneCodeApi({
        ...data,
        user,
    });
    return result;
};

export const tolinkme: PageConfirmPhoneCodeConnectProps = {
    onSubmit,
    urlRedirect: url?.nameCategories,
};
