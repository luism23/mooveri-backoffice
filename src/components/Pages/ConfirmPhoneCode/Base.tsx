import {
    FormConfirmPhoneCode,
    FormConfirmPhoneCodeStyles,
} from '@/components/Form/ConfirmPhoneCode';
import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    LoadPageFunctionRespond,
    RenderDataLoadPage,
    RenderLoaderProps,
} from '@/components/Render';
import { FormConfirmPhoneCodeBaseProps } from '@/components/Form/ConfirmPhoneCode/Base';
import Theme, { ThemesType } from '@/config/theme';
import { useState } from 'react';

export interface PageConfirmPhoneCodeConnectProps
    extends FormConfirmPhoneCodeBaseProps {}
export interface PageConfirmPhoneCodeClassProps {
    render?: RenderLoaderProps;
    form?: {
        styleTemplate?: FormConfirmPhoneCodeStyles | ThemesType;
    };
}

export interface PageConfirmPhoneCodeBaseProps {}

export interface PageConfirmPhoneCodeProps
    extends PageConfirmPhoneCodeBaseProps,
        PageConfirmPhoneCodeClassProps,
        PageConfirmPhoneCodeConnectProps {}

export const PageConfirmPhoneCodeBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Confirm Phone',
    },
    form = {
        styleTemplate: Theme?.styleTemplate ?? '_default',
    },
    ...props
}: PageConfirmPhoneCodeProps) => {
    const [phone, setPhone] = useState('');
    const onLoadPage = async ({ query }: RenderDataLoadPage) => {
        const phone = query?.id;

        setPhone(phone);

        const r: LoadPageFunctionRespond = 'ok';
        return r;
    };
    return (
        <>
            <RenderLoader {...render} loadPage={onLoadPage}>
                <FormConfirmPhoneCode {...props} {...form} phone={phone} />
            </RenderLoader>
        </>
    );
};
export default PageConfirmPhoneCodeBase;
