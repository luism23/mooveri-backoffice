import { PageMaintenanceClassProps } from '@/components/Pages/Maintenance/Base';
import { TwoColumns } from '@/layout/TwoColumns';

export const mooveri: PageMaintenanceClassProps = {
    render: {
        Layout: TwoColumns,
        title: 'Maintenance',
        LayoutProps: {
            styleTemplate: 'mooveriCustomerLogin',
        },
    },
    styleContentWidth: {
        size: 500,
        className: `
            m-h-auto
        `,
    },
    styleContentWidthImg: {
        size: 200,
        className: `
            m-h-auto
        `,
    },
    styleImg: {
        src: 'maintenance.png',
        styleTemplate: 'tolinkme',
    },
    styleTitle: {
        className: 'text-center',
        styleTemplate: 'mooveri',
        typeStyle: 'h7',
    },
};
