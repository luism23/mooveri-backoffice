import { Story, Meta } from "@storybook/react";


import { PageMaintenanceProps, PageMaintenance } from "./index";

export default {
    title: "Page/PageMaintenance",
    component: PageMaintenance,
} as Meta;

const Template: Story<PageMaintenanceProps> = (args) => (
    <PageMaintenance {...args}>Test Children</PageMaintenance>
);

export const Index = Template.bind({});