import { LayoutLogin } from '@/layout/Login';
import RenderLoader, {
    RenderLoaderProps,
    RenderLoadPageProps,
} from '@/components/Render';
import { useLang } from '@/lang/translate';
import ContentWidth, { ContentWidthProps } from '@/components/ContentWidth';
import Image, { ImageProps } from '@/components/Image';
import Space from '@/components/Space';
import Title, { TitleProps } from '@/components/Title';

export interface PageMaintenanceConnectProps extends RenderLoadPageProps {}

export interface PageMaintenanceClassProps {
    render?: RenderLoaderProps;
    styleContentWidth?: ContentWidthProps;
    styleContentWidthImg?: ContentWidthProps;
    styleTitle?: TitleProps;
    styleImg?: ImageProps;
}

export interface PageMaintenanceBaseProps {}

export interface PageMaintenanceProps
    extends PageMaintenanceBaseProps,
        PageMaintenanceClassProps,
        PageMaintenanceConnectProps {}

export const PageMaintenanceBase = ({
    render = {
        Layout: LayoutLogin,
        title: 'Succesfull Activated',
    },
    styleContentWidth = {},
    styleContentWidthImg = {},
    styleImg = {},
    styleTitle = {},
}: PageMaintenanceProps) => {
    const _t = useLang();
    return (
        <>
            <RenderLoader {...render}>
                <ContentWidth {...styleContentWidth}>
                    <ContentWidth {...styleContentWidthImg}>
                        <Image {...styleImg} />
                    </ContentWidth>
                    <Space size={50} />
                    <Title {...styleTitle}>
                        {_t('We are doing some maintenance')}
                    </Title>
                </ContentWidth>
            </RenderLoader>
        </>
    );
};
export default PageMaintenanceBase;
