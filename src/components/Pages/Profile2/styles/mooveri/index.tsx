import { PageProfile2ClassProps } from '@/components/Pages/Profile2/Base';
import { Layout } from '@/layout/Backoffice';
import { LayoutBase } from '@/layout/Base';

export const mooveri: PageProfile2ClassProps = {
    render: {
        Layout: LayoutBase,
        title: 'Profile2',
        LayoutProps: {
            styleTemplate: 'mooveri',
        },
        validateLogin: true,
    },
};

export const mooveriBackoffice: PageProfile2ClassProps = {
    render: {
        Layout: Layout,
        title: 'Profile2',
        LayoutProps: {
            styleTemplate: 'mooveriBackoffice',
        },
        validateLogin: true,
    },
};
