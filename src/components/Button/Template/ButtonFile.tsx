import Button, { ButtonProps } from '@/components/Button';
import InputFile from '@/components/Input/File';
import { InputFileBaseProps } from '@/components/Input/File/Base';

export interface ButtonFileProps extends ButtonProps {
    file?: InputFileBaseProps;
}

export const ButtonFile = ({ file = {}, ...props }: ButtonFileProps) => {
    return (
        <>
            <Button classNameBtn="pos-r" {...props}>
                {props.children}
                <InputFile {...file} styleTemplate="_defaultBtnFile" />
            </Button>
        </>
    );
};
export default ButtonFile;
