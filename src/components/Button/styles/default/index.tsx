import { ButtonClassProps } from '@/components/Button/Base';

export const _default: ButtonClassProps = {
    className: `
    border-radius-50
    font-nunito
    color-white 
    bg-blue
    p-h-40
    p-v-11
    font-w-500
    font-15
    width-p-100
    border-0
    flex-align-center

    animation-boxScan
    animation-fill-mode-forwards
    animation-duration-30
    animation-iteration-count-1
`,
    classNameLoader: `
    width-25 
    height-25 
    border-radius-100 
    border-3 
    border-style-solid 
    border-t-transparent 
    animation 
    animation-rotate
    animation-duration-10
    m-auto
`,
    classNameDisabled: `
    bg-gray-6
    color-white-3
    not-allowed
`,
};

export const _default2: ButtonClassProps = {
    className: `
    border-radius-50
    font-nunito
    color-white 
    p-h-40
    p-v-11
    font-w-500
    font-15
    width-p-100
    border-0
    bg-dark-jungle-green
    flex-align-center

`,
    classNameLoader: `
    width-25 
    height-25 
    border-radius-100 
    border-3 
    border-style-solid 
    border-t-transparent 
    animation 
    animation-rotate
    animation-duration-10
    m-auto
`,
    classNameDisabled: `
    bg-gray-6
    color-white-3
    not-allowed
`,
};

export const _default3: ButtonClassProps = {
    className: `
    border-radius-50
    font-nunito
    color-independence 
    bg-white 
    p-h-40
    p-v-8
    font-w-500
    font-15
    width-p-100
    border-bright-gray
    flex-align-center

`,
    classNameLoader: `
    width-25 
    height-25 
    border-radius-100 
    border-3 
    border-style-solid 
    border-t-transparent 
    animation 
    animation-rotate
    animation-duration-10
    m-auto
`,
    classNameDisabled: `
    bg-gray-6
    color-white-3
    not-allowed
`,
};

export const _default4: ButtonClassProps = {
    className: `
    border-radius-50
    font-nunito
    color-dark-jungle-green
    bg-white
    p-h-40
    p-v-12
    font-w-600
    font-14
    width-p-100
    border-0
    flex-align-center

`,
    classNameLoader: `
    width-25 
    height-25 
    border-radius-100 
    border-3 
    border-style-solid 
    border-t-transparent 
    animation 
    animation-rotate
    animation-duration-10
    m-auto
`,
    classNameDisabled: `
    bg-gray-6
    color-white-3
    not-allowed
`,
};

export const _default5: ButtonClassProps = {
    className: `
    border-radius-50
    font-nunito
    color-white  
    color-dark-jungle-green-hover
    bg-dark-jungle-green 
    bg-white-hover
    p-h-14
    p-v-12
    font-w-600
    font-14
    width-p-100
    border-white33 
    
    
    

`,
    classNameLoader: `
    width-25 
    height-25 
    border-radius-100 
    border-3 
    border-style-solid 
    border-t-transparent 
    animation 
    animation-rotate
    animation-duration-10
    m-auto
`,
    classNameDisabled: `
    bg-gray-6
    color-white-3
    not-allowed
`,
};
