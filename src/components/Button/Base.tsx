import { PropsWithChildren } from 'react';

export interface ButtonBaseProps extends PropsWithChildren {
    loader?: boolean;
    disabled?: boolean;
    onClick?: (e?: any) => void;
    icon?: any;
    iconPosition?: 'left' | 'right';
    isBtn?: boolean;
    classNameBtn?: string;
}

export interface ButtonClassProps {
    className?: string;
    classNameLoader?: string;
    classNameDisabled?: string;
    classNameNotDisabled?: string;
}

export const BaseStyle = {
    className: `
        width-p-100 
        p-h-15 p-v-10
        font-18 font-w-900 line-h-8
        font-montserrat
        flex flex-justify-center flex-align-center
        text-center
        border-radius-80
        border-4 border-style-solid
    `,
    classNameLoader: `
        width-25 height-25 
        border-radius-100 border-3 border-style-solid border-t-transparent 
        animation animation-rotate
        animation-duration-10
    `,
    classNameDisabled: `
        filter-brightness-8
        opacity-5
        not-allowed
    `,
};

export interface ButtonProps extends ButtonBaseProps, ButtonClassProps {}

export const ButtonBase = ({
    children,
    loader = false,
    disabled = false,
    onClick = () => {},
    className = '',
    classNameLoader = '',
    classNameDisabled = '',
    classNameNotDisabled = '',
    icon = '',
    iconPosition = 'left',
    isBtn = true,
    classNameBtn = '',
    ...props
}: PropsWithChildren<ButtonProps>) => {
    const Tag = isBtn ? 'button' : 'div';

    return (
        <>
            <Tag
                onClick={disabled ? () => 1 : onClick}
                className={`${classNameBtn} ${className} ${
                    disabled ? classNameDisabled : classNameNotDisabled
                }`}
                disabled={loader || disabled}
                {...props}
            >
                {loader ? (
                    <div className={classNameLoader}></div>
                ) : (
                    <>
                        {iconPosition == 'left' && icon}
                        {children}
                        {iconPosition == 'right' && icon}
                    </>
                )}
            </Tag>
        </>
    );
};
export default ButtonBase;
