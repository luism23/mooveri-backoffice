# Button

## Import

```js
import { Button, ButtonStyles } from '@/components/Button';
```

## Props

```ts
interface ButtonProps {
    styleTemplate?: ButtonStyles;
    loader?: boolean;
    disabled?: boolean;
    onClick?: (e?: any) => void;
    icon?: any;
    iconPosition?: 'left' | 'right';
    children?: any;
}
```

## Use

```js
<Button>Test Children</Button>
```
