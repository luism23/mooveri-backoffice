import { TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { useData } from 'fenextjs-hook/cjs/useData';
import { Popup, PopupStyles } from '../Popup';
import { Collapse, CollapseStyles } from '../Collapse';

import Font from '@/svg/Font';
import { ConfigText, ConfigTextStyles } from '../Input/ConfigText';
import { InfoConfig, InfoConfigType } from '@/interfaces/Info';
import { getText } from '@/functions/getText';
import { getCorrectColor } from '@/functions/getCorrectColor';
import { useMemo, useState } from 'react';
import LoaderPage from '../Loader/LoaderPage';
import Reload from '@/svg/Reload';

export interface ConfigInfoStyleClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    styleTemplateConfigText?: ConfigTextStyles | ThemesType;

    classNameExample?: string;
    classNameContentExampleText?: string;

    classNameReload?: string;
}

export interface ConfigInfoStyleBaseProps {
    defaultValue?: InfoConfig;
    onChange?: (data: InfoConfig) => void;
    infoData?: {
        name?: string;
        description?: string;
        web?: string;
    };
}

export interface ConfigInfoStyleProps
    extends ConfigInfoStyleClassProps,
        ConfigInfoStyleBaseProps {}

export const ConfigInfoStyleBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    styleTemplateConfigText = Theme?.styleTemplate ?? '_default',

    classNameReload = '',

    classNameExample = '',
    classNameContentExampleText = '',

    defaultValue = {},
    onChange,
    infoData,
}: ConfigInfoStyleProps) => {
    const _t = useLang();
    const [loader, setLoader] = useState(false);
    const [isActive, setIsActive] = useState<InfoConfigType | ''>('');

    const defaultValue_ = useMemo(() => defaultValue, []);
    const {
        data,
        onChangeData,
        dataMemo: example,
        onRestart,
    } = useData<InfoConfig>(defaultValue_, {
        onChangeDataAfter: (data: InfoConfig) => {
            onChange?.(data);
        },
        onMemo: (data: InfoConfig) => {
            return (
                <div className={`${classNameExample}`}>
                    <span className={classNameContentExampleText}>
                        {_t('Vista Previa')}
                    </span>
                    {data.name
                        ? getText(data.name, infoData?.name ?? 'Name', '', {
                              background: getCorrectColor(
                                  data?.name?.color ?? '#ffffff'
                              ),
                          })
                        : ''}
                    {data.description
                        ? getText(
                              data.description,
                              infoData?.description ?? 'Description',
                              '',
                              {
                                  background: getCorrectColor(
                                      data?.description?.color ?? '#ffffff'
                                  ),
                              }
                          )
                        : ''}
                    {data.web
                        ? getText(data.web, infoData?.web ?? 'Web', '', {
                              background: getCorrectColor(
                                  data?.web?.color ?? '#ffffff'
                              ),
                          })
                        : ''}
                </div>
            );
        },
    });

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }
        return (
            <>
                {example}
                <Space size={25} />
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Name')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'name'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'name' : '');
                    }}
                >
                    <ConfigText
                        textExample={null}
                        onChange={onChangeData('name')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data?.name}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Description')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'description'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'description' : '');
                    }}
                >
                    <ConfigText
                        textExample={null}
                        onChange={onChangeData('description')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data?.description}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Web')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'web'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'web' : '');
                    }}
                >
                    <ConfigText
                        textExample={null}
                        onChange={onChangeData('web')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data?.web}
                    />
                </Collapse>
            </>
        );
    }, [data, isActive, loader, example, _t]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };

    return (
        <>
            <Popup
                btn={_t('Profile Info')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigInfoStyleBase;
