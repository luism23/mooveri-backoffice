import { Story, Meta } from "@storybook/react";

import { ConfigInfoStyleProps, ConfigInfoStyle } from "./index";

export default {
    title: "ConfigInfoStyle/ConfigInfoStyle",
    component: ConfigInfoStyle,
} as Meta;

const ConfigInfoStyleIndex: Story<ConfigInfoStyleProps> = (args) => (
    <ConfigInfoStyle {...args}>Test Children</ConfigInfoStyle>
);

export const Index = ConfigInfoStyleIndex.bind({});
Index.args = {};
