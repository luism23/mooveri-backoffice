# ConfigInfoStyle

## Dependencies

[ConfigInfoStyle](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigInfoStyle)

```js
import { ConfigInfoStyle } from '@/components/ConfigInfoStyle';
```

## Import

```js
import {
    ConfigInfoStyle,
    ConfigInfoStyleStyles,
} from '@/components/ConfigInfoStyle';
```

## Props

```tsx
interface ConfigInfoStyleProps {}
```

## Use

```js
<ConfigInfoStyle>ConfigInfoStyle</ConfigInfoStyle>
```
