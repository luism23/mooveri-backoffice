# Request

## Dependencies

[Request](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Request)

```js
import { Request } from '@/components/Request';
```

## Import

```js
import { Request, RequestStyles } from '@/components/Request';
```

## Props

```tsx
interface RequestProps {}
```

## Use

```js
<Request>Request</Request>
```
