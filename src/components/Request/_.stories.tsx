import { Story, Meta } from "@storybook/react";

import { RequestProps, Request } from "./index";

export default {
    title: "Request/Request",
    component: Request,
} as Meta;

const RequestIndex: Story<RequestProps> = (args) => (
    <Request {...args}>Test Children</Request>
);

export const Index = RequestIndex.bind({});
Index.args = {};
