import { Story, Meta } from "@storybook/react";

import { CardProps, Card } from "./index";

export default {
    title: "Card/Card",
    component: Card,
} as Meta;

const CardIndex: Story<CardProps> = (args) => (
    <Card {...args}>Test Children</Card>
);

export const Index = CardIndex.bind({});
Index.args = {
    img: "Frame_8.png",
    name: "Reporting",
    title: "Stay on top of things with always up-to-date reporting features.",
    text: "We talked about reporting in the section above but we needed three items here, so mentioning it one more time for posterity.",
    styleTemplate: "_default",
};
