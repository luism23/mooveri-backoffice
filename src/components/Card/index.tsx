import { useMemo } from 'react';

import * as styles from '@/components/Card/styles';

import { Theme, ThemesType } from '@/config/theme';

import { CardBaseProps, CardBase } from '@/components/Card/Base';

export const CardStyle = { ...styles } as const;

export type CardStyles = keyof typeof CardStyle;

export interface CardProps extends CardBaseProps {
    styleTemplate?: CardStyles | ThemesType;
}

export const Card = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CardProps) => {
    const Style = useMemo(
        () => CardStyle[styleTemplate as CardStyles] ?? CardStyle._default,
        [styleTemplate]
    );

    return <CardBase {...Style} {...props} />;
};
export default Card;
