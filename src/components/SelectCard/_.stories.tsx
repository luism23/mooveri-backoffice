import { Story, Meta } from "@storybook/react";

import { SelectCardProps, SelectCard } from "./index";

export default {
    title: "SelectCard/SelectCard",
    component: SelectCard,
} as Meta;

const SelectCardIndex: Story<SelectCardProps> = (args) => (
    <SelectCard {...args}>Test Children</SelectCard>
);

export const Index = SelectCardIndex.bind({});
Index.args = {};
