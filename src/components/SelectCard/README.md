# SelectCard

## Dependencies

[SelectCard](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/SelectCard)

```js
import { SelectCard } from '@/components/SelectCard';
```

## Import

```js
import { SelectCard, SelectCardStyles } from '@/components/SelectCard';
```

## Props

```tsx
interface SelectCardProps {}
```

## Use

```js
<SelectCard>SelectCard</SelectCard>
```
