import { Link, LinkProps } from '@/components/Link';

import { url } from '@/data/routes';

import { Theme, ThemesType } from '@/config/theme';

import { useUser } from '@/hook/useUser';

export interface ImgProfileClassProps {
    LinkProps?: LinkProps;
    styleTemplateImg?: ThemesType;
    classNameImg?: string;
    iconUserNotImg?: any;
}

export interface ImgProfileBaseProps {
    url?: string;
}

export interface ImgProfileProps
    extends ImgProfileBaseProps,
        ImgProfileClassProps {}

export const ImgProfileBase = ({
    LinkProps = {},
    styleTemplateImg = Theme?.styleTemplate ?? '_default',
    classNameImg = '',

    iconUserNotImg = null,
    ...props
}: ImgProfileProps) => {
    const { user, load } = useUser();

    if (!load) {
        return <></>;
    }

    return (
        <Link href={props?.url ?? url.profile} {...LinkProps}>
            {user?.img && user?.img != '' ? (
                <>
                    <img
                        src={user?.img}
                        alt={user.name}
                        className={classNameImg}
                    />
                </>
            ) : (
                <>
                    {iconUserNotImg ? (
                        iconUserNotImg
                    ) : (
                        <>
                            <img
                                src={`/image/${styleTemplateImg}/user.png`}
                                alt={user.name}
                                className={classNameImg}
                            />
                        </>
                    )}
                </>
            )}
        </Link>
    );
};
