import { useMemo } from 'react';

import * as styles from '@/components/User/LogOut/styles';

import { Theme, ThemesType } from '@/config/theme';

import { LogOutBase, LogOutBaseProps } from '@/components/User/LogOut/Base';

export const LogOutStyle = { ...styles } as const;

export type LogOutStyles = keyof typeof LogOutStyle;

export interface LogOutProps extends LogOutBaseProps {
    styleTemplate?: LogOutStyles | ThemesType;
}

export const LogOut = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: LogOutProps) => {
    const Style = useMemo(
        () =>
            LogOutStyle[styleTemplate as LogOutStyles] ?? LogOutStyle._default,
        [styleTemplate]
    );

    return <LogOutBase {...props} {...Style} />;
};
