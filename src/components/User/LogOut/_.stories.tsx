import { Story, Meta } from "@storybook/react";

import { LogOut,LogOutProps } from "./index";

export default {
    title: "User/LogOut",
    component: LogOut,
} as Meta;

const Template: Story<LogOutProps> = (args) => (
    <LogOut {...args}>Test Children</LogOut>
);

export const Index = Template.bind({});
Index.args = {};
