import { ImgProfile, ImgProfileProps } from '@/components/User/ImgProfile';

import { useLang } from '@/lang/translate';
import { Exit } from '@/svg/exit';

import { useUser } from '@/hook/useUser';

export interface LogOutClassProps {
    classNameContent?: string;

    classNameContentExit?: string;
    iconExit?: any;

    ImgProfileProps?: ImgProfileProps;
}

export interface LogOutBaseProps {
    urlProfile?: string;
}

export interface LogOutProps extends LogOutBaseProps, LogOutClassProps {}

export const LogOutBase = ({
    classNameContent = '',
    classNameContentExit = '',
    iconExit = <Exit size={15} />,

    ImgProfileProps = {},
}: LogOutProps) => {
    const _t = useLang();

    const { load, onLogOut } = useUser();

    if (!load) {
        return <></>;
    }

    return (
        <div className={classNameContent}>
            <ImgProfile {...ImgProfileProps} />
            <span className={classNameContentExit} onClick={onLogOut}>
                {iconExit}
                {_t('Log Out')}
            </span>
        </div>
    );
};
