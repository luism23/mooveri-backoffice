# Layer

## Dependencies

[Layer](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Layer)

```js
import { Layer } from '@/components/Layer';
```

## Import

```js
import { Layer, LayerStyles } from '@/components/Layer';
```

## Props

```tsx
interface LayerProps {}
```

## Use

```js
<Layer>Layer</Layer>
```
