export interface TemplateClassProps {}

export interface TemplateBaseProps {}

export interface TemplateProps extends TemplateClassProps, TemplateBaseProps {}

export const TemplateBase = ({}: TemplateProps) => {
    return <></>;
};
export default TemplateBase;
