import { Story, Meta } from "@storybook/react";

import { TemplateProps, Template } from "./index";

export default {
    title: "List/Template",
    component: Template,
} as Meta;

const TemplateIndex: Story<TemplateProps> = (args) => (
    <Template {...args}>Test Children</Template>
);

export const Index = TemplateIndex.bind({});
Index.args = {};
