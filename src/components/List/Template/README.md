# Template

## Dependencies

[Template](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/List/Template)

```js
import { Template } from '@/components/List/Template';
```

## Import

```js
import { Template, TemplateStyles } from '@/components/List/Template';
```

## Props

```tsx
interface TemplateProps {}
```

## Use

```js
<Template>Template</Template>
```
