import { CardNumber, CardNumberStyles } from '@/components/CardNumber';
import ContentWidth from '@/components/ContentWidth';
import { NotItemStyles } from '@/components/NotItem';
import { NotItemPayment } from '@/components/NotItem/Template/Payment';
import Space from '@/components/Space';
import { Button, ButtonStyles } from '@/components/Button';
import Theme, { ThemesType } from '@/config/theme';
import { PaymentProps } from '@/interfaces/Payment';
import { useLang } from '@/lang/translate';
import { useMemo, useState } from 'react';
import Link from '@/components/Link';
import url from '@/data/routes';
import { onDeleteCardType } from '@/interfaces/Card';

export interface PaymentsClassProps {
    styleTemplateNotItems?: NotItemStyles | ThemesType;
    styleTemplateItems?: CardNumberStyles | ThemesType;

    sizeContentWidtCardNumbers?: number;
    sizeSpaceCardNumber?: number;

    styleTemplateButton?: ButtonStyles | ThemesType;
}

export interface PaymentsBaseProps {
    payments?: PaymentProps[];
    onDelete?: onDeleteCardType;
}

export interface PaymentsProps extends PaymentsClassProps, PaymentsBaseProps {}

export const PaymentsBase = ({
    styleTemplateNotItems = Theme.styleTemplate ?? '_default',
    styleTemplateItems = Theme.styleTemplate ?? '_default',
    sizeContentWidtCardNumbers = -1,
    sizeSpaceCardNumber = -1,
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    onDelete,
    ...props
}: PaymentsProps) => {
    const _t = useLang();

    const [payments, setPayments] = useState(props?.payments ?? []);

    const content = useMemo(() => {
        if (payments.length == 0) {
            return (
                <>
                    <NotItemPayment styleTemplate={styleTemplateNotItems} />
                </>
            );
        }

        return (
            <>
                <ContentWidth size={sizeContentWidtCardNumbers}>
                    {payments.map((card, i) => (
                        <>
                            <CardNumber
                                key={i}
                                {...card.card}
                                id={card.id}
                                styleTemplate={styleTemplateItems}
                                onDelete={onDelete}
                                onDeleteOk={() => {
                                    setPayments((pre) =>
                                        pre.filter((e, j) => i != j)
                                    );
                                }}
                            />
                            <Space size={sizeSpaceCardNumber} />
                        </>
                    ))}
                    <Link
                        href={url.myAccount.payments.add}
                        styleTemplate="noDecoration"
                    >
                        <Button styleTemplate={styleTemplateButton}>
                            {_t('Add Other Payment Method')}
                        </Button>
                    </Link>
                </ContentWidth>
            </>
        );
    }, [payments]);

    return <>{content}</>;
};
export default PaymentsBase;
