import { useMemo } from 'react';

import * as styles from '@/components/List/Payments/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PaymentsBaseProps,
    PaymentsBase,
} from '@/components/List/Payments/Base';

export const PaymentsStyle = { ...styles } as const;

export type PaymentsStyles = keyof typeof PaymentsStyle;

export interface PaymentsProps extends PaymentsBaseProps {
    styleTemplate?: PaymentsStyles | ThemesType;
}

export const Payments = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PaymentsProps) => {
    const Style = useMemo(
        () =>
            PaymentsStyle[styleTemplate as PaymentsStyles] ??
            PaymentsStyle._default,
        [styleTemplate]
    );

    return <PaymentsBase {...Style} {...props} />;
};
export default Payments;
