import { Story, Meta } from "@storybook/react";

import { PaymentsProps, Payments } from "./index";

export default {
    title: "List/Payments",
    component: Payments,
} as Meta;

const PaymentsIndex: Story<PaymentsProps> = (args) => (
    <Payments {...args}>Test Children</Payments>
);

export const Index = PaymentsIndex.bind({});
Index.args = {
    payments: [
        {
            id: "1",
            card: {
                type: "visa",
                number: 4111111111111111,
            },
        },
    ],
};

export const NotItem = PaymentsIndex.bind({});
NotItem.args = {};
