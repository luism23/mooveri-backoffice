import { Story, Meta } from "@storybook/react";

import { AddressesProps, Addresses } from "./index";

export default {
    title: "List/Addresses",
    component: Addresses,
} as Meta;

const AddressesIndex: Story<AddressesProps> = (args) => (
    <Addresses {...args}>Test Children</Addresses>
);

export const Index = AddressesIndex.bind({});
Index.args = {};
