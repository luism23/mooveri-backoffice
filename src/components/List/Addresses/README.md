# Addresses

## Dependencies

[Addresses](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/List/Addresses)

```js
import { Addresses } from '@/components/List/Addresses';
```

## Import

```js
import { Addresses, AddressesStyles } from '@/components/List/Addresses';
```

## Props

```tsx
interface AddressesProps {}
```

## Use

```js
<Addresses>Addresses</Addresses>
```
