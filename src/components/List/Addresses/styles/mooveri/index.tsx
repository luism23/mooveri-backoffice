import { AddressesClassProps } from '@/components/List/Addresses/Base';

export const mooveri: AddressesClassProps = {
    styleTemplateNotItems: 'mooveri',
    sizeContentWidtCardNumbers: 284,
    classNameContentAddress: `
        m-b-20
    `,
    TitleProps: {
        className: `
            m-b-6    
        `,
        styleTemplate: 'mooveri6',
    },
    classNameContentNameAddress: `
        flex
        flex-align-center
        flex-gap-column-5
    `,
    NameProps: {
        styleTemplate: 'mooveri7',
        className: 'm-r-auto',
    },
    LinkProps: {
        styleTemplate: 'mooveri5',
    },
    styleTemplateButton: 'mooveriAddPayment',
};
