import { NotItemClassProps } from '@/components/NotItem/Base';

export const mooveri: NotItemClassProps = {
    sizeTitle: 300,
    classNameContentTitle: `
        text-center
        m-auto    
        m-b-20
    `,
    classNameTitle: `

    `,
    styleTemplateTitle: 'mooveri9',

    classNameContent: `
        pos-r
    `,
    classNameContentIcon: `
    
    `,
    classNameContentText: `
        m-auto
    `,
    classNameText: `
        text-center
    `,
    styleTemplateLink: 'mooveri4',
    styleTemplateText: 'mooveri5',
    styleTemplateButton: 'mooveriAddPayment',
    classNameLink: `
        flex
        flex-justify-center
    `,
    styleTemplateImg: 'mooveri',
    classNameContentImg: `
        flex
        flex-justify-center
        m-auto
    `,
    sizeButton: 285,
    sizeText: 185,
    sizeImg: 220,
    classNameContentLink: `
        flex
        flex-justify-center
        m-t-11
    `,
};
export const mooveri2: NotItemClassProps = {
    ...mooveri,
    classNameContentTitle: `
        pos-a
        inset-0
        m-auto
        flex
        flex-justify-center
        flex-align-center
        text-center
    `,
    sizeTitle: -1,
    styleTemplateTitle: 'mooveri9gray',
};
