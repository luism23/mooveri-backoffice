import { NotItem, NotItemProps } from '@/components/NotItem';

export const NotItemDetails = ({ ...props }: NotItemProps) => {
    return <NotItem {...props} text="No Details" srcImg="Details-items.png" />;
};
export default NotItemDetails;
