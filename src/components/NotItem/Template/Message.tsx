import { NotItem, NotItemProps } from '@/components/NotItem';

export const NotItemMessage = ({ ...props }: NotItemProps) => {
    return (
        <NotItem
            {...props}
            title="There are not messages yet!"
            srcImg="messages.png"
        />
    );
};
export default NotItemMessage;
