# NotItem

## Dependencies

[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)
[Image](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Image)
[Space](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Space)
[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)
[Link](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Link)
[Button](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Button)

```js
import { ContentWidth } from '@/components/NotItem';
import { Image } from '@/components/Image';
import { Space } from '@/components/Space';
import { Text } from '@/components/Text';
import { Link } from '@/components/Link';
import { Button } from '@/components/Button';
```

## Import

```js
import { NotItem, NotItemStyles } from '@/components/NotItem';
```

## Props

```tsx
interface NotItemProps {}
```

## Use

```js
<NotItem>NotItem</NotItem>
```
