import { Story, Meta } from "@storybook/react";

import { NotItemProps, NotItem } from "./index";

import { NotItemPayment } from "./Template/Payment";
import { NotItemManage } from "./Template/Manage";
import { NotItemSearch } from "./Template/Search";
import { NotItemMessage } from "./Template/Message";
import { NotItemDetails } from "./Template/Details";

export default {
    title: "NotItem/NotItem",
    component: NotItem,
} as Meta;

const NotItemIndex: Story<NotItemProps> = (args) => (
    <NotItem {...args}>Test Children</NotItem>
);

export const Index = NotItemIndex.bind({});
Index.args = {

    
};


const NotItemPaymentIndex: Story<NotItemProps> = (args) => (
    <NotItemPayment {...args}>Test Children</NotItemPayment>
);

export const Payment = NotItemPaymentIndex.bind({});
Payment.args = {
};


const NotItemManageIndex: Story<NotItemProps> = (args) => (
    <NotItemManage {...args}>Test Children</NotItemManage>
);

export const Manage = NotItemManageIndex.bind({});
Manage.args = {
};


const NotItemSearchIndex: Story<NotItemProps> = (args) => (
    <NotItemSearch {...args}>Test Children</NotItemSearch>
);

export const Search = NotItemSearchIndex.bind({});
Search.args = {
};

const NotItemMessageIndex: Story<NotItemProps> = (args) => (
    <NotItemMessage {...args}>Test Children</NotItemMessage>
);

export const Message = NotItemMessageIndex.bind({});
Message.args = {
};

const NotItemDetailsIndex: Story<NotItemProps> = (args) => (
    <NotItemDetails {...args}>Test Children</NotItemDetails>
);

export const Details = NotItemDetailsIndex.bind({});
Details.args = {
};
