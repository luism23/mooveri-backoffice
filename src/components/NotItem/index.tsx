import { useMemo } from 'react';

import * as styles from '@/components/NotItem/styles';

import { Theme, ThemesType } from '@/config/theme';

import { NotItemBaseProps, NotItemBase } from '@/components/NotItem/Base';

export const NotItemStyle = { ...styles } as const;

export type NotItemStyles = keyof typeof NotItemStyle;

export interface NotItemProps extends NotItemBaseProps {
    styleTemplate?: NotItemStyles | ThemesType;
}

export const NotItem = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: NotItemProps) => {
    const Style = useMemo(
        () =>
            NotItemStyle[styleTemplate as NotItemStyles] ??
            NotItemStyle._default,
        [styleTemplate]
    );

    return <NotItemBase {...Style} {...props} />;
};
export default NotItem;
