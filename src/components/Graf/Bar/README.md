# Bar

## Dependencies

[Bar](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Bar)

```js
import { Bar } from '@/components/Bar';
```

## Import

```js
import { Bar, BarStyles } from '@/components/Bar';
```

## Props

```tsx
interface BarProps {}
```

## Use

```js
<Bar>Bar</Bar>
```
