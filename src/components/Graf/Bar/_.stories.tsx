import { Story, Meta } from "@storybook/react";

import { BarProps, Bar } from "./index";

export default {
    title: "Graf/Bar",
    component: Bar,
} as Meta;

const BarIndex: Story<BarProps> = (args) => (
    <Bar {...args}>Test Children</Bar>
);

export const Index = BarIndex.bind({});
Index.args = {
    labels:[
        "January",
        "February",
        "March",
    ],
    datasets:[
        {
            label: "aasd",
            data: [1,2,3],
            borderColor: "red",
        },
    ]
};
