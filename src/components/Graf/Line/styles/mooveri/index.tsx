import { LineClassProps } from '@/components/Graf/Line/Base';

export const mooveri: LineClassProps = {
    ContentWidthProps: {
        size: -1,
        className: `
            color-white
        `,
    },
    styleLine: {
        color: 'white',
    },
};
