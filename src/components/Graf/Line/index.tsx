import { useMemo } from 'react';

import * as styles from '@/components/Graf/Line/styles';

import { Theme, ThemesType } from '@/config/theme';

import { LineBaseProps, LineBase } from '@/components/Graf/Line/Base';

export const LineStyle = { ...styles } as const;

export type LineStyles = keyof typeof LineStyle;

export interface LineProps extends LineBaseProps {
    styleTemplate?: LineStyles | ThemesType;
}

export const Line = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: LineProps) => {
    const Style = useMemo(
        () => LineStyle[styleTemplate as LineStyles] ?? LineStyle._default,
        [styleTemplate]
    );

    return <LineBase {...Style} {...props} />;
};
export default Line;
