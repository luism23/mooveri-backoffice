import { Line, LineProps } from '@/components/Graf/Line';
import { Months } from '@/data/components/Month';

export const LineByMonth = (props: LineProps) => {
    return (
        <>
            <Line labels={Months} {...props} />
        </>
    );
};
