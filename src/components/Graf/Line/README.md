# Line

## Dependencies

[Line](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Line)

```js
import { Line } from '@/components/Line';
```

## Import

```js
import { Line, LineStyles } from '@/components/Line';
```

## Props

```tsx
interface LineProps {}
```

## Use

```js
<Line>Line</Line>
```
