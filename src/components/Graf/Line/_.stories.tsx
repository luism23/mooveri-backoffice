import { Story, Meta } from "@storybook/react";

import { LineProps, Line } from "./index";

export default {
    title: "Graf/Line",
    component: Line,
} as Meta;

const LineIndex: Story<LineProps> = (args) => (
    <Line {...args}>Test Children</Line>
);

export const Index = LineIndex.bind({});
Index.args = {
    labels:[
        "January",
        "February",
        "March",
    ],
    datasets:[
        {
            label: "aasd",
            data: [1,2,3],
            borderColor: "red",
        },
    ]
};
