import { Story, Meta } from "@storybook/react";

import { CategoriesSelectedProps, CategoriesSelected } from "./index";

export default {
    title: "Category/CategoriesSelected",
    component: CategoriesSelected,
} as Meta;

const CategoriesSelectedIndex: Story<CategoriesSelectedProps> = (args) => (
    <CategoriesSelected {...args}>Test Children</CategoriesSelected>
);

export const Index = CategoriesSelectedIndex.bind({});
Index.args = {};
