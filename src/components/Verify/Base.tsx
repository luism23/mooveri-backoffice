import { useLang } from '@/lang/translate';
import ListCheck from '@/svg/ListCheck';

export interface VerifyClassProps {
    classNameContent?: string;
    classNameConetentIcon?: string;
    classNameVerify?: string;
    classNameNotVerify?: string;
}

export interface VerifyBaseProps {
    verify?: boolean;
}

export interface VerifyProps extends VerifyClassProps, VerifyBaseProps {}

export const VerifyBase = ({
    classNameContent = '',
    classNameConetentIcon = '',
    classNameVerify = '',
    classNameNotVerify = '',
    verify = false,
}: VerifyProps) => {
    const _t = useLang();
    return (
        <div className={classNameContent}>
            {verify ? (
                <span className={classNameVerify}>
                    -{' '}
                    <span className={classNameConetentIcon}>
                        <ListCheck size={18} />
                    </span>
                    {_t('Verify')}
                </span>
            ) : (
                <span className={classNameNotVerify}>
                    {' '}
                    - {_t('Unverified')}
                </span>
            )}
        </div>
    );
};
export default VerifyBase;
