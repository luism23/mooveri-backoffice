import { ItemScanProps } from '@/interfaces/Scan/ItemScan';
import { Close } from '@/svg/close';

export interface ListItemScanClassProps {
    classNameContentScans?: string;
    classNameItemScan?: string;
    classNameItemScanImg?: string;
    classNameItemScanName?: string;
    classNameItemScanDelete?: string;
}

export interface ListItemScanBaseProps {
    files?: ItemScanProps[];
    removeFile?: (i: number) => void;
}

export interface ListItemScanProps
    extends ListItemScanClassProps,
        ListItemScanBaseProps {}

export const ListItemScanBase = ({
    classNameContentScans = '',
    classNameItemScan = '',
    classNameItemScanImg = '',
    classNameItemScanName = '',
    classNameItemScanDelete = '',

    files = [],
    removeFile = (i: number) => i,
}: ListItemScanProps) => {
    return (
        <>
            <div className={classNameContentScans}>
                {files.map((file, i) => {
                    return (
                        <>
                            <div className={classNameItemScan} key={i}>
                                <img
                                    className={classNameItemScanImg}
                                    src={file.fileData}
                                    alt=""
                                />
                                <div className={classNameItemScanName}>
                                    {file?.name ?? file?.text}
                                </div>
                                <div
                                    className={classNameItemScanDelete}
                                    onClick={() => {
                                        removeFile(i);
                                    }}
                                >
                                    <Close />
                                </div>
                            </div>
                        </>
                    );
                })}
            </div>
        </>
    );
};
export default ListItemScanBase;
