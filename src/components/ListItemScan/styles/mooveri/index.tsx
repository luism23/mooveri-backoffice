import { ListItemScanClassProps } from '@/components/ListItemScan/Base';

export const mooveri: ListItemScanClassProps = {
    classNameContentScans: `
        flex
        flex-gap-column-10
        flex-gap-row-10
        p-v-10
    `,
    classNameItemScan: `
        width-50
        flex
        flex-column
        flex-gap-row-2
        flex-align-center
        pos-r
    `,
    classNameItemScanImg: `
        width-50
        height-50
        border-1
        border-style-dashed
        border-grayAvatar
        p-5
        border-radius-10
    `,
    classNameItemScanName: `
        font-nunito
        font-10
        text-capitalize
        color-Charcoal
    `,
    classNameItemScanDelete: `
        pos-a
        top-0
        right-0
        transition-5
        cursor-pointer
        
        transform
        transform-translate-X-p-50
        transform-translate-Y-p--50

        width-15
        height-15
        bg-red
        border-radius-100
        flex
        flex-align-center
        flex-justify-center
        p-h-4

        color-white

        transform-scale-X-12-hover
        transform-scale-Y-12-hover
    `,
};

export const mooveri2: ListItemScanClassProps = {
    ...mooveri,
    classNameItemScanImg: `
        width-50
        height-50
        border-1
        border-style-dashed
        border-white
        p-5
        border-radius-10
    `,
    classNameItemScanName: `
        font-nunito
        font-10
        text-capitalize
        color-white
    `,
};
