# ListItemScan

## Dependencies

[ListItemScan](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ListItemScan)

```js
import { ListItemScan } from '@/components/ListItemScan';
```

## Import

```js
import { ListItemScan, ListItemScanStyles } from '@/components/ListItemScan';
```

## Props

```tsx
interface ListItemScanProps {}
```

## Use

```js
<ListItemScan>ListItemScan</ListItemScan>
```
