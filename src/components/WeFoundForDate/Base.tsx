import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import { parseDateYYYYMMDD } from '@/functions/parseDate';
import { Date as SvgDate } from '@/svg/date';

export interface WeFoundForDateClassProps {
    classNameContent?: string;
    classNameTextDate?: string;
    classNameImgDate?: string;
    classNameResultText?: string;
    sizeImg?: number;
    styleTemplateTextDate?: TextStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateResults?: TextStyles | ThemesType;
}

export interface WeFoundForDateBaseProps {
    results?: number;
    date?: Date;
}

export interface WeFoundForDateProps
    extends WeFoundForDateClassProps,
        WeFoundForDateBaseProps {}

export const WeFoundForDateBase = ({
    classNameContent = '',
    classNameTextDate = '',
    classNameImgDate = '',
    classNameResultText = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateResults = Theme.styleTemplate ?? '_default',
    styleTemplateTextDate = Theme.styleTemplate ?? '_default',

    sizeImg = 7,
    results = 0,
    date = new Date(),
}: WeFoundForDateProps) => {
    return (
        <>
            <div className={classNameContent}>
                <Text styleTemplate={styleTemplateText} className="">
                    We found you
                </Text>
                <Text
                    styleTemplate={styleTemplateResults}
                    className={classNameResultText}
                >
                    {results} Results
                </Text>
                <Text
                    styleTemplate={styleTemplateTextDate}
                    className={classNameTextDate}
                >
                    for{' '}
                    <span className={classNameImgDate}>
                        <SvgDate size={sizeImg} />
                    </span>
                    {parseDateYYYYMMDD(date)}
                </Text>
            </div>
        </>
    );
};
export default WeFoundForDateBase;
