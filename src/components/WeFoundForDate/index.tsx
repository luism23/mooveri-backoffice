import { useMemo } from 'react';

import * as styles from '@/components/WeFoundForDate/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    WeFoundForDateBaseProps,
    WeFoundForDateBase,
} from '@/components/WeFoundForDate/Base';

export const WeFoundForDateStyle = { ...styles } as const;

export type WeFoundForDateStyles = keyof typeof WeFoundForDateStyle;

export interface WeFoundForDateProps extends WeFoundForDateBaseProps {
    styleTemplate?: WeFoundForDateStyles | ThemesType;
}

export const WeFoundForDate = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: WeFoundForDateProps) => {
    const Style = useMemo(
        () =>
            WeFoundForDateStyle[styleTemplate as WeFoundForDateStyles] ??
            WeFoundForDateStyle._default,
        [styleTemplate]
    );

    return <WeFoundForDateBase {...Style} {...props} />;
};
export default WeFoundForDate;
