# WeFoundForDate

## Dependencies

[Text](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Text)
[Date](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/svg/date)

```js
import { WeFoundForDate } from '@/components/WeFoundForDate';
```

## Import

```js
import {
    WeFoundForDate,
    WeFoundForDateStyles,
} from '@/components/WeFoundForDate';
```

## Props

```tsx
interface WeFoundForDateProps {
    results?: number;
    date?: Date;
}
```

## Use

```js
<WeFoundForDate>WeFoundForDate</WeFoundForDate>
```
