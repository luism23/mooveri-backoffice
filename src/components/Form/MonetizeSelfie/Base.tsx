import { useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataFormMonetizeSelfieForm } from '@/interfaces/FormMonetizeSelfieForm';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { InputTextStyles } from '@/components/Input/Text';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import Button, { ButtonStyles } from '@/components/Button';
import { FormMonetizeSelfieFormYup } from '@/validations/MonetizeSelfieForm';
import Theme, { ThemesType } from '@/config/theme';
import Text, { TextStyles } from '@/components/Text';
import InputAvatar, { InputAvatarStyles } from '@/components/Input/Avatar';
import InputFile from '@/components/Input/File';
export interface MonetizeSelfieFormClassProps {
    sizeContent?: number;
    classNameBtn?: string;
    classNameCol?: string;
    classNamecont?: string;
    classNameContent?: string;
    classNameContent2?: string;
    sizeContentInputs?: number;
    sizeContentBtn?: number;
    classNameContentCol?: string;
    classNameContentLink?: string;
    classNameContentCol2?: string;
    classNameContentInputs?: string;
    classNameContentInputs2?: string;
    inputs?: DataFormMonetizeSelfieForm<boolean>;
    styleTemplateText?: InputTextStyles | ThemesType;
    styleTemplateTexts?: TextStyles | ThemesType;
    styleTemplateBtn?: ButtonStyles | ThemesType;
    classNameContentBtn?: string;
    classNameCheckbox18?: string;
    classNameContenttext?: string;
    styleTemplateTitle?: TextStyles | ThemesType;
    classNameContentIcon?: string;
    styleTemplateAvatar?: InputAvatarStyles | ThemesType;
    classNameContentAvatar?: string;
    classNameContentBtnCamara?: string;
    sizeContentBtnCamara?: number;
    classNameContentCamara?: string;
}

export type onSubmintMonetizeSelfieForm = (
    data: DataFormMonetizeSelfieForm
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface MonetizeSelfieFormBaseProps {
    title?: string;
    BtnNext?: string;
    text?: string;
    icon?: any;
    iconCamera?: any;
    onSubmit?: onSubmintMonetizeSelfieForm;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    onClick?: (uuid?: string) => void;
    Next?: string;
    defaultValue?: DataFormMonetizeSelfieForm;
    onChange?: (data: DataFormMonetizeSelfieForm) => void;
    extraLoader?: boolean;
}
export interface MonetizeSelfieFormProps
    extends MonetizeSelfieFormBaseProps,
        MonetizeSelfieFormClassProps {}

export const MonetizeSelfieFormBase = ({
    Next,
    onClick,
    inputs = {
        avatar: true,
    },
    sizeContent = -1,
    sizeContentBtn = 400,
    sizeContentBtnCamara = 0,
    classNameContentBtnCamara = '',
    title = '',
    classNameContent = '',
    sizeContentInputs = -1,
    classNameContentInputs = '',
    styleTemplateBtn = Theme?.styleTemplate ?? '_default',
    styleTemplateAvatar = Theme?.styleTemplate ?? '_default',
    classNameContentBtn = '',
    classNameContentIcon = '',
    styleTemplateTexts = Theme.styleTemplate ?? '_default',
    icon = null,
    text = '',
    classNameContentAvatar = '',
    classNameContenttext = '',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    defaultValue = {},
    onChange,
    BtnNext = 'Next',
    iconCamera = '',
    extraLoader = undefined,
    classNameBtn = '',
    classNameContentCamara = '',
    ...props
}: MonetizeSelfieFormProps) => {
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);

    const { data, onChangeData } = useData<DataFormMonetizeSelfieForm>(
        defaultValue,
        {
            onChangeDataAfter: onChange,
        }
    );

    const MonetizeSelfieFormYupV = useMemo(
        () => FormMonetizeSelfieFormYup(data, inputs),
        [data]
    );

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataFormMonetizeSelfieForm>
                    id="MonetizeSelfieSelfieForm"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={MonetizeSelfieFormYupV.v}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.home);
                    }}
                >
                    <ContentWidth
                        size={sizeContentInputs}
                        className={classNameContentInputs}
                    >
                        <div className={classNameContenttext}>
                            <Text styleTemplate={styleTemplateTitle}>
                                {_t(title)}
                            </Text>
                        </div>
                        <div className={classNameContentAvatar}>
                            {data?.avatar?.fileData === '' ||
                            data?.avatar?.fileData == undefined ? (
                                <div className={classNameContentIcon}>
                                    {icon}
                                </div>
                            ) : (
                                <InputAvatar
                                    styleTemplate={styleTemplateAvatar}
                                    value={{
                                        fileData: data?.avatar?.fileData,
                                    }}
                                    useValue={true}
                                    defaultValue={data?.avatar?.fileData ?? ''}
                                    onSubmit={async (e) => {
                                        onChangeData('avatar')(e?.fileData);
                                        return {
                                            status: 'ok',
                                        };
                                    }}
                                />
                            )}
                        </div>

                        <div className={`${classNameContenttext}`}>
                            <Text styleTemplate={styleTemplateTexts}>
                                {_t(text)}
                            </Text>
                        </div>
                        <Space size={50} />
                        <ContentWidth
                            size={sizeContentBtnCamara}
                            className={classNameContentBtnCamara}
                        >
                            <InputFile
                                styleTemplate="tolinkme3"
                                accept={['png', 'jpg', 'jpeg', 'gif', 'webp']}
                                defaultValue={data?.avatar?.fileData ?? ''}
                                onChange={onChangeData('avatar')}
                            >
                                {(!data?.avatar ||
                                    data?.avatar?.fileData === '') && (
                                    <Button
                                        classNameBtn={classNameBtn}
                                        styleTemplate="tolinkme"
                                    >
                                        <span
                                            className={classNameContentCamara}
                                        >
                                            {iconCamera}
                                        </span>
                                        {_t('Take')}
                                    </Button>
                                )}
                            </InputFile>
                        </ContentWidth>
                    </ContentWidth>
                    <Space size={40} />
                    <ContentWidth
                        size={sizeContentBtn}
                        className={classNameContentBtn}
                    >
                        <Button
                            disabled={disabled}
                            styleTemplate={styleTemplateBtn}
                            loader={extraLoader ?? loader}
                            onClick={() => {
                                onClick?.(Next);
                            }}
                        >
                            {_t(BtnNext)}
                        </Button>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default MonetizeSelfieFormBase;
