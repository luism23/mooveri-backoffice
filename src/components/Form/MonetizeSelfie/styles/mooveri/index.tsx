import { MonetizeSelfieFormClassProps } from '@/components/Form/MonetizeSelfie/Base';

export const mooveri: MonetizeSelfieFormClassProps = {
    classNameContent: `
        m-h-auto
    `,
    sizeContentInputs: 330,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
};

export const mooveriCompany: MonetizeSelfieFormClassProps = {
    ...mooveri,
};
