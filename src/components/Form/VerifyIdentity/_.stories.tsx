import { Story, Meta } from "@storybook/react";

import { FormVerifyIdentityProps, FormVerifyIdentity } from "./index";

export default {
    title: "Form/VerifyIdentity",
    component: FormVerifyIdentity,
} as Meta;

const Template: Story<FormVerifyIdentityProps> = (args) => <FormVerifyIdentity {...args} />;

export const Index = Template.bind({});
Index.args = {
   
    
};
