import { useMemo } from 'react';

import * as styles from '@/components/Form/ProfileEditInf/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormProfileEditInfBaseProps,
    FormProfileEditInfBase,
} from '@/components/Form/ProfileEditInf/Base';

export const FormProfileEditInfStyle = { ...styles } as const;

export type FormProfileEditInfStyles = keyof typeof FormProfileEditInfStyle;

export interface FormProfileEditInfProps extends FormProfileEditInfBaseProps {
    styleTemplate?: FormProfileEditInfStyles | ThemesType;
}

export const FormProfileEditInf = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormProfileEditInfProps) => {
    const Style = useMemo(
        () =>
            FormProfileEditInfStyle[
                styleTemplate as FormProfileEditInfStyles
            ] ?? FormProfileEditInfStyle._default,
        [styleTemplate]
    );

    return <FormProfileEditInfBase {...Style} {...props} />;
};
export default FormProfileEditInf;
