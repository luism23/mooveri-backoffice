import { useMemo } from 'react';

import * as styles from '@/components/Form/ChatMesseges/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormChatMessegesBaseProps,
    FormChatMessegesBase,
} from '@/components/Form/ChatMesseges/Base';

export const FormChatMessegesStyle = { ...styles } as const;

export type FormChatMessegesStyles = keyof typeof FormChatMessegesStyle;

export interface FormChatMessegesProps extends FormChatMessegesBaseProps {
    styleTemplate?: FormChatMessegesStyles | ThemesType;
}

export const FormChatMesseges = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormChatMessegesProps) => {
    const Style = useMemo(
        () =>
            FormChatMessegesStyle[styleTemplate as FormChatMessegesStyles] ??
            FormChatMessegesStyle._default,
        [styleTemplate]
    );

    return <FormChatMessegesBase {...Style} {...props} />;
};
export default FormChatMesseges;
