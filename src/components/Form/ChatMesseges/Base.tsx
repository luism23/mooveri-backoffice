import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Form, SubmitResult } from '@/components/Form/Base';

import { ChatMessegesYup } from '@/validations/ChatMesseges';

import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';

import { DataChatMesseges } from '@/interfaces/ChatMesseges';
import { useData } from 'fenextjs-hook/cjs/useData';
import { UserLoginProps, useUser } from '@/hook/useUser';
import Plane from '@/svg/plane';

export type DataTypeFormChat = string;

export interface FormChatMessegesClassProps {
    classNameContentForm?: string;
    classNameContentInputs?: string;
    classNameContentInput?: string;
    classNameContentButton?: string;
    styleTemplateInput?: DataChatMesseges<InputTextStyles | ThemesType>;
    styleTemplateButton?: ButtonStyles | ThemesType;
    inputs?: DataChatMesseges<boolean>;
}

export type onSubmintChatMesseges = (
    user: UserLoginProps
) => (
    data: DataChatMesseges<DataTypeFormChat>
) => Promise<SubmitResult> | SubmitResult;

export interface FormChatMessegesBaseProps {
    defaultData?: DataChatMesseges<DataTypeFormChat>;
    onSubmit?: onSubmintChatMesseges;
    sizeIcon?: any;
}

export interface FormChatMessegesProps
    extends FormChatMessegesClassProps,
        FormChatMessegesBaseProps {}

export const FormChatMessegesBase = ({
    classNameContentForm = '',
    classNameContentInputs = '',
    classNameContentInput = '',
    classNameContentButton = '',
    styleTemplateInput = {
        chat: Theme.styleTemplate ?? '_default',
    },
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    inputs = {
        chat: true,
    },
    sizeIcon,

    defaultData = {},
    ...props
}: FormChatMessegesProps) => {
    const { user } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } =
        useData<DataChatMesseges<DataTypeFormChat>>(defaultData);

    const ChatMessegesYupV = useMemo(
        () => ChatMessegesYup(data, inputs),
        [data]
    );

    return (
        <>
            <div className={classNameContentForm}>
                <Form<DataChatMesseges<DataTypeFormChat>>
                    id="chat-messeges"
                    data={data}
                    onSubmit={props?.onSubmit?.(user)}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={ChatMessegesYupV.v}
                    onAfterSubmit={() => {
                        onChangeData('chat')('');
                    }}
                >
                    <div className={classNameContentInputs}>
                        <div className={classNameContentInput}>
                            {inputs.chat ? (
                                <>
                                    <InputText
                                        placeholder="Write a Message"
                                        onChange={onChangeData('chat')}
                                        value={data.chat as string}
                                        styleTemplate={
                                            styleTemplateInput.chat as InputTextStyles
                                        }
                                        yup={null}
                                    />
                                </>
                            ) : (
                                <></>
                            )}
                        </div>
                        <div className={classNameContentButton}>
                            <Button
                                styleTemplate={styleTemplateButton}
                                disabled={disabled}
                                loader={loader}
                            >
                                <Plane size={sizeIcon} />
                            </Button>
                        </div>
                    </div>
                </Form>
            </div>
        </>
    );
};
export default FormChatMessegesBase;
