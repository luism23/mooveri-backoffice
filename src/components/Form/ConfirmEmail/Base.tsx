import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Title, TitleStyles, TitleStylesType } from '@/components/Title';
import { Text, TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { useLang } from '@/lang/translate';

import Theme, { ThemesType } from '@/config/theme';
import { LinkStyles } from '@/components/Link';
import { InputTelStyles } from '@/components/Input/Tel';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import InputEmail from '@/components/InputYup/Email';
import { useData } from 'fenextjs-hook/cjs/useData';
import { DataConfirmEmail } from '@/interfaces/ConfirmEmail';
import { ConfirEmailYup } from '@/validations/ConfirmEmail';

export interface FormConfirmEmailClassProps {
    classNameContent?: string;
    classNameTitle?: string;
    classNameContentText?: string;
    classNameText?: string;

    typeStyleTemplateTitle?: TitleStylesType;
    styleTemplateTitle?: TitleStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateInput?: InputTelStyles | ThemesType;
    styleTemplateLink?: LinkStyles;
    styleTemplateButton?: ButtonStyles | ThemesType;
}

export type onSubmintConfirmEmail = (
    data: DataConfirmEmail,
    user: UserLoginProps
) => Promise<SubmitResult> | SubmitResult;

export interface FormConfirmEmailBaseProps {
    onSubmit: onSubmintConfirmEmail;
    token?: string;
    urlRedirect?: string;
}

export interface FormConfirmEmailProps
    extends FormConfirmEmailBaseProps,
        FormConfirmEmailClassProps {}

export const FormConfirmEmailBase = ({
    classNameContent = '',
    classNameTitle = '',
    classNameContentText = '',
    classNameText = '',

    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateInput = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    typeStyleTemplateTitle = 'h1',

    ...props
}: FormConfirmEmailProps) => {
    const route = useRouter();
    const _t = useLang();
    const { user } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataConfirmEmail>({
        email: '',
    });

    const YUP = useMemo(
        () =>
            ConfirEmailYup(data, {
                email: true,
            }),
        [data]
    );

    return (
        <>
            <ContentWidth size={350} className={classNameContent}>
                <Form<DataConfirmEmail>
                    id="confirm-phone"
                    data={data}
                    onSubmit={async (data: DataConfirmEmail) => {
                        return await props.onSubmit(data, user);
                    }}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={YUP}
                    onAfterSubmit={() => {
                        route.push(
                            `${props?.urlRedirect ?? url.confirmEmail.code}`
                        );
                    }}
                >
                    <Title
                        typeStyle={typeStyleTemplateTitle}
                        styleTemplate={styleTemplateTitle}
                        className={classNameTitle}
                    >
                        {_t('Confirm your Email')}
                    </Title>
                    <Space size={15} />
                    <ContentWidth size={230} className={classNameContentText}>
                        <Text
                            styleTemplate={styleTemplateText}
                            className={classNameText}
                        >
                            {_t(
                                'We will send you an Email with a code to confirm your identity'
                            )}
                        </Text>
                    </ContentWidth>
                    <Space size={15} />
                    <InputEmail
                        styleTemplate={styleTemplateInput}
                        placeholder={_t('Email')}
                        onChange={onChangeData('email')}
                    />
                    <Space size={14} />
                    <Button
                        styleTemplate={styleTemplateButton}
                        disabled={disabled}
                        loader={loader}
                    >
                        {_t('Send Email Activation code')}
                    </Button>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormConfirmEmailBase;
