import { useMemo } from 'react';

import * as styles from '@/components/Form/PersonalEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormPersonalEditBaseProps,
    FormPersonalEditBase,
} from '@/components/Form/PersonalEdit/Base';

export const FormPersonalEditStyle = { ...styles } as const;

export type FormPersonalEditStyles = keyof typeof FormPersonalEditStyle;

export interface FormPersonalEditProps extends FormPersonalEditBaseProps {
    styleTemplate?: FormPersonalEditStyles | ThemesType;
}

export const FormPersonalEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormPersonalEditProps) => {
    const Style = useMemo(
        () =>
            FormPersonalEditStyle[styleTemplate as FormPersonalEditStyles] ??
            FormPersonalEditStyle._default,
        [styleTemplate]
    );

    return <FormPersonalEditBase {...Style} {...props} />;
};
export default FormPersonalEdit;
