import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import {
    InputPassword,
    InputRepeatPassword,
} from '@/components/InputYup/Password';
import { Title, TitleStylesType, TitleStyles } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataChangePassword } from '@/interfaces/ChangePassword';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { ChangePasswordYup } from '@/validations/ChangePassword';
import { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import { useRouter } from 'next/router';
import url from '@/data/routes';

export interface FormChangePasswordClassProps {
    classNameContent?: string;
    classNameTitle?: string;

    styleTemplateButton?: ButtonStyles | ThemesType;
    typeStyleTemplateTitle?: TitleStylesType;
    styleTemplateTitle?: TitleStyles | ThemesType;

    styleTemplateInputs?: DataChangePassword<InputTextStyles | ThemesType>;
    inputs?: DataChangePassword<boolean>;
}

export type onSubmintChangePassword = (
    data: DataChangePassword
) => Promise<SubmitResult> | SubmitResult;

export interface FormChangePasswordBaseProps {
    onSubmit: onSubmintChangePassword;
    token?: string;
    urlRedirect?: string;
}
export interface FormChangePasswordProps
    extends FormChangePasswordBaseProps,
        FormChangePasswordClassProps {}

export const FormChangePasswordBase = ({
    classNameContent = '',
    classNameTitle = '',

    typeStyleTemplateTitle = 'h1',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',

    styleTemplateButton = Theme.styleTemplate ?? '_default',

    styleTemplateInputs = {
        password: Theme.styleTemplate ?? '_default',
        repeatPassword: Theme.styleTemplate ?? '_default',
    },
    inputs = {
        password: true,
        repeatPassword: true,
    },

    token,
    urlRedirect,
    ...props
}: FormChangePasswordProps) => {
    const _t = useLang();
    const router = useRouter();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataChangePassword>({
        token,
        password: '',
        repeatPassword: '',
    });

    const ChangePasswordYupV = useMemo(
        () => ChangePasswordYup(data, inputs),
        [data]
    );

    return (
        <>
            <ContentWidth size={300} className={classNameContent}>
                <Form<DataChangePassword>
                    id="change-password"
                    data={data}
                    onSubmit={props.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={ChangePasswordYupV}
                    onAfterSubmit={() => {
                        router.push(urlRedirect ?? url.login);
                    }}
                >
                    <Title
                        typeStyle={typeStyleTemplateTitle}
                        className={classNameTitle}
                        styleTemplate={styleTemplateTitle}
                    >
                        {_t('Change Password')}
                    </Title>
                    <Space size={12} />

                    {inputs.password ? (
                        <>
                            <InputPassword
                                styleTemplate={styleTemplateInputs.password}
                                defaultValue={data.password}
                                placeholder={_t('New Password')}
                                onChange={onChangeData('password')}
                            />
                            <Space size={6} />
                        </>
                    ) : (
                        <></>
                    )}
                    {inputs.repeatPassword ? (
                        <>
                            <InputRepeatPassword
                                styleTemplate={
                                    styleTemplateInputs.repeatPassword
                                }
                                defaultValue={data.repeatPassword}
                                password={data.password}
                                placeholder={_t('Repeat Password')}
                                onChange={onChangeData('repeatPassword')}
                            />
                            <Space size={6} />
                        </>
                    ) : (
                        <></>
                    )}
                    <Space size={6} />

                    <Button
                        styleTemplate={styleTemplateButton}
                        disabled={disabled}
                        loader={loader}
                    >
                        {_t('Save')}
                    </Button>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormChangePasswordBase;
