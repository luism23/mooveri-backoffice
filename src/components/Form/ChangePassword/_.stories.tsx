import { Story, Meta } from "@storybook/react";

import { FormChangePasswordProps, FormChangePassword } from "./index";
import { DataChangePassword } from "@/interfaces/ChangePassword";
import log from "@/functions/log";

export default {
    title: "Form/ChangePassword",
    component: FormChangePassword,
} as Meta;

const Template: Story<FormChangePasswordProps> = (args) => <FormChangePassword {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataChangePassword)=> {
        log("DataChangePassword",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "register ok",
        };
    }
};
