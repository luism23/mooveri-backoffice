import { FormSuccesfullActivatedClassProps } from '@/components/Form/SuccesfullActivated/Base';

export const mooveri: FormSuccesfullActivatedClassProps = {
    classNameContent: `
    m-h-auto 
    `,
    classNameContentTitle: `
    text-center color-white
    `,
    classNameContentImg: `
    m-h-auto
    `,
    imgFace: 'face.png',
    styleTemplateImgFace: 'mooveri',
    classNameImg: `
    `,
    classNameLink: `
        text-decoration-none
                
    `,
    styleTemplateLink: 'mooveri',
    styleTemplateButton: 'mooveri',
    styleTemplateTitle: 'mooveri',
    typeStyleTemplateTitle: 'h7',
};
