import { Story, Meta } from "@storybook/react";

import { FormSuccesfullActivatedProps, FormSuccesfullActivated } from "./index";


export default {
    title: "Form/SuccesfullActivated",
    component: FormSuccesfullActivated,
} as Meta;

const Template: Story<FormSuccesfullActivatedProps> = (args) => (
    <FormSuccesfullActivated {...args} />
);

export const Index = Template.bind({});
Index.args = {
}
