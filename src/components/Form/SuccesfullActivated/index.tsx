import { useMemo } from 'react';

import * as styles from '@/components/Form/SuccesfullActivated/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormSuccesfullActivatedBaseProps,
    FormSuccesfullActivatedBase,
} from '@/components/Form/SuccesfullActivated/Base';

export const FormSuccesfullActivatedStyle = { ...styles } as const;

export type FormSuccesfullActivatedStyles =
    keyof typeof FormSuccesfullActivatedStyle;

export interface FormSuccesfullActivatedProps
    extends FormSuccesfullActivatedBaseProps {
    styleTemplate?: FormSuccesfullActivatedStyles | ThemesType;
}

export const FormSuccesfullActivated = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormSuccesfullActivatedProps) => {
    const Style = useMemo(
        () =>
            FormSuccesfullActivatedStyle[
                styleTemplate as FormSuccesfullActivatedStyles
            ] ?? FormSuccesfullActivatedStyle._default,
        [styleTemplate]
    );

    return <FormSuccesfullActivatedBase {...Style} {...props} />;
};
export default FormSuccesfullActivated;
