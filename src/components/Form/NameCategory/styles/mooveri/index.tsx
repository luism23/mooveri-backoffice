import { FormNameCategoryClassProps } from '@/components/Form/NameCategory/Base';

export const mooveri: FormNameCategoryClassProps = {
    classNameContent: `
    m-h-auto
  `,
    classNameTitle: `
  text-center color-white
  `,
    styleTemplateTitle: 'mooveri',
    typeStyleTemplateTitle: 'h7',
    classNameContentText: `
  m-h-auto
  `,
    classNameText: `
  text-center
  `,
    styleTemplateInput: 'mooveri',
    styleTemplateButton: 'mooveri',
};
