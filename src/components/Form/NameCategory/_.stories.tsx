import { Story, Meta } from "@storybook/react";

import { FormNameCategoryProps, FormNameCategory } from "./index";
import { DataNameCategory } from "@/interfaces/NameCategory";
import log from "@/functions/log";

export default {
    title: "Form/NameCategory",
    component: FormNameCategory,
} as Meta;

const Template: Story<FormNameCategoryProps> = (args) => (
    <FormNameCategory {...args} />
);


export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataNameCategory)=> {
        log("DataNameCategory",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "register ok",
        };
    }
};
