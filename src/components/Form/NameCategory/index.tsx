import { useMemo } from 'react';

import * as styles from '@/components/Form/NameCategory/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormNameCategoryBaseProps,
    FormNameCategoryBase,
} from '@/components/Form/NameCategory/Base';

export const FormNameCategoryStyle = { ...styles } as const;

export type FormNameCategoryStyles = keyof typeof FormNameCategoryStyle;

export interface FormNameCategoryProps extends FormNameCategoryBaseProps {
    styleTemplate?: FormNameCategoryStyles | ThemesType;
}

export const FormNameCategory = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormNameCategoryProps) => {
    const Style = useMemo(
        () =>
            FormNameCategoryStyle[styleTemplate as FormNameCategoryStyles] ??
            FormNameCategoryStyle._default,
        [styleTemplate]
    );

    return <FormNameCategoryBase {...Style} {...props} />;
};
export default FormNameCategory;
