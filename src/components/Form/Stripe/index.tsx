import { useMemo } from 'react';

import * as styles from '@/components/Form/Stripe/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormStripeBaseProps,
    FormStripeBase,
} from '@/components/Form/Stripe/Base';

export const FormStripeStyle = { ...styles } as const;

export type FormStripeStyles = keyof typeof FormStripeStyle;

export interface FormStripeProps extends FormStripeBaseProps {
    styleTemplate?: FormStripeStyles | ThemesType;
}

export const FormStripe = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormStripeProps) => {
    const Style = useMemo(
        () =>
            FormStripeStyle[styleTemplate as FormStripeStyles] ??
            FormStripeStyle._default,
        [styleTemplate]
    );

    return <FormStripeBase {...Style} {...props} />;
};
export default FormStripe;
