import { useMemo, useState } from 'react';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { PaymentMethodResult } from '@stripe/stripe-js';
import { DataFormStripe } from '@/interfaces/FormStripe';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import { FormStripeYup } from '@/validations/FormStripe';
import Theme, { ThemesType } from '@/config/theme';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import Text, { TextStyles } from '@/components/Text';
import InputTel from '@/components/Input/Tel';
import { InputSelectCSC } from 'fenextjs-component/cjs/Input/SelectCSC';
import { useNotification } from '@/hook/useNotification';
import Stripe from '@/components/Input/Stripe';
import { POST_STRIPE_CARD } from '@/api/tolinkme/Payment';

export interface FormStripeClassProps {
    text2?: string;
    text3?: string;
    inputs?: DataFormStripe<boolean>;
    classNameContent?: string;
    classNameTitle?: string;
    styleTemplateText?: InputTextStyles | ThemesType;
    styleTemplateTitle?: TextStyles | ThemesType;
    sizeContentInputs?: number;
    classNameContentInputs?: string;
}

export type onSubmintStripe = (
    data: DataFormStripe
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormStripeBaseProps {
    text?: string;
    onSubmit?: onSubmintStripe | undefined;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    defaultValue?: DataFormStripe;
    loader?: boolean;
}

export interface FormStripeProps
    extends FormStripeBaseProps,
        FormStripeClassProps {}

export interface CustomSubmitResult<T> extends SubmitResult<T> {
    paymentId?: string;
}

export const FormStripeBase = ({
    inputs = {
        LastName: true,
        MiddleName: true,
        firstName: true,
        phone: true,
        country: true,
        state: true,
        city: true,
        stripe_before: true,
    },
    classNameContent = '',
    classNameTitle = '',
    classNameContentInputs = '',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    sizeContentInputs = -1,
    ...props
}: FormStripeProps) => {
    const { user } = useUser();
    const { pop } = useNotification();
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData, setData } = useData<DataFormStripe>({
        stripe_before: {
            complete: false,
        },
    });
    const FormStripeYupV = useMemo(() => FormStripeYup(data, inputs), [data]);

    const onSaveCard = async (stripe_after: PaymentMethodResult) => {
        setLoader(true);
        const result: SubmitResult = await POST_STRIPE_CARD({
            user,
            data: {
                ...data,
                stripe_after,
            },
        });

        pop({
            type: 'ok',
            message: _t(result?.message ?? ''),
        });
        setLoader(false);
    };
    return (
        <>
            <ContentWidth className={classNameContent}>
                <Form<DataFormStripe>
                    id="Stripe"
                    data={{ ...data }}
                    //onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={FormStripeYupV.v}
                    onAfterSubmit={async (
                        data: CustomSubmitResult<UserLoginProps>
                    ) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.home);
                    }}
                >
                    <ContentWidth
                        size={sizeContentInputs}
                        className={`formStripeContent ${classNameContentInputs}`}
                    >
                        <div
                            className={`formStripeContentDiv1 ${classNameContent}`}
                        >
                            <Text
                                className={classNameTitle}
                                styleTemplate={styleTemplateTitle}
                            >
                                {_t('Name')}
                            </Text>
                            <InputText
                                placeholder="First Name"
                                yup={FormStripeYupV.y.firstName}
                                defaultValue={data.firstName}
                                onChange={onChangeData('firstName')}
                                styleTemplate={styleTemplateText}
                            />
                            <InputText
                                placeholder="Middle Name"
                                yup={FormStripeYupV.y.MiddleName}
                                defaultValue={data.MiddleName}
                                onChange={onChangeData('MiddleName')}
                                styleTemplate={styleTemplateText}
                            />
                            <InputText
                                placeholder="Last Name"
                                yup={FormStripeYupV.y.LastName}
                                defaultValue={data.LastName}
                                onChange={onChangeData('LastName')}
                                styleTemplate={styleTemplateText}
                            />
                        </div>
                        <div
                            className={`formStripeContentDiv2 ${classNameContent}`}
                        >
                            <Text
                                className={classNameTitle}
                                styleTemplate={styleTemplateTitle}
                            >
                                {_t('Bank Account Details')}
                            </Text>
                            <Stripe
                                onSaveCard={onSaveCard}
                                onChange={onChangeData('stripe_before')}
                                textBtn="Confirm"
                                disabled={disabled}
                            />
                        </div>

                        <div
                            className={`formStripeContentDiv3 ${classNameContent}`}
                        >
                            <Text
                                className={classNameTitle}
                                styleTemplate={styleTemplateTitle}
                            >
                                {_t('Address')}
                            </Text>
                            <InputSelectCSC
                                typeSelect="select"
                                onChange={(csc) => {
                                    setData({
                                        ...data,
                                        ...csc,
                                    });
                                }}
                                defaultValue={{
                                    country: data.country,
                                    state: data.state,
                                    city: data.city,
                                }}
                                useContainer={false}
                                _t={_t}
                            />
                        </div>
                        <div
                            className={`formStripeContentDiv4 ${classNameContent}`}
                        >
                            <Text
                                className={classNameTitle}
                                styleTemplate={styleTemplateTitle}
                            >
                                {_t('Contact Details')}
                            </Text>
                            <InputTel
                                placeholder="Mobile Number (For SMS)"
                                yup={FormStripeYupV.y.phone}
                                defaultValue={data.phone}
                                onChange={onChangeData('phone')}
                                styleTemplate="tolinkme6"
                            />
                        </div>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};

export default FormStripeBase;
