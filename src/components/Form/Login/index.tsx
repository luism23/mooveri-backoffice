import { useMemo } from 'react';

import * as styles from '@/components/Form/Login/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormLoginBaseProps,
    FormLoginBase,
} from '@/components/Form/Login/Base';

export const FormLoginStyle = { ...styles } as const;

export type FormLoginStyles = keyof typeof FormLoginStyle;

export interface FormLoginProps extends FormLoginBaseProps {
    styleTemplate?: FormLoginStyles | ThemesType;
}

export const FormLogin = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormLoginProps) => {
    const Style = useMemo(
        () =>
            FormLoginStyle[styleTemplate as FormLoginStyles] ??
            FormLoginStyle._default,
        [styleTemplate]
    );

    return <FormLoginBase {...Style} {...props} />;
};
export default FormLogin;
