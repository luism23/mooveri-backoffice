import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';

import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';

import url from '@/data/routes';

import Formstep1Base, { Formstep1ClassProps } from './steps/step1';
import Formstep2Base, { Formstep2ClassProps } from './steps/step2';
import Formstep3Base, { Formstep3ClassProps } from './steps/step3';

import Theme, { ThemesType } from '@/config/theme';
import BarSteps from '@/components/BarSteps';
import { DataFormConfirm } from '@/interfaces/FormConfirm';
import { ConfirmStep1 } from '@/validations/ConfirmStep1';
import { ConfirmStep2 } from '@/validations/ConfirmStep2';
import { ConfirmStep3 } from '@/validations/ConfirmStep3';
import { DataFormConfirmStep1Props } from '@/interfaces/FormConfirmStep1';
import { DataFormConfirmStep2Props } from '@/interfaces/FormConfirmStep2';
import { DataFormConfirmStep3Props } from '@/interfaces/FormConfirmStep3';
import Link, { LinkStyles } from '@/components/Link';

export interface FormCompanyConfirmClassProps {
    classNameContent?: string;
    classNameBtn?: string;
    classNameContentButtons?: string;
    classNameContentButtonPre?: string;
    classNameContentButtonNext?: string;
    classNameCircleDisabled?: string;
    classNameSkip?: string;
    skip?: string;
    styleStep1?: Formstep1ClassProps;
    styleStep2?: Formstep2ClassProps;
    styleStep3?: Formstep3ClassProps;
    iconButtonPre?: any;
    iconButtonNext?: any;
    styleTemplateButtonPre?: ButtonStyles | ThemesType;
    styleTemplateButtonNext?: ButtonStyles | ThemesType;
    styleTemplateSkips?: LinkStyles | ThemesType;

    inputs?: DataFormConfirm<boolean>;
}

export type onSubmintCompanyConfirm = (
    data: DataFormConfirm
) => Promise<SubmitResult<DataFormConfirm>> | SubmitResult<DataFormConfirm>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormCompanyConfirmBaseProps {
    onSubmit?: onSubmintCompanyConfirm;
    onValidateUsername?: onValidateUsername;
    urlRediret?: string;
}
export interface FormCompanyConfirmProps
    extends FormCompanyConfirmBaseProps,
        FormCompanyConfirmClassProps {}

export const FormCompanyConfirmBase = ({
    classNameContent = '',
    classNameContentButtons = '',
    classNameContentButtonPre = '',
    classNameContentButtonNext = '',
    classNameBtn = '',
    classNameSkip = '',
    styleTemplateButtonPre = Theme.styleTemplate ?? '_default',
    styleTemplateButtonNext = Theme.styleTemplate ?? '_default',
    styleTemplateSkips = Theme.styleTemplate ?? '_default',

    styleStep1 = {},
    styleStep2 = {},
    styleStep3 = {},
    iconButtonPre,
    iconButtonNext,
    inputs = {
        companyName: true,
        legalName: true,
        yearFounded: true,
        contactName: true,
        city: true,
        numberTrucks: true,
        numberWorkers: true,
        phone: true,
        phoneAlternative: true,
    },
    skip = url.myAccount.index,
    ...props
}: FormCompanyConfirmProps) => {
    const _t = useLang();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const [step, setStep] = useState<number>(0);
    const { data, setData } = useData<DataFormConfirm<any>>({});
    const ArrayForm = useMemo(
        () => [
            <Formstep1Base
                key={1}
                {...styleStep1}
                defaultData={data as DataFormConfirmStep1Props}
                onChange={(nData) => {
                    setData({
                        ...data,
                        ...nData,
                    });
                }}
            />,
            <Formstep2Base
                key={2}
                {...styleStep2}
                defaultData={data as DataFormConfirmStep2Props}
                onChange={(nData) => {
                    setData({
                        ...data,
                        ...nData,
                    });
                }}
            />,
            <Formstep3Base
                key={3}
                {...styleStep3}
                defaultData={data as DataFormConfirmStep3Props}
                onChange={(nData) => {
                    setData({
                        ...data,
                        ...nData,
                    });
                }}
            />,
        ],
        [data]
    );
    const FormSelect = useMemo(() => ArrayForm[step], [step, ArrayForm]);

    const ArrayYup = [ConfirmStep1, ConfirmStep2, ConfirmStep3];
    const YupSelect = useMemo(
        () => ArrayYup[step](data, inputs),
        [data, inputs, step, ArrayYup]
    );

    const next = () => {
        setStep((pre) => Math.min(pre + 1, ArrayForm.length - 1));
    };
    const pre = () => {
        setStep((pre) => Math.max(pre - 1, 0));
    };

    return (
        <>
            <div className={classNameContent}>
                <Space size={10} />
                <BarSteps nItems={3} nActive={1} />

                <Link
                    className={classNameSkip}
                    href={skip}
                    styleTemplate={styleTemplateSkips}
                >
                    {_t('Skip')}
                </Link>
                <Form<DataFormConfirm>
                    data={data}
                    yup={YupSelect.v}
                    disabled={disabled || step < ArrayForm.length - 1}
                    onSubmit={props?.onSubmit}
                    loader={loader}
                    onChangeLoader={setLoader}
                    onChangeDisable={setDisabled}
                >
                    {FormSelect}
                </Form>
                <Space size={20} />
                <div className={classNameContentButtons}>
                    <div className={classNameContentButtonPre}>
                        {step != 0 ? (
                            <Button
                                styleTemplate={styleTemplateButtonPre}
                                classNameBtn={classNameBtn}
                                icon={iconButtonPre}
                                iconPosition="left"
                                onClick={pre}
                                isBtn={false}
                                loader={loader}
                            >
                                {_t('Previus')}
                            </Button>
                        ) : (
                            <></>
                        )}
                    </div>
                    <div className={classNameContentButtonNext}>
                        {step == ArrayForm.length - 1 ? (
                            <Button
                                styleTemplate={styleTemplateButtonNext}
                                classNameBtn={classNameBtn}
                                icon={iconButtonNext}
                                iconPosition="right"
                                disabled={disabled}
                                loader={loader}
                            >
                                {_t('Send')}
                            </Button>
                        ) : (
                            <Button
                                styleTemplate={styleTemplateButtonNext}
                                classNameBtn={classNameBtn}
                                icon={iconButtonNext}
                                iconPosition="right"
                                disabled={disabled}
                                loader={loader}
                                onClick={next}
                                isBtn={false}
                            >
                                {_t('Next')}
                            </Button>
                        )}
                    </div>
                </div>
            </div>
        </>
    );
};
export default FormCompanyConfirmBase;
