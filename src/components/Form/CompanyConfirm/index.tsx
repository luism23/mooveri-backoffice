import { useMemo } from 'react';

import * as styles from '@/components/Form/CompanyConfirm/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormCompanyConfirmBaseProps,
    FormCompanyConfirmBase,
} from '@/components/Form/CompanyConfirm/Base';

export const FormCompanyConfirmStyle = { ...styles } as const;

export type FormCompanyConfirmStyles = keyof typeof FormCompanyConfirmStyle;

export interface FormCompanyConfirmProps extends FormCompanyConfirmBaseProps {
    styleTemplate?: FormCompanyConfirmStyles | ThemesType;
}

export const FormCompanyConfirm = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormCompanyConfirmProps) => {
    const Style = useMemo(
        () =>
            FormCompanyConfirmStyle[
                styleTemplate as FormCompanyConfirmStyles
            ] ?? FormCompanyConfirmStyle._default,
        [styleTemplate]
    );

    return <FormCompanyConfirmBase {...Style} {...props} />;
};
export default FormCompanyConfirm;
