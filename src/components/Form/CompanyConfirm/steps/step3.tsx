import { SubmitResult } from '@/components/Form/Base';
import InputNumber from '@/components/Input/Number';
import Space from '@/components/Space';
import Text, { TextStyles } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';

import { DataFormConfirmStep3Props } from '@/interfaces/FormConfirmStep3';
import { useLang } from '@/lang/translate';
import { ConfirmStep3 } from '@/validations/ConfirmStep3';
import { useMemo } from 'react';

export interface Formstep3ClassProps {
    classNameContent?: string;
    classNameText?: string;
    classNameTextSub?: string;
    classNameContentCol?: string;
    styleTemplateTextHello?: TextStyles | ThemesType;
    styleTemplateTexBusiness?: TextStyles | ThemesType;

    inputs?: DataFormConfirmStep3Props<boolean>;
}

export type onSubmintstep3 = () =>
    | Promise<SubmitResult<DataFormConfirmStep3Props>>
    | SubmitResult<DataFormConfirmStep3Props>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface Formstep3BaseProps {
    defaultData?: DataFormConfirmStep3Props;
    onSubmit?: onSubmintstep3;
    onChange?: (data: DataFormConfirmStep3Props<string | number>) => void;
    onValidateUsername?: onValidateUsername;
    urlRediret?: string;
}
export interface Formstep3Props
    extends Formstep3BaseProps,
        Formstep3ClassProps {}

export const Formstep3Base = ({
    classNameContent = '',
    classNameTextSub = '',
    classNameText = '',
    classNameContentCol = '',

    styleTemplateTextHello = Theme.styleTemplate ?? '_default',
    styleTemplateTexBusiness = Theme.styleTemplate ?? '_default',
    defaultData = {},
    inputs = {
        numberTrucks: true,
        numberWorkers: true,
    },
    ...props
}: Formstep3Props) => {
    const _t = useLang();
    const { data, onChangeData } = useData<
        DataFormConfirmStep3Props<string | number>
    >(defaultData, {
        onChangeDataAfter: props.onChange,
    });

    const ConfirmStep3YupV = useMemo(() => ConfirmStep3(data, inputs), [data]);
    return (
        <>
            <div className={classNameContent}>
                <Space size={25} />
                <Text
                    styleTemplate={styleTemplateTextHello}
                    className={classNameText}
                >
                    <span className={classNameTextSub}>
                        {_t('This information is important.')}
                    </span>
                </Text>
                <Space size={25} />
                <div className={classNameContentCol}>
                    <Text styleTemplate={styleTemplateTexBusiness}>
                        {_t('Legal Info')}
                    </Text>
                </div>
                <Space size={25} />
                <div className={classNameContentCol}>
                    <div className="width-p-100">
                        <InputNumber
                            styleTemplate="mooveriInputNumber"
                            min={1}
                            val={data.numberTrucks as number}
                            onChange={onChangeData('numberTrucks')}
                            yup={ConfirmStep3YupV.y.numberTrucks}
                        />
                        <Space size={25} />
                        <InputNumber
                            styleTemplate="mooveriInputNumber"
                            min={1}
                            val={data.numberWorkers as number}
                            onChange={onChangeData('numberWorkers')}
                            yup={ConfirmStep3YupV.y.numberWorkers}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default Formstep3Base;
