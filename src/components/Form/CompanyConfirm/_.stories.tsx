import { Story, Meta } from "@storybook/react";

import { FormCompanyConfirmProps, FormCompanyConfirm } from "./index";
import log from "@/functions/log";
import { DataFormConfirm } from "@/interfaces/FormConfirm";

export default {
    title: "Form/CompanyConfirm",
    component: FormCompanyConfirm,
} as Meta;

const Template: Story<FormCompanyConfirmProps> = (args) => <FormCompanyConfirm {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataFormConfirm)=> {
        log("DataLogin",data)
        await new Promise((r) => setTimeout(r, 3000));

        return {
            status: "ok",
            message: "Company confirm ok",
        };
    }
    

};
