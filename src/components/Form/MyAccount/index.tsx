import { useMemo } from 'react';

import * as styles from '@/components/Form/MyAccount/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormMyAccountBaseProps,
    FormMyAccountBase,
} from '@/components/Form/MyAccount/Base';

export const FormMyAccountStyle = { ...styles } as const;

export type FormMyAccountStyles = keyof typeof FormMyAccountStyle;

export interface FormMyAccountProps extends FormMyAccountBaseProps {
    styleTemplate?: FormMyAccountStyles | ThemesType;
}

export const FormMyAccount = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormMyAccountProps) => {
    const Style = useMemo(
        () =>
            FormMyAccountStyle[styleTemplate as FormMyAccountStyles] ??
            FormMyAccountStyle._default,
        [styleTemplate]
    );

    return <FormMyAccountBase {...Style} {...props} />;
};
export default FormMyAccount;
