import { Story, Meta } from "@storybook/react";

import { FormPayProps, FormPay } from "./index";

export default {
    title: "Form/Pay",
    component: FormPay,
} as Meta;

const Template: Story<FormPayProps> = (args) => <FormPay {...args} />;

export const Index = Template.bind({});
Index.args = {
    
};
