import { useMemo, useState } from 'react';

import Stripe from '@/components/Input/Stripe';
import { TitleStyles } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { DataFormPayment } from '@/interfaces/FormPayment';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { FormPaymentYup } from '@/validations/Payment';
import { UserLoginProps, useUser } from '@/hook/useUser';
import Theme, { ThemesType } from '@/config/theme';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import url from '@/data/routes';
import { DataRsLogin } from '@/interfaces/RsLogin';
import Text, { TextStyles } from '@/components/Text';
import InputCheckbox, {
    InputCheckboxStyles,
} from '@/components/Input/Checkbox';
import Exclamation from '@/svg/exclamation';
import Payment from '@/components/Payment';
import { Button } from 'fenextjs-component';
import { POST_STRIPE_CARD_USER } from '@/api/tolinkme/Payment';
import { useNotification } from '@/hook/useNotification';
import { PaymentMethodResult } from '@stripe/stripe-js';
import UnicornWealthy from '@/lottin/UnicornWealthy';
import Link from '@/components/Link';
import Verified from '@/svg/Verified';

export interface FormPaymentClassProps {
    sizeContent?: number;
    classNameContent?: string;
    classNameContentTitle?: string;
    classNameContentCheckbox?: string;
    classNameContentCol2?: string;
    classNameContentSvg?: string;
    styleTemplateTitle?: TitleStyles | ThemesType;
    styleTemplateInputCheckbox?: InputCheckboxStyles | ThemesType;
    sizeContentInputs?: number;
    classNameContentInputs?: string;
    styleTemplateInputs?: DataFormPayment<InputTextStyles | ThemesType>;
    inputs?: DataFormPayment<boolean>;
    classNameContentSizeText?: string;
    rsLogin?: DataRsLogin<boolean> | null;
    classNameTitle?: string;
    styleTemplateText?: TextStyles | ThemesType;
    text1?: string;
    text2?: string;
    text18?: string;
    LinkLogin?: string;
}

export type onSubmintPayment = (
    data: DataFormPayment
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface CustomSubmitResult<T> extends SubmitResult<T> {
    paymentId?: string;
}

export interface FormPaymentBaseProps {
    text?: string;
    onSubmit?: onSubmintPayment | undefined;
    urlRediret?: string;
    onValidateUsername?: onValidateUsername;
    defaultValue?: DataFormPayment;
    loader?: boolean;
    btn_uuid: string;
}
export interface FormPaymentProps
    extends FormPaymentBaseProps,
        FormPaymentClassProps {}

export const FormPaymentBase = ({
    sizeContent = -1,
    classNameContent = '',
    classNameContentTitle = '',
    classNameContentCheckbox = '',
    classNameTitle = '',
    text1 = 'Click here to confirm that you are at least',
    text2 = ' and the age of majority in your place of residence.',
    text18 = '18 years old',
    classNameContentCol2 = '',
    classNameContentSvg = '',
    classNameContentSizeText = '',
    //styleTemplateTitle = Theme.styleTemplate ?? '_default',
    sizeContentInputs = -1,
    classNameContentInputs = '',
    //styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateInputCheckbox = Theme.styleTemplate ?? '_default',
    inputs = {
        stripe_before: true,
        email: true,
        country: true,
        state: true,
        city: true,
        nameOnTheCard: true,
        Street: true,
        confirm18: true,
        zipPostalCode: true,
    },
    btn_uuid,
}: //...props
FormPaymentProps) => {
    const _t = useLang();
    const { user } = useUser();
    const { pop } = useNotification();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);

    const { data, onChangeData } = useData<DataFormPayment>({
        stripe_before: {
            complete: false,
        },
    });
    const [showForm, setShowForm] = useState(true);
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);

    const FormPaymentYupV = useMemo(() => FormPaymentYup(data, inputs), [data]);

    const onSaveCard = async (stripe_after: PaymentMethodResult) => {
        setLoader(true);
        const result: SubmitResult = await POST_STRIPE_CARD_USER({
            user,
            btn_uuid,
            data: {
                ...data,
                stripe_after,
            },
        });
        pop({
            type: result.status,
            message: _t(result?.message ?? ''),
        });
        setLoader(false);
        if (result.status === 'ok') {
            setShowForm(false);
            setShowSuccessMessage(true);
        }
    };

    return (
        <>
            {showForm && (
                <>
                    <ContentWidth
                        size={sizeContent}
                        className={classNameContent}
                    >
                        <Form<DataFormPayment>
                            id="Stripe"
                            data={{ ...data }}
                            disabled={disabled}
                            onChangeDisable={setDisabled}
                            loader={loader}
                            onChangeLoader={setLoader}
                            yup={FormPaymentYupV.v}
                            onAfterSubmit={async () => {}}
                        >
                            <Space size={30} />
                            <ContentWidth
                                className={classNameContentSizeText}
                                size={350}
                            >
                                <Text
                                    styleTemplate="tolinkme"
                                    className={classNameContentTitle}
                                >
                                    {_t(
                                        'We are fully compliant with Payment Card Industry Data Security Standards.'
                                    )}
                                </Text>
                            </ContentWidth>
                            {/*
                                 <Space size={12} />
                             <Text
                                 className={classNameTitle}
                                 styleTemplate="tolinkme55"
                             >
                                 {_t('Billing Details')}
                             </Text>
                                */}
                            <ContentWidth
                                size={sizeContentInputs}
                                className={`ContentPaymentForm ${classNameContentInputs}`}
                            >
                                {/*
                                      <InputSelectCSC
                                      typeSelect="select"
                                      onChange={(csc) => {
                                          setData({
                                              ...data,
                                              ...csc,
                                          });
                                      }}
                                      defaultValue={{
                                          country: data.country,
                                          state: data.state,
                                          city: data.city,
                                      }}
                                      useContainer={false}
                                      _t={_t}
                                  />
                                  <div className="FormAddPayment3">
                                      <InputText
                                          yup={FormPaymentYupV.y.Street}
                                          defaultValue={data.Street}
                                          placeholder={'Street'}
                                          onChange={onChangeData('Street')}
                                      />
                                  </div>
                                  <div className="FormAddPayment5">
                                      <InputText
                                          yup={FormPaymentYupV.y.zipPostalCode}
                                          defaultValue={data.zipPostalCode}
                                          onChange={onChangeData('zipPostalCode')}
                                          placeholder={'Zip/Postal Code'}
                                      />
                                  </div>
                                    */}
                                <div className={`FormAddPayment6`}>
                                    <Text
                                        className={classNameTitle}
                                        styleTemplate="tolinkme55"
                                    >
                                        {_t('Card Details')}
                                    </Text>
                                    {/*
                                         <InputText
                                           placeholder="Email"
                                           yup={FormPaymentYupV.y.email}
                                           defaultValue={data.email}
                                           onChange={onChangeData('email')}
                                       />
                                        */}
                                    <Space size={6} />
                                    <InputText
                                        placeholder="Name on the card"
                                        yup={FormPaymentYupV.y.nameOnTheCard}
                                        defaultValue={data.nameOnTheCard}
                                        onChange={onChangeData('nameOnTheCard')}
                                    />
                                    <Space size={6} />
                                    <Stripe
                                        onSaveCard={onSaveCard}
                                        onChange={onChangeData('stripe_before')}
                                        textBtn={_t('Pay Now')}
                                        disabled={disabled}
                                        sizeButton={370}
                                        options={{
                                            style: {
                                                base: {
                                                    iconColor: '#fff',
                                                    color: '#fff',
                                                    fontWeight: '500',

                                                    fontSize: '15px',

                                                    ':-webkit-autofill': {
                                                        color: '#fff',
                                                    },
                                                    '::placeholder': {
                                                        color: '#fff',
                                                    },
                                                },
                                                invalid: {
                                                    iconColor: '#da2424',
                                                    color: '#a90202',
                                                },
                                            },
                                        }}
                                    />
                                </div>
                            </ContentWidth>
                            <Space size={18} />
                            <div className={classNameContentCol2}>
                                <InputCheckbox
                                    label={
                                        <>
                                            <span className="text-left flex flex-wrap">
                                                <span className="p-r-2">
                                                    {_t(text1)}
                                                </span>
                                                <span className="font-w-900 p-r-2">
                                                    {_t(text18)}
                                                </span>
                                                {_t(text2)}
                                            </span>
                                        </>
                                    }
                                    defaultValue={data.confirm18}
                                    onChange={onChangeData('confirm18')}
                                    styleTemplate={styleTemplateInputCheckbox}
                                />
                            </div>
                            <Space size={10} />
                            <div className={classNameContentCheckbox}>
                                <div className={classNameContentSvg}>
                                    <Exclamation size={20} />
                                </div>
                                <div>
                                    <Text
                                        styleTemplate="tolinkme51"
                                        className="text-justify p-r-5"
                                    >
                                        {_t(
                                            `Tolinkme will make a one-time charge of $0.10 when adding your payment card. The charges on your credit card statement will appear as "Tolinkme".`
                                        )}
                                    </Text>
                                </div>
                            </div>
                            <Space size={10} />
                            <Payment />
                            <Space size={50} />
                        </Form>
                    </ContentWidth>
                </>
            )}

            {showSuccessMessage && (
                <ContentWidth className={classNameContentSizeText} size={350}>
                    <Space size={20} />
                    <Text
                        styleTemplate="tolinkme56"
                        className="flex flex-justify-center"
                    >
                        <span className="m-r-5 flex">
                            <Verified size={25} />
                        </span>
                        {_t('Successful payment')}
                    </Text>
                    <Space size={20} />
                    <ContentWidth className="m-auto" size={180}>
                        <UnicornWealthy />
                    </ContentWidth>
                    <Space size={20} />
                    <Link href={url.mySubscriptions}>
                        <Button
                            disabled={false}
                            className="bg-black color-white bg-black-hover"
                        >
                            {_t('Go to my')} {_t('Purchase')}
                        </Button>
                    </Link>
                </ContentWidth>
            )}
        </>
    );
};
export default FormPaymentBase;
