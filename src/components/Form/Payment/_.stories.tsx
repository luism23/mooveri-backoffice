import { Story, Meta } from "@storybook/react";

import { FormPaymentProps, FormPayment } from "./index";
import { DataFormPayment } from "@/interfaces/FormPayment";
import log from "@/functions/log";

export default {
    title: "Form/Payment",
    component: FormPayment,
} as Meta;

const Template: Story<FormPaymentProps> = (args) => <FormPayment {...args} />;

export const Index = Template.bind({});
Index.args = {
    
    onValidateUsername:  async (data: string)=> {
        log("onValidateUsername",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }
    },
    
    onSubmit:  async (data: DataFormPayment)=> {
        log("DataFormPayment",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "Payment ok",
        };
    },

};
