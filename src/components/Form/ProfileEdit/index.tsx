import { useMemo } from 'react';

import * as styles from '@/components/Form/ProfileEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormProfileEditBaseProps,
    FormProfileEditBase,
} from '@/components/Form/ProfileEdit/Base';

export const FormProfileEditStyle = { ...styles } as const;

export type FormProfileEditStyles = keyof typeof FormProfileEditStyle;

export interface FormProfileEditProps extends FormProfileEditBaseProps {
    styleTemplate?: FormProfileEditStyles | ThemesType;
}

export const FormProfileEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormProfileEditProps) => {
    const Style = useMemo(
        () =>
            FormProfileEditStyle[styleTemplate as FormProfileEditStyles] ??
            FormProfileEditStyle._default,
        [styleTemplate]
    );

    return <FormProfileEditBase {...Style} {...props} />;
};
export default FormProfileEdit;
