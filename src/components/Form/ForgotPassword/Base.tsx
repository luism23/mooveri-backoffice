import { useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputEmail } from '@/components/InputYup/Email';
import { Title, TitleProps } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataForgotPassword } from '@/interfaces/ForgotPassword';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { ForgotPasswordYup } from '@/validations/ForgotPassword';
import Text from '@/components/Text';
import { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import { useRouter } from 'next/router';
import url from '@/data/routes';

export interface FormForgotPasswordClassProps {
    classNameContentButtons?: string;
    sizeContent?: number;
    classNameContent?: string;
    classNameContentText?: string;
    classNameText?: string;

    styleTemplateEmail?: InputTextStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;

    TitleProps?: TitleProps;
}

export type onSubmintForgotPassword = (
    data: DataForgotPassword
) => Promise<SubmitResult> | SubmitResult;

export interface FormForgotPasswordBaseProps {
    onSubmit: onSubmintForgotPassword;
    urlRedirect?: string;
}

export interface FormForgotPasswordProps
    extends FormForgotPasswordBaseProps,
        FormForgotPasswordClassProps {}

export const FormForgotPasswordBase = ({
    classNameContent = '',
    sizeContent = 300,
    classNameContentText = '',
    classNameText = '',

    styleTemplateEmail = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    TitleProps = {},
    ...props
}: FormForgotPasswordProps) => {
    const router = useRouter();
    const _t = useLang();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataForgotPassword>({
        email: '',
    });

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataForgotPassword>
                    id="forgot-passwod"
                    data={data}
                    onSubmit={props.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={ForgotPasswordYup}
                    onAfterSubmit={() => {
                        router.push(props?.urlRedirect ?? url.forgotPasswordOk);
                    }}
                >
                    <Title {...TitleProps}>{_t('Forgot Password')}</Title>
                    <Space size={15} />
                    <ContentWidth size={230} className={classNameContentText}>
                        <Text className={classNameText}>
                            {_t(
                                'We will send you an email with a link with recovery instructions.'
                            )}
                        </Text>
                    </ContentWidth>
                    <Space size={19} />
                    <InputEmail
                        styleTemplate={styleTemplateEmail}
                        defaultValue={data.email}
                        placeholder={_t('Email')}
                        onChange={onChangeData('email')}
                    />
                    <Space size={12} />
                    <Button
                        styleTemplate={styleTemplateButton}
                        disabled={disabled}
                        loader={loader}
                    >
                        {_t('Send Link')}
                    </Button>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormForgotPasswordBase;
