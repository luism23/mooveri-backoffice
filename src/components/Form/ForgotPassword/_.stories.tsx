import { Story, Meta } from "@storybook/react";

import { FormForgotPasswordProps, FormForgotPassword } from "./index";
import { DataForgotPassword } from "@/interfaces/ForgotPassword";
import log from "@/functions/log";

export default {
    title: "Form/ForgotPassword",
    component: FormForgotPassword,
} as Meta;

const Template: Story<FormForgotPasswordProps> = (args) => <FormForgotPassword {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataForgotPassword)=> {
        log("DataForgotPassword",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "register ok",
        };
    }
};
