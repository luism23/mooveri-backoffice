import { useMemo } from 'react';

import * as styles from '@/components/Form/MonetizeCategory/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormMonetizeCategoryBaseProps,
    FormMonetizeCategoryBase,
} from '@/components/Form/MonetizeCategory/Base';

export const FormMonetizeCategoryStyle = { ...styles } as const;

export type FormMonetizeCategoryStyles = keyof typeof FormMonetizeCategoryStyle;

export interface FormMonetizeCategoryProps
    extends FormMonetizeCategoryBaseProps {
    styleTemplate?: FormMonetizeCategoryStyles | ThemesType;
}

export const FormMonetizeCategory = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormMonetizeCategoryProps) => {
    const Style = useMemo(
        () =>
            FormMonetizeCategoryStyle[
                styleTemplate as FormMonetizeCategoryStyles
            ] ?? FormMonetizeCategoryStyle._default,
        [styleTemplate]
    );

    return <FormMonetizeCategoryBase {...Style} {...props} />;
};
export default FormMonetizeCategory;
