import { Story, Meta } from "@storybook/react";

import { FormMonetizeCategoryProps, FormMonetizeCategory } from "./index";

export default {
    title: "Form/MonetizeCategory",
    component: FormMonetizeCategory,
} as Meta;

const Template: Story<FormMonetizeCategoryProps> = (args) => <FormMonetizeCategory {...args} />;

export const Index = Template.bind({});
Index.args = {
 
    
};
