import { useMemo } from 'react';

import * as styles from '@/components/Form/ManageAddreessEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormManageAddreessEditBaseProps,
    FormManageAddreessEditBase,
} from '@/components/Form/ManageAddreessEdit/Base';

export const FormManageAddreessEditStyle = { ...styles } as const;

export type FormManageAddreessEditStyles =
    keyof typeof FormManageAddreessEditStyle;

export interface FormManageAddreessEditProps
    extends FormManageAddreessEditBaseProps {
    styleTemplate?: FormManageAddreessEditStyles | ThemesType;
}

export const FormManageAddreessEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormManageAddreessEditProps) => {
    const Style = useMemo(
        () =>
            FormManageAddreessEditStyle[
                styleTemplate as FormManageAddreessEditStyles
            ] ?? FormManageAddreessEditStyle._default,
        [styleTemplate]
    );

    return <FormManageAddreessEditBase {...Style} {...props} />;
};
export default FormManageAddreessEdit;
