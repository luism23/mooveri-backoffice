import { useMemo, useState } from 'react';

import { Space } from '@/components/Space';
import { SubmitResult } from '@/components/Form/Base';

import { ManageAddressYup } from '@/validations/ManageAddress';

import { useLang } from '@/lang/translate';

import Theme, { ThemesType } from '@/config/theme';
import {
    DataManageAddress,
    DataManageAddressInputs,
} from '@/interfaces/ManageAddress';
import { useData } from 'fenextjs-hook/cjs/useData';
import { UserLoginProps, useUser } from '@/hook/useUser';
import InputEdit, { InputEditStyles } from '@/components/Input/Edit';

export interface FormManageAddreessEditClassProps {
    classNameContent?: string;

    styleTemplateInput?: DataManageAddress<InputEditStyles | ThemesType>;
    inputs?: DataManageAddress<boolean>;
}

export type onUpdateAddressType = (
    user: UserLoginProps
) => (data: DataManageAddress) => Promise<SubmitResult> | SubmitResult;

export interface FormManageAddreessEditBaseProps {
    defaultData?: DataManageAddress;
    onSubmit?: onUpdateAddressType;
    token?: string;
}

export interface FormManageAddreessEditProps
    extends FormManageAddreessEditClassProps,
        FormManageAddreessEditBaseProps {}

export const FormManageAddreessEditBase = ({
    styleTemplateInput = {
        address: Theme.styleTemplate ?? '_default',
        city: Theme.styleTemplate ?? '_default',
        apt: Theme.styleTemplate ?? '_default',
        zipcode: Theme.styleTemplate ?? '_default',
        location: Theme.styleTemplate ?? '_default',
    },

    inputs = {
        address: true,
        city: true,
        apt: true,
        zipcode: true,
        location: true,
    },

    defaultData = {},
    ...props
}: FormManageAddreessEditProps) => {
    const _t = useLang();
    const { user } = useUser();
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataManageAddress>(defaultData);
    const [activeEdit, setActiveEdit] = useState<
        DataManageAddressInputs | 'all'
    >('all');

    const ManageAddressYupV = useMemo(
        () => ManageAddressYup(data, inputs),
        [data]
    );

    const onSaveValue =
        (id: DataManageAddressInputs) => async (value: string) => {
            setLoader(true);
            onChangeData(id)(value);

            const result: SubmitResult | undefined = await props?.onSubmit?.(
                user
            )({
                ...data,
                [id]: value,
            });

            setLoader(false);
            const r: SubmitResult = {
                status: 'ok',
            };
            return result ?? r;
        };

    return (
        <>
            {inputs.address ? (
                <>
                    <InputEdit
                        label={_t('Address')}
                        onChange={onSaveValue('address')}
                        styleTemplate={styleTemplateInput.address}
                        yup={ManageAddressYupV.y.address}
                        defaultValue={data.address}
                        loader={loader}
                        activeEdit={
                            activeEdit == 'all' || activeEdit == 'address'
                        }
                        setStatus={(e) => {
                            setActiveEdit(e == 'edit' ? 'all' : 'address');
                        }}
                        onSave={onSaveValue('address')}
                    />
                    <Space size={5} />
                </>
            ) : (
                <></>
            )}
            {inputs.city ? (
                <>
                    <InputEdit
                        label={_t('City')}
                        onChange={onSaveValue('city')}
                        styleTemplate={styleTemplateInput.city}
                        yup={ManageAddressYupV.y.city}
                        defaultValue={data.city}
                        loader={loader}
                        activeEdit={activeEdit == 'all' || activeEdit == 'city'}
                        setStatus={(e) => {
                            setActiveEdit(e == 'edit' ? 'all' : 'city');
                        }}
                        onSave={onSaveValue('city')}
                    />
                    <Space size={5} />
                </>
            ) : (
                <></>
            )}
            {inputs.apt ? (
                <>
                    <InputEdit
                        label={_t('Apt,Suite,etc')}
                        onChange={onSaveValue('apt')}
                        styleTemplate={styleTemplateInput.apt}
                        yup={ManageAddressYupV.y.apt}
                        defaultValue={data.apt}
                        loader={loader}
                        activeEdit={activeEdit == 'all' || activeEdit == 'apt'}
                        setStatus={(e) => {
                            setActiveEdit(e == 'edit' ? 'all' : 'apt');
                        }}
                        onSave={onSaveValue('apt')}
                    />
                    <Space size={5} />
                </>
            ) : (
                <></>
            )}
            {inputs.zipcode ? (
                <>
                    <InputEdit
                        label={_t('Zip Code')}
                        onChange={onSaveValue('zipcode')}
                        styleTemplate={styleTemplateInput.zipcode}
                        yup={ManageAddressYupV.y.zipcode}
                        defaultValue={data.zipcode}
                        loader={loader}
                        activeEdit={
                            activeEdit == 'all' || activeEdit == 'zipcode'
                        }
                        setStatus={(e) => {
                            setActiveEdit(e == 'edit' ? 'all' : 'zipcode');
                        }}
                        onSave={onSaveValue('zipcode')}
                    />
                    <Space size={5} />
                </>
            ) : (
                <></>
            )}

            {inputs.location ? (
                <>
                    <InputEdit
                        label={_t('Name of Location')}
                        onChange={onSaveValue('location')}
                        styleTemplate={styleTemplateInput.location}
                        yup={ManageAddressYupV.y.location}
                        defaultValue={data.location}
                        loader={loader}
                        activeEdit={
                            activeEdit == 'all' || activeEdit == 'location'
                        }
                        setStatus={(e) => {
                            setActiveEdit(e == 'edit' ? 'all' : 'location');
                        }}
                        onSave={onSaveValue('location')}
                    />
                    <Space size={5} />
                </>
            ) : (
                <></>
            )}
        </>
    );
};
export default FormManageAddreessEditBase;
