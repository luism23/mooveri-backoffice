import { useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputEmail } from '@/components/InputYup/Email';
import { Title, TitleProps } from '@/components/Title';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { DataForgotPasswordPayment } from '@/interfaces/ForgotPasswordPayment';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { ForgotPasswordPaymentYup } from '@/validations/ForgotPasswordPayment';
import Text from '@/components/Text';
import { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import Image from '@/components/Image';

export interface FormForgotPasswordPaymentClassProps {
    classNameContentButtons?: string;
    sizeContent?: number;
    classNameContent?: string;
    classNameContentText?: string;
    sizeContentWidth?: number;
    classNameContentWidth?: string;
    classNameText?: string;
    sizeContentWidthImg?: number;
    classNameContentWidthImg?: string;
    styleTemplateImg?: ThemesType;
    img?: string;
    styleTemplateEmail?: InputTextStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;
    title?: string;
    text?: string;
    TitleProps?: TitleProps;
}

export type onSubmintForgotPasswordPayment = (
    data: DataForgotPasswordPayment
) => Promise<SubmitResult> | SubmitResult;

export interface FormForgotPasswordPaymentBaseProps {
    onSubmit: onSubmintForgotPasswordPayment;
    urlRedirect?: string;
}

export interface FormForgotPasswordPaymentProps
    extends FormForgotPasswordPaymentBaseProps,
        FormForgotPasswordPaymentClassProps {}

export const FormForgotPasswordPaymentBase = ({
    classNameContent = '',
    sizeContent = 300,
    classNameContentText = '',
    styleTemplateEmail = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    sizeContentWidth = 300,
    classNameContentWidth = '',
    classNameText = '',
    sizeContentWidthImg = 100,
    classNameContentWidthImg = '',
    img = 'sendEmail.png',
    styleTemplateImg = Theme.styleTemplate ?? '_default',
    title = 'Succesfully Register',
    text = 'Please check your email to confirm',
    TitleProps = {},
    ...props
}: FormForgotPasswordPaymentProps) => {
    const _t = useLang();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const [formSubmitted, setFormSubmitted] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataForgotPasswordPayment>({
        email: '',
    });

    const onAfterSubmit = () => {
        setFormSubmitted(true);
    };

    return (
        <>
            <ContentWidth
                size={300}
                className="m-auto p-t-10 pos-a top-70 text-center"
            >
                <Text className="font-20" styleTemplate="tolinkme27">
                    {_t('Forgot Password')}
                </Text>
            </ContentWidth>
            <ContentWidth size={sizeContent} className={classNameContent}>
                {formSubmitted ? (
                    <>
                        <ContentWidth
                            size={sizeContentWidth}
                            className={classNameContentWidth}
                        >
                            <Title {...TitleProps}>{_t(title)}</Title>
                            <Space size={28} />
                            <Text className={classNameText}>{_t(text)}</Text>
                            <Space size={28} />
                            <ContentWidth
                                size={sizeContentWidthImg}
                                className={classNameContentWidthImg}
                            >
                                <Image
                                    src={img}
                                    styleTemplate={styleTemplateImg}
                                />
                            </ContentWidth>
                            <Space size={28} />
                        </ContentWidth>
                    </>
                ) : (
                    <>
                        <Form<DataForgotPasswordPayment>
                            id="forgot-passwod"
                            data={data}
                            onSubmit={props.onSubmit}
                            disabled={disabled}
                            onChangeDisable={setDisabled}
                            loader={loader}
                            onChangeLoader={setLoader}
                            yup={ForgotPasswordPaymentYup}
                            onAfterSubmit={onAfterSubmit}
                        >
                            <Space size={15} />
                            <ContentWidth
                                size={230}
                                className={classNameContentText}
                            >
                                <Text className={classNameText}>
                                    {_t(
                                        'We will send you an email with a link with recovery instructions.'
                                    )}
                                </Text>
                            </ContentWidth>
                            <Space size={19} />
                            <InputEmail
                                styleTemplate={styleTemplateEmail}
                                defaultValue={data.email}
                                placeholder={_t('Email')}
                                onChange={onChangeData('email')}
                            />
                            <Space size={12} />
                            <Button
                                styleTemplate={styleTemplateButton}
                                disabled={disabled}
                                loader={loader}
                            >
                                {_t('Send Link')}
                            </Button>
                        </Form>
                    </>
                )}
            </ContentWidth>
        </>
    );
};
export default FormForgotPasswordPaymentBase;
