import { useMemo } from 'react';

import * as styles from '@/components/Form/ForgotPasswordPayment/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormForgotPasswordPaymentBaseProps,
    FormForgotPasswordPaymentBase,
} from '@/components/Form/ForgotPasswordPayment/Base';

export const FormForgotPasswordPaymentStyle = { ...styles } as const;

export type FormForgotPasswordPaymentStyles =
    keyof typeof FormForgotPasswordPaymentStyle;

export interface FormForgotPasswordPaymentProps
    extends FormForgotPasswordPaymentBaseProps {
    styleTemplate?: FormForgotPasswordPaymentStyles | ThemesType;
}

export const FormForgotPasswordPayment = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormForgotPasswordPaymentProps) => {
    const Style = useMemo(
        () =>
            FormForgotPasswordPaymentStyle[
                styleTemplate as FormForgotPasswordPaymentStyles
            ] ?? FormForgotPasswordPaymentStyle._default,
        [styleTemplate]
    );

    return <FormForgotPasswordPaymentBase {...Style} {...props} />;
};
export default FormForgotPasswordPayment;
