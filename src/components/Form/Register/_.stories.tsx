import { Story, Meta } from "@storybook/react";

import { FormRegisterProps, FormRegister } from "./index";
import { DataRegister } from "@/interfaces/Register";
import log from "@/functions/log";

export default {
    title: "Form/Register",
    component: FormRegister,
} as Meta;

const Template: Story<FormRegisterProps> = (args) => <FormRegister {...args} />;

export const Index = Template.bind({});
Index.args = {
    onSubmit:  async (data: DataRegister)=> {
        log("DataRegister",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "register ok",
        };
    },
    onValidateUsername:  async (data: string)=> {
        log("onValidateUsername",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }
    },

};
