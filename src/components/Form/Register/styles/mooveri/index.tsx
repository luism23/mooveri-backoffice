import { FormRegisterClassProps } from '@/components/Form/Register/Base';
import url from '@/data/routes';

export const mooveri: FormRegisterClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameContentTitle: `
        text-center
        color-sea
    `,
    classNameContentLinkRegister: `
        text-center
    `,
    styleTemplateInputs: {
        userName: 'mooveri',
        email: 'mooveri',
        firstName: 'mooveri',
        lastName: 'mooveri',
        phone: 'mooveri',
        password: 'mooveri',
        repeatPassword: 'mooveri',
    },
    styleTemplateButton: 'mooveri',
    styleTemplateLinkRegister: 'mooveri',

    inputs: {
        email: true,
        userName: false,
        firstName: true,
        lastName: true,
        phone: true,
        password: true,
        repeatPassword: true,
    },
    rsLogin: {
        google: true,
        facebook: true,
    },
    stylesRsLogin: {
        classNameContainer: `
            p-15
            p-md-v-33 
            bg-whiteThree
            m-b-14
        `,
        classNameContent: `
            m-h-auto
            flex
            flex-nowrap
            flex-column
            flex-gap-row-7
        `,
        sizeContent: 300,
        styleTemplateButtonGogle: 'mooveri',
        styleTemplateButtonFacebook: 'mooveri',
    },
    sizeContentInputs: 330,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
    typeStyleTemplateTitle: 'h7',
};

export const mooveriCompany: FormRegisterClassProps = {
    ...mooveri,
    rsLogin: {
        facebook: false,
        google: true,
    },
    LinkLogin: url.company.login,
};
