import log from "@/functions/log";
import { Story, Meta } from "@storybook/react";

import { FormConfirmPhoneCodeProps, FormConfirmPhoneCode } from "./index";

export default {
    title: "Form/ConfirmPhoneCode",
    component: FormConfirmPhoneCode,
} as Meta;

const Template: Story<FormConfirmPhoneCodeProps> = (args) => (
    <FormConfirmPhoneCode {...args} />
);

export const Index = Template.bind({});
Index.args = {
    onSubmit: async (data: {code:string}) => {
        log("DataCode",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "Confirm phone ok",
        };
    },
};
