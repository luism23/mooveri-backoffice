import { FormConfirmPhoneCodeClassProps } from '@/components/Form/ConfirmPhoneCode/Base';

export const mooveri: FormConfirmPhoneCodeClassProps = {
    classNameContent: `
    m-h-auto
  `,
    classNameTitle: `
    text-center color-white
  `,
    styleTemplateTitle: 'mooveri',
    typeStyleTemplateTitle: 'h7',
    classNameContentText: `
    m-h-auto
  `,
    classNameText: `
    text-center
  `,
    classNameContentLink: `
    text-right
  `,
    styleTemplateInput: 'mooveri',
    styleTemplateText: 'mooveri',
    styleTemplateButton: 'mooveri',
    styleTemplateLink: 'mooveri',
};
