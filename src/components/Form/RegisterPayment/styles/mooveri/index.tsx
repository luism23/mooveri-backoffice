import { FormRegisterPaymentClassProps } from '@/components/Form/RegisterPayment/Base';
import url from '@/data/routes';

export const mooveri: FormRegisterPaymentClassProps = {
    classNameContent: `
        m-h-auto
    `,
    styleTemplateInputs: {
        userName: 'mooveri',
        email: 'mooveri',
        firstName: 'mooveri',
        lastName: 'mooveri',
        phone: 'mooveri',
        password: 'mooveri',
        repeatPassword: 'mooveri',
    },
    styleTemplateButton: 'mooveri',
    inputs: {
        email: true,
        userName: false,
        firstName: true,
        lastName: true,
        phone: true,
        password: true,
        repeatPassword: true,
    },
    sizeContentInputs: 330,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
};

export const mooveriCompany: FormRegisterPaymentClassProps = {
    ...mooveri,
    LinkLogin: url.company.login,
};
