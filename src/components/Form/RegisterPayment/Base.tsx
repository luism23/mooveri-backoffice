import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputMe } from '@/components/Input/Me';
import { InputEmail } from '@/components/InputYup/Email';
import {
    InputPassword,
    InputRepeatPassword,
} from '@/components/InputYup/Password';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { DataRegisterPayment } from '@/interfaces/RegisterPayment';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { RegisterPaymentYup } from '@/validations/RegisterPayment';
import { UserLoginProps, useUser } from '@/hook/useUser';
import Theme, { ThemesType } from '@/config/theme';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import Text from '@/components/Text';

export interface FormRegisterPaymentClassProps {
    sizeContent?: number;
    classNameContent?: string;
    sizeContentInputs?: number;
    classNameContentInputs?: string;
    styleTemplateInputs?: DataRegisterPayment<InputTextStyles | ThemesType>;
    inputs?: DataRegisterPayment<boolean>;
    styleTemplateButton?: ButtonStyles | ThemesType;

    LinkLogin?: string;
}

export type onSubmintRegisterPayment = (
    data: DataRegisterPayment
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormRegisterPaymentBaseProps {
    onSubmit?: onSubmintRegisterPayment;
    onValidateUsername?: onValidateUsername;
    urlRediret?: string;
}
export interface FormRegisterPaymentProps
    extends FormRegisterPaymentBaseProps,
        FormRegisterPaymentClassProps {}

export const FormRegisterPaymentBase = ({
    sizeContent = -1,
    classNameContent = '',
    sizeContentInputs = -1,
    classNameContentInputs = '',

    styleTemplateInputs = {
        userName: Theme.styleTemplate ?? '_default',
        email: Theme.styleTemplate ?? '_default',
        firstName: Theme.styleTemplate ?? '_default',
        lastName: Theme.styleTemplate ?? '_default',
        password: Theme.styleTemplate ?? '_default',
        repeatPassword: Theme.styleTemplate ?? '_default',
        phone: Theme.styleTemplate ?? '_default',
    },
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    inputs = {
        email: true,
        userName: true,
        firstName: true,
        lastName: true,
        phone: true,
        password: true,
        repeatPassword: true,
    },

    ...props
}: FormRegisterPaymentProps) => {
    const _t = useLang();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataRegisterPayment>({});

    const RegisterPaymentYupV = useMemo(
        () => RegisterPaymentYup(data, inputs),
        [data]
    );

    const validateUserName = async () => {
        setDisabled(true);
        await props?.onValidateUsername?.(data?.userName ?? '');
    };

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <ContentWidth
                    size={300}
                    className="m-auto p-t-10 pos-a top-70 text-center"
                >
                    <Text className="font-20" styleTemplate="tolinkme27">
                        {_t('Sign Up Now')}
                    </Text>
                </ContentWidth>
                <Form<DataRegisterPayment, UserLoginProps>
                    id="RegisterPayment"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={RegisterPaymentYupV}
                    validateAfterYup={validateUserName}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                    }}
                >
                    <Space size={20} />
                    <ContentWidth
                        size={sizeContentInputs}
                        className={classNameContentInputs}
                    >
                        {inputs.userName ? (
                            <>
                                <InputMe
                                    styleTemplate={styleTemplateInputs.userName}
                                    defaultValue={data.userName}
                                    placeholder={_t('/username')}
                                    onChange={onChangeData('userName')}
                                    onChangeValidateAfterYup={
                                        props?.onValidateUsername
                                    }
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.firstName ? (
                            <>
                                <InputText
                                    styleTemplate={
                                        styleTemplateInputs.firstName
                                    }
                                    defaultValue={data.firstName}
                                    placeholder={_t('First Name')}
                                    onChange={onChangeData('firstName')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.email ? (
                            <>
                                <InputEmail
                                    styleTemplate={styleTemplateInputs.email}
                                    defaultValue={data.email}
                                    placeholder={_t('Email')}
                                    onChange={onChangeData('email')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.password ? (
                            <>
                                <InputPassword
                                    styleTemplate={styleTemplateInputs.password}
                                    defaultValue={data.password}
                                    placeholder={_t('Password')}
                                    onChange={onChangeData('password')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        {inputs.repeatPassword ? (
                            <>
                                <InputRepeatPassword
                                    styleTemplate={
                                        styleTemplateInputs.repeatPassword
                                    }
                                    defaultValue={data.repeatPassword}
                                    password={data.password}
                                    placeholder={_t('Repeat Password')}
                                    onChange={onChangeData('repeatPassword')}
                                />
                                <Space size={6} />
                            </>
                        ) : (
                            <></>
                        )}
                        <Space size={6} />
                        <Button
                            styleTemplate={styleTemplateButton}
                            disabled={disabled}
                            loader={loader}
                        >
                            {_t('Create Account')}
                        </Button>
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormRegisterPaymentBase;
