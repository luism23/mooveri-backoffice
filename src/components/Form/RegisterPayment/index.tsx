import { useMemo } from 'react';

import * as styles from '@/components/Form/RegisterPayment/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormRegisterPaymentBaseProps,
    FormRegisterPaymentBase,
} from '@/components/Form/RegisterPayment/Base';

export const FormRegisterPaymentStyle = { ...styles } as const;

export type FormRegisterPaymentStyles = keyof typeof FormRegisterPaymentStyle;

export interface FormRegisterPaymentProps extends FormRegisterPaymentBaseProps {
    styleTemplate?: FormRegisterPaymentStyles | ThemesType;
}

export const FormRegisterPayment = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormRegisterPaymentProps) => {
    const Style = useMemo(
        () =>
            FormRegisterPaymentStyle[
                styleTemplate as FormRegisterPaymentStyles
            ] ?? FormRegisterPaymentStyle._default,
        [styleTemplate]
    );

    return <FormRegisterPaymentBase {...Style} {...props} />;
};
export default FormRegisterPayment;
