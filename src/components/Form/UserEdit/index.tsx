import { useMemo } from 'react';

import * as styles from '@/components/Form/UserEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormUserEditBaseProps,
    FormUserEditBase,
} from '@/components/Form/UserEdit/Base';

export const FormUserEditStyle = { ...styles } as const;

export type FormUserEditStyles = keyof typeof FormUserEditStyle;

export interface FormUserEditProps extends FormUserEditBaseProps {
    styleTemplate?: FormUserEditStyles | ThemesType;
}

export const FormUserEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormUserEditProps) => {
    const Style = useMemo(
        () =>
            FormUserEditStyle[styleTemplate as FormUserEditStyles] ??
            FormUserEditStyle._default,
        [styleTemplate]
    );

    return <FormUserEditBase {...Style} {...props} />;
};
export default FormUserEdit;
