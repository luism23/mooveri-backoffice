import { Story, Meta } from "@storybook/react";

import { FormMonetizerFormProps, FormMonetizerForm } from "./index";

export default {
    title: "Form/MonetizerForm",
    component: FormMonetizerForm,
} as Meta;

const Template: Story<FormMonetizerFormProps> = (args) => <FormMonetizerForm {...args} />;

export const Index = Template.bind({});
Index.args = {
    
    
};
