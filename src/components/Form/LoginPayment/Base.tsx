import { useMemo, useState } from 'react';
import { Button, ButtonStyles } from '@/components/Button';
import { InputEmail } from '@/components/InputYup/Email';
import { InputPassword } from '@/components/InputYup/Password';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { DataLoginPayment } from '@/interfaces/LoginPayment';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { LoginPaymentYup } from '@/validations/LoginPayment';
import Theme, { ThemesType } from '@/config/theme';
import { UserLoginPaymentProps, useUser } from '@/hook/useUser';
import { InputTextStyles } from '@/components/Input/Text';
import Text from '@/components/Text';

export interface FormLoginPaymentClassProps {
    classNameContent?: string;
    classNameContentInputs?: string;
    styleTemplateInputs?: DataLoginPayment<InputTextStyles | ThemesType>;
    inputs?: DataLoginPayment<boolean>;
    sizeContent?: number;
    sizeContentInputs?: number;
    styleTemplateButton?: ButtonStyles | ThemesType;
    linkForgotPassword?: string;
    linkRegister?: string;
}

export type onSubmintLoginPayment = (
    data: DataLoginPayment
) =>
    | Promise<SubmitResult<UserLoginPaymentProps>>
    | SubmitResult<UserLoginPaymentProps>;

export interface FormLoginPaymentBaseProps {
    onSubmit: onSubmintLoginPayment;
    urlRedirect?: string;
}

export interface FormLoginPaymentProps
    extends FormLoginPaymentBaseProps,
        FormLoginPaymentClassProps {}

export const FormLoginPaymentBase = ({
    classNameContent = '',
    classNameContentInputs = '',
    sizeContent = 300,
    sizeContentInputs = 300,
    styleTemplateInputs = {
        email: Theme.styleTemplate ?? '_default',
        password: Theme.styleTemplate ?? '_default',
    },
    inputs = {
        email: true,
        password: true,
    },

    styleTemplateButton = Theme.styleTemplate ?? '_default',
    ...props
}: FormLoginPaymentProps) => {
    const _t = useLang();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataLoginPayment>({});
    const LoginPaymentYupV = useMemo(
        () => LoginPaymentYup(data, inputs),
        [data]
    );
    const _onLoginPayment = (data: SubmitResult<UserLoginPaymentProps>) => {
        onLogin(data);
    };

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <ContentWidth
                    size={300}
                    className="m-auto p-t-10 pos-a top-70 text-center"
                >
                    <Text className="font-20" styleTemplate="tolinkme27">
                        {_t('Login Now')}
                    </Text>
                </ContentWidth>
                <Form<DataLoginPayment>
                    id="LoginPayment"
                    data={data}
                    onSubmit={props.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={LoginPaymentYupV}
                    onAfterSubmit={_onLoginPayment}
                >
                    <Space size={20} />
                    <ContentWidth
                        size={sizeContentInputs}
                        className={classNameContentInputs}
                    >
                        {inputs.email ? (
                            <InputEmail
                                defaultValue={data.email}
                                placeholder={_t('Email')}
                                onChange={onChangeData('email')}
                                styleTemplate={styleTemplateInputs.email}
                            />
                        ) : (
                            <></>
                        )}
                        <Space size={6} />
                        {inputs.password ? (
                            <InputPassword
                                defaultValue={data.password}
                                placeholder={_t('Password')}
                                onChange={onChangeData('password')}
                                styleTemplate={styleTemplateInputs.password}
                            />
                        ) : (
                            <></>
                        )}
                        <Space size={12} />
                        <Button
                            styleTemplate={styleTemplateButton}
                            disabled={disabled}
                            loader={loader}
                        >
                            {_t('Login')}
                        </Button>
                        <Space size={15} />
                    </ContentWidth>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormLoginPaymentBase;
