import { FormLoginClassProps } from '@/components/Form/Login/Base';
import url from '@/data/routes';

export const mooveri: FormLoginClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
    sizeContentInputs: 330,
    styleTemplateButton: 'mooveri',
    styleTemplateLinkForgotPassword: 'mooveri',
    classNameLinkTextForgotPassword: `
    font-12 font-nunito text-decoration-underline

    `,
    classNameLinkRegister: `
    font-14 font-nunito font-w-600
    color-seaDark color-warmGreyThree-hover
    text-decoration-underline
    `,
    styleTemplateLinkRegister: 'mooveri2',
    sizeContent: -1,
    titleProps: {
        className: `
            font-23
            font-w-900
            color-sea
            text-center 
            p-v-16
        `,
        styleTemplate: 'mooveri',
        type: 'h1',
        typeStyle: 'h2',
    },

    inputs: {
        password: true,
        email: true,
    },
    stylesRsLogin: {
        classNameContainer: `
            p-15
            p-md-v-33 
            bg-whiteThree
            m-b-14
        `,
        classNameContent: `
            m-h-auto
            flex
            flex-nowrap
            flex-column
            flex-gap-row-7
        `,
        sizeContent: 300,
        styleTemplateButtonGogle: 'mooveri',
        styleTemplateButtonFacebook: 'mooveri',
    },
    styleTemplateInputs: {
        email: 'mooveri',
        password: 'mooveri',
    },
};
export const mooveriCompany: FormLoginClassProps = {
    ...mooveri,
    rsLogin: {
        facebook: false,
        google: true,
    },
    linkForgotPassword: url.company.forgotPassword,
    linkRegister: url.company.register,
};

export const mooveriBackoffice: FormLoginClassProps = {
    ...mooveri,
    rsLogin: null,
    links: null,
};
