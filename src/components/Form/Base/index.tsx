import * as Yup from 'yup';
import type { AnySchema } from 'yup';
import { PropsWithChildren, useEffect } from 'react';

import log from '@/functions/log';
import { useLang } from '@/lang/translate';

import { useNotification } from '@/hook/useNotification';

export type onError = (error: string) => void;
export type onOk = () => Promise<void> | void;

export interface SubmitResult<R = any> {
    status: 'ok' | 'error';
    message?: string;
    data?: R;
}

export interface FormProps<T, R = any> {
    id?: string;
    data: T;
    onSubmit?: (data: T) => Promise<SubmitResult<R>> | SubmitResult<R>;
    onAfterSubmit?: (data: SubmitResult<R>) => void;
    disabled?: boolean;
    onChangeDisable?: (disabled: boolean) => void;
    loader?: boolean;
    onChangeLoader?: (disabled: boolean) => void;
    yup?: AnySchema;
    validateAfterYup?: (data: T) => Promise<void> | void;
    className?: string;
}
export const Form = <T, R = any>({
    id = '',
    data,
    disabled = true,
    onChangeDisable,
    onChangeLoader,
    children,
    yup = Yup.object().shape({}),
    validateAfterYup = async (data: T) => {
        log('validateAfterYup', data);
    },
    className = '',
    ...props
}: PropsWithChildren<FormProps<T, R>>) => {
    const { pop } = useNotification();
    const _t = useLang();
    const onSendForm = async () => {
        onChangeLoader?.(true);

        try {
            const result = await props?.onSubmit?.(data);

            pop({
                type: result?.status ?? 'ok',
                message: _t(result?.message ?? ''),
            });

            if (result?.status == 'ok') {
                if (id != '') {
                    const w: any = window;
                    if (w?.dataLayer?.push) {
                        w.dataLayer?.push?.({
                            event: `form-${id}`,
                        });
                    }
                }
                props?.onAfterSubmit?.(result);
            }
        } catch (error: any) {
            pop({
                type: 'error',
                message: _t(error?.message ?? error ?? ''),
            });
        }
        onChangeLoader?.(false);
    };

    const onSubmit = async (e: any) => {
        e.preventDefault();
        if (disabled) {
            return;
        }
        onValidateData(onSendForm, (error: string) => {
            pop({
                type: 'error',
                message: _t(error),
            });
        });
    };

    const onValidateData = (onOk: onOk | null, onError: onError | null) => {
        yup.validate(data)
            .then(async function () {
                try {
                    await validateAfterYup(data);
                    onChangeDisable?.(false);
                } catch (error: any) {
                    onChangeDisable?.(true);
                    log('error', error, 'red');
                    onError?.(error.message);
                    return;
                }
                onOk?.();
            })
            .catch(function (error: any) {
                onChangeDisable?.(true);
                log('onValidateData error', error, 'red');
                onError?.(error.message);

                return;
            });
    };
    useEffect(() => {
        onValidateData(null, null);
    }, [data, yup]);

    return (
        <>
            <form className={className} onSubmit={onSubmit}>
                {children}
            </form>
        </>
    );
};
export default Form;
