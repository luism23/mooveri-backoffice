import { useMemo } from 'react';

import * as styles from '@/components/Form/Monetize/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormMonetizeBaseProps,
    FormMonetizeBase,
} from '@/components/Form/Monetize/Base';

export const FormMonetizeStyle = { ...styles } as const;

export type FormMonetizeStyles = keyof typeof FormMonetizeStyle;

export interface FormMonetizeProps extends FormMonetizeBaseProps {
    styleTemplate?: FormMonetizeStyles | ThemesType;
}

export const FormMonetize = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormMonetizeProps) => {
    const Style = useMemo(
        () =>
            FormMonetizeStyle[styleTemplate as FormMonetizeStyles] ??
            FormMonetizeStyle._default,
        [styleTemplate]
    );

    return <FormMonetizeBase {...Style} {...props} />;
};
export default FormMonetize;
