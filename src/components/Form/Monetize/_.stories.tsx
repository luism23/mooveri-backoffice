import { Story, Meta } from "@storybook/react";

import { FormMonetizeProps, FormMonetize } from "./index";
export default {
    title: "Form/Monetize",
    component: FormMonetize,
} as Meta;

const Template: Story<FormMonetizeProps> = (args) => <FormMonetize {...args} />;

export const Index = Template.bind({});
Index.args = {
   
};
