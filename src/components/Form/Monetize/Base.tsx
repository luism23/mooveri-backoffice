import { useMemo, useState } from 'react';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';
import { useData } from 'fenextjs-hook/cjs/useData';
import { useLang } from '@/lang/translate';
import { UserLoginProps, useUser } from '@/hook/useUser';
import Theme, { ThemesType } from '@/config/theme';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import { useRouter } from 'next/router';
import url from '@/data/routes';
import Text, { TextStyles } from '@/components/Text';
import { DataFormMonetize } from '@/interfaces/FormMonetize';
import { FromMonetizeYup } from '@/validations/Monetize';
import InputRadio from '@/components/Input/Radio';
import {
    MonetizeRecurrent,
    PeriodRecurrent,
    TypeRecurrent,
} from '@/data/components/Monetize';
import InputMonetize from '@/components/Input/Monetize';
import money from '@/functions/money';
import Padlock from '@/svg/padlock';

export interface FormMonetizeClassProps {
    sizeContent?: number;
    classNameInputR?: string;
    classNameInputT?: string;
    classNameContent?: string;
    classNameMaxLength?: string;
    classNameAmount?: string;
    inputs?: DataFormMonetize<boolean>;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateInputNumber?: InputTextStyles | ThemesType;
    classNameDescription?: string;
    classNameContentDescription?: string;
}

export type onSubmintMonetize = (
    data: DataFormMonetize
) => Promise<SubmitResult<UserLoginProps>> | SubmitResult<UserLoginProps>;

export type onValidateUsername = (username: string) => Promise<void> | void;

export interface FormMonetizeBaseProps {
    onChange?: (data: DataFormMonetize) => void;
    defaultValue?: DataFormMonetize;
    urlRediret?: string;
    onSubmitData?: (data: DataFormMonetize) => Promise<SubmitResult>;
    defaultPage?: number;
}
export interface FormMonetizeProps
    extends FormMonetizeBaseProps,
        FormMonetizeClassProps {
    onSubmit?: onSubmintMonetize;
}

export const FormMonetizeBase = ({
    inputs = {
        amount: true,
    },
    sizeContent = -1,

    onChange = () => {},
    classNameAmount = '',
    classNameInputT = '',
    classNameInputR = '',
    classNameContent = '',
    classNameMaxLength = '',
    classNameContentDescription = '',
    classNameDescription = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateInputNumber = Theme?.styleTemplate ?? '_default',
    defaultValue = {},

    ...props
}: FormMonetizeProps) => {
    const _t = useLang();
    const router = useRouter();
    const { onLogin } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const { data, onChangeData } = useData<DataFormMonetize>(defaultValue, {
        onChangeDataAfter: onChange,
    });

    const FormMonetizeYupV = useMemo(
        () => FromMonetizeYup(data, inputs),
        [data]
    );
    const [bioLength, setBioLength] = useState(0);

    const BioMaxLength = 60;

    const isCreateMonetize = useMemo(
        () => defaultValue.amount != undefined,
        []
    );
    const ContentVariante = useMemo(() => {
        if (data.recurrent === 'Flat_Rate') {
            return <></>;
        }
        return (
            <div>
                <div>
                    <Text styleTemplate={styleTemplateText}>
                        {_t('Period')}
                    </Text>
                    <div className={classNameInputR}>
                        <InputRadio
                            disabled={isCreateMonetize}
                            styleTemplate={'tolinkme3'}
                            onChange={(e) => {
                                onChangeData('period')(
                                    PeriodRecurrent[e].label
                                );
                            }}
                            options={PeriodRecurrent}
                            selectedRadioDefault={
                                PeriodRecurrent.findIndex(
                                    (e) => e.label == data.period
                                ) ?? 0
                            }
                        />
                    </div>
                </div>
            </div>
        );
    }, [data, _t, isCreateMonetize]);

    return (
        <>
            <ContentWidth size={sizeContent} className={classNameContent}>
                <Form<DataFormMonetize>
                    id="Monetize"
                    data={data}
                    onSubmit={props?.onSubmit}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    yup={FormMonetizeYupV.v}
                    onAfterSubmit={(data) => {
                        onLogin(data);
                        router.push(props?.urlRediret ?? url.home);
                    }}
                >
                    {isCreateMonetize ? (
                        <>
                            <div className="">
                                <div>
                                    <Text
                                        className="flex flex-align-center"
                                        styleTemplate={styleTemplateText}
                                    >
                                        {_t('Type of collection recurrence')}{' '}
                                        <span className="flex p-l-10">
                                            <Padlock size={12} />
                                        </span>
                                    </Text>
                                    <div className={`${classNameInputR}`}>
                                        <InputRadio
                                            disabled={isCreateMonetize}
                                            styleTemplate={'tolinkme3'}
                                            onChange={(e) => {
                                                onChangeData('recurrent')(
                                                    MonetizeRecurrent[e].label
                                                );
                                            }}
                                            options={MonetizeRecurrent}
                                            selectedRadioDefault={
                                                MonetizeRecurrent.findIndex(
                                                    (e) =>
                                                        e.label ==
                                                        data?.recurrent
                                                ) ?? 0
                                            }
                                        />
                                    </div>
                                </div>
                                <div>{ContentVariante}</div>
                                {inputs.amount ? (
                                    <div className={classNameAmount}>
                                        <Text
                                            className="flex flex-align-center"
                                            styleTemplate={styleTemplateText}
                                        >
                                            {_t('Amount')}{' '}
                                            <span className="flex p-l-10 ">
                                                <Padlock size={12} />
                                            </span>
                                        </Text>

                                        <InputMonetize
                                            disabled={true}
                                            onChange={(v: number) => {
                                                console.log(v);

                                                onChangeData('amount')(`${v}`);
                                            }}
                                            min={0}
                                            max={1000}
                                            placeholder={money(0)}
                                            styleTemplate={
                                                styleTemplateInputNumber
                                            }
                                            props={{
                                                style: {
                                                    height: '3rem',
                                                },
                                            }}
                                            defaultValue={
                                                data.amount !== undefined
                                                    ? parseFloat(data.amount)
                                                    : ''
                                            }
                                        />
                                    </div>
                                ) : (
                                    <></>
                                )}

                                <div>
                                    <Text
                                        className="flex flex-align-center"
                                        styleTemplate={styleTemplateText}
                                    >
                                        {_t('Type')}{' '}
                                        <span className="flex p-l-10 ">
                                            <Padlock size={12} />
                                        </span>
                                    </Text>
                                    <div className={classNameInputT}>
                                        <InputRadio
                                            disabled={isCreateMonetize}
                                            label={data.optional}
                                            styleTemplate={'tolinkme3'}
                                            onChange={(e) => {
                                                onChangeData('type')(
                                                    TypeRecurrent[e].label
                                                );
                                            }}
                                            options={TypeRecurrent}
                                            selectedRadioDefault={
                                                TypeRecurrent.findIndex(
                                                    (e) => e.label == data?.type
                                                ) ?? 0
                                            }
                                        />
                                    </div>
                                </div>
                            </div>
                        </>
                    ) : (
                        <>
                            <div>
                                <Text styleTemplate={styleTemplateText}>
                                    {_t('Type of collection recurrence')}
                                </Text>
                                <div className={classNameInputR}>
                                    <InputRadio
                                        styleTemplate={'tolinkme3'}
                                        onChange={(e) => {
                                            onChangeData('recurrent')(
                                                MonetizeRecurrent[e].label
                                            );
                                        }}
                                        options={MonetizeRecurrent}
                                        selectedRadioDefault={
                                            MonetizeRecurrent.findIndex(
                                                (e) =>
                                                    e.label == data?.recurrent
                                            ) ?? 0
                                        }
                                    />
                                </div>
                            </div>
                            <div>{ContentVariante}</div>
                            {inputs.amount ? (
                                <div className={classNameAmount}>
                                    <Text styleTemplate={styleTemplateText}>
                                        {_t('Amount')}
                                    </Text>

                                    <InputMonetize
                                        onChange={(v: number) => {
                                            console.log(v);

                                            onChangeData('amount')(`${v}`);
                                        }}
                                        min={0}
                                        max={1000}
                                        placeholder={money(0)}
                                        styleTemplate={styleTemplateInputNumber}
                                        props={{
                                            style: {
                                                height: '3rem',
                                            },
                                        }}
                                        defaultValue={
                                            data.amount !== undefined
                                                ? parseFloat(data.amount)
                                                : ''
                                        }
                                    />
                                </div>
                            ) : (
                                <></>
                            )}

                            <div>
                                <Text styleTemplate={styleTemplateText}>
                                    {_t('Type')}
                                </Text>
                                <div className={classNameInputT}>
                                    <InputRadio
                                        label={data.optional}
                                        styleTemplate={'tolinkme3'}
                                        onChange={(e) => {
                                            onChangeData('type')(
                                                TypeRecurrent[e].label
                                            );
                                        }}
                                        options={TypeRecurrent}
                                        selectedRadioDefault={
                                            TypeRecurrent.findIndex(
                                                (e) => e.label == data?.type
                                            ) ?? 0
                                        }
                                    />
                                </div>
                            </div>
                        </>
                    )}
                    <div className={classNameContentDescription}>
                        {inputs.description ? (
                            <>
                                <Text
                                    className={classNameDescription}
                                    styleTemplate={styleTemplateText}
                                >
                                    {_t('Description')}
                                </Text>
                                <InputText
                                    placeholder={_t(
                                        data.recurrent === 'Flat_Rate'
                                            ? 'Just one monthly tip for my content'
                                            : 'Add something to describe your content'
                                    )}
                                    styleTemplate="tolinkme14"
                                    props={{
                                        maxLength: BioMaxLength,
                                        style: {
                                            height: '3rem',
                                        },
                                    }}
                                    type="textarea"
                                    onChange={(value) => {
                                        onChangeData('description')(value);
                                        setBioLength(value.length);
                                    }}
                                    defaultValue={data?.description ?? ''}
                                    value={data?.description}
                                />

                                <div className={classNameMaxLength}>
                                    {bioLength}/{BioMaxLength}
                                </div>
                            </>
                        ) : (
                            <></>
                        )}
                    </div>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormMonetizeBase;
