import { Story, Meta } from "@storybook/react";

import { FormManageAddreessProps, FormManageAddreess } from "./index";

export default {
    title: "Form/ManageAddreess",
    component: FormManageAddreess,
} as Meta;

const Template: Story<FormManageAddreessProps> = (args) => (
    <FormManageAddreess {...args} />
);


export const Index = Template.bind({});
Index.args = {
};
