import { useMemo } from 'react';

import * as styles from '@/components/Form/ManageAddreess/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormManageAddreessBaseProps,
    FormManageAddreessBase,
} from '@/components/Form/ManageAddreess/Base';

export const FormManageAddreessStyle = { ...styles } as const;

export type FormManageAddreessStyles = keyof typeof FormManageAddreessStyle;

export interface FormManageAddreessProps extends FormManageAddreessBaseProps {
    styleTemplate?: FormManageAddreessStyles | ThemesType;
}

export const FormManageAddreess = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormManageAddreessProps) => {
    const Style = useMemo(
        () =>
            FormManageAddreessStyle[
                styleTemplate as FormManageAddreessStyles
            ] ?? FormManageAddreessStyle._default,
        [styleTemplate]
    );

    return <FormManageAddreessBase {...Style} {...props} />;
};
export default FormManageAddreess;
