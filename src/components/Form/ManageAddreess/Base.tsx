import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';

import { ManageAddressYup } from '@/validations/ManageAddress';

import { useLang } from '@/lang/translate';

import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import url from '@/data/routes';
import { useRouter } from 'next/router';
import Google, { GoogleStyles } from '@/components/Input/Google';
import { DataManageAddress } from '@/interfaces/ManageAddress';
import { useData } from 'fenextjs-hook/cjs/useData';
import { GoogleAddres } from '@/interfaces/Google/addres';
import { UserLoginProps, useUser } from '@/hook/useUser';

export interface FormManageAddreessClassProps {
    classNameContent?: string;

    styleTemplateInput?: DataManageAddress<InputTextStyles | ThemesType>;
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateGoogle?: GoogleStyles | ThemesType;
    inputs?: DataManageAddress<boolean>;
}

export type onSubmintManageAddress = (
    user: UserLoginProps
) => (data: DataManageAddress) => Promise<SubmitResult> | SubmitResult;

export interface FormManageAddreessBaseProps {
    onSubmit?: onSubmintManageAddress;
    token?: string;
    urlRedirect?: string;
}

export interface FormManageAddreessProps
    extends FormManageAddreessClassProps,
        FormManageAddreessBaseProps {}

export const FormManageAddreessBase = ({
    styleTemplateInput = {
        address: Theme.styleTemplate ?? '_default',
        city: Theme.styleTemplate ?? '_default',
        apt: Theme.styleTemplate ?? '_default',
        zipcode: Theme.styleTemplate ?? '_default',
        location: Theme.styleTemplate ?? '_default',
    },
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateGoogle = Theme.styleTemplate ?? '_default',

    inputs = {
        address: true,
        city: true,
        apt: true,
        zipcode: true,
        location: true,
    },

    urlRedirect = url.profile,
    ...props
}: FormManageAddreessProps) => {
    const _t = useLang();
    const { user } = useUser();
    const route = useRouter();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data, onChangeData } = useData<DataManageAddress>({});

    const ManageAddressYupV = useMemo(
        () => ManageAddressYup(data, inputs),
        [data]
    );

    return (
        <>
            <Form<DataManageAddress>
                data={data}
                onSubmit={props?.onSubmit?.(user)}
                disabled={disabled}
                onChangeDisable={setDisabled}
                loader={loader}
                onChangeLoader={setLoader}
                yup={ManageAddressYupV.v}
                onAfterSubmit={() => {
                    route.push(urlRedirect);
                }}
            >
                {inputs.address ? (
                    <>
                        <InputText
                            label={_t('Address')}
                            onChange={onChangeData('address')}
                            styleTemplate={styleTemplateInput.address}
                            yup={ManageAddressYupV.y.address}
                        />
                        <Space size={5} />
                    </>
                ) : (
                    <></>
                )}
                {inputs.city ? (
                    <>
                        <InputText
                            label={_t('City')}
                            onChange={onChangeData('city')}
                            styleTemplate={styleTemplateInput.city}
                            yup={ManageAddressYupV.y.city}
                        />
                        <Space size={5} />
                    </>
                ) : (
                    <></>
                )}
                {inputs.apt ? (
                    <>
                        <InputText
                            label={_t('Apt,Suite,etc')}
                            onChange={onChangeData('apt')}
                            styleTemplate={styleTemplateInput.apt}
                            yup={ManageAddressYupV.y.apt}
                        />
                        <Space size={5} />
                    </>
                ) : (
                    <></>
                )}
                {inputs.zipcode ? (
                    <>
                        <InputText
                            label={_t('Zip Code')}
                            onChange={onChangeData('zipcode')}
                            styleTemplate={styleTemplateInput.zipcode}
                            yup={ManageAddressYupV.y.zipcode}
                        />
                        <Space size={5} />
                    </>
                ) : (
                    <></>
                )}

                {inputs.location ? (
                    <>
                        <Google
                            placeholder=""
                            label={_t('Name of Location')}
                            styleTemplate={styleTemplateGoogle}
                            yup={ManageAddressYupV.y.location}
                            onChange={(addres: GoogleAddres) => {
                                onChangeData('location')(
                                    addres?.formatedAddress ?? ''
                                );
                            }}
                        />
                        <Space size={5} />
                    </>
                ) : (
                    <></>
                )}

                <Space size={21} />
                <Button
                    styleTemplate={styleTemplateButton}
                    disabled={disabled}
                    loader={loader}
                >
                    {_t('Add Addresses')}
                </Button>
            </Form>
        </>
    );
};
export default FormManageAddreessBase;
