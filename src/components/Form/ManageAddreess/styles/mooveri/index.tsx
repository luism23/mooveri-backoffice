import { FormManageAddreessClassProps } from '@/components/Form/ManageAddreess/Base';

export const mooveri: FormManageAddreessClassProps = {
    classNameContent: `
    m-h-auto
  `,
    styleTemplateInput: {
        address: 'mooveriManage',
        apt: 'mooveriManage',
        city: 'mooveriManage',
        zipcode: 'mooveriManage',
    },
    styleTemplateButton: 'mooveri',
    styleTemplateGoogle: 'mooveriManage',
    inputs: {
        address: true,
        city: true,
        apt: true,
        zipcode: true,
        location: true,
    },
};
