import { Story, Meta } from "@storybook/react";

import { FormAddPaymentProps, FormAddPayment } from "./index";
import { DataFormPayment } from "@/interfaces/FormPayment";
import log from "@/functions/log";

export default {
    title: "Form/AddPayment",
    component: FormAddPayment,
} as Meta;

const Template: Story<FormAddPaymentProps> = (args) => <FormAddPayment {...args} />;

export const Index = Template.bind({});
Index.args = {
    
    onValidateUsername:  async (data: string)=> {
        log("onValidateUsername",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }
    },
    
    onSubmit:  async (data: DataFormPayment)=> {
        log("DataFormAddPayment",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "AddPayment ok",
        };
    },

};
