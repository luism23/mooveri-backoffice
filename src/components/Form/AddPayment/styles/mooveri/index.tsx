import { FormPaymentClassProps } from '@/components/Form/Payment/Base';
import url from '@/data/routes';

export const mooveri: FormPaymentClassProps = {
    classNameContent: `
        m-h-auto
    `,
    classNameContentTitle: `
        text-center
        color-sea
    `,

    styleTemplateInputs: {
        state: 'mooveri',
        email: 'mooveri',
        country: 'mooveri',
        city: 'mooveri',
    },
    inputs: {
        email: true,
        city: true,
        country: true,
        state: true,
    },
    rsLogin: {
        google: true,
        facebook: true,
    },

    sizeContentInputs: 330,
    classNameContentInputs: `
        m-h-auto
        p-h-15
    `,
};

export const mooveriCompany: FormPaymentClassProps = {
    ...mooveri,
    rsLogin: {
        facebook: false,
        google: true,
    },
    LinkLogin: url.company.login,
};
