import { Story, Meta } from "@storybook/react";

import { FormBussinessInformationProps, FormBussinessInformation } from "./index";

export default {
    title: "Form/BussinessInformation",
    component: FormBussinessInformation,
} as Meta;

const Template: Story<FormBussinessInformationProps> = (args) => (
    <FormBussinessInformation {...args} />
);



export const Index = Template.bind({});
Index.args = {
};