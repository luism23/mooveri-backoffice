import { FormBussinessInformationClassProps } from '@/components/Form/BussinessInformation/Base';

export const mooveri: FormBussinessInformationClassProps = {
    styleTemplateInput: {
        company: 'mooveriManage',
        legalName: 'mooveriManage',
        yearFounded: 'mooveriManage',
        contactName: 'mooveriManage',
        tel: 'mooveriTelCompany',
        alternativeTel: 'mooveriTelCompany',
    },
    styleTemplateTextLogo: 'mooveri5',
    styleTemplateButton: 'mooveri',
    inputs: {
        company: true,
        legalName: true,
        yearFounded: true,
        contactName: true,
        tel: true,
        alternativeTel: true,
    },
    styleTemplateTexUpload: 'mooveri4',
    classNameContent: `
        flex
        flex-gap-40
        flex-gap-column-40
    `,
    classNameContentAvatar: `
        flex-12 
        flex-md-3 
        m-b-20 
        m-md-b-0 
        flex 
        flex-justify-center 
        flex-aling-start 
        flex-align-content-start
        text-center
   `,
    classNameContentForm: `
        flex-12 
        flex-md-9
        
   `,
};
