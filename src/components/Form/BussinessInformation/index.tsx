import { useMemo } from 'react';

import * as styles from '@/components/Form/BussinessInformation/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormBussinessInformationBaseProps,
    FormBussinessInformationBase,
} from '@/components/Form/BussinessInformation/Base';

export const FormBussinessInformationStyle = { ...styles } as const;

export type FormBussinessInformationStyles =
    keyof typeof FormBussinessInformationStyle;

export interface FormBussinessInformationProps
    extends FormBussinessInformationBaseProps {
    styleTemplate?: FormBussinessInformationStyles | ThemesType;
}

export const FormBussinessInformation = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormBussinessInformationProps) => {
    const Style = useMemo(
        () =>
            FormBussinessInformationStyle[
                styleTemplate as FormBussinessInformationStyles
            ] ?? FormBussinessInformationStyle._default,
        [styleTemplate]
    );

    return <FormBussinessInformationBase {...Style} {...props} />;
};
export default FormBussinessInformation;
