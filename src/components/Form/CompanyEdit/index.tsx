import { useMemo } from 'react';

import * as styles from '@/components/Form/CompanyEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormCompanyEditBaseProps,
    FormCompanyEditBase,
} from '@/components/Form/CompanyEdit/Base';

export const FormCompanyEditStyle = { ...styles } as const;

export type FormCompanyEditStyles = keyof typeof FormCompanyEditStyle;

export interface FormCompanyEditProps extends FormCompanyEditBaseProps {
    styleTemplate?: FormCompanyEditStyles | ThemesType;
}

export const FormCompanyEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormCompanyEditProps) => {
    const Style = useMemo(
        () =>
            FormCompanyEditStyle[styleTemplate as FormCompanyEditStyles] ??
            FormCompanyEditStyle._default,
        [styleTemplate]
    );

    return <FormCompanyEditBase {...Style} {...props} />;
};
export default FormCompanyEdit;
