import { FormCompanyEditClassProps } from '@/components/Form/CompanyEdit/Base';

export const mooveri: FormCompanyEditClassProps = {
    styleTemplateInputAvatar: 'mooveriBackoffice',
    styleTemplateInputText: 'mooveriBackoffice',
    styleTemplateInputTel: 'mooveriBackoffice',
    styleTemplateButton: 'mooveriBackoffice',
    styleTemplateConfigHorarios: 'mooveriBackoffice',
    GridProps: {
        // areas:["ABC"],
        className: `
            grid-columns-3
            column-gap-10
            row-gap-10
            m-b-20
        `,
    },
    classNameContentInput: `
        grid-
        flex
        flex-direction-column
        row-gap-10
    `,
    classNameContentInputLabel: `
        font-16 font-nunito
        color-white
        font-nunito 
        font-w-800
        d-block
        width-p-100
    `,
    ContentWidthLinkProps: {
        size: 300,
        className: `
            overflow-hidden
            
        `,
        style: {
            width: '15rem',
        },
    },
    classNameContentInputFileLink: `
        d-block
        width-p-100
        text-white-space-nowrap
        text-overflow-ellipsis
        overflow-hidden
    `,
};
