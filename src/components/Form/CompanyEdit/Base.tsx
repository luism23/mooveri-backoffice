import { useMemo, useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';

import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

import {
    CompanyDetailsProps,
    CompanyPricesProps,
    CompanyProps,
    CompanyVerificationProps,
} from '@/interfaces/Company';

import { useData } from 'fenextjs-hook/cjs/useData';

import { useLang } from '@/lang/translate';

import { CompanyYup } from '@/validations/Company';

import InputText, { InputTextStyles } from '@/components/Input/Text';
import Theme, { ThemesType } from '@/config/theme';
import { Grid, GridProps } from '@/components/Grid';
import { InputAvatar, InputAvatarStyles } from '@/components/Input/Avatar';
import { InputTel, InputTelStyles } from '@/components/Input/Tel';
import InputFile from '@/components/Input/File';
import { FrontIdentification } from '@/svg/FrontIdentification';
import { InputSelect } from '@/components/Input/Select';
import { STATUS } from '@/interfaces/MooveriStatus';
import Link from '@/components/Link';
import InputNumber from '@/components/Input/Number';
import {
    ConfigHorarios,
    ConfigHorariosStyles,
} from '@/components/ConfigHorarios';
import { useRouter } from 'next/router';
import { UserProps } from '@/interfaces/User';
import Text from '@/components/Text';
import InputPassword from '@/components/Input/Password';
import url from '@/data/routes';

export interface FormCompanyEditClassProps {
    classNameContent?: string;
    classNameContentInputLabel?: string;
    classNameContentInputFileLink?: string;
    ContentWidthLinkProps?: ContentWidthProps;

    styleTemplateInputText?: InputTextStyles | ThemesType;
    styleTemplateInputTel?: InputTelStyles | ThemesType;
    styleTemplateInputAvatar?: InputAvatarStyles | ThemesType;
    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateConfigHorarios?: ConfigHorariosStyles | ThemesType;

    GridProps?: GridProps;
    classNameContentInput?: string;
}

export type onSubmintCompanyEdit = (
    data: CompanyProps
) => Promise<SubmitResult> | SubmitResult;

export interface FormCompanyEditBaseProps {
    defaultData: CompanyProps;
    onSubmit?: onSubmintCompanyEdit;

    showPassword?: boolean;
}

export interface FormCompanyEditProps
    extends FormCompanyEditClassProps,
        FormCompanyEditBaseProps {}
export const FormCompanyEditBase = ({
    classNameContent = '',
    classNameContentInputLabel = '',
    classNameContentInputFileLink = '',
    ContentWidthLinkProps = {},
    styleTemplateInputText = Theme.styleTemplate ?? '_default',
    styleTemplateInputTel = Theme.styleTemplate ?? '_default',
    styleTemplateInputAvatar = Theme.styleTemplate ?? '_default',
    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateConfigHorarios = Theme.styleTemplate ?? '_default',
    GridProps = {},
    classNameContentInput = '',

    defaultData,
    showPassword = false,
    ...props
}: FormCompanyEditProps) => {
    const _t = useLang();
    const router = useRouter();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const { data: company_details, onChangeData: onChangeCompanyDetails } =
        useData<CompanyDetailsProps>(defaultData?.company_details ?? {});

    const { data: user_company, onChangeData: onChangeCompanyUser } =
        useData<UserProps>(defaultData?.user_company ?? {});

    const { data: company_prices, onChangeData: onChangeCompanyPrices } =
        useData<CompanyPricesProps>(defaultData?.company_prices ?? {});

    const {
        data: company_verification,
        onChangeData: onChangeCompanyVerification,
    } = useData<CompanyVerificationProps>(
        defaultData?.company_verification ?? {}
    );
    const { data, onChangeData, dataMemo } = useData<
        CompanyProps,
        CompanyProps
    >(defaultData, {
        onMemo: (d) => {
            return {
                ...d,
                company_details,
                company_verification,
                company_prices,
                user_company,
            };
        },
    });

    const onSave = async () => {
        const result = await props?.onSubmit?.(dataMemo ?? data);
        router.push(url.companies.index);
        if (result) {
            return result;
        }
        const r: SubmitResult = {
            status: 'ok',
        };
        return r;
    };

    const validate = useMemo(() => CompanyYup(dataMemo ?? data), [data]);

    return (
        <>
            <ContentWidth className={classNameContent} size={-1}>
                <Form<CompanyProps>
                    id="user-edit"
                    data={data}
                    onSubmit={onSave}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={validate}
                >
                    <Grid {...GridProps}>
                        <div className={`${classNameContentInput}`}>
                            <span className={`${classNameContentInputLabel}`}>
                                {_t('Logo')}
                            </span>
                            <InputAvatar
                                defaultValue={{
                                    fileData: (data?.imagen ?? '') as string,
                                }}
                                styleTemplate={styleTemplateInputAvatar}
                                onSubmit={(e) => {
                                    onChangeData('imagen')(e?.fileData);
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <span className={`${classNameContentInputLabel}`}>
                                {_t('Conver Photo or Banner')}
                            </span>
                            <InputAvatar
                                defaultValue={{
                                    fileData: (data?.imagen_banner ??
                                        '') as string,
                                }}
                                styleTemplate={styleTemplateInputAvatar}
                                onSubmit={(e) => {
                                    onChangeData('imagen_banner')(e?.fileData);
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}></div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Name"
                                label={'Name'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={(data?.name ?? '') as string}
                                onChange={onChangeData('name')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Legal Name"
                                label={'Legal Name'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (data?.legal_name ?? '') as string
                                }
                                onChange={onChangeData('legal_name')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Description"
                                label={'Description'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (data?.description ?? '') as string
                                }
                                type="textarea"
                                onChange={onChangeData('description')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputTel
                                placeholder="Local Phone"
                                label={'Local Phone'}
                                styleTemplate={styleTemplateInputTel}
                                defaultValue={{
                                    code: (data?.phone_1 ?? '')?.split(
                                        ' '
                                    )[0] as string,
                                    tel: (data?.phone_1 ?? '')?.split?.(
                                        ' '
                                    )?.[1] as string,
                                }}
                                onChange={(e) => {
                                    onChangeData('phone_1')(
                                        `${e.code} ${e.tel}`
                                    );
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputTel
                                placeholder="Mobile Phone"
                                label={'Mobile Phone'}
                                styleTemplate={styleTemplateInputTel}
                                defaultValue={{
                                    code: (data?.phone_2 ?? '')?.split(
                                        ' '
                                    )[0] as string,
                                    tel: (data?.phone_2 ?? '')?.split?.(
                                        ' '
                                    )?.[1] as string,
                                }}
                                onChange={(e) => {
                                    onChangeData('phone_2')(
                                        `${e.code} ${e.tel}`
                                    );
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Zip Code"
                                label={'Zip Code'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={(data?.zip_code ?? '') as string}
                                onChange={onChangeData('zip_code')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="SSN"
                                label={'SSN'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={(data?.ssn ?? '') as string}
                                onChange={onChangeData('ssn')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="EIN"
                                label={'EIN'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={(data?.ein ?? '') as string}
                                onChange={onChangeData('ein')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}></div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Contact Name"
                                label={'Contact Name'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (company_details.contact_name ??
                                        '') as string
                                }
                                onChange={onChangeCompanyDetails(
                                    'contact_name'
                                )}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Year Founded"
                                label={'Year Founded'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (company_details.year_founded ??
                                        '') as string
                                }
                                onChange={onChangeCompanyDetails(
                                    'year_founded'
                                )}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Facebook Page"
                                label={'Facebook Page'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (company_details.facebook_page_link ??
                                        '') as string
                                }
                                onChange={onChangeCompanyDetails(
                                    'facebook_page_link'
                                )}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Instagram Page"
                                label={'Instagram Page'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (company_details.instagram_page_link ??
                                        '') as string
                                }
                                onChange={onChangeCompanyDetails(
                                    'instagram_page_link'
                                )}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Website"
                                label={'Website'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (company_details.website_link ??
                                        '') as string
                                }
                                onChange={onChangeCompanyDetails(
                                    'website_link'
                                )}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Linkedin"
                                label={'Linkedin'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (company_details.linkedin_link ??
                                        '') as string
                                }
                                onChange={onChangeCompanyDetails(
                                    'linkedin_link'
                                )}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputNumber
                                placeholder="Trucks Have"
                                label={'Trucks Have'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={parseFloat(
                                    `${company_details?.trucks_have ?? 0}`
                                )}
                                onChange={onChangeCompanyDetails('trucks_have')}
                                min={0}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputNumber
                                placeholder="Workers Have"
                                label={'Workers Have'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={parseFloat(
                                    `${company_details?.workers_have ?? 0}`
                                )}
                                onChange={onChangeCompanyDetails(
                                    'workers_have'
                                )}
                                min={0}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}></div>
                        <div className={`${classNameContentInput}`}>
                            <span className={`${classNameContentInputLabel}`}>
                                {_t('Local License')}
                            </span>
                            {company_verification.local_license_link &&
                            company_verification.local_license_link != '' ? (
                                <>
                                    <div>
                                        <ContentWidth
                                            {...ContentWidthLinkProps}
                                        >
                                            <Link
                                                href={
                                                    company_verification.local_license_link
                                                }
                                                className={
                                                    classNameContentInputFileLink
                                                }
                                                targetBlank={true}
                                            >
                                                {
                                                    company_verification.local_license_link
                                                }
                                            </Link>
                                        </ContentWidth>
                                    </div>
                                    <InputSelect
                                        label={'Status'}
                                        defaultValue={{
                                            value:
                                                company_verification.local_license_status ??
                                                '',
                                            text:
                                                company_verification.local_license_status ??
                                                '',
                                        }}
                                        onChange={(e) => {
                                            onChangeCompanyVerification(
                                                'local_license_status'
                                            )(e.value);
                                        }}
                                        options={
                                            Object?.values(STATUS)?.map(
                                                (e) => ({
                                                    value: `${e}`,
                                                    text: `${e}`,
                                                })
                                            ) ?? []
                                        }
                                        styleTemplate={styleTemplateInputText}
                                    />
                                </>
                            ) : (
                                <>
                                    <span
                                        className={`${classNameContentInputLabel}`}
                                    >
                                        {_t('No file uploaded')}
                                    </span>
                                </>
                            )}
                            <InputFile
                                defaultValue={
                                    (company_verification.local_license_link ??
                                        '') as string
                                }
                                onChange={(e) => {
                                    onChangeCompanyVerification(
                                        'local_license_link'
                                    )(e.fileData);
                                }}
                            >
                                <FrontIdentification size={250} />
                            </InputFile>
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <span className={`${classNameContentInputLabel}`}>
                                {_t('Document License')}
                            </span>

                            {company_verification.dot_license_link &&
                            company_verification.dot_license_link != '' ? (
                                <>
                                    <ContentWidth {...ContentWidthLinkProps}>
                                        <Link
                                            href={
                                                company_verification.dot_license_link
                                            }
                                            className={
                                                classNameContentInputFileLink
                                            }
                                            targetBlank={true}
                                        >
                                            {
                                                company_verification.dot_license_link
                                            }
                                        </Link>
                                    </ContentWidth>

                                    <InputSelect
                                        label={'Status'}
                                        defaultValue={{
                                            value:
                                                company_verification.dot_license_status ??
                                                '',
                                            text:
                                                company_verification.dot_license_status ??
                                                '',
                                        }}
                                        onChange={(e) => {
                                            onChangeCompanyVerification(
                                                'dot_license_status'
                                            )(e.value);
                                        }}
                                        options={
                                            Object?.values(STATUS)?.map(
                                                (e) => ({
                                                    value: `${e}`,
                                                    text: `${e}`,
                                                })
                                            ) ?? []
                                        }
                                        styleTemplate={styleTemplateInputText}
                                    />
                                </>
                            ) : (
                                <>
                                    <span
                                        className={`${classNameContentInputLabel}`}
                                    >
                                        {_t('No file uploaded')}
                                    </span>
                                </>
                            )}

                            <InputFile
                                defaultValue={
                                    (company_verification.dot_license_link ??
                                        '') as string
                                }
                                onChange={(e) => {
                                    onChangeCompanyVerification(
                                        'dot_license_link'
                                    )(e.fileData);
                                }}
                            >
                                <FrontIdentification size={250} />
                            </InputFile>
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <span className={`${classNameContentInputLabel}`}>
                                {_t('CDL')}
                            </span>

                            {company_verification.cdl_link &&
                            company_verification.cdl_link != '' ? (
                                <>
                                    <ContentWidth {...ContentWidthLinkProps}>
                                        <Link
                                            href={company_verification.cdl_link}
                                            className={
                                                classNameContentInputFileLink
                                            }
                                            targetBlank={true}
                                        >
                                            {company_verification.cdl_link}
                                        </Link>
                                    </ContentWidth>

                                    <InputSelect
                                        label={'Status'}
                                        defaultValue={{
                                            value:
                                                company_verification.cdl_status ??
                                                '',
                                            text:
                                                company_verification.cdl_status ??
                                                '',
                                        }}
                                        onChange={(e) => {
                                            onChangeCompanyVerification(
                                                'cdl_status'
                                            )(e.value);
                                        }}
                                        options={
                                            Object?.values(STATUS)?.map(
                                                (e) => ({
                                                    value: `${e}`,
                                                    text: `${e}`,
                                                })
                                            ) ?? []
                                        }
                                        styleTemplate={styleTemplateInputText}
                                    />
                                </>
                            ) : (
                                <>
                                    <span
                                        className={`${classNameContentInputLabel}`}
                                    >
                                        {_t('No file uploaded')}
                                    </span>
                                </>
                            )}
                            <InputFile
                                defaultValue={
                                    (company_verification.cdl_link ??
                                        '') as string
                                }
                                onChange={(e) => {
                                    onChangeCompanyVerification('cdl_link')(
                                        e.fileData
                                    );
                                }}
                            >
                                <FrontIdentification size={250} />
                            </InputFile>
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputNumber
                                placeholder="Price Overtime"
                                label={'Price Overtime'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={parseFloat(
                                    `${company_prices?.overtime_value ?? 0}`
                                )}
                                onChange={onChangeCompanyPrices(
                                    'overtime_value'
                                )}
                                min={0}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputNumber
                                placeholder="Price Standar"
                                label={'Price Standar'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={parseFloat(
                                    `${company_prices?.standar_value ?? 0}`
                                )}
                                onChange={onChangeCompanyPrices(
                                    'standar_value'
                                )}
                                min={0}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputNumber
                                placeholder="Price Weekend Rate"
                                label={'Price Weekend Rate'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={parseFloat(
                                    `${company_prices?.weekend_rate_value ?? 0}`
                                )}
                                onChange={onChangeCompanyPrices(
                                    'weekend_rate_value'
                                )}
                                min={0}
                            />
                        </div>

                        <div>
                            <ConfigHorarios
                                defaultValue={data.company_horarios}
                                styleTemplate={styleTemplateConfigHorarios}
                                onChange={onChangeData('company_horarios')}
                            />
                        </div>
                        <div>
                            <Text styleTemplate="mooveri8">{_t('User')}</Text>
                            <br />
                        </div>

                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="First Name"
                                label={'First Name'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (user_company.first_name ?? '') as string
                                }
                                onChange={onChangeCompanyUser('first_name')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Last Name"
                                label={'Last Name'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (user_company.last_name ?? '') as string
                                }
                                onChange={onChangeCompanyUser('last_name')}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputTel
                                placeholder="Phone"
                                label={'Phone'}
                                styleTemplate={styleTemplateInputTel}
                                defaultValue={{
                                    code: (user_company.phone ?? '')?.split(
                                        ' '
                                    )[0] as string,
                                    tel: (user_company.phone ?? '')?.split?.(
                                        ' '
                                    )?.[1] as string,
                                }}
                                onChange={(e) => {
                                    onChangeCompanyUser('phone')(
                                        `${e.code} ${e.tel}`
                                    );
                                }}
                            />
                        </div>
                        <div className={`${classNameContentInput}`}>
                            <InputText
                                placeholder="Email"
                                label={'Email'}
                                styleTemplate={styleTemplateInputText}
                                defaultValue={
                                    (user_company.email ?? '') as string
                                }
                                onChange={onChangeCompanyUser('email')}
                            />
                        </div>
                        {showPassword ? (
                            <>
                                <div className={`${classNameContentInput}`}>
                                    <InputPassword
                                        placeholder="Password"
                                        label={'Password'}
                                        styleTemplate={styleTemplateInputText}
                                        onChange={onChangeCompanyUser(
                                            'password'
                                        )}
                                    />
                                </div>
                            </>
                        ) : (
                            <></>
                        )}
                        <div>
                            <Grid {...GridProps}>
                                <div className={`${classNameContentInput}`}>
                                    <Button
                                        styleTemplate={styleTemplateButton}
                                        loader={loader}
                                        disabled={disabled}
                                    >
                                        {_t('Save')}
                                    </Button>
                                </div>
                            </Grid>
                        </div>
                    </Grid>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormCompanyEditBase;
