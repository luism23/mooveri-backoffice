import { Story, Meta } from "@storybook/react";

import { FormCompanyEditProps, FormCompanyEdit } from "./index";

export default {
    title: "Form/CompanyEdit",
    component: FormCompanyEdit,
} as Meta;

const Template: Story<FormCompanyEditProps> = (args) => (
    <FormCompanyEdit {...args} />
);

export const Index = Template.bind({});
Index.args = {
    
};
