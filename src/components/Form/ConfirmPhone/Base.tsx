import { useState } from 'react';

import { Button, ButtonStyles } from '@/components/Button';
import { InputTelValue } from '@/components/Input/Tel/Base';
import { InputTel } from '@/components/InputYup/Tel';
import { Title, TitleStyles, TitleStylesType } from '@/components/Title';
import { Text, TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';
import { Form, SubmitResult } from '@/components/Form/Base';
import { ContentWidth } from '@/components/ContentWidth';

import { useLang } from '@/lang/translate';

import { ValidateTel } from '@/validations/tel';
import Theme, { ThemesType } from '@/config/theme';
import { LinkStyles } from '@/components/Link';
import { InputTelStyles } from '@/components/Input/Tel';
import { UserLoginProps, useUser } from '@/hook/useUser';
import { useRouter } from 'next/router';
import url from '@/data/routes';

export interface FormConfirmPhoneClassProps {
    classNameContent?: string;
    classNameTitle?: string;
    classNameContentText?: string;
    classNameText?: string;

    typeStyleTemplateTitle?: TitleStylesType;
    styleTemplateTitle?: TitleStyles | ThemesType;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateInput?: InputTelStyles | ThemesType;
    styleTemplateLink?: LinkStyles;
    styleTemplateButton?: ButtonStyles | ThemesType;
}

export type onSubmintConfirmPhone = (
    data: InputTelValue,
    user: UserLoginProps
) => Promise<SubmitResult> | SubmitResult;

export interface FormConfirmPhoneBaseProps {
    onSubmit: onSubmintConfirmPhone;
    token?: string;
    urlRedirect?: string;
}

export interface FormConfirmPhoneProps
    extends FormConfirmPhoneBaseProps,
        FormConfirmPhoneClassProps {}

export const FormConfirmPhoneBase = ({
    classNameContent = '',
    classNameTitle = '',
    classNameContentText = '',
    classNameText = '',

    styleTemplateButton = Theme.styleTemplate ?? '_default',
    styleTemplateInput = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    typeStyleTemplateTitle = 'h1',

    ...props
}: FormConfirmPhoneProps) => {
    const route = useRouter();
    const _t = useLang();
    const { user } = useUser();
    const [disabled, setDisabled] = useState<boolean>(true);
    const [loader, setLoader] = useState<boolean>(false);
    const [data, onChangeData] = useState<InputTelValue>({
        code: '+57',
        tel: '',
    });

    return (
        <>
            <ContentWidth size={350} className={classNameContent}>
                <Form<InputTelValue>
                    id="confirm-phone"
                    data={data}
                    onSubmit={async (data: InputTelValue) => {
                        return await props.onSubmit(data, user);
                    }}
                    disabled={disabled}
                    onChangeDisable={setDisabled}
                    loader={loader}
                    onChangeLoader={setLoader}
                    yup={ValidateTel({
                        require: true,
                    })}
                    onAfterSubmit={() => {
                        route.push(
                            `${props?.urlRedirect ?? url.confirmPhone.code}/${
                                data.code
                            }-${data.tel}`
                        );
                    }}
                >
                    <Title
                        typeStyle={typeStyleTemplateTitle}
                        styleTemplate={styleTemplateTitle}
                        className={classNameTitle}
                    >
                        {_t('Confirm your Phone')}
                    </Title>
                    <Space size={15} />
                    <ContentWidth size={230} className={classNameContentText}>
                        <Text
                            styleTemplate={styleTemplateText}
                            className={classNameText}
                        >
                            {_t(
                                'We will send you an SMS with a code to confirm your identity'
                            )}
                        </Text>
                    </ContentWidth>
                    <Space size={15} />
                    <InputTel
                        styleTemplate={styleTemplateInput}
                        defaultValue={data}
                        placeholder={_t('Phone Number')}
                        onChange={onChangeData}
                    />
                    <Space size={14} />
                    <Button
                        styleTemplate={styleTemplateButton}
                        disabled={disabled}
                        loader={loader}
                    >
                        {_t('Send SMS Activation code')}
                    </Button>
                </Form>
            </ContentWidth>
        </>
    );
};
export default FormConfirmPhoneBase;
