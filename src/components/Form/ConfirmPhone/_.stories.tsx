import { Story, Meta } from "@storybook/react";

import { FormConfirmPhoneProps,FormConfirmPhone } from "./index";
import { InputTelValue } from "@/components/Input/Tel/Base";
import log from "@/functions/log";

export default {
    title: "Form/ConfirmPhone",
    component: FormConfirmPhone,
} as Meta;

const Template: Story<FormConfirmPhoneProps> = (args) => (
    <FormConfirmPhone {...args} />
);

export const Index = Template.bind({});
Index.args = {
    onSubmit: async (data: InputTelValue) => {
        log("InputTelValue",data)
        await new Promise((r) => setTimeout(r, 2000));
        const error =false
        if(error){
            throw {
                message : "Error"
            }
        }

        return {
            status: "ok",
            message: "Confirm phone ok",
        };
    },
};
