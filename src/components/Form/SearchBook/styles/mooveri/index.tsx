import { FormSearchBookClassProps } from '@/components/Form/SearchBook/Base';

export const mooveri: FormSearchBookClassProps = {
    styleTemplateInput: {
        date: 'mooveri3',
        moveSize: 'mooveri3',
        movingFrom: 'mooveri3',
        movingTo: 'mooveri3',
        doPacking: 'mooveriManage',
        doStorage: 'mooveriManage',
        numberStairs: 'mooveri3',
        youStairs: 'mooveriManage',
    },
    styleTemplateButton: 'mooveriSearch',
    styleTemplateGoogle: 'mooveriManage',
    inputs: {
        date: true,
        moveSize: true,
        movingFrom: true,
        movingTo: true,
        doPacking: true,
        doStorage: true,
        numberStairs: true,
        scan: true,
        youStairs: true,
    },
    styleTemplateButtonScan: 'mooveri',
    styleTemplateCheckBox: 'mooveri',
    classNameText: `
        text-center
        color-greyishBrown
    `,
    GridProps: {
        className: `
            gap-10
            no-div-void
        `,
        areas: ['AB'],
    },
};
