import { Story, Meta } from "@storybook/react";

import { FormSearchBookProps, FormSearchBook } from "./index";
import { DataSearchBook } from "@/interfaces/SearchBook";
import { SubmitResult } from "../Base";
import { DataTypeFormSearch } from "./Base";

export default {
    title: "Form/SearchBook",
    component: FormSearchBook,
} as Meta;

const Template: Story<FormSearchBookProps> = (args) => (
    <FormSearchBook {...args} />
);



export const Index = Template.bind({});
Index.args = {
    onSubmit: async (data: DataSearchBook<DataTypeFormSearch>) => {
        await new Promise((r) => setTimeout(r, 2000));
        const result: SubmitResult = {
            status:"ok",
            message:"ok",
            data
        }
        return result
    }
};