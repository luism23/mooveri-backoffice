import { useMemo } from 'react';

import * as styles from '@/components/Form/SearchBook/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FormSearchBookBaseProps,
    FormSearchBookBase,
} from '@/components/Form/SearchBook/Base';

export const FormSearchBookStyle = { ...styles } as const;

export type FormSearchBookStyles = keyof typeof FormSearchBookStyle;

export interface FormSearchBookProps extends FormSearchBookBaseProps {
    styleTemplate?: FormSearchBookStyles | ThemesType;
}

export const FormSearchBook = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FormSearchBookProps) => {
    const Style = useMemo(
        () =>
            FormSearchBookStyle[styleTemplate as FormSearchBookStyles] ??
            FormSearchBookStyle._default,
        [styleTemplate]
    );

    return <FormSearchBookBase {...Style} {...props} />;
};
export default FormSearchBook;
