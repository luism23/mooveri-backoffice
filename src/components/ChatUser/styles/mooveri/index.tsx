import { ChatUserClassProps } from '@/components/ChatUser/Base';

export const mooveri: ChatUserClassProps = {
    classNameContent: `
        flex 
        flex-align-center 
        p-h-15 
        p-v-10
        bg-white
        box-shadow
        box-shadow-inset
        box-shadow-c-greenish-cyan
        transition-5
        cursor-pointer
    `,
    classNameContentActive: `
        box-shadow-x--10
    `,
    classNameContentNotActive: `
    `,
    classNameContentDate: `
        flex 
        flex-align-center
        text-right
        flex-justify-right
        m-l-auto
    `,
    classNameImg: `
        width-50
        height-50
        border-radius-100
    `,
    styleTemplateTextUser: 'mooveri15',
    styleTemplateTextSend: 'mooveri13',
    sizeImg: 10,
    classNameContentUserText: `
        m-r-auto
    `,
    classNameContentImg: `
        m-r-10
    `,
    styleTemplateTextDate: 'mooveri15',
    classNameImgDate: `
        m-r-5
    `,
    styleTemplateTextMsj: 'mooveri14',
};
