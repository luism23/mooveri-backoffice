import Theme, { ThemesType } from '@/config/theme';
import { parseDateYYYYMMDD } from '@/functions/parseDate';
import { useLang } from '@/lang/translate';
import { Date as SvgDate } from '@/svg/date';
import Text, { TextStyles } from '../Text';

export interface ChatUserClassProps {
    classNameContent?: string;
    classNameContentActive?: string;
    classNameContentNotActive?: string;
    classNameImgDate?: string;
    classNameTextDate?: string;
    classNameContentImg?: string;
    classNameContentUserText?: string;
    classNameContentDate?: string;
    classNameImg?: string;
    sizeImg?: number;

    styleTemplateTextUser?: TextStyles | ThemesType;
    styleTemplateTextDate?: TextStyles | ThemesType;
    styleTemplateTextMsj?: TextStyles | ThemesType;
    styleTemplateTextSend?: TextStyles | ThemesType;
}

export interface ChatUserBaseProps {
    user?: string;
    msj?: string;
    date?: Date;
    img?: string;
    statusMsj?: 'view' | 'send' | 'none';
    DateOfLastMsj?: Date;
    active?: boolean;
}

export interface ChatUserProps extends ChatUserClassProps, ChatUserBaseProps {}

export const ChatUserBase = ({
    classNameContent = '',
    classNameContentActive = '',
    classNameContentNotActive = '',
    classNameTextDate = '',
    classNameContentImg = '',
    classNameContentUserText = '',
    classNameContentDate = '',
    classNameImg = '',
    classNameImgDate = '',
    user,
    msj,

    sizeImg = 7,
    img,

    styleTemplateTextUser = Theme.styleTemplate ?? '_default',
    styleTemplateTextDate = Theme.styleTemplate ?? '_default',
    styleTemplateTextMsj = Theme.styleTemplate ?? '_default',
    styleTemplateTextSend = Theme.styleTemplate ?? '_default',

    active = false,
    date = new Date(),
    statusMsj = 'none',
    DateOfLastMsj = new Date(),
}: ChatUserProps) => {
    const _t = useLang();
    return (
        <>
            <div
                className={`${classNameContent} ${
                    active ? classNameContentActive : classNameContentNotActive
                }`}
            >
                <div className={classNameContentImg}>
                    <img className={classNameImg} src={img} alt="" />
                </div>
                <div className={classNameContentUserText}>
                    <Text styleTemplate={styleTemplateTextUser}>{user}</Text>
                    <Text styleTemplate={styleTemplateTextMsj}>{msj}</Text>
                    {statusMsj != 'none' ? (
                        <>
                            <Text styleTemplate={styleTemplateTextSend}>
                                {_t(statusMsj == 'send' ? 'Enviado' : 'Visto')}:{' '}
                                {parseDateYYYYMMDD(DateOfLastMsj)}
                            </Text>
                        </>
                    ) : (
                        <></>
                    )}
                </div>
                <div className={classNameContentDate}>
                    <Text
                        styleTemplate={styleTemplateTextDate}
                        className={classNameTextDate}
                    >
                        <span className={classNameImgDate}>
                            <SvgDate size={sizeImg} />
                        </span>
                        {parseDateYYYYMMDD(date)}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default ChatUserBase;
