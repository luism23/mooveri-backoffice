export interface BarStepsClassProps {
    classNameContentStep?: string;
    classNameContentCircle?: string;
    classNameLine?: string;
    classNameCircleDisabled?: string;
    classNameCircleActive?: string;
}

export interface BarStepsBaseProps {
    nItems?: number;
    nActive?: number;
}

export interface BarStepsProps extends BarStepsClassProps, BarStepsBaseProps {}

export const BarStepsBase = ({
    classNameContentStep = '',
    classNameContentCircle = '',
    classNameLine = '',
    classNameCircleDisabled = '',
    classNameCircleActive = '',

    nItems = 1,
    nActive = 1,
}: BarStepsProps) => {
    const elements: number[] = new Array(nItems).fill(1);

    return (
        <>
            <div className={classNameContentStep}>
                {elements.map((e, i) => (
                    <div key={i} className={classNameContentCircle}>
                        {i != 0 ? (
                            <span className={classNameLine}></span>
                        ) : (
                            <></>
                        )}
                        <span
                            className={
                                nActive == i + 1
                                    ? classNameCircleActive
                                    : classNameCircleDisabled
                            }
                        ></span>
                        {i != elements.length - 1 ? (
                            <span className={classNameLine}></span>
                        ) : (
                            <></>
                        )}
                    </div>
                ))}
            </div>
        </>
    );
};
export default BarStepsBase;
