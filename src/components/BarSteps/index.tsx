import { useMemo } from 'react';

import * as styles from '@/components/BarSteps/styles';

import { Theme, ThemesType } from '@/config/theme';

import { BarStepsBaseProps, BarStepsBase } from '@/components/BarSteps/Base';

export const BarStepsStyle = { ...styles } as const;

export type BarStepsStyles = keyof typeof BarStepsStyle;

export interface BarStepsProps extends BarStepsBaseProps {
    styleTemplate?: BarStepsStyles | ThemesType;
}

export const BarSteps = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BarStepsProps) => {
    const Style = useMemo(
        () =>
            BarStepsStyle[styleTemplate as BarStepsStyles] ??
            BarStepsStyle._default,
        [styleTemplate]
    );

    return <BarStepsBase {...Style} {...props} />;
};
export default BarSteps;
