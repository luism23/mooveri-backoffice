import { BarStepsClassProps } from '@/components/BarSteps/Base';

export const mooveri: BarStepsClassProps = {
    classNameContentStep: `
        flex 
        flex-justify-center
`,
    classNameContentCircle: `
        flex 
        flex-align-center 
        flex-nowrap
`,
    classNameCircleActive: ` 
        bg-sea 
        border-sea 
        flex  
        border-style-solid 
        border-1 
        border-radius-50 
        p-12 
`,
    classNameCircleDisabled: `
        bg-white 
        border-whiteThree 
        flex  
        border-style-solid 
        border-1 
        border-radius-50 
        p-10 
`,
    classNameLine: `
        width-25 
        flex 
        p-v-1
        bg-whiteThree
`,
};
