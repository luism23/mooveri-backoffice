import { Story, Meta } from "@storybook/react";

import { BarStepsProps, BarSteps } from "./index";

export default {
    title: "BarSteps/BarSteps",
    component: BarSteps,
} as Meta;

const BarStepsIndex: Story<BarStepsProps> = (args) => (
    <BarSteps {...args}>Test Children</BarSteps>
);

export const Index = BarStepsIndex.bind({});
Index.args = {};
