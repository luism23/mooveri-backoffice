import { InputText, InputTextProps } from '@/components/Input/Text';

import { ValidateText } from '@/validations/text';
import { ErrorEmail } from '@/data/error/email';

export const InputEmail = (props: InputTextProps) => (
    <InputText
        {...props}
        yup={ValidateText({
            require: true,
            type: 'email',
            errors: ErrorEmail,
        })}
    />
);

export default InputEmail;
