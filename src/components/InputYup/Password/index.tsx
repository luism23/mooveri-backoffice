import {
    InputPassword as InputPassword_,
    InputPasswordProps,
} from '@/components/Input/Password';

import { ValidateText } from '@/validations/text';
import { ErrorPassword } from '@/data/error/password';

export const InputPassword = (props: InputPasswordProps) => (
    <InputPassword_
        {...props}
        yup={ValidateText({
            require: true,
            min: 6,
            max: 20,
            errors: ErrorPassword,
        })}
    />
);

export interface InputRepeatPasswordProps extends InputPasswordProps {
    password?: string;
}

export const InputRepeatPassword = ({
    password,
    ...props
}: InputRepeatPasswordProps) => (
    <InputPassword_
        {...props}
        yup={ValidateText({
            require: true,
            errors: ErrorPassword,
        }).oneOf([password], ErrorPassword.noRepeat)}
    />
);

export default InputPassword;
