import { useMemo } from 'react';

import * as styles from '@/components/MessegesUser/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    MessegesUserBaseProps,
    MessegesUserBase,
} from '@/components/MessegesUser/Base';

export const MessegesUserStyle = { ...styles } as const;

export type MessegesUserStyles = keyof typeof MessegesUserStyle;

export interface MessegesUserProps extends MessegesUserBaseProps {
    styleTemplate?: MessegesUserStyles | ThemesType;
}

export const MessegesUser = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: MessegesUserProps) => {
    const Style = useMemo(
        () =>
            MessegesUserStyle[styleTemplate as MessegesUserStyles] ??
            MessegesUserStyle._default,
        [styleTemplate]
    );

    return <MessegesUserBase {...Style} {...props} />;
};
export default MessegesUser;
