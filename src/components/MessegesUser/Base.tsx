import Theme, { ThemesType } from '@/config/theme';
import { parseDateYYYYMMDD } from '@/functions/parseDate';
import Text, { TextStyles } from '../Text';

export interface MessegesUserClassItemProps {
    classNameContent?: string;
    classNameContentImg?: string;
    classNameContentUserText?: string;
    classNameImg?: string;
    classNameContentUserDate?: string;
    classNameContentUser?: string;

    styleTemplateTextUser?: TextStyles | ThemesType;
    styleTemplateTextDate?: TextStyles | ThemesType;
    styleTemplateTextMsj?: TextStyles | ThemesType;
}

export interface MessegesUserClassProps {
    styleNormal?: MessegesUserClassItemProps;
    styleMe?: MessegesUserClassItemProps;
}
export interface MessegesUserBaseProps {
    user?: string;
    msj?: string;
    date?: Date;
    img?: string;
    my?: boolean;
}

export interface MessegesUserProps
    extends MessegesUserClassProps,
        MessegesUserBaseProps {}

export const MessegesUserBase = ({
    styleNormal = {
        classNameContent: '',
        classNameContentImg: '',
        classNameContentUserText: '',
        classNameImg: '',
        classNameContentUserDate: '',
        classNameContentUser: '',
        styleTemplateTextUser: Theme.styleTemplate ?? '_default',
        styleTemplateTextMsj: Theme.styleTemplate ?? '_default',
        styleTemplateTextDate: Theme.styleTemplate ?? '_default',
    },
    styleMe = {
        classNameContent: '',
        classNameContentImg: '',
        classNameContentUserText: '',
        classNameImg: '',
        classNameContentUserDate: '',
        classNameContentUser: '',
        styleTemplateTextUser: Theme.styleTemplate ?? '_default',
        styleTemplateTextMsj: Theme.styleTemplate ?? '_default',
        styleTemplateTextDate: Theme.styleTemplate ?? '_default',
    },

    user,
    msj,
    img,
    my = false,

    date = new Date(),
}: MessegesUserProps) => {
    const {
        classNameContent,
        classNameContentImg,
        classNameImg,
        classNameContentUserText,
        classNameContentUserDate,
        classNameContentUser,
        styleTemplateTextUser,
        styleTemplateTextDate,
        styleTemplateTextMsj,
    } = my ? styleMe : styleNormal;

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentImg}>
                    <img className={classNameImg} src={img} alt="" />
                </div>
                <div className={classNameContentUserText}>
                    <div className={classNameContentUserDate}>
                        <div className={classNameContentUser}>
                            <Text styleTemplate={styleTemplateTextUser}>
                                {user}
                            </Text>
                        </div>
                        <div>
                            <Text styleTemplate={styleTemplateTextDate}>
                                {parseDateYYYYMMDD(date)}
                            </Text>
                        </div>
                    </div>
                    <Text styleTemplate={styleTemplateTextMsj}>{msj}</Text>
                </div>
            </div>
        </>
    );
};
export default MessegesUserBase;
