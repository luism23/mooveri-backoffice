# Toltip

## Dependencies

[ContentWidth](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ContentWidth)

```js
import { ContentWidth } from '@/components/ContentWidth';
```

## Import

```js
import { Toltip, ToltipStyles } from '@/components/Toltip';
```

## Props

```tsx
interface ToltipProps {}
```

## Use

```js
<Toltip>Toltip</Toltip>
```
