import { PropsWithChildren } from 'react';
import Text from '../Text';

export interface ToltipClassProps {}

export interface ToltipBaseProps extends PropsWithChildren {
    tooltipMessage?: string;
    classNameContentToltip?: string;
}

export interface ToltipProps extends ToltipClassProps, ToltipBaseProps {}

export const ToltipBase = ({
    tooltipMessage = '',
    classNameContentToltip = '',
    children,
}: ToltipProps) => {
    return (
        <>
            <div className={`${classNameContentToltip} tooltip-parent`}>
                <div className="">{children}</div>
                <div className="tooltip">
                    <Text className="text-center" styleTemplate="tolinkme">
                        {tooltipMessage}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default ToltipBase;
