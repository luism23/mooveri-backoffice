import { useMemo } from 'react';

import * as styles from '@/components/ListSuscripcionBotton/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ListSuscripcionBottonBaseProps,
    ListSuscripcionBottonBase,
} from '@/components/ListSuscripcionBotton/Base';

export const ListSuscripcionBottonStyle = { ...styles } as const;

export type ListSuscripcionBottonStyles =
    keyof typeof ListSuscripcionBottonStyle;

export interface ListSuscripcionBottonProps
    extends ListSuscripcionBottonBaseProps {
    styleListSuscripcionBotton?: ListSuscripcionBottonStyles | ThemesType;
}

export const ListSuscripcionBotton = ({
    styleListSuscripcionBotton = Theme?.styleTemplate ?? '_default',
    ...props
}: ListSuscripcionBottonProps) => {
    const Style = useMemo(
        () =>
            ListSuscripcionBottonStyle[
                styleListSuscripcionBotton as ListSuscripcionBottonStyles
            ] ?? ListSuscripcionBottonStyle._default,
        [styleListSuscripcionBotton]
    );

    return <ListSuscripcionBottonBase {...Style} {...props} />;
};
export default ListSuscripcionBotton;
