import money from '@/functions/money';
import { useLang } from '@/lang/translate';
import React from 'react';
import Text from '../Text';

export interface ListSuscripcionBottonClassProps {
    classNameContentButtonSubscription?: string;
    classNameContentEmail?: string;
    classNameContentCancel?: string;
    classNameContentStatistics?: string;
    classNameContentButton?: string;
    className?: string;
}

export interface ListSuscripcionBottonBaseProps {
    buttonName?: string;
    subscriptionValue?: number;
    totalmountpricebalance?: number;
    numSubscribers?: number;
    emailSubscribers?: any[];
    icon?: string;
}

export interface ListSuscripcionBottonProps
    extends ListSuscripcionBottonClassProps,
        ListSuscripcionBottonBaseProps {}

export const ListSuscripcionBottonBase = ({
    classNameContentButtonSubscription = '',
    classNameContentStatistics = '',
    classNameContentButton = '',
    classNameContentEmail = '',
    classNameContentCancel = '',
    className = '',
    buttonName = 'Button Name',
    subscriptionValue = 8,
    totalmountpricebalance = 0,
    numSubscribers = 0,
    icon,
    emailSubscribers = [],
}: ListSuscripcionBottonProps) => {
    const _t = useLang();
    return (
        <div className={className}>
            <div className={classNameContentButtonSubscription}>
                <div className={classNameContentButton}>
                    <div className="flex flex-align-center flex-justify-center border-style-solid border-3 border-black border-radius-10 height-40">
                        <img
                            className="width-20 height-20 m-r-5"
                            src={icon}
                            alt=""
                        />
                        <Text
                            className="text-capitalize m-r-5"
                            styleTemplate="tolinkme10"
                        >
                            {buttonName}
                        </Text>
                        <Text styleTemplate="tolinkme10">
                            {_t('Subscribed')}
                        </Text>
                    </div>
                </div>
                <div className={classNameContentStatistics}>
                    <Text styleTemplate="tolinkme20">
                        {money(totalmountpricebalance)}/{_t('Month')}
                    </Text>
                    <Text styleTemplate="tolinkme18">
                        {money(subscriptionValue)}/{_t('Month')}
                    </Text>
                </div>
            </div>
            <Text styleTemplate="tolinkme10">{numSubscribers} Subscribers</Text>
            <div className="subscribers">
                {emailSubscribers.map((email, index) => (
                    <div
                        className={classNameContentButtonSubscription}
                        key={index}
                    >
                        <div className={classNameContentEmail}>{email}</div>
                        <div className={classNameContentCancel}></div>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default ListSuscripcionBottonBase;
