import { Story, Meta } from "@storybook/react";

import { ListSuscripcionBottonProps, ListSuscripcionBotton } from "./index";

export default {
    title: "ListSuscripcionBotton/ListSuscripcionBotton",
    component: ListSuscripcionBotton,
} as Meta;

const ListSuscripcionBottonIndex: Story<ListSuscripcionBottonProps> = (args) => (
    <ListSuscripcionBotton {...args}>Test Children</ListSuscripcionBotton>
);

export const Index = ListSuscripcionBottonIndex.bind({});
Index.args = {};
