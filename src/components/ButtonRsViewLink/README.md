# ButtonRsViewLink

## Dependencies

[ButtonRsViewLink](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ButtonRsViewLink)

```js
import { ButtonRsViewLink } from '@/components/ButtonRsViewLink';
```

## Import

```js
import {
    ButtonRsViewLink,
    ButtonRsViewLinkStyles,
} from '@/components/ButtonRsViewLink';
```

## Props

```tsx
interface ButtonRsViewLinkProps {}
```

## Use

```js
<ButtonRsViewLink>ButtonRsViewLink</ButtonRsViewLink>
```
