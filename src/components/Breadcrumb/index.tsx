import { useMemo } from 'react';

import * as styles from '@/components/Breadcrumb/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    BreadcrumbBaseProps,
    BreadcrumbBase,
} from '@/components/Breadcrumb/Base';

export const BreadcrumbStyle = { ...styles } as const;

export type BreadcrumbStyles = keyof typeof BreadcrumbStyle;

export interface BreadcrumbProps extends BreadcrumbBaseProps {
    styleTemplate?: BreadcrumbStyles | ThemesType;
}

export const Breadcrumb = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BreadcrumbProps) => {
    const Style = useMemo(
        () =>
            BreadcrumbStyle[styleTemplate as BreadcrumbStyles] ??
            BreadcrumbStyle._default,
        [styleTemplate]
    );

    return <BreadcrumbBase {...Style} {...props} />;
};
export default Breadcrumb;
