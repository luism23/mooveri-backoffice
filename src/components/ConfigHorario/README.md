# ConfigHorario

## Dependencies

[ConfigHorario](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigHorario)

```js
import { ConfigHorario } from '@/components/ConfigHorario';
```

## Import

```js
import { ConfigHorario, ConfigHorarioStyles } from '@/components/ConfigHorario';
```

## Props

```tsx
interface ConfigHorarioProps {}
```

## Use

```js
<ConfigHorario>ConfigHorario</ConfigHorario>
```
