import { ConfigHorarioClassProps } from '@/components/ConfigHorario/Base';

export const mooveri: ConfigHorarioClassProps = {};

export const mooveriBackoffice: ConfigHorarioClassProps = {
    classNameContentHorario: `
    
    `,
    classNameContentTitle: `
        flex
        flex-align-center
        
        font-16 font-nunito
        color-white
        font-nunito 
        font-w-800
        width-p-100
        
    `,
    styleTemplateConfigSchedules: 'mooveriBackoffice',
};
