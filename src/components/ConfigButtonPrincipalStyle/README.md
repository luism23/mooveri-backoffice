# ConfigButtonPrincipalStyle

## Dependencies

[ConfigButtonPrincipalStyle](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigButtonPrincipalStyle)

```js
import { ConfigButtonPrincipalStyle } from '@/components/ConfigButtonPrincipalStyle';
```

## Import

```js
import {
    ConfigButtonPrincipalStyle,
    ConfigButtonPrincipalStyleStyles,
} from '@/components/ConfigButtonPrincipalStyle';
```

## Props

```tsx
interface ConfigButtonPrincipalStyleProps {}
```

## Use

```js
<ConfigButtonPrincipalStyle>
    ConfigButtonPrincipalStyle
</ConfigButtonPrincipalStyle>
```
