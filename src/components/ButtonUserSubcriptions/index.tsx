import { useMemo } from 'react';

import * as styles from '@/components/ButtonUserSubcriptions/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonUserSubcriptionsBaseProps,
    ButtonUserSubcriptionsBase,
} from '@/components/ButtonUserSubcriptions/Base';

export const ButtonUserSubcriptionsStyle = { ...styles } as const;

export type ButtonUserSubcriptionsStyles =
    keyof typeof ButtonUserSubcriptionsStyle;

export interface ButtonUserSubcriptions
    extends ButtonUserSubcriptionsBaseProps {
    styleButtonUserSubcriptions?: ButtonUserSubcriptionsStyles | ThemesType;
}

export const ButtonUserSubcriptions = ({
    styleButtonUserSubcriptions = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonUserSubcriptions) => {
    const Style = useMemo(
        () =>
            ButtonUserSubcriptionsStyle[
                styleButtonUserSubcriptions as ButtonUserSubcriptionsStyles
            ] ?? ButtonUserSubcriptionsStyle._default,
        [styleButtonUserSubcriptions]
    );

    return <ButtonUserSubcriptionsBase {...Style} {...props} />;
};
export default ButtonUserSubcriptions;
