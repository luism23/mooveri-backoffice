import { useLang } from '@/lang/translate';
import Link from '@/components/Link';
import ContentWidth from '../ContentWidth';

import { url as urlRedirect } from '@/data/routes';
import Text from '../Text';
import { DELETE_SUBCRIPTION_ID } from '@/api/tolinkme/CustomerSuscriptions';
import { useNotification } from '@/hook/useNotification';
import { useState } from 'react';
import { SubmitResult } from '../Form/Base';
import { useUser } from '@/hook/useUser';
import UserAccount from '@/svg/userAccount';

export interface ButtonUserSubcriptionsClassProps {}

export interface ButtonUserSubcriptionsBaseProps {
    icon?: string;
    title?: any;
    Month?: string;
    money?: string;
    ImgUserSubscriptions?: string;
    userSubscriptions?: string;
    Unsuscribe?: any;
    trash?: any;
    url?: string;
    Subscribed?: any;
    uuid_transation?: any;
    deleteSubscriptions?: any;
    statusCancel?: any;
    //idButton?: any
}

export interface ButtonUserSubcriptionsProps
    extends ButtonUserSubcriptionsClassProps,
        ButtonUserSubcriptionsBaseProps {}

export const ButtonUserSubcriptionsBase = ({
    icon,
    title = 'TikTok',
    Month = 'Month',
    money = '8,50',
    ImgUserSubscriptions = '',
    userSubscriptions = 'michellegiraldo',
    Unsuscribe,
    trash,
    url,
    uuid_transation,
    Subscribed,
    statusCancel,
}: //idButton
ButtonUserSubcriptionsProps) => {
    const _t = useLang();
    const { pop } = useNotification();
    const [isDelete, setIsDelete] = useState(false);
    const { user } = useUser();

    const deleteSubscriptions = async () => {
        const result: SubmitResult = await DELETE_SUBCRIPTION_ID({
            uuid: uuid_transation,
            user,
        });

        if (result.status == 'ok') {
            setIsDelete(true);
            window.location.reload();
        }
        pop({
            type: result.status,
            message: _t(result?.message ?? ''),
        });
    };
    if (isDelete) {
        return <></>;
    }

    if (statusCancel === 'CANCELLED') {
        return (
            <>
                <ContentWidth className="p-10">
                    <div className="flex flex-justify-between">
                        <div className="width-p-35 flex flex-align-center">
                            <div className="border-radius-100 width-80 height-80">
                                {ImgUserSubscriptions !== '' ||
                                ImgUserSubscriptions == null ||
                                ImgUserSubscriptions == undefined ? (
                                    <div>
                                        <img
                                            className={` height-80
                                        width-80
                                        border-radius-50
                                        object-fit-cover`}
                                            src={ImgUserSubscriptions}
                                        />
                                    </div>
                                ) : (
                                    <div
                                        className="
                                        flex 
                                        flex-justify-center
                                        m-h-auto
                                        height-80
                                        width-80
                                        bg-whiteThree
                                        overflow-hidden
                                        color-white
                                        border-radius-100
                                "
                                    >
                                        <UserAccount size={65} />
                                    </div>
                                )}
                            </div>
                        </div>
                        <div className="width-p-65">
                            <div className="flex flex-align-center flex-justify-between m-v-5">
                                <Text styleTemplate="tolinkme4">
                                    @{userSubscriptions}
                                </Text>
                                <Text styleTemplate="tolinkme10">
                                    <span className="m-r-1">$</span>
                                    {money}/{_t(Month)}
                                </Text>
                            </div>
                            <Link
                                styleTemplate="tolinkm15"
                                className="cursor-pointer linkUnSubscribed"
                                href={`${url}`}
                            >
                                <a className="text-decoration-none">
                                    <div className="flex flex-align-center flex-justify-center border-style-solid border-3 border-black height-40 border-radius-10">
                                        <span className="m-r-5 flex">
                                            <img
                                                className="width-20 height-20"
                                                src={icon}
                                                alt=""
                                            />
                                        </span>
                                        <Text
                                            className="text-capitalize text-decoration-none"
                                            styleTemplate="tolinkme10"
                                        >
                                            {title} {_t(Subscribed)}
                                        </Text>
                                    </div>
                                </a>
                            </Link>
                        </div>
                    </div>

                    {/*
                        <Link
                        href={urlRedirect.pay + `/${idButton}`}
                        className="text-right"
                    >
                        <Text
                            className="
                                        flex
                                        flex-justify-right
                                        flex-align-center
                                        flex-align-center
                                        cursor-pointer 
                                        text-decoration-underline
                                        color-darkAqua 
                                        font-w-900 
                                        m-t-4
                                    "
                        >
                            {_t('Resubscribe')}
                        </Text>
                    </Link>
                        
                        */}
                </ContentWidth>
            </>
        );
    }

    return (
        <>
            <ContentWidth className="p-10">
                <div className="flex flex-justify-between">
                    <div className="width-p-35 flex flex-align-center">
                        <div className="border-radius-100 width-80 height-80">
                            {ImgUserSubscriptions !== '' ||
                            ImgUserSubscriptions == null ||
                            ImgUserSubscriptions == undefined ? (
                                <div>
                                    <img
                                        className={` height-80
                                        width-80
                                        border-radius-50
                                        object-fit-cover`}
                                        src={ImgUserSubscriptions}
                                    />
                                </div>
                            ) : (
                                <div
                                    className="
                                        flex 
                                        flex-justify-center
                                        m-h-auto
                                        height-80
                                        width-80
                                        bg-whiteThree
                                        overflow-hidden
                                        color-white
                                        border-radius-100
                                "
                                >
                                    <UserAccount size={65} />
                                </div>
                            )}
                        </div>
                    </div>
                    <div className="width-p-65">
                        <div className="flex flex-align-center flex-justify-between m-v-5">
                            <Text styleTemplate="tolinkme4">
                                @{userSubscriptions}
                            </Text>
                            <Text styleTemplate="tolinkme10">
                                <span className="m-r-1">$</span>
                                {money}/{_t(Month)}
                            </Text>
                        </div>
                        <Link
                            styleTemplate="tolinkm15"
                            className="cursor-pointer linkUnSubscribed"
                            href={`${url}`}
                        >
                            <a className="text-decoration-none">
                                <div className="flex flex-align-center flex-justify-center border-style-solid border-3 border-black height-40 border-radius-10">
                                    <span className="m-r-5 flex">
                                        <img
                                            className="width-20 height-20"
                                            src={icon}
                                            alt=""
                                        />
                                    </span>
                                    <Text
                                        className="text-capitalize text-decoration-none"
                                        styleTemplate="tolinkme10"
                                    >
                                        {title} {_t(Subscribed)}
                                    </Text>
                                </div>
                            </a>
                        </Link>
                    </div>
                </div>

                <Link
                    onClick={deleteSubscriptions}
                    href={urlRedirect.mySubscriptions}
                    className="text-right"
                >
                    <Text
                        className="
                                        flex
                                        flex-justify-right
                                        flex-align-center
                                        flex-align-center
                                        cursor-pointer 
                                        text-decoration-underline
                                        color-darkAqua 
                                        font-w-900 
                                        m-t-4
                                    "
                    >
                        <span className="m-r-5 flex">{trash}</span>{' '}
                        {_t(Unsuscribe)}
                    </Text>
                </Link>
            </ContentWidth>
        </>
    );
};

export default ButtonUserSubcriptionsBase;
