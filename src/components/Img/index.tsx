import CSS from 'csstype';
import { Theme, ThemeProps } from '@/config/theme';

export interface ImgProps extends ThemeProps {
    src?: string;
    name?: string;
    className?: string;
    classNameImg?: string;
    capas?: CSS.Properties[];
}

export const Img = ({
    src = '',
    name = '',
    className = '',
    classNameImg = '',
    capas = [],
    styleTemplate = Theme.styleTemplate ?? '_default',
}: ImgProps) => {
    return (
        <>
            <picture className={className}>
                <source
                    srcSet={`/img/${styleTemplate}/1920/${src}`}
                    media="(min-width: 1680px)"
                />
                <source
                    srcSet={`/img/${styleTemplate}/1680/${src}`}
                    media="(min-width: 1440px)"
                />
                <source
                    srcSet={`/img/${styleTemplate}/1440/${src}`}
                    media="(min-width: 1024px)"
                />
                <source
                    srcSet={`/img/${styleTemplate}/1024/${src}`}
                    media="(min-width: 992px)"
                />
                <source
                    srcSet={`/img/${styleTemplate}/768/${src}`}
                    media="(min-width: 575px)"
                />
                <img
                    src={`/img/${styleTemplate}/575/${src}`}
                    alt={name}
                    className={classNameImg}
                />
                {capas.map((e, i) => (
                    <div className="capa" style={e} key={i} />
                ))}
            </picture>
        </>
    );
};
export default Img;
