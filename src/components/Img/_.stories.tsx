import { Story, Meta } from "@storybook/react";

import { ImgProps, Img } from "./index";

export default {
    title: "Img/Img",
    component: Img,
} as Meta;

const Template: Story<ImgProps> = (args) => <Img {...args}>Test Children</Img>;

export const Index = Template.bind({});
Index.args = {
    src: "banner-home.png",
};
