# Img

## Import

```js
import { Img } from '@/components/Img';
```

## Props

```ts
interface ImgProps {
    src: string;
    name?: string;
    className?: string;
    classNameImg?: string;
    capas?: CSS.Properties[];
}
```

## Use

```js
<Img src="img.png" />
```
