import { Story, Meta } from "@storybook/react";

import { RenderLoaderProps, RenderLoader } from "./index";

export default {
    title: "Loader/RenderLoader",
    component: RenderLoader,
} as Meta;

const Template: Story<RenderLoaderProps> = (args) => (
    <RenderLoader {...args}>Test Children</RenderLoader>
);

export const Index = Template.bind({});
Index.args = {};
