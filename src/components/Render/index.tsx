import Head from 'next/head';
import { PropsWithChildren, useMemo } from 'react';
import { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import { LayoutBase } from '@/layout/Base';

import LoaderPage from '@/components/Loader/LoaderPage';
import { useUser, UserLoginProps, validateTokenUser } from '@/hook/useUser';
import url from '@/data/routes';
import { Theme, ThemesType } from '@/config/theme';
import { useLang } from '@/lang/translate';
import Page404 from '../Pages/404';

export type LoadPageFunctionData = UserLoginProps;
export type LoadPageFunctionRespond = 'ok' | 'error' | 401 | '404';

export interface RenderDataLoadPage {
    props?: RenderDataLoadPage;
    user?: UserLoginProps;
    query?: {
        [id: string]: any;
    };
}

export type RenderLoadPage = (
    data: RenderDataLoadPage
) => Promise<LoadPageFunctionRespond>;

export interface RenderLoadPageProps {
    loadPage?: RenderLoadPage;
}

export interface RenderLoaderProps extends RenderLoadPageProps {
    title?: string;
    description?: string;
    titleLoader?: boolean;
    favicon?: string;
    validateLogin?: boolean;
    validateNotLogin?: boolean;
    Layout?: any;
    LayoutProps?: {
        styleTemplate?: ThemesType | string;
        [id: string]: any;
    };
    loadHeadBeforeRender?: boolean;
}

export const RenderLoader = ({
    title = 'Title Page',
    description = '',
    titleLoader = true,
    favicon = undefined,
    validateNotLogin = false,
    validateLogin = false,
    loadPage = async () => 'ok',
    children,
    Layout = LayoutBase,
    LayoutProps = {},
    loadHeadBeforeRender = false,
}: PropsWithChildren<RenderLoaderProps>) => {
    const [is404, setIs404] = useState(false);
    const router = useRouter();
    const [loading, setLoading] = useState(3);
    const { user, load } = useUser();
    const [loadR, setLoadR] = useState(false);
    const _t = useLang();

    const notLoginRedirect = () => {
        router.push(url.login);
    };
    const LoginRedirect = () => {
        router.push(url.home);
    };

    const onValidateLogin = async () => {
        if (!validateTokenUser(user)) {
            notLoginRedirect();
            return;
        }
        onLoad(user);
    };

    const onValidateNotLogin = async () => {
        if (validateTokenUser(user)) {
            LoginRedirect();
            return;
        }
        onLoad(user);
    };

    const onLoad = async (user: any) => {
        let resultLoadPage: LoadPageFunctionRespond;

        try {
            resultLoadPage = await loadPage?.({ user, query: router.query });
        } catch (error: any) {
            resultLoadPage = error?.code ?? 401;
        }
        if (resultLoadPage === 'ok') {
            setLoadR(true);
        } else if (resultLoadPage === 401) {
            notLoginRedirect();
        } else if (resultLoadPage === 'error') {
            router.push('/upps-error');
        } else if (resultLoadPage === '404') {
            setIs404(true);
        } else {
            router.push(resultLoadPage);
        }
    };
    const onLoadEffect = () => {
        if (process.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE' && !loadPage) {
            setLoadR(true);
            return;
        }
        if (!router.isReady) {
            return;
        }
        if (load) {
            if (validateLogin) {
                onValidateLogin();
            } else if (validateNotLogin) {
                onValidateNotLogin();
            } else {
                onLoad(user);
            }
        }
    };
    useEffect(() => {
        onLoadEffect();
    }, [load, user, router.isReady]);
    useEffect(() => {
        if (!loadR) {
            setTimeout(() => {
                setLoading(loading > 10 ? 3 : loading + 1);
            }, 500);
        }
    }, [loading]);
    useEffect(() => {
        if (process?.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE') {
            // onLoadEffect();
        }
    }, [loadPage]);

    const titlePage = useMemo(() => {
        if (is404) {
            return '404';
        }
        if (loadR) {
            return _t(title);
        }
        if (titleLoader) {
            return `${_t('Loading')}${new Array(loading).fill('.').join('')}`;
        } else {
            return _t(title);
        }
    }, [is404, loadR, title, loading, titleLoader]);

    const faviconPage = useMemo(() => {
        if (favicon) {
            return favicon;
        }
        return `/image/${Theme.styleTemplate}/favicon.ico`;
    }, [favicon]);

    const HeadPage = useMemo(() => {
        if (loadHeadBeforeRender && !loadR) {
            return <></>;
        }
        return (
            <Head>
                <title>{titlePage}</title>
                <link rel="icon" type="image/png" href={faviconPage} />
                <meta property="og:title" content={titlePage} />
                <meta property="og:description" content={description} />
                <meta property="og:image" content={faviconPage} />
            </Head>
        );
    }, [titlePage, faviconPage, loadHeadBeforeRender, loadR]);

    return (
        <>
            {HeadPage}
            {is404 ? (
                <>
                    <Page404 />
                </>
            ) : (
                <>
                    {Layout != null ? (
                        <Layout {...LayoutProps}>
                            {loadR ? (
                                children
                            ) : (
                                <>
                                    <LoaderPage />
                                </>
                            )}
                        </Layout>
                    ) : (
                        <>
                            {loadR ? (
                                children
                            ) : (
                                <>
                                    <LoaderPage />
                                </>
                            )}
                        </>
                    )}
                </>
            )}
        </>
    );
};

export default RenderLoader;
