import { RSShareClassProps } from '@/components/RS/Share/Base';

export const mooveri: RSShareClassProps = {
    classNameContentGlobal: `
        container
    `,
    classNameContent: `
        m-t-20
        flex 
        flex-justify-between
        flex-column-row
        flex-gap-row-15
        width-p-100
        
    `,
    classNameLink: `
        flex
        width-35
        m-auto
    `,
    classNameImg: `
        
    `,
    useText: true,
    useRsText: true,
    styleTemplateText: 'mooveri3',
    styleTemplateTextLink: 'mooveri19',
    classNameTextLink: `
    m-t-5
    text-capitalize
    `,
    classNameContentRs: `
    flex
    flex-column
    flex-justify-between
    `,
};
