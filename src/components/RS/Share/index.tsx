import { useMemo } from 'react';

import * as styles from '@/components/RS/Share/styles';

import { Theme, ThemesType } from '@/config/theme';

import { RSShareBaseProps, RSShareBase } from '@/components/RS/Share/Base';

export const RSShareStyle = { ...styles } as const;

export type RSShareStyles = keyof typeof RSShareStyle;

export interface RSShareProps extends RSShareBaseProps {
    styleTemplate?: RSShareStyles | ThemesType;
}

export const RSShare = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: RSShareProps) => {
    const Style = useMemo(
        () =>
            RSShareStyle[styleTemplate as RSShareStyles] ??
            RSShareStyle._default,
        [styleTemplate]
    );

    return <RSShareBase {...Style} {...props} />;
};
export default RSShare;
