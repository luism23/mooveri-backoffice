import Image from '@/components/Image';
import Text, { TextStyles } from '@/components/Text';
import { TypeRS_Share, RS_Share } from '@/data/components/RS';
import { useMemo } from 'react';

import { useLang } from '@/lang/translate';
import Theme, { ThemesType } from '@/config/theme';

export interface RSShareClassProps {
    classNameContentGlobal?: string;
    classNameContentText?: string;
    classNameTextLink?: string;
    classNameContent?: string;
    classNameLink?: string;
    classNameImg?: string;
    classNameText?: string;
    classNameContentRs?: string;
    useText?: boolean;
    useRsText?: boolean;

    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateTextLink?: TextStyles | ThemesType;
}

export type RSShareType = { [id in TypeRS_Share]?: string };

export interface RSShareBaseProps {
    url?: string;
    title?: string;
}

export interface RSShareProps extends RSShareClassProps, RSShareBaseProps {}

export const RSSharePreUrl: RSShareType = {
    whatsapp: 'https://api.whatsapp.com/send?text=',
    facebook: 'https://www.facebook.com/sharer.php?u=',
    twitter: 'https://twitter.com/share?url=',
    // google: 'https://plus.google.com/share?url=',
    telegram: 'https://t.me/share/url?url=',
    pinterest: 'https://pinterest.com/pin/create/bookmarklet/?url=',
    linkedin: 'https://www.linkedin.com/shareArticle?url=',
    email: 'mailto:?body=',
};

export const RSShareBase = ({
    classNameContentGlobal = '',
    classNameContentText = '',
    classNameContentRs = '',
    classNameContent = '',
    classNameLink = '',
    classNameTextLink = '',
    classNameImg = '',
    classNameText = '',
    useText = false,
    useRsText = false,
    styleTemplateText = Theme.styleTemplate ?? '_default',
    styleTemplateTextLink = Theme.styleTemplate ?? '_default',

    title = 'Share your Mooveri Profile',
    url = '',
}: RSShareProps) => {
    const _t = useLang();
    const links = useMemo(
        () =>
            RS_Share.map((link, i) => (
                <div className={classNameContentRs} key={i}>
                    <a
                        href={RSSharePreUrl[link] + url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className={classNameLink}
                    >
                        <Image
                            className={classNameImg}
                            src={`/rs/${link}.png`}
                        />
                    </a>
                    {useRsText ? (
                        <>
                            <Text
                                styleTemplate={styleTemplateTextLink}
                                className={classNameTextLink}
                            >
                                {link}
                            </Text>
                        </>
                    ) : (
                        <></>
                    )}
                </div>
            )),
        [url, RS_Share, useRsText]
    );

    return (
        <>
            <div className={classNameContentGlobal}>
                <div className={classNameContentText}>
                    {useText ? (
                        <>
                            <Text
                                styleTemplate={styleTemplateText}
                                className={classNameText}
                            >
                                {_t(title)}
                            </Text>
                        </>
                    ) : (
                        <></>
                    )}
                </div>
                <div className={classNameContent}>{links}</div>
            </div>
        </>
    );
};
export default RSShareBase;
