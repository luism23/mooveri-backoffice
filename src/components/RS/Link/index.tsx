import { RSLinkBase, RSLinkBaseProps } from '@/components/RS/Link/Base';

import * as styles from '@/components/RS/Link/styles';

import { Theme, ThemesType } from '@/config/theme';
import { useMemo } from 'react';

export const LinkStyle = { ...styles } as const;

export type LinkStyles = keyof typeof LinkStyle;

export interface RSLinkProps extends RSLinkBaseProps {
    styleTemplate?: LinkStyles | ThemesType;
}

export const RSLink = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',

    ...props
}: RSLinkProps) => {
    const Styles = useMemo(
        () => LinkStyle[styleTemplate as LinkStyles] ?? LinkStyle._default,
        [styleTemplate]
    );
    return (
        <>
            <RSLinkBase {...props} {...Styles} />
        </>
    );
};

export default RSLink;
