import { Story, Meta } from "@storybook/react";

import { RSLinkBaseProps } from "./Base";
import { RSLink } from "./index";

export default {
    title: "RS/RSLink",
    component: RSLink,
} as Meta;

const TemplateSocial: Story<RSLinkBaseProps> = (args) => (
    <RSLink {...args}>Test Children</RSLink>
);

export const RS = TemplateSocial.bind({});
RS.args = {
    id: "bongacams",
    img: "bongacams",
    href: "#",
    blocked: true,
    name: "BongaCams",
};
