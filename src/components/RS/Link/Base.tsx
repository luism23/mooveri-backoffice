import { Text, TextStyles } from '@/components/Text';

import { Theme, ThemesType } from '@/config/theme';
import { TypeRS, RSItem } from '@/data/components/RS';
import Image from '@/components/Image';

import { useLang } from '@/lang/translate';
import {
    buttonHeightType,
    buttonRoundType,
} from '@/components/InfoProfileEdit/Base';
import { useMemo } from 'react';
import Layer from '@/components/Layer';
import { useUser, validateTokenUser } from '@/hook/useUser';
import url from '@/data/routes';

export interface RSLinkClassProps {
    classNameContent?: string;
    classNameContentNoRounding?: string;
    classNameContentSemiRounded?: string;
    classNameContentRounded?: string;

    classNameContentText?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateTextBlocked?: TextStyles | ThemesType;
    classNameContentLayer?: string;
    classNameContentImg?: string;
    classNameImg?: string;

    classNameBlocked?: string;
}

export interface RSLinkBaseProps extends Omit<RSItem, 'id' | 'img'> {
    uuid?: string;
    id: TypeRS;
    img: TypeRS;
    buttonRound?: buttonRoundType;
    btnColor?: string;
    btnBgColor?: string;
    showIconButton?: boolean;
    btnBorderColor?: string;
    buttonHeight?: buttonHeightType;
    layer?: boolean;
    onClick?: (uuid?: string) => void;
    price?: number;
    textSub?: string;
    textTime?: string;
}

export interface RSLinkProps extends RSLinkBaseProps, RSLinkClassProps {}

export const RSLinkBase = ({
    classNameContent = '',
    classNameContentNoRounding = '',
    classNameContentSemiRounded = '',
    classNameContentRounded = '',

    classNameContentImg = '',
    classNameImg = '',

    classNameContentText = '',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    styleTemplateTextBlocked = Theme?.styleTemplate ?? '_default',
    classNameContentLayer = '',
    classNameBlocked = '',
    uuid,
    name,
    img,
    blocked,
    layer = false,
    href,
    buttonRound = 'semi-rounded',
    btnColor = '#00000000',
    btnBgColor = '#00000000',
    showIconButton = true,
    btnBorderColor = '#00000000',
    buttonHeight = 'regular',
    onClick,
    price = 8.5,
    textSub = 'Subscribe',
    textTime = 'Month',
}: RSLinkProps) => {
    const _t = useLang();

    const pV = useMemo(() => {
        const p = {
            slim: '.4rem',
            regular: '.6rem',
            strong: '.8rem',
        };
        return p[buttonHeight];
    }, [buttonHeight]);

    const { user, load } = useUser();

    const urlLoginRegister = useMemo(() => {
        if (!load) {
            return '#';
        }
        const isLogin = validateTokenUser(user);

        const urlLogin = `${url.login}/${uuid}`;

        const urlPay = `${url.pay}/${uuid}`;

        return isLogin ? urlPay : urlLogin;
    }, [user, load]);

    return (
        <a
            href={(blocked ? urlLoginRegister : href) ?? '#'}
            target="_blank"
            rel="noopener noreferrer"
            onClick={() => {
                onClick?.(uuid);
            }}
            className={`${classNameContent} ${blocked ? classNameBlocked : ''}
                ${
                    buttonRound == 'no-rounding'
                        ? classNameContentNoRounding
                        : buttonRound == 'semi-rounded'
                        ? classNameContentSemiRounded
                        : classNameContentRounded
                }
                ${blocked ? classNameContentLayer : ''}
            `}
            style={{
                paddingTop: pV,
                paddingBottom: pV,

                ...(btnBgColor == '#00000000'
                    ? {}
                    : {
                          background: btnBgColor,
                      }),
                ...(btnBorderColor == '#00000000'
                    ? {}
                    : {
                          borderColor: btnBorderColor,
                      }),
            }}
        >
            <div className={classNameContentImg}>
                {showIconButton ? (
                    <Image src={`rs/${img}.png`} className={classNameImg} />
                ) : (
                    <></>
                )}
            </div>

            <div className={classNameContentText}>
                {blocked ? (
                    <Text
                        styleTemplate={styleTemplateTextBlocked}
                        style={{
                            ...(btnColor == '#00000000'
                                ? {}
                                : {
                                      color: btnColor,
                                  }),
                        }}
                    >
                        {_t('Get Now')}
                    </Text>
                ) : (
                    <Text
                        styleTemplate={styleTemplateText}
                        style={{
                            ...(btnColor == '#00000000'
                                ? {}
                                : {
                                      color: btnColor,
                                  }),
                        }}
                    >
                        {name}
                    </Text>
                )}
            </div>
            <div className={`${classNameContentLayer}`}>
                {layer ? (
                    <Layer
                        price={price}
                        subscribe={_t(textSub)}
                        period={_t(textTime)}
                    />
                ) : (
                    <></>
                )}
            </div>
        </a>
    );
};
export default RSLinkBase;
