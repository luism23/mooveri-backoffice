import { InputAddEdit, InputAddEditStyles } from '@/components/Input/AddEdit';
import { Image } from '@/components/Image';
import { Title, TitleStylesType, TitleStyles } from '@/components/Title';

import { Theme, ThemesType } from '@/config/theme';

import { RSItem } from '@/data/components/RS';
import { SubmitResult } from '@/components/Form/Base';

export interface RSAddEditClassProps {
    classNameContent?: string;

    classNameContentTitle?: string;
    styleTemplateTitle?: TitleStyles | ThemesType;
    typeStyleTitle?: TitleStylesType;

    classNameContentInputAddEdit?: string;
    styleTemplateInput?: InputAddEditStyles | ThemesType;

    classNameImgInputAddEdit?: string;
}
export interface RSAddEditDataProps {
    [id: string]: string;
}

export interface RSItemC<T> extends Omit<RSItem, 'id'> {
    id: T;
}

export interface RSAddEditBaseProps<T> {
    title?: string;
    RS?: RSItem[];
    defaultValue?: RSAddEditDataProps;
    onSave?: (id: T) => (value: string) => Promise<SubmitResult>;
    onDelete?: (id: T) => () => Promise<SubmitResult>;
}

export interface RSAddEditProps<T>
    extends RSAddEditBaseProps<T>,
        RSAddEditClassProps {}

export const RSAddEditBase = <T,>({
    classNameContent = '',

    classNameContentInputAddEdit = '',
    styleTemplateInput = Theme?.styleTemplate ?? '_default',

    classNameContentTitle = '',
    styleTemplateTitle = Theme?.styleTemplate ?? '_default',
    typeStyleTitle = 'h11',

    classNameImgInputAddEdit = '',

    title = '',
    RS = [],
    defaultValue = {},
    ...props
}: RSAddEditProps<T>) => {
    return (
        <div className={classNameContent}>
            <div className={classNameContentTitle}>
                <Title
                    styleTemplate={styleTemplateTitle}
                    type={'h4'}
                    typeStyle={typeStyleTitle}
                >
                    {title}
                </Title>
            </div>
            {RS.map((rs, i) => {
                const ID: T = rs.id;
                return (
                    <div className={classNameContentInputAddEdit} key={i}>
                        <InputAddEdit
                            name={rs.name}
                            placeholder={rs.placeholder}
                            icon={
                                <Image
                                    src={`rs/${rs.img}`}
                                    className={classNameImgInputAddEdit}
                                />
                            }
                            styleTemplate={styleTemplateInput}
                            defaultValue={
                                defaultValue?.[
                                    typeof ID == 'string' ? ID : ''
                                ] ?? ''
                            }
                            onSave={props?.onSave?.(ID)}
                            onDelete={props?.onDelete?.(ID)}
                        />
                    </div>
                );
            })}
        </div>
    );
};
export default RSAddEditBase;
