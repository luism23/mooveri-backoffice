import { useLang } from '@/lang/translate';
import { RS_Social, RS_Social_Type } from '@/data/components/RS';

import {
    RSAddEditBase,
    RSAddEditBaseProps,
} from '@/components/RS/AddEdit/Base';

import * as styles from '@/components/RS/AddEdit/styles';

import { Theme, ThemesType } from '@/config/theme';
import { useMemo } from 'react';

export const AddEditStyle = { ...styles } as const;

export type AddEditStyles = keyof typeof AddEditStyle;

export interface RSAddEditProps<T> extends RSAddEditBaseProps<T> {
    styleTemplate?: AddEditStyles | ThemesType;
}

export const RSAddEdit = <T,>({
    styleTemplate = Theme?.styleTemplate ?? '_default',

    ...props
}: RSAddEditProps<T>) => {
    const Styles = useMemo(
        () =>
            AddEditStyle[styleTemplate as AddEditStyles] ??
            AddEditStyle._default,
        [styleTemplate]
    );
    return (
        <>
            <RSAddEditBase<T> {...props} {...Styles} />
        </>
    );
};

export const RSAddEditSocial = (props: RSAddEditProps<RS_Social_Type>) => {
    const _t = useLang();
    return (
        <RSAddEdit<RS_Social_Type>
            {...props}
            RS={RS_Social}
            title={_t('Social')}
        />
    );
};
