import { RSAddEditClassProps } from '@/components/RS/AddEdit/Base';

export const _default: RSAddEditClassProps = {
    classNameContentInputAddEdit: `
    
    `,
    styleTemplateInput: '_default',
    classNameContentTitle: `
    
    `,
    styleTemplateTitle: '_default',
    typeStyleTitle: 'h11',
    classNameImgInputAddEdit: `
        height-35
    `,
};
