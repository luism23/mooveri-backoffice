import { Story, Meta } from "@storybook/react";

import { RSLinkConfigBaseProps } from "./Base";
import { RSLinkConfig } from "./index";

export default {
    title: "RS/RSLinkConfig",
    component: RSLinkConfig,
} as Meta;

const TemplateSocial: Story<RSLinkConfigBaseProps> = (args) => (
    <RSLinkConfig {...args}>Test Children</RSLinkConfig>
);

export const RS = TemplateSocial.bind({});
RS.args = {
    active:true,
    rs:"camsoda",
    url:"sadasd"
};
