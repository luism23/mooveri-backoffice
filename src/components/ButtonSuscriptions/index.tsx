import { useMemo } from 'react';

import * as styles from '@/components/ButtonSuscriptions/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ButtonSuscriptionsBaseProps,
    ButtonSuscriptionsBase,
} from '@/components/ButtonSuscriptions/Base';

export const ButtonSuscriptionsStyle = { ...styles } as const;

export type ButtonSuscriptionsStyles = keyof typeof ButtonSuscriptionsStyle;

export interface ButtonSuscriptions extends ButtonSuscriptionsBaseProps {
    styleButtonSuscriptions?: ButtonSuscriptionsStyles | ThemesType;
}

export const ButtonSuscriptions = ({
    styleButtonSuscriptions = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonSuscriptions) => {
    const Style = useMemo(
        () =>
            ButtonSuscriptionsStyle[
                styleButtonSuscriptions as ButtonSuscriptionsStyles
            ] ?? ButtonSuscriptionsStyle._default,
        [styleButtonSuscriptions]
    );

    return <ButtonSuscriptionsBase {...Style} {...props} />;
};
export default ButtonSuscriptions;
