import { useLang } from '@/lang/translate';
import Text from '../Text';

export interface ButtonSuscriptionsClassProps {
    classNameContent?: string;
    classNameContentButton?: string;
    classNameContentIconLogo?: string;
    classNameTitleButton?: string;
}

export interface ButtonSuscriptionsBaseProps {
    countSubscriptors?: any;
    Subscriptors?: any;
    IconLogo?: string;
    titleButton?: string;
    amountButton?: string;
    month?: any;
    onClick?: () => void;
}

export interface ButtonSuscriptionsProps
    extends ButtonSuscriptionsClassProps,
        ButtonSuscriptionsBaseProps {}

export const ButtonSuscriptionsBase = ({
    classNameContent = '',
    classNameContentButton = '',
    classNameContentIconLogo = '',
    classNameTitleButton = '',
    countSubscriptors,
    Subscriptors,
    IconLogo,
    titleButton,
    month,
    amountButton,
    onClick,
}: ButtonSuscriptionsProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent} onClick={onClick}>
                <div className="width-p-100 flex">
                    <div className={classNameContentButton}>
                        <div className={classNameContentIconLogo}>
                            <img
                                className="width-20 height-20"
                                src={IconLogo}
                                alt=""
                            />
                        </div>
                        <div className="flex">
                            <Text
                                className={classNameTitleButton}
                                styleTemplate="tolinkme10"
                            >
                                {titleButton}
                            </Text>
                            <Text styleTemplate="tolinkme10">
                                {_t('Subscribed')}
                            </Text>
                        </div>
                    </div>
                </div>
                <div className="width-p-45 text-right">
                    <Text styleTemplate="tolinkme54">
                        ${amountButton}/{_t(month)}
                    </Text>
                    <Text
                        className="flex flex-justify-rignt"
                        styleTemplate="tolinkme53"
                    >
                        {countSubscriptors} {_t(Subscriptors)}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default ButtonSuscriptionsBase;
