import { useMemo } from 'react';

import * as styles from '@/components/ConfigButtonAnimations/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigButtonAnimationsBaseProps,
    ConfigButtonAnimationsBase,
} from '@/components/ConfigButtonAnimations/Base';

export const ConfigButtonAnimationsStyle = { ...styles } as const;

export type ConfigButtonAnimationsStyles =
    keyof typeof ConfigButtonAnimationsStyle;

export interface ConfigButtonAnimationsProps
    extends ConfigButtonAnimationsBaseProps {
    styleTemplate?: ConfigButtonAnimationsStyles | ThemesType;
}

export const ConfigButtonAnimations = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigButtonAnimationsProps) => {
    const Style = useMemo(
        () =>
            ConfigButtonAnimationsStyle[
                styleTemplate as ConfigButtonAnimationsStyles
            ] ?? ConfigButtonAnimationsStyle._default,
        [styleTemplate]
    );

    return <ConfigButtonAnimationsBase {...Style} {...props} />;
};
export default ConfigButtonAnimations;
