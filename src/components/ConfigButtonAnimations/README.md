# ConfigButtonAnimations

## Dependencies

[ConfigButtonAnimations](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigButtonAnimations)

```js
import { ConfigButtonAnimations } from '@/components/ConfigButtonAnimations';
```

## Import

```js
import {
    ConfigButtonAnimations,
    ConfigButtonAnimationsStyles,
} from '@/components/ConfigButtonAnimations';
```

## Props

```tsx
interface ConfigButtonAnimationsProps {}
```

## Use

```js
<ConfigButtonAnimations>ConfigButtonAnimations</ConfigButtonAnimations>
```
