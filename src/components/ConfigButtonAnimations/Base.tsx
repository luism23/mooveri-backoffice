import { TextStyles } from '@/components/Text';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { useData } from 'fenextjs-hook/cjs/useData';
import { Popup, PopupStyles } from '../Popup';
import { Collapse, CollapseStyles } from '../Collapse';

import {
    ButtonConfig,
    ButtonAnimationsConfig,
    ButtonAnimationsConfigType,
} from '@/interfaces/Button';
import { InputTextStyles } from '../Input/Text';
import { useMemo, useState } from 'react';
import { Reload } from '@/svg/Reload';
import LoaderPage from '../Loader/LoaderPage';
import { getBtn } from '@/functions/getBtn';
import { RSLinkConfigDataProps } from '../RS/LinkConfig/Base';
import Mouse from '@/svg/Mouse';
import { InputRadioButtonsAnimations } from '../Input/Radio/Template/InputRadioButtonsAnimations';
import Infinite from '@/svg/Infinite';
import Spinner from '@/svg/Spinner';

export interface ConfigButtonAnimationsClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    styleTemplateSelectAnimation?: InputTextStyles | ThemesType;

    classNameReload?: string;
}

export interface ConfigButtonAnimationsBaseProps {
    defaultValue?: ButtonAnimationsConfig;
    onChange?: (data: ButtonAnimationsConfig) => void;
    links?: RSLinkConfigDataProps[];
    btnConfig?: ButtonConfig;
}

export interface ConfigButtonAnimationsProps
    extends ConfigButtonAnimationsClassProps,
        ConfigButtonAnimationsBaseProps {}

export const ConfigButtonAnimationsBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    classNameReload = '',

    defaultValue = {},
    onChange,
    links = [],
    btnConfig = {},
}: ConfigButtonAnimationsProps) => {
    const _t = useLang();

    const [loader, setLoader] = useState(false);
    const [isActive, setIsActive] = useState<ButtonAnimationsConfigType | ''>(
        'on-page-load'
    );
    const defaultValue_ = useMemo(() => defaultValue, []);
    const { data, onChangeData, onRestart } = useData<ButtonAnimationsConfig>(
        defaultValue_,
        {
            onChangeDataAfter: (data: ButtonAnimationsConfig) => {
                onChange?.(data);
            },
        }
    );

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }
        return (
            <>
                <Collapse
                    header={
                        <>
                            <Spinner size={15} />
                            {_t('Animation in Load Page')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'on-page-load'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'on-page-load' : '');
                    }}
                >
                    <div className="p-h-15 p-v-15 flex flex-gap-16 flex-justify-between flex-align-center">
                        <div className="flex-12 flex-sm-6">
                            <InputRadioButtonsAnimations
                                onChange={onChangeData('on-page-load')}
                                defaultValue={data?.['on-page-load']}
                            />
                        </div>
                        <div className="flex-12 flex-sm-6">
                            <div className="d-block width-p-100">
                                {getBtn(
                                    links[0] ?? {
                                        rs: 'facebook',
                                    },
                                    {
                                        ...btnConfig,
                                        className: `fa-${data?.['on-page-load']}`,
                                    }
                                )}
                            </div>
                        </div>
                    </div>
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Infinite size={15} />
                            {_t('Infinite Animation')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'infinite'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'infinite' : '');
                    }}
                >
                    <div className="p-h-15 p-v-15 flex flex-gap-16 flex-justify-between flex-align-center">
                        <div className="flex-12 flex-sm-6">
                            <InputRadioButtonsAnimations
                                onChange={onChangeData('infinite')}
                                defaultValue={data?.infinite}
                            />
                        </div>
                        <div className="flex-12 flex-sm-6">
                            <div className="d-block width-p-100">
                                {getBtn(
                                    links[0] ?? {
                                        rs: 'facebook',
                                    },
                                    {
                                        ...btnConfig,
                                        className: `fa-${data.infinite}`,
                                    }
                                )}
                            </div>
                        </div>
                    </div>
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Mouse size={15} />
                            {_t('Hover Animation')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'hover'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'hover' : '');
                    }}
                >
                    <div className="p-h-15 p-v-15 flex flex-gap-16 flex-justify-between flex-align-center">
                        <div className="flex-12 flex-sm-6">
                            <InputRadioButtonsAnimations
                                onChange={onChangeData('hover')}
                                defaultValue={data?.hover}
                            />
                        </div>
                        <div className="flex-12 flex-sm-6">
                            <div className="d-block width-p-100">
                                {getBtn(
                                    links[0] ?? {
                                        rs: 'facebook',
                                    },
                                    {
                                        ...btnConfig,
                                        className: `fa-${data.hover}`,
                                    }
                                )}
                            </div>
                        </div>
                    </div>
                </Collapse>
            </>
        );
    }, [data, isActive, loader, btnConfig, _t]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };

    return (
        <>
            <Popup
                btn={_t('Button Animations')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigButtonAnimationsBase;
