import { Story, Meta } from "@storybook/react";

import { ConfigButtonAnimationsProps, ConfigButtonAnimations } from "./index";

export default {
    title: "ConfigButtonAnimations/ConfigButtonAnimations",
    component: ConfigButtonAnimations,
} as Meta;

const ConfigButtonAnimationsIndex: Story<ConfigButtonAnimationsProps> = (args) => (
    <ConfigButtonAnimations {...args}>Test Children</ConfigButtonAnimations>
);

export const Index = ConfigButtonAnimationsIndex.bind({});
Index.args = {};
