import { useMemo } from 'react';

import * as styles from '@/components/Iframe/styles';

import { Theme, ThemesType } from '@/config/theme';

import { IframeBaseProps, IframeBase } from '@/components/Iframe/Base';

export const IframeStyle = { ...styles } as const;

export type IframeStyles = keyof typeof IframeStyle;

export interface IframeProps extends IframeBaseProps {
    styleTemplate?: IframeStyles | ThemesType;
}

export const Iframe = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: IframeProps) => {
    const Style = useMemo(
        () =>
            IframeStyle[styleTemplate as IframeStyles] ?? IframeStyle._default,
        [styleTemplate]
    );

    return <IframeBase {...Style} {...props} />;
};
export default Iframe;
