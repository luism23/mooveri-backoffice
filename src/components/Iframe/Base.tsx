import CSS from 'csstype';

import { Loader } from '@/svg/loader';

export interface IframeClassProps {
    classNameContent?: string;
    styleContentMovil?: CSS.Properties;
    styleContentDesktop?: CSS.Properties;

    classNameContentIframe?: string;

    classNameMovil?: string;
    styleMovil?: CSS.Properties;
    classNameDesktop?: string;
    styleDesktop?: CSS.Properties;

    classNameIframe?: string;
    styleIframe?: CSS.Properties;

    classNameLoader?: string;
    sizeLoader?: number;
}

export interface IframeBaseProps {
    url: string;
    type?: 'movil' | 'desktop';
}

export interface IframeProps extends IframeClassProps, IframeBaseProps {}

export const IframeBase = ({
    classNameContent = '',
    styleContentMovil = {},
    styleContentDesktop = {},

    classNameContentIframe = '',

    classNameIframe = '',
    classNameMovil = '',
    classNameDesktop = '',

    styleIframe = {},
    styleMovil = {},
    styleDesktop = {},

    classNameLoader = '',
    sizeLoader = 40,

    type = 'desktop',
    url = '',
}: IframeProps) => {
    return (
        <div
            className={classNameContent}
            style={type == 'movil' ? styleContentMovil : styleContentDesktop}
        >
            <div
                className={`${classNameContentIframe} ${
                    type == 'movil' ? classNameMovil : classNameDesktop
                }`}
                style={type == 'movil' ? styleMovil : styleDesktop}
            >
                <div className={classNameLoader}>
                    <Loader size={sizeLoader} />
                </div>
                <iframe
                    src={url}
                    className={`${classNameIframe}`}
                    style={styleIframe}
                />
            </div>
        </div>
    );
};
export default IframeBase;
