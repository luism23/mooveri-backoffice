import Theme, { ThemesType } from '@/config/theme';
import Text from '@/components/Text';
import { TextStyles } from '@/components/Text';

export interface IconTextClassProps {
    classNameContent?: string;
    classNameContentIcon?: string;
    classNameContentText?: string;
    classNameText?: string;
    styleTemplateText?: TextStyles | ThemesType;
    sizeIcon?: number;
}

export interface IconTextBaseProps {
    icon?: any;
    text: string;
}

export interface IconTextProps extends IconTextClassProps, IconTextBaseProps {}

export const IconTextBase = ({
    classNameContent = '',
    classNameContentIcon = '',
    classNameContentText = '',
    classNameText = '',
    styleTemplateText = Theme.styleTemplate ?? '_default',
    icon,
    text,
}: IconTextProps) => {
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentIcon}>{icon}</div>
                <div className={classNameContentText}>
                    <Text
                        className={classNameText}
                        styleTemplate={styleTemplateText}
                    >
                        {text}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default IconTextBase;
