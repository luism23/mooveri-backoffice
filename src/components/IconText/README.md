# IconText

## Dependencies

[Text](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Text)

```js
import { Text } from '@/components/Text';
```

## Import

```js
import { IconText, IconTextStyles } from '@/components/IconText';
```

## Props

```tsx
interface IconTextProps {}
```

## Use

```js
<IconText>IconText</IconText>
```
