import { PropsWithChildren } from 'react';

import Header from '@/components/Header';
import Footer from '@/components/Footer';

export interface ContentClassProps {}

export interface ContentBaseProps extends PropsWithChildren {
    header?: boolean;
    footer?: boolean;
    className?: string;
}

export interface ContentProps extends ContentClassProps, ContentBaseProps {}

export const ContentBase = ({
    children,
    header = true,
    footer = true,
    className = '',
}: ContentProps) => {
    return (
        <>
            {header && <Header />}
            <div className={`content ${className}`}>{children}</div>
            {footer && <Footer />}
        </>
    );
};
export default ContentBase;
