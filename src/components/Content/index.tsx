import { useMemo } from 'react';

import * as styles from '@/components/Content/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ContentBaseProps, ContentBase } from '@/components/Content/Base';

export const ContentStyle = { ...styles } as const;

export type ContentStyles = keyof typeof ContentStyle;

export interface ContentProps extends ContentBaseProps {
    styleTemplate?: ContentStyles | ThemesType;
}

export const Content = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ContentProps) => {
    const Style = useMemo(
        () =>
            ContentStyle[styleTemplate as ContentStyles] ??
            ContentStyle._default,
        [styleTemplate]
    );

    return <ContentBase {...Style} {...props} />;
};
export default Content;
