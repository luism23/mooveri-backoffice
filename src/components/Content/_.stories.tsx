import { Story, Meta } from "@storybook/react";

import { ContentProps, Content } from "./index";

export default {
    title: "Content/Content",
    component: Content,
} as Meta;

const Template: Story<ContentProps> = (args) => (
    <Content {...args}>Test Children</Content>
);

export const Index = Template.bind({});
Index.args = {};
