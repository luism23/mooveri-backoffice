import { TextStyles, Text } from '@/components/Text';
import Theme, { ThemesType } from '@/config/theme';
import { numberCount } from '@/functions/numberCount';
import { CountryNames } from '@/data/components/ContryName';

export interface CountryCountClassProps {
    classNameContent?: string;
    classNameContentText?: string;
    classNameText?: string;
    classNameContentCount?: string;
    classNameCount?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateCount?: TextStyles | ThemesType;
}

export interface CountryCountBaseProps {
    country: string;
    count: number;
}

export interface CountryCountProps
    extends CountryCountClassProps,
        CountryCountBaseProps {}

export const CountryCountBase = ({
    classNameContent = '',
    classNameContentText = '',
    classNameText = '',
    classNameContentCount = '',
    classNameCount = '',
    styleTemplateCount = Theme.styleTemplate ?? '_default',
    styleTemplateText = Theme.styleTemplate ?? '_default',

    country,
    count,
}: CountryCountProps) => {
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentText}>
                    <Text
                        className={classNameText}
                        styleTemplate={styleTemplateText}
                    >
                        {CountryNames?.[country] ?? country}
                    </Text>
                </div>
                <div className={classNameContentCount}>
                    <Text
                        className={classNameCount}
                        styleTemplate={styleTemplateCount}
                    >
                        {numberCount(count)}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default CountryCountBase;
