import { useMemo } from 'react';

import * as styles from '@/components/CountryCount/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CountryCountBaseProps,
    CountryCountBase,
} from '@/components/CountryCount/Base';

export const CountryCountStyle = { ...styles } as const;

export type CountryCountStyles = keyof typeof CountryCountStyle;

export interface CountryCountProps extends CountryCountBaseProps {
    styleTemplate?: CountryCountStyles | ThemesType;
}

export const CountryCount = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: CountryCountProps) => {
    const Style = useMemo(
        () =>
            CountryCountStyle[styleTemplate as CountryCountStyles] ??
            CountryCountStyle._default,
        [styleTemplate]
    );

    return <CountryCountBase {...Style} {...props} />;
};
export default CountryCount;
