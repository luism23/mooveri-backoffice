import { Story, Meta } from "@storybook/react";

import { CountryCountProps, CountryCount } from "./index";

export default {
    title: "CountryCount/CountryCount",
    component: CountryCount,
} as Meta;

const CountryCountIndex: Story<CountryCountProps> = (args) => (
    <CountryCount {...args}>Test Children</CountryCount>
);

export const Index = CountryCountIndex.bind({});
Index.args = {
    count:1,
    country:"Co"
};
