import { useMemo, PropsWithChildren } from 'react';

import * as styles from '@/components/Popup/styles';

import { Theme, ThemesType } from '@/config/theme';

import { PopupBaseProps, PopupBase } from '@/components/Popup/Base';

export const PopupStyle = { ...styles } as const;

export type PopupStyles = keyof typeof PopupStyle;

export interface PopupProps extends PopupBaseProps {
    styleTemplate?: PopupStyles | ThemesType;
}

export const Popup = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PropsWithChildren<PopupProps>) => {
    const Style = useMemo(
        () => PopupStyle[styleTemplate as PopupStyles] ?? PopupStyle._default,
        [styleTemplate]
    );

    return <PopupBase {...Style} {...props} />;
};
export default Popup;
