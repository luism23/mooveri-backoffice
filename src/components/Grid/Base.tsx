import { Properties } from 'csstype';
import { PropsWithChildren, useMemo } from 'react';

export interface GridClassProps {}

export interface GridBaseProps extends PropsWithChildren {
    areas?: string[];
    areas_sm?: string[];
    areas_md?: string[];
    areas_lg?: string[];
    areas_xl?: string[];
    className?: string;
}

export interface GridProps extends GridClassProps, GridBaseProps {}

export const GridBase = ({
    children,
    className = '',
    areas = [],
    areas_sm = undefined,
    areas_md = undefined,
    areas_lg = undefined,
    areas_xl = undefined,
}: GridProps) => {
    const N = [12, 6, 4, 3, 2, 1];
    const generarRow: (area: string, n: number) => string = (
        area: string,
        n: number
    ) => {
        if (!N[n]) {
            return '';
        }
        if (area.length >= N[n]) {
            return `'${area
                .slice(0, N[n])
                .split('')
                .map((l) => new Array(12 / N[n]).fill(l).join(' '))
                .join(' ')}'`;
        }
        return generarRow(area, n + 1);
    };

    const style: Properties<string | number> = useMemo(() => {
        const area: any = {
            default: areas,
            sm: areas_sm ?? areas,
            md: areas_md ?? areas_sm ?? areas,
            lg: areas_lg ?? areas_md ?? areas_sm ?? areas,
            xl: areas_xl ?? areas_lg ?? areas_md ?? areas_sm ?? areas,
        };
        let select: any = 'default';

        if (window.outerWidth >= 575) {
            select = 'sm';
        }
        if (window.outerWidth >= 768) {
            select = 'md';
        }
        if (window.outerWidth >= 992) {
            select = 'lg';
        }
        if (window.outerWidth >= 1200) {
            select = 'xl';
        }

        const templateAreas = area?.[select]
            ?.map((a: string) => {
                const area = a.toLocaleUpperCase();
                return generarRow(area, 0);
            })
            .join(' ');

        return {
            gridTemplateAreas: `${templateAreas}`,
        };
    }, [areas, window]);

    return (
        <>
            <div className={`grid ${className}`} style={style}>
                {children}
            </div>
        </>
    );
};
export default GridBase;
