# Grid

## Dependencies

[Grid](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Grid)

```js
import { Grid } from '@/components/Grid';
```

## Import

```js
import { Grid, GridStyles } from '@/components/Grid';
```

## Props

```tsx
interface GridProps {}
```

## Use

```js
<Grid>Grid</Grid>
```
