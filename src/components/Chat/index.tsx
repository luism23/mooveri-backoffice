import { useMemo } from 'react';

import * as styles from '@/components/Chat/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ChatBaseProps, ChatBase } from '@/components/Chat/Base';

export const ChatStyle = { ...styles } as const;

export type ChatStyles = keyof typeof ChatStyle;

export interface ChatProps extends ChatBaseProps {
    styleTemplate?: ChatStyles | ThemesType;
}

export const Chat = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ChatProps) => {
    const Style = useMemo(
        () => ChatStyle[styleTemplate as ChatStyles] ?? ChatStyle._default,
        [styleTemplate]
    );

    return <ChatBase {...Style} {...props} />;
};
export default Chat;
