import { Story, Meta } from "@storybook/react";

import { ChatProps, Chat } from "./index";

export default {
    title: "Chat/Chat",
    component: Chat,
} as Meta;

const ChatIndex: Story<ChatProps> = (args) => (
    <Chat {...args}>Test Children</Chat>
);

export const Index = ChatIndex.bind({});
Index.args = {};
