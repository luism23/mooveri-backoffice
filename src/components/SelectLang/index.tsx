import { useMemo } from 'react';

import * as styles from '@/components/SelectLang/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    SelectLangBaseProps,
    SelectLangBase,
} from '@/components/SelectLang/Base';

export const SelectLangStyle = { ...styles } as const;

export type SelectLangStyles = keyof typeof SelectLangStyle;

export interface SelectLangProps extends SelectLangBaseProps {
    styleTemplate?: SelectLangStyles | ThemesType;
}

export const SelectLang = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: SelectLangProps) => {
    const Style = useMemo(
        () =>
            SelectLangStyle[styleTemplate as SelectLangStyles] ??
            SelectLangStyle._default,
        [styleTemplate]
    );

    return <SelectLangBase {...Style} {...props} />;
};
export default SelectLang;
