# SelectLang

## Dependencies

[SelectLang](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/SelectLang)

```js
import { SelectLang } from '@/components/SelectLang';
```

## Import

```js
import { SelectLang, SelectLangStyles } from '@/components/SelectLang';
```

## Props

```tsx
interface SelectLangProps {}
```

## Use

```js
<SelectLang>SelectLang</SelectLang>
```
