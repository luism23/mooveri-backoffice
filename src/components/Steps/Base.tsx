import { useMemo } from 'react';

export interface StepsClassProps {}

export interface StepProps {
    id: string;
    content: any;
}
export interface StepsBaseProps {
    steps: StepProps[];
    currentStep: number;
}

export interface StepsProps extends StepsClassProps, StepsBaseProps {}

export const StepsBase = ({ currentStep, steps }: StepsProps) => {
    const Step = useMemo(() => {
        return steps[currentStep];
    }, [currentStep, steps]);
    return <>{Step.content}</>;
};
export default StepsBase;
