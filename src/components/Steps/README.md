# Steps

## Dependencies

[Steps](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Steps)

```js
import { Steps } from '@/components/Steps';
```

## Import

```js
import { Steps, StepsStyles } from '@/components/Steps';
```

## Props

```tsx
interface StepsProps {}
```

## Use

```js
<Steps>Steps</Steps>
```
