import { Link, LinkProps } from '@/components/Link';

import { url } from '@/data/routes';
import { useLang } from '@/lang/translate';

export const LinkRegister = (props: LinkProps) => {
    const _t = useLang();
    return (
        <Link {...props} href={props.href ?? url.register}>
            {_t("Don't Have an Account?")}
        </Link>
    );
};

export const LinkLogin = (props: LinkProps) => {
    const _t = useLang();
    return (
        <Link {...props} href={props.href ?? url.login}>
            {_t('Already Have an Account?')}
        </Link>
    );
};

export const LinkTerm = ({
    styleTemplate = 'tolinkme2',
    ...props
}: LinkProps) => {
    const _t = useLang();
    return (
        <div className="flex flex-justify-center flex-gap-column-36">
            <Link {...props} href={url.term} styleTemplate={styleTemplate}>
                {_t('Terms')}
            </Link>
            <Link
                {...props}
                href={url.servicePolicies}
                styleTemplate={styleTemplate}
            >
                {_t('Service Policy')}
            </Link>
        </div>
    );
};

export const LinkForgotPassword = (props: LinkProps) => {
    const _t = useLang();
    return (
        <Link {...props} href={props.href ?? url.forgotPassword}>
            {_t('Forgot your password?')}
        </Link>
    );
};

export const LinkBecomeMover = (props: LinkProps) => {
    const _t = useLang();
    return (
        <Link {...props} href={props.href ?? url.home}>
            {_t('Become Mover')}
        </Link>
    );
};

export const LinkMyMoves = (props: LinkProps) => {
    const _t = useLang();
    return (
        <Link {...props} href={props.href ?? url.movings}>
            {_t('My Moves')}
        </Link>
    );
};
