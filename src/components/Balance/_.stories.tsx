import { Story, Meta } from "@storybook/react";

import { BalanceProps, Balance } from "./index";

export default {
    title: "Balance/Balance",
    component: Balance,
} as Meta;

const BalanceIndex: Story<BalanceProps> = (args) => (
    <Balance {...args}>Test Children</Balance>
);

export const Index = BalanceIndex.bind({});
Index.args = {
    text: "Current Balance",
    text2: "Pending Balance",
};
