import { useMemo } from 'react';

import * as styles from '@/components/Balance/styles';

import { Theme, ThemesType } from '@/config/theme';

import { BalanceBaseProps, BalanceBase } from '@/components/Balance/Base';

export const BalanceStyle = { ...styles } as const;

export type BalanceStyles = keyof typeof BalanceStyle;

export interface BalanceProps extends BalanceBaseProps {
    styleTemplate?: BalanceStyles | ThemesType;
}

export const Balance = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BalanceProps) => {
    const Style = useMemo(
        () =>
            BalanceStyle[styleTemplate as BalanceStyles] ??
            BalanceStyle._default,
        [styleTemplate]
    );

    return <BalanceBase {...Style} {...props} />;
};
export default Balance;
