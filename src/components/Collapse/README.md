# Collapse

## Import

```js
import { Collapse, CollapseStyles } from '@/components/Collapse';
```

## Props

```ts
interface CollapseProps {
    styleTemplate?: CollapseStyles;
    header: any;
    icon: any;
    children?: any;
}
```

## Use

```js
<Collapse header="Header Test">Test Children</Collapse>
```
