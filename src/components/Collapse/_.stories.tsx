import { Story, Meta } from "@storybook/react";

import { Collapse, CollapseProps } from "./index";

export default {
    title: "Collapse/Collapse",
    component: Collapse,
} as Meta;

const Template: Story<CollapseProps> = (args) => (
    <Collapse {...args}>Test Children</Collapse>
);

export const Index = Template.bind({});
Index.args = {
    header: "hola",
};
