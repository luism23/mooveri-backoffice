import { PropsWithChildren } from 'react';

import SVGArrowCollapse from '@/svg/arrowCollapse';
import { useData } from 'fenextjs-hook/cjs/useData';

export interface CollapseDataProps {
    active?: boolean;
}

export interface CollapseBaseProps extends PropsWithChildren {
    header?: any;
    defaultActive?: boolean;
    useActive?: boolean;
    active?: boolean;
    onChangeActive?: (active?: boolean) => void;
}

export interface CollapseClassProps {
    className?: string;
    classNameActive?: string;
    classNameNotActive?: string;
    classNameHeader?: string;
    classNameHeaderActive?: string;
    classNameHeaderNotActive?: string;
    classNameContentHeader?: string;
    classNameBody?: string;
    classNameBodyActive?: string;
    classNameBodyNotActive?: string;
    classNameIcon?: string;
    classNameIconActive?: string;
    classNameIconNotActive?: string;
    icon?: any;
}
export interface CollapseProps extends CollapseBaseProps, CollapseClassProps {}

export const CollapseBase = ({
    children,
    className = '',
    classNameActive = '',
    classNameNotActive = '',
    classNameHeader = '',
    classNameHeaderActive = '',
    classNameHeaderNotActive = '',
    classNameContentHeader = '',
    classNameBody = '',
    classNameBodyActive = '',
    classNameBodyNotActive = '',
    classNameIcon = '',
    classNameIconActive = '',
    classNameIconNotActive = '',
    icon = <SVGArrowCollapse />,

    defaultActive = false,
    header,
    ...props
}: CollapseProps) => {
    const { onChangeData, dataMemo: active } = useData<
        CollapseDataProps,
        boolean
    >(
        {
            active: defaultActive,
        },
        {
            onChangeDataAfter: ({ active }) => {
                props?.onChangeActive?.(active);
            },
            onMemo: ({ active }) => {
                return (props?.useActive ? props?.active : active) ?? false;
            },
        }
    );
    return (
        <>
            <div
                className={`${className} ${
                    active ? classNameActive : classNameNotActive
                }`}
            >
                <div
                    onClick={() => {
                        onChangeData('active')(!active);
                    }}
                    className={`${classNameHeader} ${
                        active
                            ? classNameHeaderActive
                            : classNameHeaderNotActive
                    }`}
                >
                    <div className={classNameContentHeader}>{header}</div>
                    <span
                        className={`${classNameIcon} ${
                            active
                                ? classNameIconActive
                                : classNameIconNotActive
                        }`}
                    >
                        {icon}
                    </span>
                </div>
                <div
                    className={`${classNameBody} ${
                        active ? classNameBodyActive : classNameBodyNotActive
                    }`}
                >
                    {children}
                </div>
            </div>
        </>
    );
};
export default CollapseBase;
