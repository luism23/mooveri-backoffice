export const mooveri = {
    classNameA: `
        color-warmGreyThree
        color-sea-hover
        font-12
        font-w-700
        flex
        flex-align-center
        text-decoration-underline
    `,
    classNameArrow: `
        m-r-10
    `,
    sizeArrow: 17,
};

export const mooveri2 = {
    classNameA: `
        color-black font-w-900 font-13
        color-sea-hover
        font-12
        font-w-700
        flex
        flex-align-center
        text-decoration-underline
    `,
    classNameArrow: `
        m-r-10
    `,
    sizeArrow: 30,
};

export const mooveri3 = {
    classNameA: `
        color-warmGreyThree 
        color-sea-hover
        font-12
        font-w-700
        flex
        flex-align-center
        text-decoration-underline
        color-sea-hover 
    `,
    classNameArrow: `
        m-r-10
    `,
    sizeArrow: 17,
};

export const moover4 = {
    classNameA: `
        color-warmGreyThree
        color-sea-hover
        font-18
        font-w-700
        flex
        flex-align-center
        text-decoration-underline
    `,
    classNameArrow: `
        d-none
    `,
};
