import GoBack_ from 'next/link';
import { useRouter } from 'next/router';

import { useLang } from '@/lang/translate';

import { ArrowGoBack } from '@/svg/arrowGoBack';

export interface GoBackClassProps {
    classNameA?: string;
    classNameArrow?: string;
    sizeArrow?: number;
}

export interface GoBackBaseProps {
    href?: string;
    className?: string;
    onClick?: () => void;
    text?: string;
}

export interface GoBackProps extends GoBackClassProps, GoBackBaseProps {}

export const GoBackBase = ({
    classNameA = '',
    classNameArrow = '',
    sizeArrow = 25,

    text = 'Back',
    href = '#',
    className = '',
    ...props
}: GoBackProps) => {
    const _t = useLang();
    const router = useRouter();
    const onGoBack = () => {
        if (props?.onClick) {
            props?.onClick?.();
        } else {
            if (href == '#') {
                router.back();
            }
        }
    };

    return (
        <GoBack_ href={props?.onClick ? '' : href ?? '#'}>
            <a
                onClick={(e) => {
                    e.preventDefault();
                    onGoBack();
                }}
                className={`${classNameA} ${className}`}
            >
                <span className={`${classNameArrow}`}>
                    <ArrowGoBack size={sizeArrow} />
                </span>
                {_t(text)}
            </a>
        </GoBack_>
    );
};
export default GoBackBase;
