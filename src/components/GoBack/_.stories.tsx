import { Story, Meta } from "@storybook/react";

import { GoBackProps, GoBack } from "./index";

export default {
    title: "Button/GoBack",
    component: GoBack,
} as Meta;

const Template: Story<GoBackProps> = (args) => (
    <GoBack {...args}>Test Children</GoBack>
);

export const Index = Template.bind({});
Index.args = {};
