import { Story, Meta } from "@storybook/react";

import { PaymentProps, Payment } from "./index";

export default {
    title: "Payment/Payment",
    component: Payment,
} as Meta;

const PaymentIndex: Story<PaymentProps> = (args) => (
    <Payment {...args}>Test Children</Payment>
);

export const Index = PaymentIndex.bind({});
Index.args = {};
