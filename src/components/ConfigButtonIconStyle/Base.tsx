import { Text, TextStyles } from '@/components/Text';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { useData } from 'fenextjs-hook/cjs/useData';
import { PopupStyles } from '../Popup';
import { Collapse, CollapseStyles } from '../Collapse';

import Color from '@/svg/Color';
import Font from '@/svg/Font';
import Border from '@/svg/Border';
import {
    RadioBorderRadiusButton,
    RadioBorderRadiusButtonStyles,
} from '../Input/RadioBorderRadiusButton';
import {
    InputSelectBackground,
    InputSelectBackgroundStyles,
} from '../Input/SelectBackground';
import { ButtonIconConfig } from '@/interfaces/Button';
import ContentWidth from '../ContentWidth';
import InputRange from '../Input/Range';
import { InputColorStyles } from '../Input/Color';
import { ConfigBorder, ConfigBorderStyles } from '../Input/ConfigBorder';

export interface ConfigButtonIconStyleClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    styleTemplateRadioBorderButton?: RadioBorderRadiusButtonStyles | ThemesType;
    styleTemplateInputSelectBackground?:
        | InputSelectBackgroundStyles
        | ThemesType;

    classNameContentExample?: string;

    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateTitleInput?: TextStyles | ThemesType;
    styleTemplateConfigBorder?: ConfigBorderStyles | ThemesType;
}

export interface ConfigButtonIconStyleBaseProps {
    defaultValue?: ButtonIconConfig;
    onChange?: (data: ButtonIconConfig) => void;
}

export interface ConfigButtonIconStyleProps
    extends ConfigButtonIconStyleClassProps,
        ConfigButtonIconStyleBaseProps {}

export const ConfigButtonIconStyleBase = ({
    styleTemplateTitlePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    styleTemplateRadioBorderButton = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelectBackground = Theme?.styleTemplate ?? '_default',

    styleTemplateTitleInput = Theme?.styleTemplate ?? '_default',

    styleTemplateConfigBorder = Theme?.styleTemplate ?? '_default',

    defaultValue = {
        background: {
            gradient: {
                color1: '#04506c',
                deg: 130,
                color2: '#9d016e',
            },
            type: 'gradient',
        },
        padding: 5,
        size: 15,
        borderRadius: 'rounded',
        border: {
            color: 'white',
            size: 0,
            type: 'solid',
        },
    },
    onChange,
}: ConfigButtonIconStyleProps) => {
    const _t = useLang();

    const { data, onChangeData } = useData<ButtonIconConfig>(defaultValue, {
        onChangeDataAfter: (data: ButtonIconConfig) => {
            onChange?.(data);
        },
    });

    return (
        <>
            <Text styleTemplate={styleTemplateTitlePopup}>
                {_t('Customize your Icon Button')}
            </Text>
            <Collapse
                header={
                    <>
                        <Font size={15} />
                        {_t('Icon Size')}
                    </>
                }
                styleTemplate={styleTemplateCollapse}
                defaultActive={true}
            >
                <div className="p-h-15 p-v-15">
                    <InputRange
                        defaultValue={data.size}
                        min={15}
                        max={40}
                        onChange={onChangeData('size')}
                    />
                </div>
            </Collapse>
            <Collapse
                header={
                    <>
                        <Font size={15} />
                        {_t('Icon Padding')}
                    </>
                }
                styleTemplate={styleTemplateCollapse}
                defaultActive={true}
            >
                <div className="p-h-15 p-v-15">
                    <InputRange
                        defaultValue={data.padding}
                        min={0}
                        max={10}
                        onChange={onChangeData('padding')}
                    />
                </div>
            </Collapse>
            <Collapse
                header={
                    <>
                        <Border size={15} />
                        {_t('Icon Border')}
                    </>
                }
                styleTemplate={styleTemplateCollapse}
                defaultActive={true}
            >
                <Text
                    styleTemplate={styleTemplateTitleInput}
                    className="p-h-15"
                >
                    {_t('Icon Round')}
                </Text>
                <ContentWidth size={300} className="m-h-auto">
                    <RadioBorderRadiusButton
                        onChange={onChangeData('borderRadius')}
                        styleTemplate={styleTemplateRadioBorderButton}
                        defaultValue={data.borderRadius}
                    />
                </ContentWidth>
                <ConfigBorder
                    defaultValue={data.border}
                    onChange={onChangeData('border')}
                    styleTemplate={styleTemplateConfigBorder}
                />
            </Collapse>
            <Collapse
                header={
                    <>
                        <Color size={15} />
                        {_t('Icon Background')}
                    </>
                }
                styleTemplate={styleTemplateCollapse}
                defaultActive={true}
            >
                <InputSelectBackground
                    title={null}
                    onChange={onChangeData('background')}
                    styleTemplate={styleTemplateInputSelectBackground}
                    useType={{
                        color: true,
                        gradient: true,
                        img: false,
                        video: false,
                    }}
                    defaultValue={data.background}
                    useOpacity={true}
                />
            </Collapse>
        </>
    );
};
export default ConfigButtonIconStyleBase;
