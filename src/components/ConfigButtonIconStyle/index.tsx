import { useMemo } from 'react';

import * as styles from '@/components/ConfigButtonIconStyle/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigButtonIconStyleBaseProps,
    ConfigButtonIconStyleBase,
} from '@/components/ConfigButtonIconStyle/Base';

export const ConfigButtonIconStyleStyle = { ...styles } as const;

export type ConfigButtonIconStyleStyles =
    keyof typeof ConfigButtonIconStyleStyle;

export interface ConfigButtonIconStyleProps
    extends ConfigButtonIconStyleBaseProps {
    styleTemplate?: ConfigButtonIconStyleStyles | ThemesType;
}

export const ConfigButtonIconStyle = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigButtonIconStyleProps) => {
    const Style = useMemo(
        () =>
            ConfigButtonIconStyleStyle[
                styleTemplate as ConfigButtonIconStyleStyles
            ] ?? ConfigButtonIconStyleStyle._default,
        [styleTemplate]
    );

    return <ConfigButtonIconStyleBase {...Style} {...props} />;
};
export default ConfigButtonIconStyle;
