import { NotificationClassProps } from '@/components/Notification/Base';

export const mooveri: NotificationClassProps = {
    classNameContent: `
        pos-f
        top-p-100
        left-0
        p-h-15 
        p-v-10
        width-p-100
        font-nunito
        font-w-700
        transform
        z-index-10
    `,
    classNameContentActive: `
        transform-translate-Y-p--100
    `,
    classNameContentInactive: `
        opacity-0
    `,
    classNameNotification: `
        
    `,
    classNameNotificationActive: `
        
    `,
    classNameNotificationInactive: `
    `,

    classNameClose: `
        d-none
    `,
    classNameType: {
        normal: `
            bg-white
        `,
        ok: `
            bg-green
            color-white
        `,
        error: `
            bg-red
            color-white
        `,
        warning: `
            bg-orange
            color-white
        `,
    },

    delay: 10,
    time: 3000,
    speed: 3,
};

export const mooveriFixed: NotificationClassProps = {
    ...mooveri,
    classNameContent: `
        pos-r
        top-p-100
        left-0
        p-v-10
        width-p-100
        font-nunito
        font-w-700
        transform
        z-index-10
    `,
    classNameContentActive: `
    `,
    classNameContentInactive: `
        d-none
    `,
    classNameNotification: `
        flex
        flex-align-center
        flex-justify-between
        flex-nowrap
        container
        p-h-15 
    `,

    classNameClose: `
        cursor-pointer
    `,
    time: -1,
};
