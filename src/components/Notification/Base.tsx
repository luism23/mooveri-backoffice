import { PropsWithChildren, useEffect, useMemo, useState } from 'react';

import { Close } from '@/svg/close';

export type NotificationType = 'normal' | 'ok' | 'error' | 'warning';

export interface NotificationClassProps {
    classNameContent?: string;
    classNameContentInactive?: string;
    classNameContentActive?: string;

    classNameNotification?: string;
    classNameNotificationInactive?: string;
    classNameNotificationActive?: string;

    classNameClose?: string;
    iconClose?: any;

    classNameType?: {
        [id in NotificationType]?: string;
    };

    delay?: number;
    time?: number;
    speed?: number;
}

export interface NotificationBaseProps extends PropsWithChildren {
    type?: NotificationType;
    reset?: () => void;
    id?: string;
}

export interface NotificationProps
    extends NotificationClassProps,
        NotificationBaseProps {}

export const NotificationBase = ({
    classNameContent = '',
    classNameContentInactive = '',
    classNameContentActive = '',

    classNameNotification = '',
    classNameNotificationInactive = '',
    classNameNotificationActive = '',

    classNameClose = '',
    iconClose = <Close />,

    delay = 10,
    time = 3000,
    speed = 1,

    type = 'normal',

    children = '',
    reset = () => {},
    ...props
}: NotificationProps) => {
    const [active, setActive] = useState(false);

    const onTime = async () => {
        await new Promise((r) => setTimeout(r, delay));
        if (navigator?.vibrate != undefined) {
            navigator.vibrate(1000);
        }
        setActive(true);
        if (time > -1) {
            await new Promise((r) => setTimeout(r, time));
            if (navigator?.vibrate != undefined) {
                navigator.vibrate(0);
            }
            setActive(false);
            reset();
        }
    };

    useEffect(() => {
        if (children != '') {
            onTime();
        }
    }, [children]);

    const classNameType = useMemo(
        () => props?.classNameType?.[type] ?? '',
        [type]
    );

    return (
        <div
            className={`${classNameContent} ${
                active ? classNameContentActive : classNameContentInactive
            }
                ${classNameType}
            `}
            style={{
                transition: `${1 / speed}s`,
            }}
        >
            <div
                className={`${classNameNotification} ${
                    active
                        ? classNameNotificationActive
                        : classNameNotificationInactive
                }`}
            >
                {children}
                <div
                    className={classNameClose}
                    onClick={() => {
                        setActive(false);
                        reset();
                    }}
                >
                    {iconClose}
                </div>
            </div>
        </div>
    );
};
export default NotificationBase;
