import Theme, { ThemesType } from '@/config/theme';
import money from '@/functions/money';
import { useLang } from '@/lang/translate';
import Text, { TextStyles } from '../Text';

export interface ButtonAmountSuscriptionsClassProps {
    classNameContent?: string;
    classNameContentButton?: string;
    classNameContentIconLogo?: string;
    classNameContentCol1?: string;
    classNameContentCol2?: string;
    line?: string;
    styleTemplateNumber?: TextStyles | ThemesType;
    classNameContentNumberBalance?: string;
    classNameMonth?: string;
    styleTemplateTitleBalance?: TextStyles | ThemesType;
    classNameContentTitleBalance?: string;
}

export interface ButtonAmountSuscriptionsBaseProps {
    BalanceMoneyMounth?: number;
    TotalButtonMonetize?: number;
    AmountSuscription?: number;
}

export interface ButtonAmountSuscriptionsProps
    extends ButtonAmountSuscriptionsClassProps,
        ButtonAmountSuscriptionsBaseProps {}

export const ButtonAmountSuscriptionsBase = ({
    classNameContent = '',
    classNameContentCol1 = '',
    classNameContentCol2 = '',
    styleTemplateNumber = Theme.styleTemplate ?? '_default',
    styleTemplateTitleBalance = Theme.styleTemplate ?? '_default',
    classNameContentNumberBalance = '',
    classNameMonth = '',
    classNameContentTitleBalance = '',
    BalanceMoneyMounth,
    AmountSuscription,
    TotalButtonMonetize,
}: ButtonAmountSuscriptionsProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentCol1}>
                    <Text
                        styleTemplate={styleTemplateNumber}
                        className={classNameContentNumberBalance}
                    >
                        {money(BalanceMoneyMounth ?? 0)}
                        <span className={classNameMonth}>/{_t('Month')}</span>
                    </Text>
                    <Text
                        styleTemplate={styleTemplateTitleBalance}
                        className={classNameContentTitleBalance}
                    >
                        {`${TotalButtonMonetize} ${_t('Buttons')}`}
                    </Text>
                </div>

                <div className={classNameContentCol2}>
                    <Text
                        styleTemplate={styleTemplateNumber}
                        className={classNameContentNumberBalance}
                    >
                        {AmountSuscription}
                    </Text>
                    <Text
                        styleTemplate={styleTemplateTitleBalance}
                        className={classNameContentTitleBalance}
                    >
                        {_t('Subscriptors')}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default ButtonAmountSuscriptionsBase;
