import Theme, { ThemesType } from '@/config/theme';
import money from '@/functions/money';
import { useLang } from '@/lang/translate';
import Exclamation from '@/svg/exclamation';
import Text, { TextStyles } from '../Text';

export interface BalanceMoneyClassProps {
    classNameContentIcon?: string;
    currentPendingMoney?: number;
    styleTemplateNumber?: TextStyles | ThemesType;
    styleTemplateTitleBalance?: TextStyles | ThemesType;
    styleTemplateNumberPending?: TextStyles | ThemesType;
    styleTemplateTitlePending?: TextStyles | ThemesType;
    TitleCurrent?: string;
    TitleCurrentPending?: string;

    classNameContentNumberBalance?: string;
    classNameContentTitleBalance?: string;

    classNameContent?: string;
    classNameContentCol1?: string;
    classNameContentCol2?: string;
    line?: string;

    classNameContentNumberPendingBalance?: string;
    classNameContentTitlePendingBalance?: string;
}

export interface BalanceMoneyBaseProps {
    getCurrentBalance?: number;
    currentPendingMoney?: number;
}

export interface BalanceMoneyProps
    extends BalanceMoneyClassProps,
        BalanceMoneyBaseProps {}

export const BalanceMoneyBase = ({
    classNameContent = '',
    classNameContentCol1 = '',
    classNameContentCol2 = '',
    classNameContentNumberBalance = '',
    classNameContentTitleBalance = '',
    classNameContentNumberPendingBalance = '',
    classNameContentTitlePendingBalance = '',
    classNameContentIcon = '',
    styleTemplateNumber = Theme.styleTemplate ?? '_default',
    styleTemplateTitleBalance = Theme.styleTemplate ?? '_default',
    styleTemplateNumberPending = Theme.styleTemplate ?? '_default',
    styleTemplateTitlePending = Theme.styleTemplate ?? '_default',
    TitleCurrent = 'Current Balance',
    TitleCurrentPending = 'Pending Balance',
    line = '',
    getCurrentBalance,
    currentPendingMoney,
}: BalanceMoneyProps) => {
    const _t = useLang();
    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentCol1}>
                    <Text
                        styleTemplate={styleTemplateNumber}
                        className={classNameContentNumberBalance}
                    >
                        {money(getCurrentBalance ?? 0)}
                    </Text>
                    <Text
                        styleTemplate={styleTemplateTitleBalance}
                        className={classNameContentTitleBalance}
                    >
                        {_t(TitleCurrent)}
                    </Text>
                </div>
                <div className={line}></div>
                <div className={classNameContentCol2}>
                    <Text
                        styleTemplate={styleTemplateNumberPending}
                        className={classNameContentNumberPendingBalance}
                    >
                        {money(currentPendingMoney ?? 0)}
                    </Text>
                    <Text
                        styleTemplate={styleTemplateTitlePending}
                        className={classNameContentTitlePendingBalance}
                    >
                        <span className={classNameContentIcon}>
                            <Exclamation size={18} />
                        </span>
                        {_t(TitleCurrentPending)}
                    </Text>
                </div>
            </div>
        </>
    );
};
export default BalanceMoneyBase;
