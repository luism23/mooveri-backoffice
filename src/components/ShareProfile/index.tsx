import { useMemo } from 'react';

import * as styles from '@/components/ShareProfile/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ShareProfileBaseProps,
    ShareProfileBase,
} from '@/components/ShareProfile/Base';

export const ShareProfileStyle = { ...styles } as const;

export type ShareProfileStyles = keyof typeof ShareProfileStyle;

export interface ShareProfileProps extends ShareProfileBaseProps {
    styleTemplate?: ShareProfileStyles | ThemesType;
}

export const ShareProfile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ShareProfileProps) => {
    const Style = useMemo(
        () =>
            ShareProfileStyle[styleTemplate as ShareProfileStyles] ??
            ShareProfileStyle._default,
        [styleTemplate]
    );

    return <ShareProfileBase {...Style} {...props} />;
};
export default ShareProfile;
