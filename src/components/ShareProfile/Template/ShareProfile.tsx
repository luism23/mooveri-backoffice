import { ShareProfile, ShareProfileProps } from '@/components/ShareProfile';

export const Share = ({ ...props }: ShareProfileProps) => {
    return <ShareProfile {...props} />;
};
export default ShareProfile;
