# ConfigButtonStyle

## Dependencies

[ConfigButtonStyle](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ConfigButtonStyle)

```js
import { ConfigButtonStyle } from '@/components/ConfigButtonStyle';
```

## Import

```js
import {
    ConfigButtonStyle,
    ConfigButtonStyleStyles,
} from '@/components/ConfigButtonStyle';
```

## Props

```tsx
interface ConfigButtonStyleProps {}
```

## Use

```js
<ConfigButtonStyle>ConfigButtonStyle</ConfigButtonStyle>
```
