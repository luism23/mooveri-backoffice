import { TextStyles } from '@/components/Text';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { useData } from 'fenextjs-hook/cjs/useData';
import { Popup, PopupStyles } from '../Popup';
import { Collapse, CollapseStyles } from '../Collapse';

import Color from '@/svg/Color';
import Font from '@/svg/Font';
import Border from '@/svg/Border';
import Size from '@/svg/Size';
import Settings from '@/svg/Settings';
import {
    RadioSizeButton,
    RadioSizeButtonStyles,
} from '../Input/RadioSizeButton';
import {
    RadioBorderRadiusButton,
    RadioBorderRadiusButtonStyles,
} from '../Input/RadioBorderRadiusButton';
import {
    InputSelectBackground,
    InputSelectBackgroundStyles,
} from '../Input/SelectBackground';
import { ConfigText, ConfigTextStyles } from '../Input/ConfigText';
import { ButtonConfig, ButtonConfigType } from '@/interfaces/Button';
import RadioIconButton from '../Input/RadioIconButton';
import { ConfigBorder, ConfigBorderStyles } from '../Input/ConfigBorder';
import { getBtn } from '@/functions/getBtn';
import { RSLinkConfigDataProps } from '../RS/LinkConfig/Base';
import ConfigButtonIconStyle from '../ConfigButtonIconStyle';
import ConfigBoxShadow, {
    ConfigBoxShadowStyles,
} from '../Input/ConfigBoxShadow';
import Clone from '@/svg/Clone';
import { useMemo, useState } from 'react';
import LoaderPage from '../Loader/LoaderPage';
import Reload from '@/svg/Reload';

export interface ConfigButtonStyleClassProps {
    styleTemplatePopup?: PopupStyles | ThemesType;
    styleTemplateTitlePopup?: TextStyles | ThemesType;

    styleTemplateCollapse?: CollapseStyles | ThemesType;

    styleTemplateRadioSizeButton?: RadioSizeButtonStyles | ThemesType;
    styleTemplateRadioBorderButton?: RadioBorderRadiusButtonStyles | ThemesType;
    styleTemplateConfigBorder?: ConfigBorderStyles | ThemesType;
    styleTemplateConfigBoxShadow?: ConfigBoxShadowStyles | ThemesType;
    styleTemplateInputSelectBackground?:
        | InputSelectBackgroundStyles
        | ThemesType;
    styleTemplateConfigText?: ConfigTextStyles | ThemesType;

    classNameContentExample?: string;
    classNameContentExampleText?: string;
    classNameReload?: string;
}

export interface ConfigButtonStyleBaseProps {
    defaultValue?: ButtonConfig;
    onChange?: (data: ButtonConfig) => void;
    links?: RSLinkConfigDataProps[];
}

export interface ConfigButtonStyleProps
    extends ConfigButtonStyleClassProps,
        ConfigButtonStyleBaseProps {}

export const ConfigButtonStyleBase = ({
    styleTemplatePopup = Theme?.styleTemplate ?? '_default',

    styleTemplateCollapse = Theme?.styleTemplate ?? '_default',

    styleTemplateRadioSizeButton = Theme?.styleTemplate ?? '_default',
    styleTemplateRadioBorderButton = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelectBackground = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigText = Theme?.styleTemplate ?? '_default',

    styleTemplateConfigBorder = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigBoxShadow = Theme?.styleTemplate ?? '_default',

    classNameContentExample = '',
    classNameContentExampleText = '',

    classNameReload = '',

    defaultValue = {},
    links = [],
    onChange,
}: ConfigButtonStyleProps) => {
    const _t = useLang();

    const [loader, setLoader] = useState(false);
    const [isActive, setIsActive] = useState<ButtonConfigType | ''>('');

    const defaultValue_ = useMemo(() => defaultValue, []);
    const { data, onChangeData, onRestart } = useData<ButtonConfig>(
        defaultValue_,
        {
            onChangeDataAfter: (data: ButtonConfig) => {
                onChange?.(data);
            },
        }
    );

    const content = useMemo(() => {
        if (loader) {
            return <LoaderPage />;
        }
        return (
            <>
                <div className={classNameContentExample}>
                    <span className={classNameContentExampleText}>
                        {_t('Vista Previa')}
                    </span>

                    <div className="d-block width-p-100">
                        {getBtn(
                            links[0] ?? {
                                rs: 'facebook',
                            },
                            data
                        )}
                    </div>
                </div>

                <Collapse
                    header={
                        <>
                            <Size size={15} />
                            {_t('Button Type')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'type'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'type' : '');
                    }}
                >
                    <RadioSizeButton
                        onChange={onChangeData('size')}
                        styleTemplate={styleTemplateRadioSizeButton}
                        defaultValue={data.size}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Clone size={15} />
                            {_t('Button Box Shadow')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'box-shadow'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'box-shadow' : '');
                    }}
                >
                    <ConfigBoxShadow
                        defaultValue={data.boxShadow}
                        onChange={onChangeData('boxShadow')}
                        styleTemplate={styleTemplateConfigBoxShadow}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Border size={15} />
                            {_t('Button Border')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'border'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'border' : '');
                    }}
                >
                    <RadioBorderRadiusButton
                        onChange={onChangeData('borderRadius')}
                        styleTemplate={styleTemplateRadioBorderButton}
                        defaultValue={data.borderRadius}
                    />
                    <ConfigBorder
                        defaultValue={data.border}
                        onChange={onChangeData('border')}
                        styleTemplate={styleTemplateConfigBorder}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Color size={15} />
                            {_t('Button Background')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'bg'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'bg' : '');
                    }}
                >
                    <InputSelectBackground
                        title={null}
                        onChange={onChangeData('background')}
                        styleTemplate={styleTemplateInputSelectBackground}
                        useType={{
                            color: true,
                            gradient: true,
                            img: false,
                            video: false,
                        }}
                        defaultValue={data.background}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Font size={15} />
                            {_t('Button Text')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'text'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'text' : '');
                    }}
                >
                    <ConfigText
                        onChange={onChangeData('text')}
                        styleTemplate={styleTemplateConfigText}
                        defaultValue={data.text}
                    />
                </Collapse>
                <Collapse
                    header={
                        <>
                            <Settings size={15} />
                            {_t('Button Icon')}
                        </>
                    }
                    styleTemplate={styleTemplateCollapse}
                    defaultActive={true}
                    active={isActive == 'icon'}
                    useActive={true}
                    onChangeActive={(active) => {
                        setIsActive(active ? 'icon' : '');
                    }}
                >
                    <RadioIconButton
                        onChange={onChangeData('icon')}
                        styleTemplate={styleTemplateRadioSizeButton}
                        defaultValue={data.icon}
                    />
                    {data.icon == 'con' ? (
                        <ConfigButtonIconStyle
                            defaultValue={data.iconConfig}
                            onChange={onChangeData('iconConfig')}
                            styleTemplate="tolinkme"
                        />
                    ) : (
                        <></>
                    )}
                </Collapse>
            </>
        );
    }, [data, isActive, loader, _t]);

    const onRestartData = async () => {
        setLoader(true);
        onRestart();
        await new Promise((r) => setTimeout(r, 500));
        setLoader(false);
    };
    return (
        <>
            <Popup
                btn={_t('Button')}
                styleTemplate={styleTemplatePopup}
                contentBackExtra={
                    <>
                        <div
                            className={classNameReload}
                            onClick={onRestartData}
                        >
                            <Reload size={20} />
                        </div>
                    </>
                }
            >
                {content}
            </Popup>
        </>
    );
};
export default ConfigButtonStyleBase;
