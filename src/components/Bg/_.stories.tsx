import { Story, Meta } from "@storybook/react";

import { ImgProps } from "@/components/Img";
import { Bg } from "./index";

export default {
    title: "Img/Bg",
    component: Bg,
} as Meta;

const TemplateBg: Story<ImgProps> = (args) => <Bg {...args} />;

export const TypeBg = TemplateBg.bind({});
TypeBg.args = {
    src: "banner-home.png",
};
