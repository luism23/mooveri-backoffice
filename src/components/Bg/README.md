# Bg

## Dependencies

[Img](https://gitlab.com/franciscoblancojn/fenextjs/-/tree/develop/src/components/Img)
[Image](https://gitlab.com/franciscoblancojn/fenextjs/-/tree/develop/src/components/Image)

```js
import { Img, ImgProps } from '@/components/Img';
import { Image } from '@/components/Image';
```

## Import

```js
import { Bg, BgStyles } from '@/components/Bg';
```

## Props

```tsx
interface BgProps extends ImgProps {
    image?: boolean;
}
```

## Use

```js
<Bg src="banner-home.png" />
```
