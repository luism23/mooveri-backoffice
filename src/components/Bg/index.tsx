import { useMemo } from 'react';

import * as styles from '@/components/Bg/styles';

import { Theme, ThemesType } from '@/config/theme';

import { BgBaseProps, BgBase } from '@/components/Bg/Base';

export const BgStyle = { ...styles } as const;

export type BgStyles = keyof typeof BgStyle;

export interface BgProps extends BgBaseProps {
    styleTemplate?: BgStyles | ThemesType;
}

export const Bg = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: BgProps) => {
    const Style = useMemo(
        () => BgStyle[styleTemplate as BgStyles] ?? BgStyle._default,
        [styleTemplate]
    );

    return <BgBase {...Style} {...props} styleTemplate={styleTemplate} />;
};
export default Bg;
