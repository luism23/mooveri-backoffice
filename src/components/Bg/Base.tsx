import { Img, ImgProps } from '@/components/Img';
import { Image } from '@/components/Image';

export interface BgClassProps {}

export interface BgBaseProps extends ImgProps {
    image?: boolean;
}

export interface BgProps extends BgClassProps, BgBaseProps {}

export const BgBase = ({
    src = '',
    name = '',
    className = '',
    classNameImg = '',
    capas = [],
    image = false,
    styleTemplate,
}: BgProps) => {
    if (image) {
        return (
            <>
                <Image
                    src={src}
                    name={name}
                    className={`bg ${className}`}
                    styleTemplate={styleTemplate}
                />
            </>
        );
    }
    return (
        <>
            <Img
                src={src}
                name={name}
                className={`bg ${className}`}
                classNameImg={`bg-img ${classNameImg}`}
                capas={capas}
                styleTemplate={styleTemplate}
            />
        </>
    );
};

export default BgBase;
