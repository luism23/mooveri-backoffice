# ScanFile

## Dependencies

[ScanFile](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/ScanFile)

```js
import { ScanFile } from '@/components/ScanFile';
```

## Import

```js
import { ScanFile, ScanFileStyles } from '@/components/ScanFile';
```

## Props

```tsx
interface ScanFileProps {}
```

## Use

```js
<ScanFile>ScanFile</ScanFile>
```
