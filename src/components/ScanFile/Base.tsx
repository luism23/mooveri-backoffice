import ButtonScan from '@/components/Button/Template/ButtonScan';
import { ButtonStyles } from '@/components/Button';
import { useEffect, useState } from 'react';
import Theme, { ThemesType } from '@/config/theme';
import { ItemScanProps, ScanItemScanProps } from '@/interfaces/Scan/ItemScan';
import { ListItemScan, ListItemScanStyles } from '@/components/ListItemScan';

export interface ScanFileClassProps {
    styleTemplateBtn?: ButtonStyles | ThemesType;
    styleTemplateListItemScan?: ListItemScanStyles | ThemesType;
}

export interface ScanFileBaseProps {
    onChange?: (data: ItemScanProps[]) => void;
    onProssesItemScan?: ScanItemScanProps;
}

export interface ScanFileProps extends ScanFileClassProps, ScanFileBaseProps {}

export const ScanFileBase = ({
    styleTemplateBtn = Theme.styleTemplate ?? '_default',
    styleTemplateListItemScan = Theme.styleTemplate ?? '_default',

    onProssesItemScan = (e) => [e],

    ...props
}: ScanFileProps) => {
    const [files, setFiles] = useState<ItemScanProps[]>([]);

    const addFile = async (file: ItemScanProps) => {
        const n = await onProssesItemScan(file);
        setFiles((pre) => [...pre, ...n]);
    };

    const removeFile = (i: number) => {
        setFiles((pre) => pre.filter((p, j) => j != i && p));
    };

    useEffect(() => {
        props?.onChange?.(files);
    }, [files]);

    return (
        <>
            <ButtonScan
                onScan={addFile}
                styleTemplate={styleTemplateBtn}
                returnUrl={false}
                isBtn={false}
            />
            <ListItemScan
                files={files}
                removeFile={removeFile}
                styleTemplate={styleTemplateListItemScan}
            />
        </>
    );
};
export default ScanFileBase;
