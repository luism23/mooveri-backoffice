import { Story, Meta } from "@storybook/react";

import { ScanFileProps, ScanFile } from "./index";

export default {
    title: "ScanFile/ScanFile",
    component: ScanFile,
} as Meta;

const ScanFileIndex: Story<ScanFileProps> = (args) => (
    <ScanFile {...args}>Test Children</ScanFile>
);

export const Index = ScanFileIndex.bind({});
Index.args = {};
