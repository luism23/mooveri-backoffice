import { Theme, ThemeProps } from '@/config/theme';

export interface ImageProps extends ThemeProps {
    src?: string;
    name?: string;
    className?: string;
}

export const Image = ({
    src = '',
    name = '',
    className = '',
    styleTemplate = Theme.styleTemplate ?? '_default',
}: ImageProps) => {
    return (
        <>
            <img
                src={`/image/${styleTemplate}/${src}`}
                alt={name}
                className={`${className} Image src-${src}`}
            />
        </>
    );
};
export default Image;
