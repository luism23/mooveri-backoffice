# Image

## Import

```js
import { Image } from '@/components/Image';
```

## Props

```ts
interface ImageProps {
    src: string;
    name?: string;
    className?: string;
}
```

## Use

```js
<Image src="img.png" />
```
