import { Story, Meta } from "@storybook/react";

import { ImageProps, Image } from "./index";

export default {
    title: "Img/Image",
    component: Image,
} as Meta;

const Template: Story<ImageProps> = (args) => (
    <Image {...args}>Test Children</Image>
);

export const Index = Template.bind({});
Index.args = {
    src: "imageSmall.png",
};
