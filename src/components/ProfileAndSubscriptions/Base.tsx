import Theme, { ThemesType } from '@/config/theme';
import ContentWidth from '../ContentWidth';
import Space from '../Space';
import Text, { TextStyles } from '../Text';
import { useLang } from '@/lang/translate';
import { RSLinkConfigDataProps } from '../RS/LinkConfig/Base';
import { InfoProfileEditDataProps } from '../InfoProfileEdit/Base';
import ButtonPay from '../ButtonPay';
import UserAccount from '@/svg/userAccount';

export interface ProfileAndSubscriptionsClassProps {
    pay?: string;
    classNameImg?: string;
    classNameAvatar?: string;
    classNameContent?: string;
    classNameContentText?: string;
    classNameContentAvatar?: string;
    classNameContentWidthBtn?: string;
    classNameContentWidthAvatar?: string;
    styleTemplateText?: TextStyles | ThemesType;
    styleTemplateText2?: TextStyles | ThemesType;
    img?: string;
}

export interface ProfileAndSubscriptionsBaseProps {
    img?: string;
    btn: RSLinkConfigDataProps;
    text?: string;
    style: InfoProfileEditDataProps;
    text2?: string;
    text3?: string;
    NameUser?: string;
    price?: string;
    recurrence?: string;
    period?: string;
    logoIcon?: string;
}

export interface ProfileAndSubscriptionsProps
    extends ProfileAndSubscriptionsClassProps,
        ProfileAndSubscriptionsBaseProps {}

export const ProfileAndSubscriptionsBase = ({
    text = 'Login Now',
    text2 = 'Register now and get these subscription:',
    text3 = '',
    NameUser,
    classNameImg = '',
    img,
    price,
    period,
    recurrence,
    logoIcon,
    classNameAvatar = '',
    classNameContent = '',
    classNameContentText = '',
    classNameContentAvatar = '',
    classNameContentWidthBtn = '',
    classNameContentWidthAvatar = '',
    styleTemplateText = Theme?.styleTemplate ?? '_default',
    styleTemplateText2 = Theme?.styleTemplate ?? '_default',
}: ProfileAndSubscriptionsProps) => {
    const _t = useLang();

    return (
        <>
            <div className={classNameContent}>
                <ContentWidth size={450}>
                    <Space size={25} />
                    <Text styleTemplate={styleTemplateText}>{_t(text)}</Text>
                    <div>
                        <ContentWidth
                            size={100}
                            className={classNameContentWidthAvatar}
                        >
                            <div className={classNameContentAvatar}>
                                <div className={classNameImg}>
                                    {img !== '' ||
                                    img == null ||
                                    img == undefined ? (
                                        <div className={classNameImg}>
                                            <img
                                                className={classNameAvatar}
                                                src={img}
                                            />
                                        </div>
                                    ) : (
                                        <div
                                            className="
                                        flex 
                                        flex-justify-center
                                        m-h-auto
                                        height-100
                                        width-100
                                        bg-whiteThree
                                        overflow-hidden
                                        color-white
                                        border-radius-100
                                "
                                        >
                                            <UserAccount size={65} />
                                        </div>
                                    )}
                                </div>
                                <Text
                                    className="text-capitalize"
                                    styleTemplate="tolinkme45"
                                >
                                    @{NameUser ?? `user`}
                                </Text>
                            </div>
                        </ContentWidth>
                    </div>
                    <div className={classNameContentText}>
                        <Text styleTemplate={styleTemplateText2}>
                            {_t(text2)}
                        </Text>
                        <Space size={5} />
                        <Text styleTemplate={styleTemplateText2}>
                            {_t(text3)}
                        </Text>
                    </div>

                    <ContentWidth
                        size={300}
                        className={classNameContentWidthBtn}
                    >
                        <ButtonPay
                            price={price}
                            logoIcon={logoIcon}
                            subscribe={recurrence}
                            period={period}
                            classNameContentButton={`
                            bg-americanSilver
                            p-v-10
                            border-radius-10
                            width-p-100
                            height-p-100
                            `}
                        />
                    </ContentWidth>
                    <Space size={30} />
                </ContentWidth>
            </div>
        </>
    );
};
export default ProfileAndSubscriptionsBase;
