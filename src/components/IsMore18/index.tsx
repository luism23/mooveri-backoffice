import { useMemo } from 'react';

import * as styles from '@/components/IsMore18/styles';

import { Theme, ThemesType } from '@/config/theme';

import { IsMore18BaseProps, IsMore18Base } from '@/components/IsMore18/Base';

export const IsMore18Style = { ...styles } as const;

export type IsMore18Styles = keyof typeof IsMore18Style;

export interface IsMore18Props extends IsMore18BaseProps {
    styleTemplate?: IsMore18Styles | ThemesType;
}

export const IsMore18 = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: IsMore18Props) => {
    const Style = useMemo(
        () =>
            IsMore18Style[styleTemplate as IsMore18Styles] ??
            IsMore18Style._default,
        [styleTemplate]
    );

    return <IsMore18Base {...Style} {...props} />;
};
export default IsMore18;
