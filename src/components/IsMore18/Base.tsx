import { ThemesType, Theme } from '@/config/theme';
import url from '@/data/routes';
import { useLang } from '@/lang/translate';
import { EyeBar } from '@/svg/eye';
import { PropsWithChildren, useMemo, useState } from 'react';
import { Button, ButtonStyles } from '../Button';
import { ContentWidth, ContentWidthProps } from '../ContentWidth';
import { Link, LinkStyles } from '../Link';

export interface IsMore18ClassProps {
    ContentWidthProps?: ContentWidthProps;
    classNameTitle?: string;
    classNameText?: string;

    styleTemplateButton?: ButtonStyles | ThemesType;
    styleTemplateLink?: LinkStyles | ThemesType;
}

export interface IsMore18BaseProps extends PropsWithChildren {
    validate?: boolean;
}

export interface IsMore18Props extends IsMore18ClassProps, IsMore18BaseProps {}

export const IsMore18Base = ({
    ContentWidthProps = {},
    classNameTitle = '',
    classNameText = '',
    styleTemplateButton = Theme?.styleTemplate ?? '_default',
    styleTemplateLink = Theme?.styleTemplate ?? '_default',

    children,
    validate = true,
}: IsMore18Props) => {
    const _t = useLang();
    const [isMore18, setIsMore18] = useState(false);

    const CONTENT = useMemo(() => {
        if (isMore18 || !validate) {
            return children;
        }
        return (
            <>
                <ContentWidth {...ContentWidthProps}>
                    <EyeBar size={40} />
                    <div className={`${classNameTitle}`}>
                        {_t('Sensitive Content')}
                    </div>
                    <div className={`${classNameText}`}>
                        {_t(
                            'Confirm that you are of legal age to view this content.'
                        )}
                    </div>
                    <Button
                        onClick={() => {
                            setIsMore18(true);
                        }}
                        styleTemplate={styleTemplateButton}
                    >
                        {_t("I'm 18 or older")}
                    </Button>
                    <Link href={url.home} styleTemplate={styleTemplateLink}>
                        {_t("I'm under 18")}
                    </Link>
                </ContentWidth>
            </>
        );
    }, [isMore18]);

    return <>{CONTENT}</>;
};
export default IsMore18Base;
