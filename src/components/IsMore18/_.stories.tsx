import { Story, Meta } from "@storybook/react";

import { IsMore18Props, IsMore18 } from "./index";

export default {
    title: "IsMore18/IsMore18",
    component: IsMore18,
} as Meta;

const IsMore18Index: Story<IsMore18Props> = (args) => (
    <IsMore18 {...args}>Test Children</IsMore18>
);

export const Index = IsMore18Index.bind({});
Index.args = {};
