# IconNumberText

## Import

```js
import {
    IconNumberText,
    IconNumberTextStyles,
} from '@/components/IconNumberText';
```

## Props

```ts
interface IconNumberTextProps {
    styleTemplate?: IconNumberTextStyles;
    icon?: any;
    number: number;
    caraterNumber?: string;
    text: string;
}
```

## Use

```js
<IconNumberText number={10} caraterNumber="%" text="Percentage" />
```
