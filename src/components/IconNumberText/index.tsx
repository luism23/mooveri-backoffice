import { useMemo } from 'react';

import * as styles from '@/components/IconNumberText/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    IconNumberTextBaseProps,
    IconNumberTextBase,
} from '@/components/IconNumberText/Base';

export const IconNumberTextStyle = { ...styles } as const;

export type IconNumberTextStyles = keyof typeof IconNumberTextStyle;

export interface IconNumberTextProps extends IconNumberTextBaseProps {
    styleTemplate?: IconNumberTextStyles | ThemesType;
}

export const IconNumberText = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: IconNumberTextProps) => {
    const Style = useMemo(
        () =>
            IconNumberTextStyle[styleTemplate as IconNumberTextStyles] ??
            IconNumberTextStyle._default,
        [styleTemplate]
    );

    return <IconNumberTextBase {...Style} {...props} />;
};
export default IconNumberText;
