import { Status } from '@/interfaces/Status';
import { useLang } from '@/lang/translate';

export interface StatusClassProps {
    classNameContent?: string;
    classNameStatus?: {
        [id in Status]: string;
    };
    iconStatus?: {
        [id in Status]: any;
    };
}

export interface StatusBaseProps {
    text?: string;
    status?: Status;
}

export interface StatusProps extends StatusClassProps, StatusBaseProps {}

export const StatusBase = ({
    classNameContent = '',
    classNameStatus = {
        normal: '',
        warning: '',
        error: '',
        ok: '',
    },
    iconStatus = {
        normal: '',
        warning: '',
        error: '',
        ok: '',
    },

    text = '',
    status = 'normal',
}: StatusProps) => {
    const _t = useLang();

    return (
        <>
            <div className={`${classNameContent} ${classNameStatus[status]}`}>
                {_t(text)}
                {iconStatus[status]}
            </div>
        </>
    );
};
export default StatusBase;
