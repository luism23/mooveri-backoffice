import { useMemo } from 'react';

import * as styles from '@/components/Status/styles';

import { Theme, ThemesType } from '@/config/theme';

import { StatusBaseProps, StatusBase } from '@/components/Status/Base';

export const StatusStyle = { ...styles } as const;

export type StatusStyles = keyof typeof StatusStyle;

export interface StatusProps extends StatusBaseProps {
    styleTemplate?: StatusStyles | ThemesType;
}

export const Status = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: StatusProps) => {
    const Style = useMemo(
        () =>
            StatusStyle[styleTemplate as StatusStyles] ?? StatusStyle._default,
        [styleTemplate]
    );

    return <StatusBase {...Style} {...props} />;
};
export default Status;
