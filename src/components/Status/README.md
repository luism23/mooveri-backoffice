# Status

## Dependencies

[Status](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Status)

```js
import { Status } from '@/components/Status';
```

## Import

```js
import { Status, StatusStyles } from '@/components/Status';
```

## Props

```tsx
interface StatusProps {}
```

## Use

```js
<Status>Status</Status>
```
