import { Story, Meta } from "@storybook/react";

import { StatusProps, Status } from "./index";

export default {
    title: "Status/Status",
    component: Status,
} as Meta;

const StatusIndex: Story<StatusProps> = (args) => (
    <Status {...args}>Test Children</Status>
);

export const Normal = StatusIndex.bind({});
Normal.args = {
    status: "normal",
    text: "Message Status",
};

export const Ok = StatusIndex.bind({});
Ok.args = {
    status: "ok",
    text: "Message Status",
};
export const Warning = StatusIndex.bind({});
Warning.args = {
    status: "warning",
    text: "Message Status",
};
export const Error = StatusIndex.bind({});
Error.args = {
    status: "error",
    text: "Message Status",
};
