import { useMemo } from 'react';

import * as styles from '@/components/CurrentBalance/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    CurrentBalanceBaseProps,
    CurrentBalanceBase,
} from '@/components/CurrentBalance/Base';

export const CurrentBalanceStyle = { ...styles } as const;

export type CurrentBalanceStyles = keyof typeof CurrentBalanceStyle;

export interface CurrentBalanceProps extends CurrentBalanceBaseProps {
    styleCurrentBalance?: CurrentBalanceStyles | ThemesType;
}

export const CurrentBalance = ({
    styleCurrentBalance = Theme?.styleTemplate ?? '_default',
    ...props
}: CurrentBalanceProps) => {
    const Style = useMemo(
        () =>
            CurrentBalanceStyle[styleCurrentBalance as CurrentBalanceStyles] ??
            CurrentBalanceStyle._default,
        [styleCurrentBalance]
    );

    return <CurrentBalanceBase {...Style} {...props} />;
};
export default CurrentBalance;
