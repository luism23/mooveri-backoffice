import { RootClassProps } from '@/components/Root/Base';

import { createGlobalStyle } from 'styled-components';

export const mooveri: RootClassProps = {
    styledComponent: createGlobalStyle`
        :root{
            --ok:#3c6e71;
            --error:#fe8989;
            --container:50.375rem;
        }
        html {
            font-size: 18px;
            overflow: auto;
            font-family: nunito;
            @media (min-width: 575px) {
                font-size: 18px;
            }
            @media (min-width: 768px) {
                font-size: 18px;
            }
            @media (min-width: 992px) {
                font-size: 18px;
            }
            @media (min-width: 1024px) {
                font-size: 18px;
            }
            @media (min-width: 1440px) {
                font-size: 21px;
            }
            @media (min-width: 1680px) {
                font-size: 23px;
            }
            @media (min-width: 1920px) {
                font-size: 23px;
            }
        }
    `,
};

export const mooveriBackoffice: RootClassProps = mooveri;
