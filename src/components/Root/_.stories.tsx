import { Story, Meta } from "@storybook/react";

import { RootProps, Root } from "./index";

export default {
    title: "Root/Root",
    component: Root,
} as Meta;

const RootIndex: Story<RootProps> = (args) => (
    <Root {...args}>Test Children</Root>
);

export const Index = RootIndex.bind({});
Index.args = {};
