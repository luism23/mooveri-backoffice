import { useMemo } from 'react';

import * as styles from '@/components/Root/styles';

import { Theme, ThemesType } from '@/config/theme';

import { RootBaseProps, RootBase } from '@/components/Root/Base';

export const RootStyle = { ...styles } as const;

export type RootStyles = keyof typeof RootStyle;

export interface RootProps extends RootBaseProps {
    styleTemplate?: RootStyles | ThemesType;
}

export const Root = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: RootProps) => {
    const Style = useMemo(
        () => RootStyle[styleTemplate as RootStyles] ?? RootStyle._default,
        [styleTemplate]
    );

    return <RootBase {...Style} {...props} />;
};
export default Root;
