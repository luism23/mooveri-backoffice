import { PropsWithChildren } from 'react';
import CSS from 'csstype';

export interface RootClassProps {
    styledComponent?: any;
}

export interface RootBaseProps extends PropsWithChildren {
    className?: string;
    style?: CSS.Properties;
}

export interface RootProps extends RootClassProps, RootBaseProps {}

export const RootBase = ({ styledComponent = null, ...props }: RootProps) => {
    const Tag = styledComponent == null ? 'div' : styledComponent;
    return <Tag {...props} />;
};
export default RootBase;
