# Root

```js
import { Root } from '@/components/Root';
```

## Import

```js
import { Root, RootStyles } from '@/components/Root';
```

## Props

```tsx
interface RootProps {
    styleTemplate?: RootStyles;
}
```

## Use

```js
<Root>Root</Root>
```
