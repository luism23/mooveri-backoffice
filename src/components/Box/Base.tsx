import { PropsWithChildren } from 'react';
import ContentWidth from '@/components/ContentWidth';

export interface BoxClassProps {
    classNameContentBox?: string;
    sizeContentBox?: number;
}

export interface BoxBaseProps extends PropsWithChildren {
    className?: string;
}

export interface BoxProps extends BoxClassProps, BoxBaseProps {}

export const BoxBase = ({
    classNameContentBox = '',
    children,
    sizeContentBox = 240,

    className = '',
}: BoxProps) => {
    return (
        <>
            <ContentWidth
                size={sizeContentBox}
                className={`${className} ${classNameContentBox}`}
            >
                {children}
            </ContentWidth>
        </>
    );
};
export default BoxBase;
