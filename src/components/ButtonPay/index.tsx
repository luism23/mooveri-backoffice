import { useMemo } from 'react';

import * as styles from '@/components/ButtonPay/styles';

import { Theme, ThemesType } from '@/config/theme';

import { ButtonPayBaseProps, ButtonPayBase } from '@/components/ButtonPay/Base';

export const ButtonPayStyle = { ...styles } as const;

export type ButtonPayStyles = keyof typeof ButtonPayStyle;

export interface ButtonPayProps extends ButtonPayBaseProps {
    styleTemplate?: ButtonPayStyles | ThemesType;
}

export const ButtonPay = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ButtonPayProps) => {
    const Style = useMemo(
        () =>
            ButtonPayStyle[styleTemplate as ButtonPayStyles] ??
            ButtonPayStyle._default,
        [styleTemplate]
    );

    return <ButtonPayBase {...Style} {...props} />;
};
export default ButtonPay;
