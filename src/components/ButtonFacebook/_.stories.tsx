import { Story, Meta } from "@storybook/react";

import { ButtonFacebookProps, ButtonFacebook } from "./index";

export default {
    title: "Button/ButtonFacebook",
    component: ButtonFacebook,
} as Meta;

const ButtonFacebookIndex: Story<ButtonFacebookProps> = (args) => (
    <ButtonFacebook {...args}>Test Children</ButtonFacebook>
);

export const Index = ButtonFacebookIndex.bind({});
Index.args = {};
