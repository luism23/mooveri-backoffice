import { useMemo } from 'react';

import * as styles from '@/components/Testimonial/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    TestimonialBaseProps,
    TestimonialBase,
} from '@/components/Testimonial/Base';

export const TestimonialStyle = { ...styles } as const;

export type TestimonialStyles = keyof typeof TestimonialStyle;

export interface TestimonialProps extends TestimonialBaseProps {
    styleTemplate?: TestimonialStyles | ThemesType;
}

export const Testimonial = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: TestimonialProps) => {
    const Style = useMemo(
        () =>
            TestimonialStyle[styleTemplate as TestimonialStyles] ??
            TestimonialStyle._default,
        [styleTemplate]
    );

    return <TestimonialBase {...Style} {...props} />;
};
export default Testimonial;
