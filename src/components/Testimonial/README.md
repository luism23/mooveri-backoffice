# Testimonial

## Dependencies

[Testimonial](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Testimonial)

```js
import { Testimonial } from '@/components/Testimonial';
```

## Import

```js
import { Testimonial, TestimonialStyles } from '@/components/Testimonial';
```

## Props

```tsx
interface TestimonialProps {
    text: string;
    author: {
        name: string;
        position: string;
        img: string;
    };
}
```

## Use

```js
<Testimonial
    text={"sasad"}
    author={{
        name:"asa",
        position:"assa"
        img:"ssssd"
    }}
/>
```
