import { TestimonialClassProps } from '@/components/Testimonial/Base';

export const _default: TestimonialClassProps = {
    classNameContent: `
    bg-white
    p-23
    m-30
    border-radius-10
    box-shadow box-shadow-inset box-shadow-x-0 box-shadow-y-20 box-shadow-b-25 box-shadow-s-5 box-shadow-black-
    
    `,
    styleTemplateText: '_default2',
    styleTemplateAuthorName: '_default3',
    styleTemplateAuthorPosition: '_default4',

    classNameUsername: `
    d-md-flex
    p-t-20
    `,

    classNameImg: `
    width-sm-50
    border-radius-50
    m-l-auto
    `,
    classNameHr: `
        border-0
        bg-bright-gray
        height-1
    `,
};
