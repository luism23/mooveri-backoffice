import { useMemo, useState } from 'react';

export interface TabClassProps {
    classNameContent?: string;
    classNameContentTab?: string;
    classNameTab?: string;
    classNameTabActive?: string;
    classNameTabNotActive?: string;
    classNameTabBar?: string;
    classNameTabBarActive?: string;
    classNameTabBarNotActive?: string;
    classNameTabContent?: string;
}

export interface TabContentProps {
    tab: any;
    content: any;
}

export interface TabBaseProps {
    items?: TabContentProps[];
    defaultSelect?: number;
    extraTab?: any;
}

export interface TabProps extends TabClassProps, TabBaseProps {}

export const TabBase = ({
    classNameContent = '',
    classNameContentTab = '',
    classNameTab = '',
    classNameTabActive = '',
    classNameTabNotActive = '',
    classNameTabBar = '',
    classNameTabBarActive = '',
    classNameTabBarNotActive = '',
    classNameTabContent = '',

    defaultSelect = 0,
    items = [],
    extraTab = '',
}: TabProps) => {
    const [index, setIndex] = useState(defaultSelect);

    const TABS = useMemo(
        () =>
            items.map((item, i) => (
                <div
                    key={i}
                    className={`${classNameTab} ${
                        i == index ? classNameTabActive : classNameTabNotActive
                    }`}
                    onClick={() => {
                        setIndex(i);
                    }}
                >
                    {item.tab}
                    <span
                        className={`${classNameTabBar} ${
                            i == index
                                ? classNameTabBarActive
                                : classNameTabBarNotActive
                        }`}
                    ></span>
                </div>
            )),
        [items, index, classNameTab, classNameTabActive, classNameTabNotActive]
    );

    return (
        <div className={classNameContent}>
            <div className={classNameContentTab}>
                {TABS}
                {extraTab}
            </div>
            <div className={classNameTabContent}>{items[index].content}</div>
        </div>
    );
};
export default TabBase;
