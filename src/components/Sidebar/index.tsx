import { SidebarBase, SidebarBaseProps } from '@/components/Sidebar/Base';

import * as styles from '@/components/Sidebar/styles';
import { ThemesType } from '@/config/theme';

export const SidebarStyle = { ...styles } as const;

export type SidebarStyles = keyof typeof SidebarStyle;

export interface SidebarProps extends SidebarBaseProps {
    styleTemplate?: SidebarStyles | ThemesType;
}
export const Sidebar = ({
    styleTemplate = '_default',
    ...props
}: SidebarProps) => {
    const config = {
        ...props,
        ...(SidebarStyle[styleTemplate as SidebarStyles] ??
            SidebarStyle._default),
    };

    return <SidebarBase {...config} />;
};
export default Sidebar;
