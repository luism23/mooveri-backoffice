import { Story, Meta } from "@storybook/react";

import { SidebarProps, Sidebar } from "./index";

export default {
    title: "Sidebar/Sidebar",
    component: Sidebar,
} as Meta;

const Template: Story<SidebarProps> = (args) => (
    <Sidebar {...args}>Test Children</Sidebar>
);

export const Index = Template.bind({});
Index.args = {
    links: [
        {
            href: "#",
            text: "Test Link Sidebar",
        },
        {
            href: "#",
            text: "Test Link Sidebar",
        },
        {
            href: "#",
            text: "Test Link Sidebar",
        },
        {
            href: "#",
            text: "Test Link Sidebar",
        },
        {
            href: "#",
            text: "Test Link Sidebar",
        },
        {
            href: "#",
            text: "Test Link Sidebar",
        },
    ],
};
