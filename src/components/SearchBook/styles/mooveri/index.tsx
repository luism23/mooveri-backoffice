import { SearchBookClassProps } from '@/components/SearchBook/Base';

export const mooveri: SearchBookClassProps = {
    classNameContent: `
        flex
        flex-nowrap
        flex-lg-justify-center
        border-0
        border-l-1
        border-r-1
        border-style-solid
        border-black-16
        width-p-100
        bg-white
        flex-lg-row
        flex-column
        
    `,
    classNameContentResult: `
        flex-7
        pos-r
        border-0
        border-r-1
        border-style-solid
        border-black-16
        bg-white
        transition-10
        flex
        flex-column
        flex-nowrap
        transform
    `,
    classNameContentResultPre: `
        height-0
        height-max-0
        height-vh-min-0
        transform-scale-Y-0

        height-lg-auto
        height-lg-vh-max-100
        height-lg-vh-min-80
        transform-lg-scale-Y-10
    `,
    classNameContentResultPos: `
        height-vh-min-100
    `,
    classNameContentResultTop: `
        classNameContentResultTop
        top-0
        left-0
        width-p-100
        flex
        flex-nowrap
        flex-justify-between
        p-h-15
        p-v-10
        border-0
        border-b-1
        border-style-solid
        border-black-16
        flex-md-row
        flex-column
    `,
    classNameContentResultItems: `
        pos-r
        overflow-auto
        width-p-100
        height-p-100
        scroll-width-5
        scroll-color-transparent
        scroll-scrollbar-track-width-0
        scroll-scrollbar-track-color-transparent
        scroll-scrollbar-thumb-width-0
        scroll-scrollbar-thumb-color-sea
        scroll-scrollbar-thumb-border-radius-10
        height-min-50
    `,
    classNameContentResultNoItems: `
        pos-a
        inset-0
        m-auto
        flex
        flex-align-center
        flex-justify-center  
        flex-column
    `,
    classNameContentForm: `
        flex-5
        p-h-15
        p-v-15
        p-md-t-50
        bg-white
        transition-10
    `,
    classNameContentFormPre: `
        
    `,
    classNameContentFormPos: `
        
    `,

    styleTemplateGoBackStyles: 'mooveri',
    styleTemplateWeFoundForDateStyles: 'mooveri',
    styleTemplateFormSearchBookStyles: 'mooveri',
    styleTemplateNoItem: 'mooveri',
    styleTemplateTextPreLoader: 'mooveri',
};

export const mooveri2: SearchBookClassProps = {
    ...mooveri,
    classNameContent: `
        flex
        flex-nowrap
        flex-justify-center
        border-0
        width-p-100
        bg-white
        
    `,
    classNameContentPre: `
        
    `,
    classNameContentPos: `
    `,
    classNameContentResultPre: `
        transform
        transform-scale-X-5
        transform-scale-Y-5
        z-index-1
        border-0
        box-shadow
        box-shadow-c-black-16
        box-shadow-b-3
        border-radius-15
    `,
    classNameContentResultPos: `
        border-0
        box-shadow
        box-shadow-c-black-16
        box-shadow-b-3
    `,
    classNameContentFormPre: `
        transform
        transform-translate-X-p--60
        m-v-15
        z-index-2
        box-shadow
        box-shadow-c-black-16
        box-shadow-b-3
        border-radius-15
    `,
    classNameContentFormPos: `
        box-shadow
        box-shadow-c-black-16
        box-shadow-b-3
    `,
};

export const mooveri3: SearchBookClassProps = {
    ...mooveri,
    classNameContent: `
        flex
        flex-nowrap
        flex-justify-center
        border-0
        border-l-1
        border-r-1
        border-style-solid
        border-black-16
        width-p-50
        bg-white
        pos-r
        transform
        transition-10
    `,
    classNameContentPre: `
        transform-translate-X-p-50
        
    `,
    classNameContentPos: `
        transform-translate-X-p-0
    `,
    classNameContentResult: `
        flex-12
        pos-r
        p-t-50
        border-0
        border-r-1
        border-style-solid
        border-black-16
        bg-white
        transition-10
    `,
    classNameContentResultPre: `
    
    `,
    classNameContentResultPos: `
    
    `,
    classNameContentForm: `
        p-h-15
        p-b-15
        p-t-50
        bg-white
        transition-10
        pos-a
        width-p-100
        height-p-100
        inset-0
        opacity-9
        transform
        transition-10
        border-0
        border-l-1
        border-r-1
        border-style-solid
        border-black-16
    `,
    classNameContentFormPre: `
        transform-translate-X-p-0
    `,
    classNameContentFormPos: `
        transform-translate-X-p-100
    `,
};
