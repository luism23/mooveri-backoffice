import {
    FormSearchBook,
    FormSearchBookStyles,
} from '@/components/Form/SearchBook';
import { useLang } from '@/lang/translate';
import { useMemo } from 'react';
import { GoBack, GoBackStyles } from '@/components/GoBack';
import { LoaderCart, LoaderCartStop } from '@/components/Loader/LoaderCart';
import { Text, TextStyles } from '@/components/Text';
import {
    WeFoundForDate,
    WeFoundForDateStyles,
} from '@/components/WeFoundForDate';
import Theme, { ThemesType } from '@/config/theme';
import {
    DataTypeFormSearch,
    onSubmintSearchBook,
} from '@/components/Form/SearchBook/Base';
import NotItemSearch from '@/components/NotItem/Template/Search';
import { NotItemStyles } from '@/components/NotItem';
import { DataSearchBook } from '@/interfaces/SearchBook';
import {
    ItemScanProps,
    ScanItemScanProps,
    ScanItemSocketProps,
} from '@/interfaces/Scan/ItemScan';

export interface SearchBookClassProps {
    classNameContent?: string;
    classNameContentPre?: string;
    classNameContentPos?: string;

    classNameContentResult?: string;
    classNameContentResultPre?: string;
    classNameContentResultPos?: string;

    classNameContentResultTop?: string;
    classNameContentResultItems?: string;
    classNameContentResultNoItems?: string;

    classNameContentForm?: string;
    classNameContentFormPre?: string;
    classNameContentFormPos?: string;

    styleTemplateGoBackStyles?: GoBackStyles | ThemesType;
    styleTemplateWeFoundForDateStyles?: WeFoundForDateStyles | ThemesType;
    styleTemplateFormSearchBookStyles?: FormSearchBookStyles | ThemesType;
    styleTemplateNoItem?: NotItemStyles | ThemesType;
    styleTemplateTextPreLoader?: TextStyles | ThemesType;
}

export interface SearchBookBaseProps {
    pre?: boolean;
    loader?: boolean;
    companies?: [];

    onCotizar?: onSubmintSearchBook;
    onProssesItemScan?: ScanItemScanProps;
    data?: DataSearchBook<DataTypeFormSearch>;

    onSocketSend?: ScanItemSocketProps;
    socketData?: ItemScanProps[];
}

export interface SearchBookProps
    extends SearchBookClassProps,
        SearchBookBaseProps {}

export const SearchBookBase = ({
    classNameContent = '',
    classNameContentPre = '',
    classNameContentPos = '',

    classNameContentResult = '',
    classNameContentResultPre = '',
    classNameContentResultPos = '',

    classNameContentResultTop = '',

    classNameContentResultItems = '',
    classNameContentResultNoItems = '',

    classNameContentForm = '',
    classNameContentFormPre = '',
    classNameContentFormPos = '',

    styleTemplateGoBackStyles = Theme.styleTemplate ?? '_default',
    styleTemplateWeFoundForDateStyles = Theme.styleTemplate ?? '_default',
    styleTemplateFormSearchBookStyles = Theme.styleTemplate ?? '_default',
    styleTemplateNoItem = Theme.styleTemplate ?? '_default',
    styleTemplateTextPreLoader = Theme.styleTemplate ?? '_default',

    pre = true,
    companies = [],
    loader = false,

    data = {},

    ...props
}: SearchBookProps) => {
    const _t = useLang();
    const content = useMemo(() => {
        if (pre) {
            return (
                <>
                    <div className={classNameContentResultNoItems}>
                        <LoaderCartStop />
                        <Text styleTemplate={styleTemplateTextPreLoader}>
                            {_t('Esperando Cotizacion')}
                        </Text>
                    </div>
                </>
            );
        }
        if (loader) {
            return (
                <>
                    <div className={classNameContentResultNoItems}>
                        <LoaderCart />
                        <Text styleTemplate={styleTemplateTextPreLoader}>
                            {_t('Loading Moving Companies')}
                        </Text>
                    </div>
                </>
            );
        }
        if (companies.length == 0) {
            return (
                <>
                    <div className={classNameContentResultNoItems}>
                        <NotItemSearch styleTemplate={styleTemplateNoItem} />
                    </div>
                </>
            );
        }
        return <></>;
    }, [pre, loader, companies]);

    return (
        <>
            <div
                className={`${classNameContent} ${
                    pre ? classNameContentPre : classNameContentPos
                }`}
            >
                <div
                    className={`${classNameContentResult} ${
                        pre
                            ? classNameContentResultPre
                            : classNameContentResultPos
                    }`}
                >
                    <div className={classNameContentResultTop}>
                        <GoBack
                            text="Back to Search"
                            styleTemplate={styleTemplateGoBackStyles}
                        />
                        <WeFoundForDate
                            results={companies.length}
                            styleTemplate={styleTemplateWeFoundForDateStyles}
                            date={(data?.date as Date) ?? new Date()}
                        />
                    </div>
                    <div className={classNameContentResultItems}>{content}</div>
                </div>
                <div
                    className={`${classNameContentForm} ${
                        pre ? classNameContentFormPre : classNameContentFormPos
                    }`}
                >
                    <FormSearchBook
                        styleTemplate={styleTemplateFormSearchBookStyles}
                        onSubmit={props?.onCotizar}
                        onProssesItemScan={props?.onProssesItemScan}
                        onSocketSend={props?.onSocketSend}
                        socketData={props?.socketData}
                    />
                </div>
            </div>
        </>
    );
};
export default SearchBookBase;
