import { Story, Meta } from "@storybook/react";

import { SearchBookProps, SearchBook } from "./index";

export default {
    title: "SearchBook/SearchBook",
    component: SearchBook,
} as Meta;

const SearchBookIndex: Story<SearchBookProps> = (args) => (
    <SearchBook {...args}>Test Children</SearchBook>
);

export const Index = SearchBookIndex.bind({});
Index.args = {};
