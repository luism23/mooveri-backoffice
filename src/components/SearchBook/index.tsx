import { useMemo } from 'react';

import * as styles from '@/components/SearchBook/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    SearchBookBaseProps,
    SearchBookBase,
} from '@/components/SearchBook/Base';

export const SearchBookStyle = { ...styles } as const;

export type SearchBookStyles = keyof typeof SearchBookStyle;

export interface SearchBookProps extends SearchBookBaseProps {
    styleTemplate?: SearchBookStyles | ThemesType;
}

export const SearchBook = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: SearchBookProps) => {
    const Style = useMemo(
        () =>
            SearchBookStyle[styleTemplate as SearchBookStyles] ??
            SearchBookStyle._default,
        [styleTemplate]
    );

    return <SearchBookBase {...Style} {...props} />;
};
export default SearchBook;
