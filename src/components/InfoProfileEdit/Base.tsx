import CSS from 'csstype';

import { InputAvatar, InputAvatarStyles } from '@/components/Input/Avatar';
import { InputText, InputTextStyles } from '@/components/Input/Text';
import { ContentWidth } from '@/components/ContentWidth';
import { Text, TextStyles } from '@/components/Text';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { useData } from 'fenextjs-hook/cjs/useData';
import { useEffect } from 'react';
import {
    ConfigButtonStyle,
    ConfigButtonStyleStyles,
} from '../ConfigButtonStyle';
import {
    ButtonAnimationsConfig,
    ButtonConfig,
    ButtonPrincipalConfig,
} from '@/interfaces/Button';
import ConfigBackground from '../ConfigBackground';
import { ConfigBackground as TypeBackground } from '@/interfaces/ConfigBackground';
import ConfigImgProfile from '../ConfigImgProfile';
import { ConfigImgProfile as TypeImgProfile } from '@/interfaces/ConfigImgProfile';
import { ConfigInfoStyle, ConfigInfoStyleStyles } from '../ConfigInfoStyle';
import { InfoConfig } from '@/interfaces/Info';
import {
    ConfigButtonPrincipalStyle,
    ConfigButtonPrincipalStyleStyles,
} from '../ConfigButtonPrincipalStyle';
import { RSLinkConfigDataProps } from '../RS/LinkConfig/Base';
import {
    ConfigButtonAnimations,
    ConfigButtonAnimationsStyles,
} from '../ConfigButtonAnimations';

export type buttonRoundType = 'no-rounding' | 'semi-rounded' | 'rounded';

export type buttonHeightType = 'slim' | 'regular' | 'strong';

export interface InfoProfileEditDataProps {
    avatar?: string;
    name: string;
    username_title?: string;
    web: string;
    description: string;
    //TODO: eleminar
    bgDesktop?: string;
    bgMovil?: string;
    capaColor?: string;
    btnBgColor?: string;
    btnColor?: string;
    btnBorderColor?: string;
    buttonHeight?: buttonHeightType;
    fontColor?: string;
    tolinkmeLogo?: boolean;
    buttonRound?: buttonRoundType;
    showIconButton?: boolean;
    btn?: ButtonConfig;
    btnPrincipal?: ButtonPrincipalConfig;
    info?: InfoConfig;
    bg?: TypeBackground;
    img?: TypeImgProfile;
    btnAnimation?: ButtonAnimationsConfig;
}
export interface InfoProfileEditBaseProps {
    onChange?: (data: InfoProfileEditDataProps) => void;
    defaultValue?: InfoProfileEditDataProps;
    links?: RSLinkConfigDataProps[];
}

export interface InfoProfileEditClassProps {
    classNameContent?: string;
    sizeContent?: number;

    classNameContentAvatarInfo?: string;
    classNameContentAvatar?: string;
    classNameContentInfo?: string;

    styleTemplateAvatar?: InputAvatarStyles | ThemesType;
    styleTemplateTextAvatar?: TextStyles | ThemesType;
    classNameTextAvatar?: string;

    classNameContentInputText?: string;
    styleTemplateInputText?: InputTextStyles | ThemesType;

    classNameContentMoreInfo?: string;

    styleTemplateConfigButtonStyle?: ConfigButtonStyleStyles | ThemesType;
    styleTemplateConfigButtonPrincipalStyle?:
        | ConfigButtonPrincipalStyleStyles
        | ThemesType;

    styleTemplateConfigInfoStyle?: ConfigInfoStyleStyles | ThemesType;
    styleTemplateConfigButtonAnimationsStyles?:
        | ConfigButtonAnimationsStyles
        | ThemesType;

    classNameContentCheckboxImg?: string;
    styleContentCheckboxImg?: CSS.Properties;

    classNameMaxLength?: string;
}

export interface InfoProfileEditProps
    extends InfoProfileEditBaseProps,
        InfoProfileEditClassProps {}

export const InfoProfileEditBase = ({
    classNameContent = '',
    sizeContent,

    classNameContentAvatarInfo = '',
    classNameContentAvatar = '',
    classNameContentInfo = '',

    styleTemplateAvatar = Theme?.styleTemplate ?? '_default',
    styleTemplateTextAvatar = Theme?.styleTemplate ?? '_default',
    classNameTextAvatar = '',

    classNameContentInputText = '',
    styleTemplateInputText = Theme?.styleTemplate ?? '_default',

    classNameContentMoreInfo = '',

    styleTemplateConfigButtonStyle = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigButtonPrincipalStyle = Theme?.styleTemplate ??
        '_default',

    styleTemplateConfigInfoStyle = Theme?.styleTemplate ?? '_default',
    styleTemplateConfigButtonAnimationsStyles = Theme?.styleTemplate ??
        '_default',

    classNameMaxLength = '',

    onChange = () => {},
    defaultValue = {
        avatar: '',
        name: '',
        username_title: undefined,
        web: '',
        description: '',
        bgDesktop: '',
        bgMovil: '',
        capaColor: '#00000000',
        fontColor: '#ffffff',
        btnBgColor: '#00000000',
        btnColor: '#00000000',
        buttonHeight: 'regular',
        buttonRound: 'semi-rounded',
        tolinkmeLogo: true,
        showIconButton: true,
        btnBorderColor: '#00000000',
    },
    links = [],
}: InfoProfileEditProps) => {
    const _t = useLang();
    const BioMaxLength = 150;
    const { data, onChangeData } =
        useData<InfoProfileEditDataProps>(defaultValue);
    useEffect(() => {
        onChange(data);
    }, [data]);

    return (
        <>
            <ContentWidth className={classNameContent} size={sizeContent}>
                <div className={classNameContentAvatarInfo}>
                    <div
                        className={`classNamecontentAvatar ${classNameContentAvatar}`}
                    >
                        <InputAvatar
                            accept={[
                                'png',
                                'PNG',
                                'jpg',
                                'jpeg',
                                'gif',
                                'webp',
                                'JPEG',
                                'JPG',
                                'WEBP',
                                'JPEG',
                                'GIF',
                            ]}
                            styleTemplate={styleTemplateAvatar}
                            defaultValue={{
                                fileData: data.avatar ?? '',
                                text: 'avatar',
                            }}
                            onSubmit={async (e) => {
                                onChangeData('avatar')(e?.fileData);
                                return {
                                    status: 'ok',
                                };
                            }}
                        />
                        <Text
                            styleTemplate={styleTemplateTextAvatar}
                            className={classNameTextAvatar}
                        >
                            {_t('**(Max Upload File Size 5MB , JPG, PNG).')}
                        </Text>
                        <Text
                            styleTemplate={styleTemplateTextAvatar}
                            className={classNameTextAvatar}
                        >
                            {_t('**Recommended Size 400px 400px.')}
                        </Text>
                    </div>
                    <div className={classNameContentInfo}>
                        <div className={classNameContentInputText}>
                            <InputText
                                label={_t('Profile Title')}
                                placeholder={_t('Profile Title')}
                                defaultValue={data.username_title ?? data.name}
                                onChange={onChangeData('username_title')}
                                styleTemplate={styleTemplateInputText}
                            />
                        </div>
                        <div className={classNameContentInputText}>
                            <InputText
                                label={_t('Profile Url')}
                                placeholder={_t('Profile Url')}
                                defaultValue={data.name}
                                onChange={onChangeData('name')}
                                styleTemplate={styleTemplateInputText}
                            />
                        </div>
                        <div className={classNameContentInputText}>
                            <InputText
                                type="textarea"
                                label={_t('Bio')}
                                placeholder={_t(
                                    'Add something describe your content'
                                )}
                                defaultValue={data.description}
                                onChange={onChangeData('description')}
                                styleTemplate={styleTemplateInputText}
                                props={{
                                    maxLength: BioMaxLength,
                                    style: {
                                        height: '6rem',
                                    },
                                }}
                            />
                            <div className={classNameMaxLength}>
                                {BioMaxLength - data.description.length}
                            </div>
                        </div>
                        <div className={classNameContentInputText}>
                            <InputText
                                label={_t('Website')}
                                placeholder={_t('Website')}
                                defaultValue={data.web}
                                onChange={onChangeData('web')}
                                styleTemplate={styleTemplateInputText}
                            />
                        </div>
                    </div>
                </div>
                <div className={classNameContentMoreInfo}>
                    <ConfigImgProfile
                        styleTemplate={styleTemplateConfigButtonStyle}
                        onChange={onChangeData('img')}
                        defaultValue={data.img}
                        img={data.avatar}
                    />
                    <ConfigInfoStyle
                        styleTemplate={styleTemplateConfigInfoStyle}
                        onChange={onChangeData('info')}
                        defaultValue={data.info}
                        infoData={{
                            name: data.name,
                            description: data.description,
                            web: data.web,
                        }}
                    />
                    <ConfigBackground
                        styleTemplate={styleTemplateConfigButtonStyle}
                        onChange={onChangeData('bg')}
                        defaultValue={data.bg}
                    />
                    <ConfigButtonStyle
                        styleTemplate={styleTemplateConfigButtonStyle}
                        onChange={onChangeData('btn')}
                        defaultValue={data.btn}
                        links={links}
                    />
                    <ConfigButtonPrincipalStyle
                        styleTemplate={styleTemplateConfigButtonPrincipalStyle}
                        onChange={onChangeData('btnPrincipal')}
                        defaultValue={data.btnPrincipal}
                    />
                    <ConfigButtonAnimations
                        styleTemplate={
                            styleTemplateConfigButtonAnimationsStyles
                        }
                        onChange={onChangeData('btnAnimation')}
                        defaultValue={data.btnAnimation}
                        links={links}
                        btnConfig={data.btn}
                    />
                </div>
            </ContentWidth>
        </>
    );
};
export default InfoProfileEditBase;
