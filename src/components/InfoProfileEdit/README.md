# InfoProfileEdit

## Import

```js
import {
    InfoProfileEdit,
    InfoProfileEditStyles,
} from '@/components/InfoProfileEdit';
```

## Props

```ts
export interface InfoProfileEditDataProps {
    avatar?: string;
    name: string;
    web: string;
    description: string;
    bgDesktop?: string;
    bgMovil?: string;
    capaColor?: string;
    fontColor?: string;
    tolinkmeLogo?: boolean;
}
export interface InfoProfileEditProps extends InfoProfileEditDataProps {
    styleTemplate?: InfoProfileEditStyles;
    onChange?: (data: InfoProfileEditDataProps) => void;
}
```

## Use

```js
<InfoProfileEdit name="Name" web="Web" description="Description" />
```
