import { PropsWithChildren } from 'react';
import InfoProfileEditBase, {
    InfoProfileEditBaseProps,
} from '@/components/InfoProfileEdit/Base';
import * as styles from '@/components/InfoProfileEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InfoProfileEditStyle = { ...styles } as const;

export type InfoProfileEditStyles = keyof typeof InfoProfileEditStyle;

export interface InfoProfileEditProps extends InfoProfileEditBaseProps {
    styleTemplate?: InfoProfileEditStyles | ThemesType;
}

export const InfoProfileEdit = ({
    styleTemplate = Theme.styleTemplate ?? '_default',
    ...props
}: PropsWithChildren<InfoProfileEditProps>) => {
    const config = {
        ...(InfoProfileEditStyle[styleTemplate as InfoProfileEditStyles] ??
            InfoProfileEditStyle._default),
        ...props,
    };

    return <InfoProfileEditBase {...config} />;
};
export default InfoProfileEdit;
