import { Story, Meta } from "@storybook/react";

import { InfoProfileEditProps, InfoProfileEdit } from "./index";

export default {
    title: "Info/InfoProfileEdit",
    component: InfoProfileEdit,
} as Meta;

const Template: Story<InfoProfileEditProps> = (args) => (
    <InfoProfileEdit {...args}>Test Children</InfoProfileEdit>
);

export const Index = Template.bind({});
Index.args = {
    
};
