import { Popup, PopupProps } from '@/components/Popup';
import { useMemo, useState } from 'react';
import { InputRadioStyles } from '../Radio';
import { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';
import { InputSearchProps } from '../Search';
import Button, { ButtonStyles } from '@/components/Button';
import ContentWidth, { ContentWidthProps } from '@/components/ContentWidth';
import FormMonetize from '@/components/Form/Monetize';
import { DataFormMonetize } from '@/interfaces/FormMonetize';
import { MonetizeRecurrent } from '@/data/components/Monetize';
import { useLang } from '@/lang/translate';
import { numberCountString } from '@/functions/numberCount';
import Modal from '@/components/Modal';
import Text from '@/components/Text';

export interface PopupMonetizeClassProps {
    classNameContent?: string;
    classNameSelect?: string;
    PopupProps?: PopupProps;
    styleTemplateInputRadio?: InputRadioStyles | ThemesType;
    classNameItemRs?: string;
    classNameItemRsActive?: string;
    classNameItemRsNoActive?: string;
    classNameGoBack?: string;
    classNameContentSearch?: string;
    SearchProps?: InputSearchProps;
    ContentBtnProps?: ContentWidthProps;
    classNameContentBtn?: string;
    classNameContentBtnActive?: string;
    classNameContentBtnInactive?: string;
    styleTemplateButton?: ButtonStyles | ThemesType;
    isCreate?: boolean;
}

export interface PopupMonetizeBaseProps {
    defaultValue?: DataFormMonetize;
    onChange?: (data: DataFormMonetize | undefined) => void;
}

export interface PopupMonetizeProps
    extends PopupMonetizeClassProps,
        PopupMonetizeBaseProps {}

export const PopupMonetizeBase = ({
    classNameContent = '',
    classNameSelect = '',
    PopupProps = {},
    classNameGoBack = '',
    defaultValue,
    onChange,
}: PopupMonetizeProps) => {
    const _t = useLang();
    const [activePopup, setActivePopup_] = useState(false);
    const [showConfirmationModal, setShowConfirmationModal] = useState(false);
    const { data, setData } = useData<DataFormMonetize | undefined>(
        defaultValue,
        {
            onChangeDataAfter: onChange,
        }
    );

    const setActivePopup = (a: boolean) => {
        if (!a) {
            if (data && defaultValue?.amount === undefined) {
                setShowConfirmationModal(true);
            }
        }
        setActivePopup_(a);
    };

    const textBtn = useMemo(() => {
        if (data?.amount && data?.type) {
            if (data.recurrent === MonetizeRecurrent[1].label) {
                return `$${numberCountString(data.amount)} - ${_t(data.type)}`;
            } else {
                return `$${numberCountString(data.amount)}/${_t(
                    data?.period ?? ''
                )} - ${_t(data.type)}`;
            }
        }

        return 'Monetize';
    }, [data, _t]);

    const handleConfirmationModalCancel = () => {
        setShowConfirmationModal(false);
        setActivePopup(true);
    };

    const handleConfirmationModalConfirm = () => {
        setShowConfirmationModal(false);
        setData(defaultValue);
    };

    return (
        <>
            <div className={classNameContent}>
                <Popup
                    {...PopupProps}
                    btnSwActive={activePopup}
                    setBtnSwActive={setActivePopup}
                    btn={<div className={`${classNameSelect} `}>{textBtn}</div>}
                    goBackProps={{
                        text: `${_t('Back')}`,
                        className: classNameGoBack,
                    }}
                >
                    <ContentWidth size={900}>
                        <FormMonetize defaultValue={data} onChange={setData} />
                    </ContentWidth>
                </Popup>
            </div>
            {showConfirmationModal && data && (
                <Modal
                    styleTemplate="tolinkme2"
                    onActive={showConfirmationModal}
                    onClose={handleConfirmationModalCancel}
                >
                    <Text
                        className="text-center text-math-auto"
                        styleTemplate="tolinkme11"
                    >
                        {_t('Are you sure of the data to be monetized?')}
                    </Text>
                    <Text
                        className="text-center  text-math-auto m-t-5"
                        styleTemplate="tolinkme24"
                    >
                        {_t(
                            'If you publish these changes, you will only be able to modify the description of this monetization afterwards.'
                        )}
                    </Text>
                    <div className="flex flex-justify-center m-t-10">
                        <Button
                            styleTemplate="tolinkmeSave"
                            onClick={handleConfirmationModalConfirm}
                        >
                            {_t('Continue')}
                        </Button>
                        <div className="m-h-10"></div>
                        <Button
                            styleTemplate="tolinkmeCancel2"
                            onClick={handleConfirmationModalCancel}
                        >
                            {_t('Cancel')}
                        </Button>
                    </div>
                </Modal>
            )}
        </>
    );
};
export default PopupMonetizeBase;
