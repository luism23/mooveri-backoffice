import { useMemo } from 'react';

import * as styles from '@/components/Input/PopupMonetize/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    PopupMonetizeBaseProps,
    PopupMonetizeBase,
} from '@/components/Input/PopupMonetize/Base';

export const PopupMonetizeStyle = { ...styles } as const;

export type PopupMonetizeStyles = keyof typeof PopupMonetizeStyle;

export interface PopupMonetizeProps extends PopupMonetizeBaseProps {
    styleTemplate?: PopupMonetizeStyles | ThemesType;
}

export const PopupMonetize = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PopupMonetizeProps) => {
    const Style = useMemo(
        () =>
            PopupMonetizeStyle[styleTemplate as PopupMonetizeStyles] ??
            PopupMonetizeStyle._default,
        [styleTemplate]
    );

    return <PopupMonetizeBase {...Style} {...props} />;
};
export default PopupMonetize;
