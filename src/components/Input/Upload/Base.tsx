import { useMemo, useState } from 'react';

import { InputFile, InputFileStyles } from '@/components/Input/File';
import { InputFileBaseProps } from '@/components/Input/File/Base';
import { InputFileDataProps } from '@/components/Input/File/Base';
import { SubmitResult } from '@/components/Form/Base';

import { useNotification } from '@/hook/useNotification';
import CSS from 'csstype';

import { NotificationStyles } from '@/components/Notification';

import { useLang } from '@/lang/translate';
import { Theme, ThemesType } from '@/config/theme';

export type InputUploadOnSubmit = (
    video: InputFileDataProps | undefined
) => Promise<SubmitResult> | SubmitResult | void;

export interface InputUploadClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;
    classNameContentImg?: string;
    styleContentImg?: CSS.Properties;
    classNameContentIcon?: string;
    icon?: any;
    classNameRemove?: string;
    classNameRemoveMovil?: string;
    classNameRemoveDesktop?: string;
    classNameRemoveTop?: string;
    classNameRemoveDown?: string;
    classNameRemoveTopActive?: string;
    classNameRemoveDownActive?: string;
    classNameRemoveTopNotActive?: string;
    classNameRemoveDownNotActive?: string;
    classNameNameFile?: string;
    iconDelete?: any;
    iconChange?: any;
    classNameLoader?: string;
    iconLoader?: any;
    styleTemplateNotification?: NotificationStyles;
    styleTemplateFile?: InputFileStyles | ThemesType;
}

export interface InputUploadBaseProps
    extends Omit<InputFileBaseProps, 'defaultValue'> {
    defaultValue?: InputFileDataProps;
    onSubmit?: InputUploadOnSubmit;
    textUpload?: string;
}

export interface InputUploadProps
    extends InputUploadBaseProps,
        InputUploadClassProps {}

export const InputUploadBase = ({
    defaultValue,
    classNameContent = '',
    styleContent = {},
    classNameContentImg = '',
    styleContentImg = {},
    classNameContentIcon = '',
    icon = '',
    classNameRemove = '',
    classNameRemoveTop = '',
    classNameRemoveDown = '',
    classNameRemoveTopActive = '',
    classNameRemoveDownActive = '',
    classNameRemoveTopNotActive = '',
    classNameRemoveDownNotActive = '',
    classNameNameFile = '',
    iconChange = <></>,
    iconDelete = <></>,
    classNameLoader = '',
    iconLoader = <></>,
    accept = ['png'],
    textUpload = 'Upload',
    styleTemplateFile = Theme.styleTemplate ?? '_default',
    ...props
}: InputUploadProps) => {
    const { pop } = useNotification();
    const _t = useLang();
    const [showChangeDelete, setShowChangeDelete] = useState(false);
    const [loader, setLoader] = useState(false);
    const [video, setUpload] = useState<InputFileDataProps | undefined>(
        defaultValue
    );

    const onChangeUpload = async (video: InputFileDataProps | undefined) => {
        setLoader?.(true);
        try {
            const result = await props?.onSubmit?.(video);
            pop({
                type: 'ok',
                message: _t(result?.message ?? ''),
            });
        } catch (error: any) {
            pop({
                type: 'error',
                message: _t(error?.message ?? error ?? ''),
            });
        }
        setLoader?.(false);
    };
    const Content = useMemo(() => {
        if (loader) {
            return (
                <>
                    <div className={classNameLoader}>{iconLoader}</div>
                </>
            );
        }
        if (video == undefined || video.fileData == '') {
            return (
                <>
                    <InputFile
                        {...props}
                        onChange={(e) => {
                            setUpload(e);
                            onChangeUpload(e);
                            setShowChangeDelete(false);
                        }}
                        accept={accept}
                        styleTemplate={styleTemplateFile}
                    >
                        <div className={`${classNameContentIcon}`}>
                            {icon}
                            {_t(textUpload)}
                        </div>
                    </InputFile>
                </>
            );
        }
        return (
            <>
                {video.fileData && video.fileData != '' && video.text != '' ? (
                    <div className={classNameNameFile}>{video.text}</div>
                ) : (
                    <>
                        <div
                            className={`${classNameContentIcon}`}
                            style={{ zIndex: -1 }}
                        >
                            {icon}
                            {_t(textUpload)}
                        </div>
                    </>
                )}
                <div
                    className={`${classNameRemove} `}
                    onMouseLeave={() => {
                        setShowChangeDelete(false);
                    }}
                    onMouseEnter={() => {
                        setShowChangeDelete(true);
                    }}
                    onClick={() => {
                        setShowChangeDelete(true);
                    }}
                >
                    <div
                        className={`${classNameRemoveTop} ${
                            showChangeDelete
                                ? classNameRemoveTopActive
                                : classNameRemoveTopNotActive
                        }`}
                        onClick={() => {
                            setShowChangeDelete(false);
                        }}
                    >
                        <InputFile
                            onChange={(e) => {
                                if (e.fileData != '') {
                                    setUpload(e);
                                    onChangeUpload(e);
                                }
                                // setShowChangeDelete(false);
                            }}
                            accept={accept}
                            styleTemplate={styleTemplateFile}
                        >
                            {iconChange}
                        </InputFile>
                    </div>
                    <div
                        className={`${classNameRemoveDown} ${
                            showChangeDelete
                                ? classNameRemoveDownActive
                                : classNameRemoveDownNotActive
                        }`}
                        onClick={() => {
                            setUpload(undefined);
                            onChangeUpload({
                                fileData: '',
                                text: '',
                            });
                            setShowChangeDelete(false);
                        }}
                    >
                        {iconDelete}
                    </div>
                </div>
            </>
        );
    }, [video, loader, showChangeDelete, textUpload]);

    return (
        <>
            <div className={classNameContent} style={styleContent}>
                <div className={classNameContentImg} style={styleContentImg}>
                    {Content}
                </div>
            </div>
        </>
    );
};
export default InputUploadBase;
