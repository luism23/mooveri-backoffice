import { useMemo } from 'react';
import {
    InputUploadBase,
    InputUploadBaseProps,
} from '@/components/Input/Upload/Base';

import * as styles from '@/components/Input/Upload/styles';
import { Theme, ThemesType } from '@/config/theme';

export const InputUploadStyle = { ...styles } as const;
export type InputUploadStyles = keyof typeof InputUploadStyle;

export interface InputUploadProps extends InputUploadBaseProps {
    styleTemplate?: InputUploadStyles | ThemesType;
}

export const InputUpload = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputUploadProps) => {
    const Style = useMemo(
        () =>
            InputUploadStyle[styleTemplate as InputUploadStyles] ??
            InputUploadStyle._default,
        [styleTemplate]
    );

    return (
        <>
            <InputUploadBase {...props} {...Style} />
        </>
    );
};
export default InputUpload;
