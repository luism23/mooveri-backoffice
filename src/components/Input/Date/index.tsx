import { useMemo, useState } from 'react';

import { InputText, InputTextProps } from '@/components/Input/Text';
import { TypeDate } from '@/interfaces/Date';
import { parseDateToText, parseTextToDate } from '@/functions/parseDate';

export interface InputDateProps
    extends Omit<
        InputTextProps,
        'type' | 'value' | 'onChange' | 'defaultValue'
    > {
    type?: TypeDate;
    defaultValue?: Date;
    min?: Date;
    max?: Date;
    value?: Date | null;
    onChange?: (v: Date) => void;
}

export const InputDate = ({
    type = 'date',
    defaultValue = new Date(),
    value = null,
    onChange,
    min = undefined,
    max = undefined,
    ...props
}: InputDateProps) => {
    const d = useMemo(
        () =>
            `${
                defaultValue
                    ? parseDateToText({ date: defaultValue, type })
                    : ''
            }`,
        [defaultValue]
    );
    const [valueString, setValueString] = useState(d);

    const changeInput = (e: any) => {
        const text = e.target.value;
        let nDate = parseTextToDate({
            text,
            type,
        });

        if (min) {
            if (nDate.getTime() < min.getTime()) {
                nDate = min;
            }
        }
        if (max) {
            if (nDate.getTime() > max.getTime()) {
                nDate = max;
            }
        }

        const nText = parseDateToText({ date: nDate, type });

        setValueString(nText);
        onChange?.(nDate);
    };

    return (
        <InputText
            {...props}
            defaultValue={''}
            value={value ? parseDateToText({ date: value, type }) : valueString}
            onChange={() => 1}
            extraInContentInput={
                <input
                    type={type}
                    onChange={changeInput}
                    className={`
                            input
                            input-date
                            pos-a
                            top-0
                            left-0
                            width-p-100
                            height-p-100
                            opacity-0
                        `}
                />
            }
        />
    );
};
export default InputDate;
