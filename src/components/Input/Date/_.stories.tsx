import { Story, Meta } from "@storybook/react";

import { InputDateProps, InputDate } from "./index";
import {InputDateMoveDay } from "./Template/InputDateMoveDay";

export default {
    title: "Input/InputDate",
    component: InputDate,
} as Meta;

const Template: Story<InputDateProps> = (args) => (
    <InputDate {...args}>Test Children</InputDate>
);

export const Date = Template.bind({});
Date.args = {
    label: "Label Test",
    type: "date",
};

export const Time = Template.bind({});
Time.args = {
    label: "Label Test",
    type: "time",
};

export const Month = Template.bind({});
Month.args = {
    label: "Label Test",
    type: "month",
};

export const Week = Template.bind({});
Week.args = {
    label: "Label Test",
    type: "week",
};


const InputDateMoveDayIndex: Story<InputDateProps> = (args) => (
    <InputDateMoveDay {...args}>Test Children</InputDateMoveDay>
);

export const Input_Date_Move_Day = InputDateMoveDayIndex.bind({});
Input_Date_Move_Day.args = {

};

