import { useEffect, useMemo, useState } from 'react';

import { InputText, InputTextProps } from '@/components/Input/Text';

import SVGArrowCollapse from '@/svg/arrowCollapse';

export interface InputNumberProps
    extends Omit<
        InputTextProps,
        'defaultValue' | 'onChange' | 'onChangeValidate'
    > {
    val?: number | '';
    defaultValue?: number | '';
    onChange: (v: number | '') => void;
    onChangeValidate?: (v: number | '') => void;
    min?: number;
    max?: number;
}

export const InputNumber = ({
    val = '',
    defaultValue = '',
    onChange,
    ...props
}: InputNumberProps) => {
    const [value, setValue] = useState<number | ''>(defaultValue ?? '');

    const valueInput = useMemo(
        () => (value == '' ? defaultValue : value),
        [value, defaultValue]
    );

    const addValue = (add: number) => () => {
        if (props?.disabled) {
            return;
        }
        let Value = (valueInput == '' ? 0 : valueInput) + add;
        if (props.min != undefined) {
            Value = Math.max(props.min, Value);
        }
        if (props.max != undefined) {
            Value = Math.min(props.max, Value);
        }
        onChange(Value);
        setValue(Value);
    };

    useEffect(() => {
        if (val != value) {
            setValue(val);
        }
    }, [val]);

    const propsNumber = {
        classNameContentInput: `
            pos-r
        `,
        icon: (
            <>
                <div className="flex">
                    <span
                        onClick={addValue(1)}
                        className={`
                            pos-a
                            right-10
                            top-8
                            m-auto
                            m-b-8
                            flex flex-align-center
                            pointer
                            z-index-3
                            color-black color-blue-hover
                        `}
                        style={{
                            transform: 'rotateZ(180deg)',
                        }}
                    >
                        <SVGArrowCollapse size={10} />
                    </span>
                    <span
                        onClick={addValue(-1)}
                        className={`
                            pos-a
                            bottom-8
                            right-10
                            m-auto
                            flex flex-align-center
                            pointer
                            z-index-3
                            color-black color-blue-hover
                        `}
                    >
                        <SVGArrowCollapse size={10} />
                    </span>
                </div>
            </>
        ),
        onChangeValidate: (v: string) => {
            v = `${v}`.replaceAll(/[^\d-]/g, '');
            let value: number | '' = v == '' ? v : parseFloat(v);

            if (props.min != undefined && value != '') {
                value = Math.max(props.min, value);
            }
            if (props.max != undefined && value != '') {
                value = Math.min(props.max, value);
            }

            return props?.onChangeValidate?.(value) ?? value;
        },
        onChange: (v: string) => {
            v = `${v}`.replaceAll(/[^\d-]/g, '');
            const value: number | '' = v == '' ? v : parseFloat(v);
            onChange(value);
            setValue(value);
        },
    };

    return (
        <>
            <InputText
                {...props}
                {...propsNumber}
                type="number"
                value={`${valueInput ?? ''}`}
                useLoader={false}
            />
        </>
    );
};
export default InputNumber;
