import { useState } from 'react';

import { InputText, InputTextProps } from '@/components/Input/Text';

import { Eye, EyeBar } from '@/svg/eye';

export type InputPasswordProps = InputTextProps;

export const InputPassword = (props: InputPasswordProps) => {
    const [type, setType] = useState<'text' | 'password'>('password');
    const toggleTypePassword = () => {
        setType(type == 'password' ? 'text' : 'password');
    };
    const propsPassword = {
        classNameContentInput: `
            pos-r
        `,
        icon: (
            <>
                <span
                    onClick={toggleTypePassword}
                    className={`
                    pos-a
                    top-0 
                    right-10
                    bottom-0
                    m-auto
                    flex flex-align-center
                    pointer
                    z-index-1
                `}
                >
                    {type == 'password' ? (
                        <Eye size={25} />
                    ) : (
                        <EyeBar size={25} />
                    )}
                </span>
            </>
        ),
    };
    return (
        <>
            <InputText {...props} {...propsPassword} type={type} />
        </>
    );
};
export default InputPassword;
