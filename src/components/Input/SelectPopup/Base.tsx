import { Popup, PopupProps } from '@/components/Popup';
import { SelectOptionProps } from '@/components/Input/Select';
import { useMemo, useState } from 'react';
import { InputRadio, InputRadioStyles } from '../Radio';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';
import Space from '@/components/Space';
import InputSearch, { InputSearchProps } from '../Search';
import { Button, ButtonStyles } from '@/components/Button';
import { useLang } from '@/lang/translate';
import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';

export interface SelectPopupClassProps {
    classNameContent?: string;
    classNameSelect?: string;
    classNameSelectNotSelected?: string;
    PopupProps?: PopupProps;
    styleTemplateInputRadio?: InputRadioStyles | ThemesType;

    classNameItemRs?: string;
    classNameItemRsActive?: string;
    classNameItemRsNoActive?: string;

    classNameGoBack?: string;

    classNameContentSearch?: string;
    SearchProps?: InputSearchProps;

    ContentBtnProps?: ContentWidthProps;
    classNameContentBtn?: string;
    classNameContentBtnActive?: string;
    classNameContentBtnInactive?: string;
    styleTemplateButton?: ButtonStyles | ThemesType;
}

export interface SelectPopupBaseProps {
    placeholder?: string;
    defaultValue?: SelectOptionProps;
    options?: SelectOptionProps[];
    onChange?: (data: SelectOptionProps) => void;
}

export interface SelectPopupProps
    extends SelectPopupClassProps,
        SelectPopupBaseProps {}

export const SelectPopupBase = ({
    classNameContent = '',
    classNameSelect = '',
    classNameSelectNotSelected = '',
    PopupProps = {},
    styleTemplateInputRadio = Theme.styleTemplate ?? '_default',

    classNameItemRs = '',
    classNameItemRsActive = '',
    classNameItemRsNoActive = '',

    classNameGoBack = '',

    classNameContentSearch = '',
    SearchProps = {},

    ContentBtnProps = {},
    classNameContentBtn = '',
    classNameContentBtnActive = '',
    classNameContentBtnInactive = '',
    styleTemplateButton = Theme.styleTemplate ?? '_default',

    placeholder = 'Select',
    defaultValue = {
        text: '',
        value: '',
    },
    options = [],
    ...props
}: SelectPopupProps) => {
    const _t = useLang();
    const DataSelect = useData<SelectOptionProps>(defaultValue, {
        onChangeDataAfter: props?.onChange,
    });
    const [indexOptionSelect, setIndexOptionSelect] = useState(
        options.findIndex((o) => o.value == DataSelect.data.value)
    );
    const [activePopup, setActivePopup] = useState(false);

    const { data, setData, isChange, setIsChange } =
        useData<SelectOptionProps>(defaultValue);
    const [searh, setSearh] = useState('');

    const optionsList = useMemo(
        () =>
            options.filter((option) =>
                option.text
                    .toLocaleLowerCase()
                    .includes(searh.toLocaleLowerCase())
            ),
        [searh, options]
    );

    const onSelectOption = () => {
        DataSelect.setData(data);
        setActivePopup(false);
        setIsChange(false);
    };

    const onCancel = () => {
        setData(DataSelect.data);
        setIndexOptionSelect(
            options.findIndex((o) => o.value == DataSelect.data.value)
        );
        setIsChange(false);
    };

    return (
        <>
            <div className={classNameContent}>
                <Popup
                    {...PopupProps}
                    btnSwActive={activePopup}
                    setBtnSwActive={setActivePopup}
                    onClose={onCancel}
                    btn={
                        <div
                            className={`${classNameSelect} ${
                                DataSelect?.data?.value == ''
                                    ? classNameSelectNotSelected
                                    : ''
                            }`}
                        >
                            {DataSelect?.data?.html ??
                            (DataSelect?.data?.text &&
                                DataSelect?.data?.text != '')
                                ? DataSelect?.data?.text
                                : placeholder}
                        </div>
                    }
                    goBackProps={{
                        text: 'Cancelar',
                        className: classNameGoBack,
                    }}
                >
                    <ContentWidth {...ContentBtnProps}>
                        <div
                            className={`${classNameContentBtn} ${
                                isChange
                                    ? classNameContentBtnActive
                                    : classNameContentBtnInactive
                            }`}
                        >
                            <Button
                                styleTemplate={styleTemplateButton}
                                onClick={onSelectOption}
                            >
                                {_t('Aplicar')}
                            </Button>
                        </div>
                    </ContentWidth>
                    <div className={classNameContentSearch}>
                        <InputSearch
                            {...SearchProps}
                            placeholder="Search Platform"
                            onChange={setSearh}
                            onEnter={() => {
                                if (optionsList[0]) {
                                    setData(optionsList[0]);
                                }
                            }}
                        />
                    </div>
                    <Space size={25} />
                    <InputRadio
                        keyChecked={indexOptionSelect}
                        onChange={(index) => {
                            setIndexOptionSelect(index);
                            setData(optionsList[index]);
                        }}
                        selectedRadioDefault={optionsList.findIndex(
                            (option) => option.value == data?.value ?? ''
                        )}
                        options={optionsList.map((option, i) => ({
                            label: (
                                <div
                                    key={i}
                                    className={`${classNameItemRs} ${
                                        data?.value == option.value
                                            ? classNameItemRsActive
                                            : classNameItemRsNoActive
                                    }`}
                                >
                                    {option.html ?? option.text ?? option.value}
                                </div>
                            ),
                            title: option?.title,
                        }))}
                        styleTemplate={styleTemplateInputRadio}
                    />

                    <Space size={50} />
                </Popup>
            </div>
        </>
    );
};
export default SelectPopupBase;
