# SelectPopup

## Dependencies

[SelectPopup](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/SelectPopup)

```js
import { SelectPopup } from '@/components/Input/SelectPopup';
```

## Import

```js
import { SelectPopup, SelectPopupStyles } from '@/components/Input/SelectPopup';
```

## Props

```tsx
interface SelectPopupProps {}
```

## Use

```js
<SelectPopup>SelectPopup</Input/SelectPopup>
```
