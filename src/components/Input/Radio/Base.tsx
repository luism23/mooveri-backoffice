import log from '@/functions/log';
import { useLang } from '@/lang/translate';
import SVGCheck from '@/svg/check';

export interface InputRadioClassProps {
    classNameLabel?: string;
    classNameText?: string;
    classNameRadio?: string;
    classNameRadioActive?: string;
    classNameRadioInactive?: string;
    classNameTitleOption?: string;
    classNameFirstTitleOption?: string;
    classNameAnyTitleOption?: string;

    iconCheck?: any;
}

export interface InputRadioBaseProps {
    i?: number;
    label?: any;
    labelPosition?: 'right' | 'left';
    onChange?: (i: number) => void;
    checked?: boolean;
    keyChecked?: number;
    disabled?: boolean;
}
export interface InputRadioProps
    extends InputRadioBaseProps,
        InputRadioClassProps {}
export const InputRadioBase = ({
    i = 0,
    classNameLabel = '',
    classNameText = '',
    classNameRadio = '',
    classNameRadioActive = '',
    classNameRadioInactive = '',
    label = '',
    labelPosition = 'right',
    onChange = (i) => {
        log('onChange', i);
    },
    checked = false,
    iconCheck = <SVGCheck />,
    disabled = false,
}: InputRadioProps) => {
    const _t = useLang();
    const onChecked = () => {
        onChange(i);
    };
    console.log(disabled);
    return (
        <label className={classNameLabel} onClick={onChecked}>
            {labelPosition == 'left' ? (
                <span className={classNameText}>{_t(label)}</span>
            ) : (
                <></>
            )}
            <span
                className={`${classNameRadio} ${
                    checked ? classNameRadioActive : classNameRadioInactive
                }`}
            >
                {checked && <>{iconCheck}</>}
            </span>
            {labelPosition == 'right' ? (
                <span className={classNameText}>{_t(label)}</span>
            ) : (
                <></>
            )}
        </label>
    );
};
export default InputRadioBase;
