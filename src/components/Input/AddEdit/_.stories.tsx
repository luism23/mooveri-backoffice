import { Story, Meta } from "@storybook/react";

import { Image } from "@/components/Image";

import { InputAddEditProps, InputAddEdit } from "./index";
import log from "@/functions/log";

export default {
    title: "Input/InputAddEdit",
    component: InputAddEdit,
} as Meta;

const Template: Story<InputAddEditProps> = (args) => (
    <InputAddEdit {...args}>Test Children</InputAddEdit>
);

export const Index = Template.bind({});
Index.args = {
    icon: <Image src="rs/instagram.png" className="width-30" />,
    name: "instagram",
    onSave: async (data: string) => {
        log("onSave",data)
        return {
            status: "ok",
            message: "Save ok",
        };
    },
};

export const Error_ = Template.bind({});
Error_.args = {
    icon: <Image src="rs/instagram.png" className="width-30" />,
    name: "instagram",
    onSave: async (data: string) => {
        log("onSave",data)
        throw {
            message: "error",
        };
        return {
            status: "ok",
            message: "Save ok",
        };
    },
};
