import {
    InputAddEditBase,
    InputAddEditBaseProps,
} from '@/components/Input/AddEdit/Base';
import * as styles from '@/components/Input/AddEdit/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputAddEditStyle = { ...styles } as const;
export type InputAddEditStyles = keyof typeof InputAddEditStyle;

export interface InputAddEditProps extends InputAddEditBaseProps {
    styleTemplate?: InputAddEditStyles | ThemesType;
}

export const InputAddEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputAddEditProps) => {
    return (
        <>
            <InputAddEditBase
                {...props}
                {...(InputAddEditStyle[styleTemplate as InputAddEditStyles] ??
                    InputAddEditStyle._default)}
            />
        </>
    );
};
export default InputAddEdit;
