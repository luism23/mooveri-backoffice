import { InputAddEditClassProps } from '@/components/Input/AddEdit/Base';

export const _default: InputAddEditClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-15
        flex-justify-between
    `,
    classNameContentIcon: `
        flex-3
    `,
    classNameContentInput: `
        flex-3
    `,
    classNameName: `
        flex-3
        color-greyish
        font-nunito
        font-15
        font-w-900
        text-capitalize
    `,
    classNameContentAddEdit: `
        flex-3
        flex
        flex-align-center
        flex-gap-column-15
        flex-nowrap
    `,
    classNameAdd: `
        color-greyishBrownTwo
        font-nunito
        font-11
        pointer
    `,
    classNameEdit: `
        color-greyishBrownTwo
        font-nunito
        font-12
        font-w-600
        pointer
    `,
    classNameCancel: `
        color-pinkishGreyThree
        font-nunito
        font-14
        font-w-900
        pointer
    `,
    classNameSave: `
        color-brightSkyBlue
        font-nunito
        font-14
        font-w-900
        pointer
    `,
    classNameDelete: `
        color-greyishBrownTwo
        pointer
    `,
};
