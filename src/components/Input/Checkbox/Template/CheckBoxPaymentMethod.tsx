import { DELETE_STRIPE_CARD_USER } from '@/api/tolinkme/Payment';
import { SubmitResult } from '@/components/Form/Base';
import Image from '@/components/Image';
import { InputCheckbox, InputCheckboxProps } from '@/components/Input/Checkbox';
import Link from '@/components/Link';
import Text from '@/components/Text';
import { useNotification } from '@/hook/useNotification';
import { useUser } from '@/hook/useUser';
import { useLang } from '@/lang/translate';
import Check2 from '@/svg/check';
import { useState, useEffect } from 'react';
import Trash from '@/svg/trash';

export interface CheckBoxPaymentMethodProps extends InputCheckboxProps {
    classNameCheckbox?: string;
    classNameContent?: string;
    classNameCheckboxActive?: string;
    number?: string;
    card?: string;
    id: string;
    showDeleteButton?: any;
}

export const CheckBoxPaymentMethod = ({
    classNameContent = `
    flex 
    flex-justify-between 
    flex-align-center 
    flex-nowrap
  `,
    classNameCheckbox = `
    width-15
    height-15
    aspect-ratio-1-1
    bg-darkAqua 
    border-radius-5
    m-h-5
    flex 
    flex-justify-between 
    flex-align-center 
  `,
    classNameCheckboxActive = `
    bg-transparent
    color-brightPink
    border-2 
    border-style-solid 
    border-brightPink
  `,
    number = '3578',
    card = 'dinersClub',
    id,
    showDeleteButton,
    ...props
}: CheckBoxPaymentMethodProps) => {
    const _t = useLang();
    const { user } = useUser();
    const { pop } = useNotification();
    const [isDelete, setIsDelete] = useState(false);
    const [isChecked, setIsChecked] = useState(props.value);

    const deleteCard = async () => {
        if (!showDeleteButton) {
            return;
        }
        const result: SubmitResult = await DELETE_STRIPE_CARD_USER({
            user,
            paymentMethodId: id,
        });

        if (result.status == 'ok') {
            setIsDelete(true);
        }
        pop({
            type: result.status,
            message: _t(result?.message ?? ''),
        });
    };

    useEffect(() => {
        setIsChecked(props.value);
    }, [props.value]);

    if (isDelete) {
        return <></>;
    }

    return (
        <div className="m-v-10 width-p-100">
            <InputCheckbox
                {...props}
                styleTemplate="tolinkme13"
                value={isChecked}
                onChange={(e) => {
                    const checked = e;
                    setIsChecked(checked);
                    if (props.onChange) {
                        props.onChange(checked);
                    }
                }}
                label={
                    <>
                        <span className={classNameContent}>
                            <div
                                className={`${classNameCheckbox} ${
                                    isChecked ? classNameCheckboxActive : ''
                                }`}
                            >
                                {isChecked ? <Check2 size={9} /> : ''}
                            </div>
                            <span className="flex flex-nowrap flex-align-center width-p-15">
                                <Image
                                    styleTemplate="tolinkme"
                                    className={`width-p-100 height-20 ContentimgCard`}
                                    src={`cards/${card}.png`}
                                />
                            </span>
                            <span className="flex flex-nowrap flex-align-center">
                                <Text
                                    className="width-p-100"
                                    styleTemplate={'tolinkme52'}
                                >
                                    <span
                                        className={`ContentCard`}
                                    >{`xxxx xxxx xxxx ${number}`}</span>
                                </Text>
                            </span>

                            <Link styleTemplate={'tolinkme6'}>
                                {_t('Manage')}
                            </Link>
                        </span>
                    </>
                }
            />
            {showDeleteButton && (
                <Link
                    onClick={deleteCard}
                    className="flex flex-justify-right width-p-100 text-right p-r-15 p-t-5"
                >
                    <span className="flex m-r-5">
                        <Trash size={12} />
                    </span>{' '}
                    {_t('Delete')}
                </Link>
            )}
        </div>
    );
};

export default CheckBoxPaymentMethod;
