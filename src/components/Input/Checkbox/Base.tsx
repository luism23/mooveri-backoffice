import { useMemo, useState } from 'react';

import SVGCheck from '@/svg/check';
import log from '@/functions/log';

export const BaseStyle = {
    classNameLabel: `
        flex
        flex-align-center
        pointer
    `,
    classNameText: `
        font-20 font-montserrat
    `,
    classNameCheckbox: `
        flex
        width-10
        height-10
        m-h-5
        color-white
        p-1
        border-style-solid
        border-1
    `,
    classNameCheckboxActive: `
    `,
    classNameCheckboxInactive: `
    `,
};

export interface InputCheckboxClassProps {
    classNameLabel?: string;
    classNameLabelActive?: string;
    classNameLabelInactive?: string;
    classNameText?: string;
    classNameContentCheckbox?: string;
    classNameContentCheckboxActive?: string;
    classNameContentCheckboxInactive?: string;
    classNameCheckbox?: string;
    classNameCheckboxActive?: string;
    classNameCheckboxInactive?: string;
    icon?: any;
}
export interface InputCheckboxBaseProps {
    label?: any;
    labelPosition?: 'right' | 'left';
    onChange?: (e: boolean) => void;
    defaultValue?: boolean;
    useValue?: boolean;
    value?: boolean;
    disabled?: boolean;
    onValidateCheck?: () => Promise<void> | void;
}
export interface InputCheckboxProps
    extends InputCheckboxBaseProps,
        InputCheckboxClassProps {}

export const InputCheckboxBase = ({
    classNameLabel = '',
    classNameLabelActive = '',
    classNameLabelInactive = '',
    classNameText = '',

    classNameContentCheckbox = '',
    classNameContentCheckboxActive = '',
    classNameContentCheckboxInactive = '',

    classNameCheckbox = '',
    classNameCheckboxActive = '',
    classNameCheckboxInactive = '',

    label = '',
    labelPosition = 'right',
    onChange = (e) => {
        log('onChange', e);
    },
    defaultValue = false,
    useValue = false,
    value = false,
    disabled = false,
    icon = <SVGCheck />,
    onValidateCheck = async () => {},
}: InputCheckboxProps) => {
    const [checked_, setChecked] = useState(defaultValue === true);

    const checked = useMemo(
        () => (useValue ? value : checked_),
        [checked_, useValue, value]
    );

    const onChecked = async () => {
        if (disabled) {
            return;
        }
        if (!checked) {
            await onValidateCheck();
        }
        setChecked(!checked);
        onChange(!checked);
    };

    return (
        <label
            className={`${classNameLabel}  ${
                checked ? classNameLabelActive : classNameLabelInactive
            }`}
        >
            {labelPosition == 'left' && label != '' && (
                <span className={classNameText}>{label}</span>
            )}
            <input
                type="checkbox"
                checked={checked}
                onChange={onChecked}
                hidden
            />
            <span
                className={`${classNameContentCheckbox} ${
                    checked
                        ? classNameContentCheckboxActive
                        : classNameContentCheckboxInactive
                }`}
            >
                <span
                    className={`${classNameCheckbox} ${
                        checked
                            ? classNameCheckboxActive
                            : classNameCheckboxInactive
                    }`}
                >
                    {checked && icon}
                </span>
            </span>
            {labelPosition == 'right' && label != '' && (
                <span className={classNameText}>{label}</span>
            )}
        </label>
    );
};
export default InputCheckboxBase;
