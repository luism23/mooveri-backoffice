import { Story, Meta } from "@storybook/react";

import { InputPopupFileProps, InputPopupFile } from "./index";

export default {
    title: "InputPopupFile/InputPopupFile",
    component: InputPopupFile,
} as Meta;

const InputPopupFileIndex: Story<InputPopupFileProps> = (args) => (
    <InputPopupFile {...args}>Test Children</InputPopupFile>
);

export const Index = InputPopupFileIndex.bind({});
Index.args = {};
