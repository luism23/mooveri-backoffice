import { useMemo } from 'react';

import * as styles from '@/components/Input/InputPopupFile/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InputPopupFileBaseProps,
    InputPopupFileBase,
} from '@/components/Input/InputPopupFile/Base';

export const InputPopupFileStyle = { ...styles } as const;

export type InputPopupFileStyles = keyof typeof InputPopupFileStyle;

export interface InputPopupFileProps extends InputPopupFileBaseProps {
    styleTemplate?: InputPopupFileStyles | ThemesType;
}

export const InputPopupFile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputPopupFileProps) => {
    const Style = useMemo(
        () =>
            InputPopupFileStyle[styleTemplate as InputPopupFileStyles] ??
            InputPopupFileStyle._default,
        [styleTemplate]
    );

    return <InputPopupFileBase {...Style} {...props} />;
};
export default InputPopupFile;
