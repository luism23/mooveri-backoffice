# ConfigText

## Dependencies

[ConfigText](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/ConfigText)

```js
import { ConfigText } from '@/components/Input/ConfigText';
```

## Import

```js
import { ConfigText, ConfigTextStyles } from '@/components/Input/ConfigText';
```

## Props

```tsx
interface ConfigTextProps {}
```

## Use

```js
<ConfigText>ConfigText</Input/ConfigText>
```
