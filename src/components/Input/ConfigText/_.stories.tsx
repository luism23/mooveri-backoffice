import { Story, Meta } from "@storybook/react";

import { ConfigTextProps, ConfigText } from "./index";

export default {
    title: "Input/ConfigText",
    component: ConfigText,
} as Meta;

const ConfigTextIndex: Story<ConfigTextProps> = (args) => (
    <ConfigText {...args}>Test Children</ConfigText>
);

export const Index = ConfigTextIndex.bind({});
Index.args = {};
