import { useMemo } from 'react';

import * as styles from '@/components/Input/ConfigText/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigTextBaseProps,
    ConfigTextBase,
} from '@/components/Input/ConfigText/Base';

export const ConfigTextStyle = { ...styles } as const;

export type ConfigTextStyles = keyof typeof ConfigTextStyle;

export interface ConfigTextProps extends ConfigTextBaseProps {
    styleTemplate?: ConfigTextStyles | ThemesType;
}

export const ConfigText = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigTextProps) => {
    const Style = useMemo(
        () =>
            ConfigTextStyle[styleTemplate as ConfigTextStyles] ??
            ConfigTextStyle._default,
        [styleTemplate]
    );

    return <ConfigTextBase {...Style} {...props} />;
};
export default ConfigText;
