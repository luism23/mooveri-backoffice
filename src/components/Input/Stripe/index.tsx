import { useMemo } from 'react';

import * as styles from '@/components/Input/Stripe/styles';

import { Theme, ThemesType } from '@/config/theme';

import { StripeBaseProps, StripeBase } from '@/components/Input/Stripe/Base';

export const StripeStyle = { ...styles } as const;

export type StripeStyles = keyof typeof StripeStyle;

export interface StripeProps extends StripeBaseProps {
    styleTemplate?: StripeStyles | ThemesType;
}

export const Stripe = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: StripeProps) => {
    const Style = useMemo(
        () =>
            StripeStyle[styleTemplate as StripeStyles] ?? StripeStyle._default,
        [styleTemplate]
    );

    return <StripeBase {...Style} {...props} />;
};
export default Stripe;
