import {
    loadStripe,
    PaymentMethodResult,
    StripeCardElementChangeEvent,
} from '@stripe/stripe-js';

import {
    CardElement,
    Elements,
    useElements,
    useStripe,
} from '@stripe/react-stripe-js';
import { Button, ButtonProps, ButtonStyles } from '@/components/Button';
import { useLang } from '@/lang/translate';
import { useState } from 'react';
import ContentWidth from '@/components/ContentWidth';
import Theme, { ThemesType } from '@/config/theme';

export interface StripeClassProps {
    classNameContent?: string;
    classNameContentCardElement?: string;
    classNameCardElement?: string;
    classNameContentBtn?: string;
    ButtonProps?: ButtonProps;

    classNameBtn?: string;
    styleTemplateButton?: ButtonStyles | ThemesType;
    options?: object;
}

export interface StripeBaseProps {
    onSaveCard?: (PaymentMethodResult: PaymentMethodResult) => Promise<void>;
    onChange?: (event: StripeCardElementChangeEvent) => void;
    textBtn?: string;
    urlRedirect?: string;
    disabled?: boolean;
    classNameBtn?: string;
    styleTemplateButton?: ButtonStyles | ThemesType;
    options?: object;
    sizeButton?: number;
}

export interface StripeProps extends StripeClassProps, StripeBaseProps {}

export const StripeBaseCardElement = ({
    disabled = true,
    classNameContent = '',
    options,
    classNameContentCardElement = '',
    classNameCardElement = '',
    classNameContentBtn = '',
    classNameBtn = '',
    ButtonProps = {},
    sizeButton = 400,
    textBtn = 'Save Card',
    onSaveCard,
    onChange,
    styleTemplateButton = Theme.styleTemplate ?? '_default',
}: StripeProps) => {
    const _t = useLang();
    const [loader, setLoader] = useState<boolean>(false);
    const stripe = useStripe();
    const elements: any = useElements();
    const addPayment = async () => {
        setLoader(true);
        if (stripe) {
            const Payment = await stripe.createPaymentMethod({
                type: 'card',
                card: elements.getElement(CardElement),
            });
            await onSaveCard?.(Payment);
        }

        setLoader(false);
    };
    return (
        <div className={classNameContent}>
            <div className={classNameContentCardElement}>
                <CardElement
                    onChange={onChange}
                    className={classNameCardElement}
                    options={options}
                />
            </div>
            <ContentWidth size={sizeButton} className={classNameContentBtn}>
                <Button
                    {...ButtonProps}
                    onClick={addPayment}
                    classNameBtn={classNameBtn}
                    loader={loader}
                    styleTemplate={styleTemplateButton}
                    disabled={disabled}
                >
                    {_t(textBtn)}
                </Button>
            </ContentWidth>
        </div>
    );
};

export const StripeBase = (props: StripeProps) => {
    if (process?.env?.['NEXT_PUBLIC_STORYBOK'] == 'TRUE') {
        return <></>;
    }
    try {
        const stripePromise = loadStripe(
            process?.env?.['NEXT_PUBLIC_STRIPE_KEY_DEV'] ?? ''
        );
        return (
            <>
                <Elements stripe={stripePromise}>
                    <StripeBaseCardElement {...props} />
                </Elements>
            </>
        );
    } catch (error) {
        return <> STRIPE_KEY empty </>;
    }
};

export default StripeBase;
