import {
    InputRangeBase,
    InputRangeBaseProps,
} from '@/components/Input/Range/Base';
import * as styles from '@/components/Input/Range/styles';
import Theme, { ThemesType } from '@/config/theme';

export const InputRangeStyle = { ...styles } as const;
export type InputRangeStyles = keyof typeof InputRangeStyle;

export interface InputRangeProps extends InputRangeBaseProps {
    styleTemplate?: InputRangeStyles | ThemesType;
}

export const InputRange = ({
    styleTemplate = Theme.styleTemplate ?? '_default',
    ...props
}: InputRangeProps) => {
    const config = {
        ...(InputRangeStyle[styleTemplate as InputRangeStyles] ??
            InputRangeStyle._default),
        ...props,
    };
    return <InputRangeBase {...config} />;
};
export default InputRange;
