import { useMemo } from 'react';
import {
    InputAvatarBase,
    InputAvatarBaseProps,
} from '@/components/Input/Avatar/Base';

import * as styles from '@/components/Input/Avatar/styles';
import { Theme, ThemesType } from '@/config/theme';

export const InputAvatarStyle = { ...styles } as const;
export type InputAvatarStyles = keyof typeof InputAvatarStyle;

export interface InputAvatarProps extends InputAvatarBaseProps {
    styleTemplate?: InputAvatarStyles | ThemesType;
}

export const InputAvatar = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputAvatarProps) => {
    const Style = useMemo(
        () =>
            InputAvatarStyle[styleTemplate as InputAvatarStyles] ??
            InputAvatarStyle._default,
        [styleTemplate]
    );

    return (
        <>
            <InputAvatarBase {...props} {...Style} />
        </>
    );
};
export default InputAvatar;
