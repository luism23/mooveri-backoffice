import { InputAvatarClassProps } from '@/components/Input/Avatar/Base';
import { Trash } from '@/svg/trash';
import { ImgAvatar } from '@/svg/imgAvatar';
import Loader from '@/svg/loader';

export const mooveri: InputAvatarClassProps = {
    classNameContent: `
        pos-r
        pointer
        bg-whiteThree
        filter-brightness-6-hover
        border-radius-100
        p-10
    `,
    styleContent: {
        width: 'min(100%,8rem)',
    },
    classNameContentImg: `
        pos-r
        width-p-100
        border-radius-100
        overflow-hidden
        bg-grayAvatar
        pointer
    `,
    styleContentImg: {
        paddingBottom: '100%',
    },
    classNameImg: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-cover
        pointer
       
    `,
    img: '+.png',
    styleTemplateImg: 'mooveri',
    classNameRemove: `
        pos-a
        inset-0
        m-auto
        bg-grayAvatar
        opacity-0
        opacity-10-hover
        color-white
        flex
        flex-align-center
        flex-justify-center
        flex-column
        flex-nowrap
        overflow-hidden
        pointer
    `,
    classNameRemoveMovil: `
        d-sm-none  
    `,
    classNameRemoveDesktop: `
        d-none d-sm-block  
    `,
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-40 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    classNameRemoveTopActive: `
    transform-translate-Y-p-0
`,
    classNameRemoveTopNotActive: `
    transform-translate-Y-p--100
`,
    classNameRemoveTop: `
    bg-sea
    pos-a
    top-0
    left-0
    width-p-100
    height-p-50
    flex
    flex-align-center
    flex-justify-center
    transform
    transition-5
`,
    classNameRemoveDownActive: `
    transform-translate-Y-p-0
`,
    classNameRemoveDownNotActive: `
    transform-translate-Y-p-100
`,
    classNameRemoveDown: `
    bg-red
    pos-a
    top-p-50
    left-0
    width-p-100
    height-p-50
    flex
    flex-align-center
    flex-justify-center
    transform
    transition-5
`,
    classNameLoader: `
    pos-a
    inset-0
    m-auto
    bg-black
    opacity-5
    color-white
    flex
    flex-align-center
    flex-justify-center
`,
    iconLoader: (
        <>
            <ImgAvatar size={30} />
        </>
    ),
};

export const mooveriProfile: InputAvatarClassProps = {
    classNameContent: `
        pos-r
        pointer
        bg-whiteThree
        border-t-l-radius-14
        border-t-r-radius-14
    `,
    styleContent: {
        width: '100%',
    },
    classNameContentImg: `
        pos-r
        width-p-100
        border-t-l-radius-14
        border-t-r-radius-14
        overflow-hidden
        bg-grayAvatar
        pointer
    `,
    styleContentImg: {
        height: '10rem',
    },
    classNameImg: `
        pos-a
        top-0
        width-p-100
        object-fit-cover
        pointer
       
    `,
    img: 'banner.png',
    styleTemplateImg: 'mooveri',
    classNameRemove: `
        pos-a
        inset-0
        m-auto
        bg-grayAvatar
        opacity-0
        opacity-10-hover
        color-white
        flex
        flex-align-center
        flex-justify-center
        flex-column
        flex-nowrap
        overflow-hidden
        pointer
    `,
    classNameRemoveMovil: `
        d-sm-none  
    `,
    classNameRemoveDesktop: `
        d-none d-sm-block  
    `,
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-40 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    classNameRemoveTopActive: `
    transform-translate-Y-p-0
`,
    classNameRemoveTopNotActive: `
    transform-translate-Y-p--100
`,
    classNameRemoveTop: `
    bg-sea
    pos-a
    top-0
    left-0
    width-p-100
    height-p-50
    flex
    flex-align-center
    flex-justify-center
    transform
    transition-5
`,
    classNameRemoveDownActive: `
    transform-translate-Y-p-0
`,
    classNameRemoveDownNotActive: `
    transform-translate-Y-p-100
`,
    classNameRemoveDown: `
    bg-red
    pos-a
    top-p-50
    left-0
    width-p-100
    height-p-50
    flex
    flex-align-center
    flex-justify-center
    transform
    transition-5
`,
    classNameLoader: `
    pos-a
    inset-0
    m-auto
    bg-black
    opacity-5
    color-white
    flex
    flex-align-center
    flex-justify-center
`,
    iconLoader: (
        <>
            <ImgAvatar size={30} />
        </>
    ),
    inf: true,
    classNameText: `
        z-index-10 
        pos-a 
        top-10 
        right-10 
        color-white 
        text-right 
        font-8 
        font-nunito 
        font-w-800
        pointer
    `,
    classNameTextPhoto: `
        z-index-10 
        pos-a
        inset-0
        m-auto
        flex 
        flex-align-center
        flex-justify-center
        text-center
        color-white 
        font-58 
        font-circular 
        font-w-700
        pointer
        opacity-2
    `,
};

export const mooveriBackoffice: InputAvatarClassProps = {
    classNameContent: `
        pos-r
        pointer
    `,
    styleContent: {
        width: 'min(100%,6rem)',
    },
    classNameContentImg: `
        pos-r
        border-radius-100
        overflow-hidden
        box-shadow
        box-shadow-inset
        box-shadow-s-3
        box-shadow-c-whiteThree
        bg-whiteThree
        pointer
        aspect-ratio-1-1
    `,
    styleContentImg: {},

    classNameImg: `
        width-p-100
        aspect-ratio-1-1
    `,
    img: 'uploadAvatar.png',
    styleTemplateImg: 'tolinkme',
    classNameRemove: `
        pos-a
        inset-0
        m-auto
        bg-black-50
        opacity-0
        opacity-10-hover
        color-white
        flex
        flex-align-center
        flex-justify-center
        flex-column
        flex-nowrap
        overflow-hidden
        pointer
    `,
    classNameRemoveMovil: `
        d-sm-none  
    `,
    classNameRemoveDesktop: `
        d-none d-sm-block  
    `,
    iconDelete: (
        <>
            <Trash size={20} />
        </>
    ),
    iconChange: (
        <>
            <span className="font-nunito color-white font-40 line-h-10 cursor-pointer">
                +
            </span>
        </>
    ),
    classNameRemoveTopActive: `
        transform-translate-Y-p-0
    `,
    classNameRemoveTopNotActive: `
        transform-translate-Y-p--100
    `,
    classNameRemoveTop: `
        bg-brightSkyBlue
        pos-a
        top-0
        left-0
        width-p-100
        height-p-50
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameRemoveDownActive: `
        transform-translate-Y-p-0
    `,
    classNameRemoveDownNotActive: `
        transform-translate-Y-p-100
    `,
    classNameRemoveDown: `
        bg-red
        pos-a
        top-p-50
        left-0
        width-p-100
        height-p-50
        flex
        flex-align-center
        flex-justify-center
        transform
        transition-5
    `,
    classNameLoader: `
        pos-a
        inset-0
        m-auto
        bg-black
        opacity-5
        color-white
        flex
        flex-align-center
        flex-justify-center
    `,
    iconLoader: (
        <>
            <Loader size={200} />
        </>
    ),
};
