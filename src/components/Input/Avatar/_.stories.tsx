import { Story, Meta } from "@storybook/react";

import { InputFileDataProps } from "@/components/Input/File/Base";
import { SubmitResult } from "@/components/Form/Base";

import { InputAvatar, InputAvatarProps } from "./index";
import log from "@/functions/log";

export default {
    title: "Input/InputAvatar",
    component: InputAvatar,
} as Meta;

const Template: Story<InputAvatarProps> = (args) => (
    <InputAvatar {...args}>Test Children</InputAvatar>
);

export const Index = Template.bind({});
Index.args = {
    onSubmit: async (data: InputFileDataProps | undefined) => {
        log("InputFileDataProps",data)
        await new Promise((r) => setTimeout(r, 2000));
        const result: SubmitResult = {
            status: "ok",
        };
        return result;
    },
};
