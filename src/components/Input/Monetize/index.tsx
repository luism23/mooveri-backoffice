import { useEffect, useState } from 'react';
import { InputText, InputTextProps } from '@/components/Input/Text';
import SVGArrowCollapse from '@/svg/arrowCollapse';
import { numberCount } from '@/functions/numberCount';

export interface InputMonetizeProps
    extends Omit<
        InputTextProps,
        'defaultValue' | 'onChange' | 'onChangeValidate'
    > {
    val?: number;
    defaultValue?: number | '';
    onChange: (v: number) => void;
    min?: number;
    max?: number;
}

export const InputMonetize = ({
    val = undefined,
    defaultValue = '',
    onChange,
    ...props
}: InputMonetizeProps) => {
    const [valueNumber, setValueNumber_] = useState<number>(
        defaultValue == '' ? 0 : defaultValue
    );
    const [value, setValue] = useState<string>(`${defaultValue}`);

    const setValueNumber = (newValue: number) => {
        if (props.disabled) {
            return;
        }
        setValueNumber_(() => {
            if (props.min !== undefined) {
                newValue = Math.max(props.min, newValue);
            }
            if (props.max !== undefined) {
                newValue = Math.min(props.max, newValue);
            }
            onChange(newValue);
            return newValue;
        });
    };

    const addValue = (add: number) => () => {
        if (props.disabled) {
            return;
        }
        if (props?.disabled) {
            return;
        }
        setValueNumber(valueNumber + add);
    };

    const onChangeInput = (v: string) => {
        if (props.disabled) {
            return;
        }
        v = `${v}`.replace(/[^\d.]/g, '');
        if (v.includes('.')) {
            const a = v.split('.');
            const d = a.shift();
            v = d + '.' + a.join('');
        }
        let n = 0;
        try {
            n = parseFloat(v);
            if (isNaN(n)) {
                throw '1';
            }
        } catch (error) {
            n = 0;
        }
        setValueNumber(n);
        setValue(v);
    };
    useEffect(() => {
        if (val !== undefined && val !== valueNumber) {
            setValueNumber(val);
        }
    }, [val]);

    const propsMonetize = {
        classNameContentInput: `
      pos-r
    `,
        icon: (
            <>
                <div className="flex">
                    <span
                        onClick={addValue(1)}
                        className={`
              pos-a
              right-10
              top-8
              m-auto
              m-b-8
              flex flex-align-center
              pointer
              z-index-3
              color-black color-blue-hover
            `}
                        style={{
                            transform: 'rotateZ(180deg)',
                        }}
                    >
                        <SVGArrowCollapse size={10} />
                    </span>
                    <span
                        onClick={addValue(-1)}
                        className={`
              pos-a
              bottom-8
              right-10
              m-auto
              flex flex-align-center
              pointer
              z-index-3
              color-black color-blue-hover
            `}
                    >
                        <SVGArrowCollapse size={10} />
                    </span>
                </div>
            </>
        ),
    };

    const parseNumberPrice = (v: string, vn: number) => {
        if (v == '') {
            return '';
        }
        const n = numberCount(parseInt(`${vn}`));
        if (v.includes('.')) {
            return n + '.' + v.split('.').at(-1)?.slice(0, 2);
        }
        return n;
    };
    return (
        <>
            <InputText
                {...propsMonetize}
                {...props}
                type="text"
                value={`${parseNumberPrice(value, valueNumber)}`}
                useLoader={false}
                onChange={onChangeInput}
            />
        </>
    );
};

export default InputMonetize;
