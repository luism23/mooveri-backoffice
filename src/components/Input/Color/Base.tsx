import CSS from 'csstype';

import Image from '@/components/Image';
import { useData } from 'fenextjs-hook/cjs/useData';

export interface InputColorClassProps {
    classNameContent?: string;
    styleContent?: CSS.Properties;
    classNameInput?: string;
    classNameColorShow?: string;
    icon?: any;
}

export interface InputColorBaseProps {
    defaultValue?: string;
    value?: string;
    useValue?: boolean;
    onChange?: (color: string) => void;
}

export interface InputColorProps
    extends InputColorBaseProps,
        InputColorClassProps {}

export const InputColorBase = ({
    classNameContent = '',
    styleContent = {},

    classNameInput = '',
    classNameColorShow = '',
    icon = <Image src="color.png" />,

    defaultValue = '',
    value = '',
    useValue = false,
    onChange = () => {},
}: InputColorProps) => {
    const { dataMemo = defaultValue, onChangeData } = useData<
        {
            value: string;
        },
        string
    >(
        {
            value: defaultValue,
        },
        {
            onMemo: ({ value: v }) => {
                return useValue ? value : v;
            },
        }
    );
    return (
        <>
            <label className={classNameContent} style={styleContent}>
                <input
                    type="color"
                    defaultValue={defaultValue}
                    onChange={(e) => {
                        onChange(e.target.value);
                        onChangeData('value')(e.target.value);
                    }}
                    className={classNameInput}
                />
                {icon}
                <div
                    className={classNameColorShow}
                    style={{ background: dataMemo }}
                ></div>
            </label>
        </>
    );
};
export default InputColorBase;
