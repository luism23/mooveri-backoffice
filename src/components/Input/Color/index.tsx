import { useMemo } from 'react';
import {
    InputColorBase,
    InputColorBaseProps,
} from '@/components/Input/Color/Base';

import * as styles from '@/components/Input/Color/styles';
import { Theme, ThemesType } from '@/config/theme';

export const InputColorStyle = { ...styles } as const;
export type InputColorStyles = keyof typeof InputColorStyle;

export interface InputColorProps extends InputColorBaseProps {
    styleTemplate?: InputColorStyles | ThemesType;
}

export const InputColor = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputColorProps) => {
    const Style = useMemo(
        () =>
            InputColorStyle[styleTemplate as InputColorStyles] ??
            InputColorStyle._default,
        [styleTemplate]
    );

    return (
        <>
            <InputColorBase {...props} {...Style} />
        </>
    );
};
export default InputColor;
