import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';
import {
    BorderRadiusButton,
    BorderRadiusButtonType,
    BorderRadiusButtonValuesType,
} from '@/interfaces/Button';
import { useLang } from '@/lang/translate';
import { InputRadio, InputRadioStyles } from '../Radio';

export interface RadioBorderRadiusButtonClassProps {
    ContentWidthProps?: ContentWidthProps;
    styleTemplateRadius?: InputRadioStyles | ThemesType;
    classNameBtn?: string;
    classNameBtnActive?: string;
    classNameBtnInactive?: string;
    classNameBtnTypes?: BorderRadiusButtonValuesType;
}

export interface RadioBorderRadiusButtonBaseProps {
    defaultValue?: BorderRadiusButtonType;
    value?: BorderRadiusButtonType;
    useValue?: boolean;
    onChange?: (value: BorderRadiusButtonType) => void;
}

export interface RadioBorderRadiusButtonProps
    extends RadioBorderRadiusButtonClassProps,
        RadioBorderRadiusButtonBaseProps {}

export const RadioBorderRadiusButtonBase = ({
    ContentWidthProps = {},
    styleTemplateRadius = Theme.styleTemplate ?? '_default',
    classNameBtn = '',
    classNameBtnActive = '',
    classNameBtnInactive = '',
    classNameBtnTypes = {
        'no-rounding': '',
        'semi-rounded': '',
        rounded: '',
    },
    defaultValue = 'semi-rounded',
    useValue = false,
    ...props
}: RadioBorderRadiusButtonProps) => {
    const _t = useLang();
    const { dataMemo: data = { value: 0 }, onChangeData } = useData<
        { value: number },
        { value: number }
    >(
        { value: BorderRadiusButton.indexOf(defaultValue) },
        {
            onChangeDataAfter: ({ value }) => {
                props?.onChange?.(BorderRadiusButton?.[value]);
            },
            onMemo: ({ value }) => {
                return {
                    value: useValue
                        ? BorderRadiusButton.indexOf(
                              props?.value ?? 'semi-rounded'
                          )
                        : value,
                };
            },
        }
    );
    return (
        <>
            <ContentWidth {...ContentWidthProps}>
                <InputRadio
                    onChange={onChangeData('value')}
                    selectedRadioDefault={data.value}
                    options={BorderRadiusButton.map((value, i) => ({
                        label: (
                            <div
                                className={`${classNameBtn} ${
                                    data.value == i
                                        ? classNameBtnActive
                                        : classNameBtnInactive
                                } ${classNameBtnTypes?.[value] ?? ''}`}
                            >
                                {_t(value)}
                            </div>
                        ),
                    }))}
                    styleTemplate={styleTemplateRadius}
                />
            </ContentWidth>
        </>
    );
};
export default RadioBorderRadiusButtonBase;
