import { Story, Meta } from "@storybook/react";

import { RadioBorderRadiusButtonProps, RadioBorderRadiusButton } from "./index";

export default {
    title: "Input/RadioBorderRadiusButton",
    component: RadioBorderRadiusButton,
} as Meta;

const RadioBorderRadiusButtonIndex: Story<RadioBorderRadiusButtonProps> = (args) => (
    <RadioBorderRadiusButton {...args}>Test Children</RadioBorderRadiusButton>
);

export const Index = RadioBorderRadiusButtonIndex.bind({});
Index.args = {};
