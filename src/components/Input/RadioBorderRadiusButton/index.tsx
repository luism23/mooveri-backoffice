import { useMemo } from 'react';

import * as styles from '@/components/Input/RadioBorderRadiusButton/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    RadioBorderRadiusButtonBaseProps,
    RadioBorderRadiusButtonBase,
} from '@/components/Input/RadioBorderRadiusButton/Base';

export const RadioBorderRadiusButtonStyle = { ...styles } as const;

export type RadioBorderRadiusButtonStyles =
    keyof typeof RadioBorderRadiusButtonStyle;

export interface RadioBorderRadiusButtonProps
    extends RadioBorderRadiusButtonBaseProps {
    styleTemplate?: RadioBorderRadiusButtonStyles | ThemesType;
}

export const RadioBorderRadiusButton = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: RadioBorderRadiusButtonProps) => {
    const Style = useMemo(
        () =>
            RadioBorderRadiusButtonStyle[
                styleTemplate as RadioBorderRadiusButtonStyles
            ] ?? RadioBorderRadiusButtonStyle._default,
        [styleTemplate]
    );

    return <RadioBorderRadiusButtonBase {...Style} {...props} />;
};
export default RadioBorderRadiusButton;
