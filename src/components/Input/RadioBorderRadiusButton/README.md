# RadioBorderRadiusButton

## Dependencies

[RadioBorderRadiusButton](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/RadioBorderRadiusButton)

```js
import { RadioBorderRadiusButton } from '@/components/Input/RadioBorderRadiusButton';
```

## Import

```js
import {
    RadioBorderRadiusButton,
    RadioBorderRadiusButtonStyles,
} from '@/components/Input/RadioBorderRadiusButton';
```

## Props

```tsx
interface RadioBorderRadiusButtonProps {}
```

## Use

```js
<RadioBorderRadiusButton>RadioBorderRadiusButton</Input/RadioBorderRadiusButton>
```
