import { InputEditClassProps } from '@/components/Input/Edit/Base';

export const _default: InputEditClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-15
        flex-justify-between
    `,
    classNameContentInput: `
        flex-3
    `,
    classNameContentEdit: `
        flex-3
        flex
        flex-align-center
        flex-gap-column-15
        flex-nowrap
    `,
    classNameCancel: `
        color-pinkishGreyThree
        font-nunito
        font-14
        font-w-900
        pointer
    `,
    classNameSave: `
        color-brightSkyBlue
        font-nunito
        font-14
        font-w-900
        pointer
    `,
};
