import {
    InputEditBase,
    InputEditBaseProps,
} from '@/components/Input/Edit/Base';
import * as styles from '@/components/Input/Edit/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputEditStyle = { ...styles } as const;
export type InputEditStyles = keyof typeof InputEditStyle;

export interface InputEditProps extends InputEditBaseProps {
    styleTemplate?: InputEditStyles | ThemesType;
}

export const InputEdit = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputEditProps) => {
    return (
        <>
            <InputEditBase
                {...props}
                {...(InputEditStyle[styleTemplate as InputEditStyles] ??
                    InputEditStyle._default)}
            />
        </>
    );
};
export default InputEdit;
