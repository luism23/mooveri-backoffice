import { Story, Meta } from "@storybook/react";

import { InputMeProps, InputMe } from "./index";

export default {
    title: "Input/InputMe",
    component: InputMe,
} as Meta;

const Template: Story<InputMeProps> = (args) => (
    <InputMe {...args}>Test Children</InputMe>
);

export const Index = Template.bind({});
Index.args = {
    label: "Label Test",
};
