import { Image } from '@/components/Image';
import { InputText, InputTextProps } from '@/components/Input/Text';

export type InputMeProps = InputTextProps;

export const InputMe = (props: InputMeProps) => {
    const icon = (
        <Image
            src="me_logo.svg"
            className="width-40 pos-a top-0 left-15 bottom-0 m-auto"
        />
    );
    return (
        <>
            <div className="contentInputMe">
                <InputText {...props} icon={icon} />
            </div>
            <style jsx global>
                {`
                    .contentInputMe .input {
                        padding-left: 4rem;
                    }
                `}
            </style>
        </>
    );
};
export default InputMe;
