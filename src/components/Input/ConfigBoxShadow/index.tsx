import { useMemo } from 'react';

import * as styles from '@/components/Input/ConfigBoxShadow/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    ConfigBoxShadowBaseProps,
    ConfigBoxShadowBase,
} from '@/components/Input/ConfigBoxShadow/Base';

export const ConfigBoxShadowStyle = { ...styles } as const;

export type ConfigBoxShadowStyles = keyof typeof ConfigBoxShadowStyle;

export interface ConfigBoxShadowProps extends ConfigBoxShadowBaseProps {
    styleTemplate?: ConfigBoxShadowStyles | ThemesType;
}

export const ConfigBoxShadow = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: ConfigBoxShadowProps) => {
    const Style = useMemo(
        () =>
            ConfigBoxShadowStyle[styleTemplate as ConfigBoxShadowStyles] ??
            ConfigBoxShadowStyle._default,
        [styleTemplate]
    );

    return <ConfigBoxShadowBase {...Style} {...props} />;
};
export default ConfigBoxShadow;
