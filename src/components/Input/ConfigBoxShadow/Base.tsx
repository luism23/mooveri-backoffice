import CSS from 'csstype';

import { useData } from 'fenextjs-hook/cjs/useData';
import {
    BoxShadowConfig,
    BoxShadowTypes,
    BoxShadowTypesName,
} from '@/interfaces/BoxShadow';
import { useLang } from '@/lang/translate';
import { InputRange, InputRangeStyles } from '../Range';
import Theme, { ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { InputColor, InputColorStyles } from '../Color';
import { InputSelect } from '../Select';
import { InputTextStyles } from '../Text';

export interface ConfigBoxShadowClassProps {
    classNameContent?: string;
    classNameContentConfig?: string;

    classNameContentConfigInput?: string;
    styleTemplateTitleInput?: TextStyles | ThemesType;
    styleTemplateInputRange?: InputRangeStyles | ThemesType;
    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateInputSelect?: InputTextStyles | ThemesType;
}

export interface ConfigBoxShadowBaseProps {
    defaultValue?: BoxShadowConfig;
    onChange?: (data: BoxShadowConfig) => void;
}

export interface ConfigBoxShadowProps
    extends ConfigBoxShadowClassProps,
        ConfigBoxShadowBaseProps {}

export const ConfigBoxShadowBase = ({
    classNameContent = '',
    classNameContentConfig = '',

    classNameContentConfigInput = '',
    styleTemplateTitleInput = Theme?.styleTemplate ?? '_default',
    styleTemplateInputRange = Theme?.styleTemplate ?? '_default',
    styleTemplateInputColor = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelect = Theme?.styleTemplate ?? '_default',

    defaultValue = {
        blur: 0,
        size: 0,
        x: 0,
        y: 0,
        type: 'normal',
        color: '#ffffff',
    },
    onChange,
}: ConfigBoxShadowProps) => {
    const _t = useLang();
    const { data, onChangeData } = useData<BoxShadowConfig, CSS.Properties>(
        defaultValue,
        {
            onChangeDataAfter: onChange,
        }
    );

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentConfig}>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('BoxShadow Type')}
                        </Text>
                        <InputSelect
                            defaultValue={{
                                value: data?.type ?? '',
                                text: _t(
                                    BoxShadowTypesName[data?.type ?? 'normal']
                                ),
                            }}
                            onChange={(d) => {
                                onChangeData('type')(d?.value ?? '');
                            }}
                            options={BoxShadowTypes.map((type) => ({
                                value: type,
                                text: _t(BoxShadowTypesName[type ?? 'normal']),
                            }))}
                            styleTemplate={styleTemplateInputSelect}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('BoxShadow X')}
                        </Text>
                        <InputRange
                            min={-20}
                            max={20}
                            defaultValue={data.x}
                            onChange={onChangeData('x')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('BoxShadow Y')}
                        </Text>
                        <InputRange
                            min={-20}
                            max={20}
                            defaultValue={data.y}
                            onChange={onChangeData('y')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('BoxShadow Blur')}
                        </Text>
                        <InputRange
                            min={0}
                            max={20}
                            defaultValue={data.blur}
                            onChange={onChangeData('blur')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('BoxShadow Size')}
                        </Text>
                        <InputRange
                            min={0}
                            max={20}
                            defaultValue={data.size}
                            onChange={onChangeData('size')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('BoxShadow Color')}
                        </Text>
                        <InputColor
                            defaultValue={data.color}
                            onChange={onChangeData('color')}
                            styleTemplate={styleTemplateInputColor}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default ConfigBoxShadowBase;
