# ConfigBoxShadow

## Dependencies

[ConfigBoxShadow](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/ConfigBoxShadow)

```js
import { ConfigBoxShadow } from '@/components/Input/ConfigBoxShadow';
```

## Import

```js
import {
    ConfigBoxShadow,
    ConfigBoxShadowStyles,
} from '@/components/Input/ConfigBoxShadow';
```

## Props

```tsx
interface ConfigBoxShadowProps {}
```

## Use

```js
<ConfigBoxShadow>ConfigBoxShadow</Input/ConfigBoxShadow>
```
