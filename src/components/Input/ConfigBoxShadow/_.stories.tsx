import { Story, Meta } from "@storybook/react";

import { ConfigBoxShadowProps, ConfigBoxShadow } from "./index";

export default {
    title: "Input/ConfigBoxShadow",
    component: ConfigBoxShadow,
} as Meta;

const ConfigBoxShadowIndex: Story<ConfigBoxShadowProps> = (args) => (
    <ConfigBoxShadow {...args}>Test Children</ConfigBoxShadow>
);

export const Index = ConfigBoxShadowIndex.bind({});
Index.args = {};
