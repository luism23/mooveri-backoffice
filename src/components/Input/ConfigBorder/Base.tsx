import { useData } from 'fenextjs-hook/cjs/useData';
import { BorderConfig, BorderTypes } from '@/interfaces/Border';
import { useLang } from '@/lang/translate';
import { InputRange, InputRangeStyles } from '../Range';
import Theme, { ThemesType } from '@/config/theme';
import { Text, TextStyles } from '@/components/Text';
import { InputColor, InputColorStyles } from '../Color';
import { InputSelect } from '../Select';
import { InputTextStyles } from '../Text';

export interface ConfigBorderClassProps {
    classNameContent?: string;
    classNameContentConfig?: string;

    classNameContentConfigInput?: string;
    styleTemplateTitleInput?: TextStyles | ThemesType;
    styleTemplateInputRange?: InputRangeStyles | ThemesType;
    styleTemplateInputColor?: InputColorStyles | ThemesType;
    styleTemplateInputSelect?: InputTextStyles | ThemesType;
}

export interface ConfigBorderBaseProps {
    defaultValue?: BorderConfig;
    value?: BorderConfig;
    useValue?: boolean;
    onChange?: (data: BorderConfig) => void;
}

export interface ConfigBorderProps
    extends ConfigBorderClassProps,
        ConfigBorderBaseProps {}

export const ConfigBorderBase = ({
    classNameContent = '',
    classNameContentConfig = '',

    classNameContentConfigInput = '',
    styleTemplateTitleInput = Theme?.styleTemplate ?? '_default',
    styleTemplateInputRange = Theme?.styleTemplate ?? '_default',
    styleTemplateInputColor = Theme?.styleTemplate ?? '_default',
    styleTemplateInputSelect = Theme?.styleTemplate ?? '_default',

    defaultValue = {
        color: 'white',
        size: 0,
        type: 'solid',
    },
    value = {
        color: 'white',
        size: 0,
        type: 'solid',
    },
    useValue = false,
    onChange,
}: ConfigBorderProps) => {
    const _t = useLang();
    const { dataMemo: data = defaultValue, onChangeData } = useData<
        BorderConfig,
        BorderConfig
    >(defaultValue, {
        onChangeDataAfter: onChange,
        onMemo: (data) => {
            return useValue ? value : data;
        },
    });

    return (
        <>
            <div className={classNameContent}>
                <div className={classNameContentConfig}>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Border Type')}
                        </Text>
                        <InputSelect
                            defaultValue={{
                                value: data?.type ?? '',
                                text: data?.type ?? '',
                            }}
                            value={data.type}
                            onChange={(d) => {
                                onChangeData('type')(d?.value ?? '');
                            }}
                            options={BorderTypes.map((type) => ({
                                value: type,
                                text: type,
                            }))}
                            styleTemplate={styleTemplateInputSelect}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Border Size')}
                        </Text>
                        <InputRange
                            min={0}
                            max={10}
                            defaultValue={data.size}
                            value={data.size}
                            useValue={true}
                            onChange={onChangeData('size')}
                            styleTemplate={styleTemplateInputRange}
                        />
                    </div>
                    <div className={classNameContentConfigInput}>
                        <Text styleTemplate={styleTemplateTitleInput}>
                            {_t('Border Color')}
                        </Text>
                        <InputColor
                            defaultValue={data.color}
                            value={data.color}
                            useValue={true}
                            onChange={onChangeData('color')}
                            styleTemplate={styleTemplateInputColor}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default ConfigBorderBase;
