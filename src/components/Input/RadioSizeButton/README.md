# RadioSizeButton

## Dependencies

[RadioSizeButton](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/RadioSizeButton)

```js
import { RadioSizeButton } from '@/components/Input/RadioSizeButton';
```

## Import

```js
import {
    RadioSizeButton,
    RadioSizeButtonStyles,
} from '@/components/Input/RadioSizeButton';
```

## Props

```tsx
interface RadioSizeButtonProps {}
```

## Use

```js
<RadioSizeButton>RadioSizeButton</Input/RadioSizeButton>
```
