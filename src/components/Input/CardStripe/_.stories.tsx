import { Story, Meta } from "@storybook/react";

import { CardStripeProps, CardStripe } from "./index";

export default {
    title: "Input/CardStripe",
    component: CardStripe,
} as Meta;

const CardStripeIndex: Story<CardStripeProps> = (args) => (
    <CardStripe {...args}>Test Children</CardStripe>
);

export const Index = CardStripeIndex.bind({});
Index.args = {
};
