import { InputScanIAClassProps } from '@/components/Input/ScanIA/Base';

export const mooveri: InputScanIAClassProps = {
    styleTemplateButton: 'mooveriSearchScan',
    styleTemplateListItemScan: 'mooveri',
    styleTemplateListItemScan2: 'mooveri2',
    classNameContentScaner: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        z-index-10
        bg-black-50
        box-shadow
        box-shadow-s-50
        box-shadow-y-50
        box-shadow-c-black
    `,
    classNameContentScanerVideo: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        z-index-10
        bg-black
        gap-0
        grid-rows-10
        box-shadow
        box-shadow-s-50
        box-shadow-y-50
        box-shadow-c-black
    `,
    classNameContentVideo: `
        pos-r
        width-p-100
        grid-B
    `,
    classNameScanerVideo: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        object-fit-cover
    `,
    classNameScanerCanvas: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        z-index-10
        opacity-0
    `,
    classNameContentLoader: `
        pos-f
        inset-0
        width-p-100
        height-p-100
        z-index-10
        flex
        flex-align-center
        flex-justify-center
        color-white
    `,
    classNameContentCloseCamera: `
        p-15
        grid-A
        bg-black
        flex
        flex-justify-between
        flex-align-center
    `,
    classNameBtnClose: `
        color-white
        border-radius-15
        bg-red-
        p-h-5
        p-v-5
        flex
        flex-justify-between
        flex-align-center
        column-gap-5
        font-w-900
        font-14
    `,
    classNameBtnDone: `
        color-white
        border-radius-15
        bg-gradient-greenishCyan2-metallicBlue2
        p-h-25
        p-v-5
        font-w-900
        font-14
    `,
    classNameBtnDoneInactive: `
        transform
        transform-scale-X-0  
    `,
    classNameError: `

        pos-f
        inset-0
        width-p-100
        height-p-100
        z-index-10
        font-20
        text-center
        flex-align-center
        flex-justify-center
        bg-red
        color-white
        font-w-700
    `,
    classNameContenBox: `
        pos-a
        inset-0
        width-p-100
        height-p-100
        z-index-10
    `,
    classNameBox: `
        bg-transparent
        pos-a
        z-index-10
        border-3
        border-style-solid
        border-greenish-cyan
        flex
        flex-align-center
        flex-justify-center
        border-radius-15

        animation
        animation-boxScan
        animation-fill-mode-forwards
        animation-duration-20
        animation-iteration-count-1
    `,
    classNameAdd: `
        p-v-15
        border-radius-15
        bg-black-50
        color-white
        p-5
        flex
        flex-align-center
        flex-justify-center
        flex-nowrap
        column-gap-10
        text-center
        font-14
        font-nunito
    `,
    classNameContentListItemScanAfterVideo: `
        grid-C
        overflow-auto
        p-t-10
    `,
    classNameContentItems: `
        width-p-90
        m-h-auto
    `,
    classNameContentItemsTitle: `
        color-white
        p-v-5
        font-w-900
        font-14
    `,
};
