import { Story, Meta } from "@storybook/react";

import { InputFile,InputFileProps } from "./index";

export default {
    title: "Input/InputFile",
    component: InputFile,
} as Meta;

const Template: Story<InputFileProps> = (args) => (
    <InputFile {...args}>Test Children</InputFile>
);

export const Index = Template.bind({});
Index.args = {
    
};
