import { useState, PropsWithChildren, useEffect, useRef } from 'react';

import { ProgressLinear } from '@/components/Progress/Linear';
import log from '@/functions/log';
import { UploadFile } from '@/firebase';
import { useLang } from '@/lang/translate';
import { FileData } from '@/interfaces/File';

export interface InputFileClassProps {
    classNameLabel?: string;
    classNameLabelUpload?: string;
    classNameLabelNotUpload?: string;
    classNameInput?: string;
    classNameError?: string;
    classNameLabelError?: string;
    classNameContentProgress?: string;
}

export interface InputFileDataProps extends FileData {}

export interface InputFileBaseProps {
    accept?: string[] | 'ALL';
    returnUrl?: boolean;
    defaultValue?: string;
    defaultText?: string;
    onChange?: (v: InputFileDataProps) => void;
    clearAfterUpload?: boolean;
    MAX_SIZE_FILE?: number;
    openFileClick?: boolean;
    fileText?: boolean;
}

export interface InputFileProps
    extends InputFileBaseProps,
        InputFileClassProps {}

export const InputFileBase = ({
    defaultValue = '',
    defaultText = '',
    classNameLabel = '',
    classNameLabelUpload = '',
    classNameLabelNotUpload = '',
    classNameInput = '',
    classNameError = '',
    classNameLabelError = '',
    classNameContentProgress = '',
    onChange = (v: InputFileDataProps) => {
        log('onChange File', v, 'aqua');
    },
    returnUrl = true,
    accept = ['png', 'jpg', 'pdf'],
    children,
    clearAfterUpload = false,
    MAX_SIZE_FILE = 5000000,
    openFileClick = false,
    fileText = false,
}: PropsWithChildren<InputFileProps>) => {
    const [error, setError] = useState('');
    const ref = useRef<any>(null);
    const _t = useLang();

    const [text, setText] = useState(defaultText ?? '');
    const [fileData, setFileData] = useState<any>(defaultValue ?? '');
    const [progress, setProgress] = useState(-1);
    const validateAccept = (nameFile: string) => {
        if (accept == 'ALL') {
            return true;
        }
        const extend = nameFile.split('.').pop();
        if (extend && !accept.includes(extend)) {
            const error = {
                message: 'File Invalid',
            };
            log('error', error, 'red');
            setError(error.message);
            setProgress(-2);
            setFileData('');
            return false;
        }
        return true;
    };
    const updateProgress = (e: any) => {
        const loadingPercentage: number = parseFloat(
            ((100 * e.loaded) / e.total).toFixed(0)
        );
        setProgress(loadingPercentage);
    };
    const toBase64 = (file: any) =>
        new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.addEventListener('progress', updateProgress);
            reader.onload = () => resolve(reader.result);
            reader.onerror = (error) => reject(error);
            if (fileText) {
                reader.readAsText(file);
            } else {
                reader.readAsDataURL(file);
            }
        });
    const uploadFile = async (e: any) => {
        const file = e.target.files[0];
        setError('');

        if (!file) {
            setProgress(-2);
            setFileData('');
        } else {
            if (file?.size > MAX_SIZE_FILE) {
                alert(`${_t('Tamaño Maximo')} ${MAX_SIZE_FILE / 1000000}mb`);
            } else {
                const nameFile = e.target.value.split('\\').pop();
                const v = validateAccept(nameFile);
                if (!v) {
                    return;
                }
                setText(nameFile);
                if (returnUrl) {
                    UploadFile(file, setProgress, setFileData);
                } else {
                    const fileUrl = await toBase64(file);
                    setFileData(fileUrl);
                }
            }
        }
        if (clearAfterUpload) {
            e.target.value = null;
            e.target.type = 'text';

            setTimeout(() => {
                e.target.type = 'file';
            }, 100);
            if (ref?.current) {
                ref.current.value = null;
            }
        }
    };
    useEffect(() => {
        if (fileData != defaultValue) {
            onChange({
                text,
                fileData,
            });
        }
    }, [text, fileData]);

    const onClickLabel = () => {
        if (openFileClick && fileData != '') {
            window.open(fileData, 'blank');
        }
    };

    return (
        <>
            <label
                className={`${classNameLabel}
                    ${
                        fileData != ''
                            ? classNameLabelUpload
                            : classNameLabelNotUpload
                    }
                    ${error != '' ? classNameLabelError : ''}
                `}
                onClick={onClickLabel}
            >
                <input
                    ref={ref}
                    type="file"
                    className={`input ${classNameInput}`}
                    onChange={uploadFile}
                    accept={
                        accept == 'ALL'
                            ? undefined
                            : accept.map((e: string) => `.${e}`).join(',')
                    }
                    hidden
                />
                {children}
            </label>
            {progress > 0 && progress < 100 && (
                <>
                    <div className={classNameContentProgress}>
                        <ProgressLinear p={progress} />
                    </div>
                </>
            )}
            {error != '' && (
                <div className={`classNameErrorAvatar ${classNameError}`}>
                    {_t(error)}
                </div>
            )}
        </>
    );
};
export default InputFileBase;
