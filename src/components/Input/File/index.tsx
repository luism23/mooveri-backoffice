import { PropsWithChildren, useMemo } from 'react';

import {
    InputFileBase,
    InputFileBaseProps,
} from '@/components/Input/File/Base';

import * as styles from '@/components/Input/File/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputFileStyle = { ...styles } as const;
export type InputFileStyles = keyof typeof InputFileStyle;

export interface InputFileProps extends InputFileBaseProps {
    styleTemplate?: InputFileStyles | ThemesType;
}

export const InputFile = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: PropsWithChildren<InputFileProps>) => {
    const Style = useMemo(
        () =>
            InputFileStyle[styleTemplate as InputFileStyles] ??
            InputFileStyle._default,
        [styleTemplate]
    );
    return (
        <>
            <InputFileBase {...props} {...Style} />
        </>
    );
};
export default InputFile;
