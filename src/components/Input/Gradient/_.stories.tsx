import { Story, Meta } from "@storybook/react";

import { InputGradientProps, InputGradient } from "./index";

export default {
    title: "Input/InputGradient",
    component: InputGradient,
} as Meta;

const InputGradientIndex: Story<InputGradientProps> = (args) => (
    <InputGradient {...args}>Test Children</InputGradient>
);

export const Index = InputGradientIndex.bind({});
Index.args = {};
