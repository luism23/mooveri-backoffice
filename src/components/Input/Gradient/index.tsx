import { useMemo } from 'react';

import * as styles from '@/components/Input/Gradient/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InputGradientBaseProps,
    InputGradientBase,
} from '@/components/Input/Gradient/Base';

export const InputGradientStyle = { ...styles } as const;

export type InputGradientStyles = keyof typeof InputGradientStyle;

export interface InputGradientProps extends InputGradientBaseProps {
    styleTemplate?: InputGradientStyles | ThemesType;
}

export const InputGradient = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputGradientProps) => {
    const Style = useMemo(
        () =>
            InputGradientStyle[styleTemplate as InputGradientStyles] ??
            InputGradientStyle._default,
        [styleTemplate]
    );

    return <InputGradientBase {...Style} {...props} />;
};
export default InputGradient;
