# InputGradient

## Dependencies

[InputGradient](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/InputGradient)

```js
import { InputGradient } from '@/components/Input/InputGradient';
```

## Import

```js
import {
    InputGradient,
    InputGradientStyles,
} from '@/components/Input/InputGradient';
```

## Props

```tsx
interface InputGradientProps {}
```

## Use

```js
<InputGradient>InputGradient</Input/InputGradient>
```
