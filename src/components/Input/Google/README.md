# Google

## Dependencies

[Google](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/Google)

```js
import { Google } from '@/components/Input/Google';
```

## Import

```js
import { Google, GoogleStyles } from '@/components/Input/Google';
```

## Props

```tsx
interface GoogleProps {}
```

## Use

```js
<Google>Google</Input/Google>
```
