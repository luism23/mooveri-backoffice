import { Story, Meta } from "@storybook/react";

import { GoogleProps, Google } from "./index";
import { InputGoogleMovingFrom } from "@/components/Input/Google/Template/InputGoogleMovingFrom"
import { InputGoogleMovingTo } from "@/components/Input/Google/Template/InputGoogleMovingTo"

import { InputGoogleConfirm } from "@/components/Input/Google/Template/InputGoogleConfirm"


export default {
    title: "Input/Google",
    component: Google,
} as Meta;

const GoogleIndex: Story<GoogleProps> = (args) => (
    <Google {...args}>Test Children</Google>
);

export const Index = GoogleIndex.bind({});
Index.args = {};


const InputGoogleMovingFromIndex: Story<GoogleProps> = (args) => (
    <InputGoogleMovingFrom {...args}>Test Children</InputGoogleMovingFrom>
);

export const Input_Google_Moving_From = InputGoogleMovingFromIndex.bind({});
Input_Google_Moving_From.args = {
    
};

const InputGoogleMovingToIndex: Story<GoogleProps> = (args) => (
    <InputGoogleMovingTo {...args}>Test Children</InputGoogleMovingTo>
);

export const Input_Google_Moving_to = InputGoogleMovingToIndex.bind({});
Input_Google_Moving_to.args = {
    
};

const InputGoogleConfirmIndex: Story<GoogleProps> = (args) => (
    <InputGoogleConfirm {...args}>Test Children</InputGoogleConfirm>
);

export const Input_Google_Confirm = InputGoogleConfirmIndex.bind({});

Input_Google_Confirm.args = {

};

