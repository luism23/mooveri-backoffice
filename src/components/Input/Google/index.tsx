import { useMemo } from 'react';

import * as styles from '@/components/Input/Google/styles';

import { Theme, ThemesType } from '@/config/theme';

import { GoogleBaseProps, GoogleBase } from '@/components/Input/Google/Base';

export const GoogleStyle = { ...styles } as const;

export type GoogleStyles = keyof typeof GoogleStyle;

export interface GoogleProps extends GoogleBaseProps {
    styleTemplate?: GoogleStyles | ThemesType;
}

export const Google = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: GoogleProps) => {
    const Style = useMemo(
        () =>
            GoogleStyle[styleTemplate as GoogleStyles] ?? GoogleStyle._default,
        [styleTemplate]
    );

    return <GoogleBase {...Style} {...props} />;
};
export default Google;
