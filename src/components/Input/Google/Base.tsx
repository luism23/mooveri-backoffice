import { GoogleAddres } from '@/interfaces/Google/addres';
import { LoadScript, Autocomplete } from '@react-google-maps/api';
import { useEffect, useState } from 'react';
import InputText, { InputTextStyles } from '@/components/Input/Text';
import { InputTextBaseProps } from '@/components/Input/Text/Base';
import Theme, { ThemesType } from '@/config/theme';

export interface GoogleClassProps {
    styleTemplateInput?: InputTextStyles | ThemesType;
    classNameAutocomplete?: string;
}

export interface GoogleBaseProps extends Omit<InputTextBaseProps, 'onChange'> {
    onChange?: (addres: GoogleAddres) => any;
    defaultAddress?: GoogleAddres;
}

export interface GoogleProps extends GoogleClassProps, GoogleBaseProps {}

export const GoogleBase = ({
    styleTemplateInput = Theme.styleTemplate ?? '_default',
    classNameAutocomplete = '',

    defaultAddress,
    onChange,
    ...props
}: GoogleProps) => {
    const [location, setLocation] = useState<GoogleAddres>({
        formatedAddress: '',
        lat: 0,
        lng: 0,
        ...defaultAddress,
    });
    const [autocompleteValue, setAutocompleteValue] =
        useState<google.maps.places.Autocomplete | null>(null);
    const onLoad = (autocomplete: google.maps.places.Autocomplete) => {
        setAutocompleteValue(autocomplete);
    };
    const onPlaceChanged = () => {
        if (autocompleteValue) {
            const lat =
                autocompleteValue?.getPlace().geometry?.location?.lat() ?? 0;
            const lng =
                autocompleteValue?.getPlace().geometry?.location?.lng() ?? 0;
            const formatedAddress =
                autocompleteValue.getPlace().formatted_address ?? '';
            setLocation({ formatedAddress, lat, lng });
        }
    };

    useEffect(() => {
        onChange?.(location);
    }, [location]);

    return (
        <>
            <LoadScript
                googleMapsApiKey={
                    process?.env?.['NEXT_PUBLIC_GOOGLE_KEY'] ?? ''
                }
                libraries={['places', 'geometry']}
            >
                <Autocomplete
                    onLoad={onLoad}
                    onPlaceChanged={onPlaceChanged}
                    className={classNameAutocomplete}
                >
                    <InputText
                        {...props}
                        styleTemplate={styleTemplateInput}
                        type="text"
                        placeholder={props.placeholder ?? 'Search your address'}
                        onChange={(e: string) => {
                            setLocation((pre) => ({
                                ...pre,
                                formatedAddress: e,
                            }));
                        }}
                        value={location?.formatedAddress}
                    />
                </Autocomplete>
            </LoadScript>
        </>
    );
};
export default GoogleBase;
