import {
    InputTextBase,
    InputTextBaseProps,
} from '@/components/Input/Text/Base';
import * as styles from '@/components/Input/Text/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InputTextStyle = { ...styles } as const;
export type InputTextStyles = keyof typeof InputTextStyle;

export interface InputTextProps extends InputTextBaseProps {
    styleTemplate?: InputTextStyles | ThemesType;
}

export const InputText = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputTextProps) => {
    return (
        <>
            <InputTextBase
                {...props}
                {...(InputTextStyle[styleTemplate as InputTextStyles] ??
                    InputTextStyle._default)}
            />
        </>
    );
};
export default InputText;
