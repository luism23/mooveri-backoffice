import { Story, Meta } from "@storybook/react";

import { SelectProps, InputSelect } from "./index";
import {InputSelectMoveSize } from "./Template/InputDateMoveSize";
import {InputDateSearch } from "./Template/InputDateSearch";


export default {
    title: "Input/Select",
    component: InputSelect,
} as Meta;

const Template: Story<SelectProps> = (args) => (
    <InputSelect {...args}>Test Children</InputSelect>
);

export const Index = Template.bind({});
Index.args = {
    options: [
        {
            text: "Test Option1",
            value: "test1",
        },
        {
            text: "Test Option2",
            value: "test2",
        },
        {
            text: "Test Option3",
            value: "test3",
        },
        {
            text: "Test Option4",
            value: "test4",
        },
    ],
};


const InputDateSearchIndex: Story<SelectProps> = (args) => (
    <InputSelectMoveSize {...args}>Test Children</InputSelectMoveSize>
);

export const Input_Date_Search = InputDateSearchIndex.bind({});
Input_Date_Search.args = {
    
};


const InputDateMoveSizeIndex: Story<SelectProps> = (args) => (
    <InputDateSearch {...args}>Test Children</InputDateSearch>
);

export const Input_Date_Move_Size = InputDateMoveSizeIndex.bind({});
Input_Date_Move_Size.args = {
    
};
