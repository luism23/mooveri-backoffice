import {
    InputText,
    InputTextProps,
    InputTextStyle,
    InputTextStyles,
} from '@/components/Input/Text';
import { useEffect, useMemo, useRef, useState } from 'react';

import { Theme } from '@/config/theme';

import { ArrowSelect } from '@/svg/arrowSelect';
import { toCapitalize } from '@/functions/toCapitalize';

export interface SelectOptionProps {
    value: string;
    text: string;
    html?: any;
    classNameOption?: string;
    title?: string;
    data?: any;
}
export interface SelectProps
    extends Omit<
        InputTextProps,
        'defaultValue' | 'validateValue' | 'onChange'
    > {
    options?: SelectOptionProps[];
    defaultValue?: SelectOptionProps;
    validateValue?: (data: SelectOptionProps) => Promise<void> | void;
    onChange?: (data: SelectOptionProps) => void;
    idSelect?: string;
    nameSelect?: string;
    useSelectMovil?: boolean;
}

export const InputSelect = ({
    options = [],
    defaultValue,
    validateValue,
    onChange,
    styleTemplate = Theme?.styleTemplate ?? '_default',
    disabled = false,
    value,
    idSelect,
    nameSelect,
    useSelectMovil = true,
    ...props
}: SelectProps) => {
    const ul = useRef(null);
    const [valueInput, setValueInput] = useState(defaultValue?.text ?? '');
    const [optionSelect, setOptionSelect] =
        useState<SelectOptionProps[]>(options);
    const [showOptions, setShowOptions] = useState(false);

    const select = async (v: SelectOptionProps) => {
        await validateValue?.(v);
        setValueInput(v.text);
        onChange?.(v);
        setShowOptions(false);
    };
    useEffect(() => {
        setOptionSelect(
            options.filter((e) => {
                return (
                    e.text
                        .toLocaleLowerCase()
                        .indexOf((valueInput ?? '').toLocaleLowerCase()) != -1
                );
            })
        );
    }, [valueInput]);

    const maxHeightUl = () => {
        if (ul.current && window) {
            const elementUl: any = ul.current;

            let e = elementUl.parentElement;
            while (e) {
                e = e.parentElement;

                const isHidden =
                    e?.style?.overflow == 'hidden' ||
                    e?.classList?.contains('overflow-hidden');
                if (isHidden) {
                    break;
                }
            }
            const heightParent = e?.offsetHeight;

            const topInput = elementUl.getBoundingClientRect().y;
            const heightL =
                e?.getBoundingClientRect().y + heightParent - topInput;

            elementUl.style.maxHeight = `min( calc(100vh - ${
                topInput + 10
            }px) , ${heightL}px )`;
        }
    };

    useEffect(() => {
        if (showOptions) {
            maxHeightUl();
        }
    }, [showOptions]);

    const StyleSelect = useMemo(
        () =>
            InputTextStyle[(styleTemplate as InputTextStyles) ?? '_default'] ??
            InputTextStyle._default,
        [styleTemplate]
    );

    return (
        <>
            <InputText
                {...props}
                disabled={disabled}
                useLoader={false}
                value={value ?? valueInput}
                onChange={setValueInput}
                styleTemplate={styleTemplate}
                props={{
                    onMouseDown: () => {
                        if (disabled) {
                            return;
                        }
                        setShowOptions(true);
                        setOptionSelect(options);
                    },
                    onInput: () => {
                        if (disabled) {
                            return;
                        }
                        setShowOptions(true);
                    },
                }}
                icon={
                    <>
                        {!disabled ? (
                            <span
                                onClick={() => {
                                    if (disabled) {
                                        return;
                                    }
                                    setShowOptions(true);
                                    setOptionSelect(options);
                                }}
                                className="d-flex height-p-100"
                            >
                                <ArrowSelect size={12} />
                            </span>
                        ) : (
                            <></>
                        )}
                    </>
                }
                extraInContentInput={
                    <>
                        {props.extraInContentInput}
                        {useSelectMovil ? (
                            <select
                                id={idSelect}
                                name={nameSelect}
                                className="pos-a top-0 left-0 width-p-100 height-p-100 opacity-0 d-sm-none"
                                style={{ opacity: 0 }}
                                onChange={(e) => {
                                    select(options[parseInt(e.target.value)]);
                                }}
                                disabled={disabled}
                            >
                                {options.length == 0 ? (
                                    <option disabled={true}>Not options</option>
                                ) : (
                                    <option disabled={true} selected={true}>
                                        Select
                                    </option>
                                )}
                                {options.map((e, i) => {
                                    return (
                                        <option
                                            key={i}
                                            value={i}
                                            onClick={() => {
                                                select(e);
                                            }}
                                            className={`text-capitalize ${e.classNameOption}`}
                                        >
                                            {toCapitalize(e.text)}
                                        </option>
                                    );
                                })}
                            </select>
                        ) : (
                            <></>
                        )}
                    </>
                }
                extraInLabel={
                    <>
                        {showOptions && (
                            <ul
                                ref={ul}
                                className={StyleSelect.classNameOptions}
                                onMouseLeave={() => {
                                    setShowOptions(false);
                                }}
                            >
                                {optionSelect.length == 0 ? (
                                    <li
                                        className={`${StyleSelect.classNameOption} ${StyleSelect.classNameOptionDisabled} disabled`}
                                    >
                                        Not options
                                    </li>
                                ) : (
                                    <li
                                        className={`${StyleSelect.classNameOption} ${StyleSelect.classNameOptionDisabled} disabled`}
                                    >
                                        Select
                                    </li>
                                )}
                                {optionSelect.map((e, i) => {
                                    return (
                                        <li
                                            key={i}
                                            className={`${
                                                StyleSelect.classNameOption
                                            }
                                                ${e.classNameOption ?? ''}
                                                `}
                                            onClick={() => {
                                                select(e);
                                            }}
                                        >
                                            {e.html ?? e.text}
                                        </li>
                                    );
                                })}
                            </ul>
                        )}
                    </>
                }
            />
        </>
    );
};
export default InputSelect;
