import { InputSelect, SelectProps } from '@/components/Input/Select';
import Date from '@/svg/date';

export const InputSelectMoveSize = ({ ...props }: SelectProps) => {
    return (
        <InputSelect
            {...props}
            placeholder="Select Size"
            label="Move Size"
            icon={<Date size={15} />}
            options={[
                {
                    text: 'Studio',
                    value: 'studio',
                },
                {
                    text: 'Studio Alcove',
                    value: 'studioAlcove',
                },
                {
                    text: '1 Bedroom Small',
                    value: 'bedroomSmall1',
                },
                {
                    text: '1 Bedroom Large',
                    value: 'bedroomLarge1',
                },
                {
                    text: '2 Bedrooms',
                    value: 'bedrooms2',
                },
                {
                    text: '3 Bedrooms',
                    value: 'bedrooms3',
                },
                {
                    text: '4+ Bedrooms',
                    value: 'bedrooms4Plus',
                },
            ]}
        />
    );
};
export default InputSelectMoveSize;
