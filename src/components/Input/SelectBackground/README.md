# InputSelectBackground

## Dependencies

[InputSelectBackground](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/InputSelectBackground)

```js
import { InputSelectBackground } from '@/components/Input/InputSelectBackground';
```

## Import

```js
import {
    InputSelectBackground,
    InputSelectBackgroundStyles,
} from '@/components/Input/InputSelectBackground';
```

## Props

```tsx
interface InputSelectBackgroundProps {}
```

## Use

```js
<InputSelectBackground>InputSelectBackground</Input/InputSelectBackground>
```
