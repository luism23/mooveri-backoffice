import { useMemo } from 'react';

import * as styles from '@/components/Input/SelectBackground/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    InputSelectBackgroundBaseProps,
    InputSelectBackgroundBase,
} from '@/components/Input/SelectBackground/Base';

export const InputSelectBackgroundStyle = { ...styles } as const;

export type InputSelectBackgroundStyles =
    keyof typeof InputSelectBackgroundStyle;

export interface InputSelectBackgroundProps
    extends InputSelectBackgroundBaseProps {
    styleTemplate?: InputSelectBackgroundStyles | ThemesType;
}

export const InputSelectBackground = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: InputSelectBackgroundProps) => {
    const Style = useMemo(
        () =>
            InputSelectBackgroundStyle[
                styleTemplate as InputSelectBackgroundStyles
            ] ?? InputSelectBackgroundStyle._default,
        [styleTemplate]
    );

    return <InputSelectBackgroundBase {...Style} {...props} />;
};
export default InputSelectBackground;
