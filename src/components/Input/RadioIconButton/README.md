# RadioIconButton

## Dependencies

[RadioIconButton](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/Input/RadioIconButton)

```js
import { RadioIconButton } from '@/components/Input/RadioIconButton';
```

## Import

```js
import {
    RadioIconButton,
    RadioIconButtonStyles,
} from '@/components/Input/RadioIconButton';
```

## Props

```tsx
interface RadioIconButtonProps {}
```

## Use

```js
<RadioIconButton>RadioIconButton</Input/RadioIconButton>
```
