import { ContentWidth, ContentWidthProps } from '@/components/ContentWidth';
import { Image, ImageProps } from '@/components/Image';
import Theme, { ThemesType } from '@/config/theme';
import { useData } from 'fenextjs-hook/cjs/useData';
import {
    IconButton as RadioIconButton,
    IconButtonType,
    IconButtonValuesType,
} from '@/interfaces/Button';
import { useLang } from '@/lang/translate';
import { InputRadio, InputRadioStyles } from '../Radio';

export interface RadioIconButtonClassProps {
    ContentWidthProps?: ContentWidthProps;
    styleTemplateRadius?: InputRadioStyles | ThemesType;
    classNameBtn?: string;
    classNameBtnActive?: string;
    classNameBtnInactive?: string;
    classNameBtnTypes?: IconButtonValuesType;
    ImageProps?: ImageProps;
}

export interface RadioIconButtonBaseProps {
    defaultValue?: IconButtonType;
    onChange?: (value: IconButtonType) => void;
}

export interface RadioIconButtonProps
    extends RadioIconButtonClassProps,
        RadioIconButtonBaseProps {}

export const RadioIconButtonBase = ({
    ContentWidthProps = {},
    styleTemplateRadius = Theme.styleTemplate ?? '_default',
    classNameBtn = '',
    classNameBtnActive = '',
    classNameBtnInactive = '',
    classNameBtnTypes = {
        sin: '',
        con: '',
    },
    ImageProps = {},

    defaultValue = 'sin',
    ...props
}: RadioIconButtonProps) => {
    const _t = useLang();
    const { data, onChangeData } = useData<{ value: number }>(
        { value: RadioIconButton.indexOf(defaultValue) },
        {
            onChangeDataAfter: ({ value }) => {
                props?.onChange?.(RadioIconButton?.[value]);
            },
        }
    );
    return (
        <>
            <ContentWidth {...ContentWidthProps}>
                <InputRadio
                    onChange={onChangeData('value')}
                    selectedRadioDefault={data.value}
                    options={RadioIconButton.map((value, i) => ({
                        label: (
                            <div
                                className={`${classNameBtn} ${
                                    data.value == i
                                        ? classNameBtnActive
                                        : classNameBtnInactive
                                } `}
                            >
                                <Image
                                    {...ImageProps}
                                    className={`${
                                        classNameBtnTypes?.[value] ?? ''
                                    } ${ImageProps.className}`}
                                />
                                {_t(value + ' icono')}
                            </div>
                        ),
                    }))}
                    styleTemplate={styleTemplateRadius}
                />
            </ContentWidth>
        </>
    );
};
export default RadioIconButtonBase;
