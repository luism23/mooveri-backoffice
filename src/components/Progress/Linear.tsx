import { useMemo } from 'react';

const BaseStyle = {
    classNameContent: `
        width-p-100 
        border-radius-15
        box-shadow box-shadow-inset box-shadow-s-2
        flex
        bg-white 
    `,
    classNameBar: `
        text-center 
        font-montserrat
        border-radius-10 
        height-20
    `,
};

const ProgressLinearStyleColor: {
    [key: string]: typeof BaseStyle;
} = {};
const ProgressLinearStyleColors = [
    'blue',
    'indigo',
    'purple',
    'pink',
    'red',
    'orange',
    'yellow',
    'green',
    'teal',
    'cyan',
    'black',
    'gray',
    'dark',
    'bunker',
];
ProgressLinearStyleColors.forEach((color) => {
    ProgressLinearStyleColor[color] = {
        classNameContent: `
            ${BaseStyle.classNameContent}
            border-${color} 
            bg-light 
            box-shadow-c-${color}
        `,
        classNameBar: `
            ${BaseStyle.classNameBar}
            bg-${color} 
            color-white
        `,
    };
});
export const ProgressLinearStyle: {
    [key: string]: typeof BaseStyle;
} = {
    default: {
        classNameContent: `
            ${BaseStyle.classNameContent}
            bg-white 
            box-shadow-c-gray
        `,
        classNameBar: `
            ${BaseStyle.classNameBar}
            bg-gray 
            color-white
        `,
    },
    ...ProgressLinearStyleColor,
};

export const ProgressLinearStyles = Object.keys(ProgressLinearStyle);

export interface ProgressLinearProps {
    styleTemplate?: string;
    p: number;
}

export const ProgressLinear = ({
    p = 50,
    styleTemplate = 'default',
    ...props
}: ProgressLinearProps) => {
    const config = {
        ...(ProgressLinearStyle[styleTemplate] ?? ProgressLinearStyle.default),
        ...props,
    };

    const Cp = useMemo(() => Math.min(Math.max(p, 0), 100).toFixed(2), [p]);

    return (
        <>
            <div className={config.classNameContent}>
                <div className={config.classNameBar} style={{ width: `${p}%` }}>
                    {Cp}%
                </div>
            </div>
        </>
    );
};
export default ProgressLinear;
