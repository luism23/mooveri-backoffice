import { Story, Meta } from "@storybook/react";

import { PaginationProps, Pagination } from "./index";

export default {
    title: "Pagination/Pagination",
    component: Pagination,
} as Meta;

const PaginationIndex: Story<PaginationProps> = (args) => (
    <Pagination {...args}>Test Children</Pagination>
);

export const Index = PaginationIndex.bind({});
Index.args = {
    nItems:100,
};
