import { PaginationClassProps } from '@/components/Pagination/Base';

import {
    PaginationUp,
    PaginationPre,
    PaginationNext,
    PaginationDown,
} from '@/svg/pagination';

export const mooveri: PaginationClassProps = {
    classNameContent: `
        flex
        flex-align-center
        flex-gap-column-10
        flex-nowrap
    `,
    classNameUp: `
        cursor-pointer
        color-white
        color-greenish-cyan-hover
    `,
    classNamePre: `
        cursor-pointer
        color-white
        color-greenish-cyan-hover
        width-20
    `,
    classNameCurrent: `
        flex
        flex-align-center
        flex-gap-column-10
        flex-nowrap
    `,
    classNameNext: `
        cursor-pointer
        color-white
        color-greenish-cyan-hover
        
    `,
    classNameDown: `
        cursor-pointer
        color-white
        color-greenish-cyan-hover
        
    `,

    icons: {
        up: <PaginationUp />,
        pre: <PaginationPre size={10} />,
        next: <PaginationNext size={10} />,
        down: <PaginationDown />,
    },

    styleTemplateInput: 'mooveri2',
};
