export interface SpaceProps {
    size: number;
}

export const Space = ({ size }: SpaceProps) => {
    return (
        <>
            <div
                className="width-p-100"
                style={{ height: `${size / 16}rem` }}
            ></div>
        </>
    );
};
export default Space;
