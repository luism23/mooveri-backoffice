import Link from 'next/link';

import { InputAvatar, InputAvatarStyles } from '@/components/Input/Avatar';
import { ContentWidth } from '@/components/ContentWidth';
import { Text, TextStyles } from '@/components/Text';
import { Space } from '@/components/Space';

import { Theme, ThemesType } from '@/config/theme';

import { useLang } from '@/lang/translate';

import { Copy } from '@/svg/copy';
import { InfoProfileEditDataProps } from '@/components/InfoProfileEdit/Base';

export type InfoProfileBaseProps = InfoProfileEditDataProps;

export interface InfoProfileClassProps {
    classNameContent?: string;
    sizeContent?: number;
    classNameContentEditView?: string;
    classNameContentEdit?: string;
    classNameContentView?: string;
    styleTemplateAvatar?: InputAvatarStyles | ThemesType;
    styleTemplateDataName?: TextStyles | ThemesType;
    styleTemplateDataWeb?: TextStyles | ThemesType;
    classNameDataDesciption?: string;
    styleTemplateDataDesciption?: TextStyles | ThemesType;
}

export interface InfoProfileProps
    extends InfoProfileBaseProps,
        InfoProfileClassProps {}

export const InfoProfileBase = ({
    classNameContent = '',
    sizeContent,
    classNameContentEditView = '',
    classNameContentEdit = '',
    classNameContentView = '',
    styleTemplateAvatar = Theme?.styleTemplate ?? '_default',
    styleTemplateDataName = Theme?.styleTemplate ?? '_default',
    styleTemplateDataWeb = Theme?.styleTemplate ?? '_default',
    classNameDataDesciption = '',
    styleTemplateDataDesciption = Theme?.styleTemplate ?? '_default',

    avatar,
    name,
    web,
    description,
    fontColor,
}: InfoProfileProps) => {
    const _t = useLang();
    return (
        <>
            <ContentWidth className={classNameContent} size={sizeContent}>
                <div className={classNameContentEditView}>
                    <div className={classNameContentEdit}></div>
                    <Link href={`/${name}`}>
                        <a className={classNameContentView}>
                            <Copy />
                            {_t('Ver Perfil')}
                        </a>
                    </Link>
                </div>
                <InputAvatar
                    styleTemplate={styleTemplateAvatar}
                    defaultValue={{
                        fileData: avatar,
                        text: name,
                    }}
                />
                <Space size={12} />
                <Text
                    styleTemplate={styleTemplateDataName}
                    className="word-break-all"
                    style={fontColor ? { color: fontColor } : {}}
                >
                    {name}
                </Text>
                <Space size={6} />
                <Text
                    styleTemplate={styleTemplateDataWeb}
                    className="word-break-all"
                    style={fontColor ? { color: fontColor } : {}}
                >
                    <a
                        href={web}
                        target="_blank"
                        className="color-currentColor text-decoration-underline"
                        rel="noreferrer"
                    >
                        {web}
                    </a>
                </Text>
                <Space size={8} />
                <Text
                    styleTemplate={styleTemplateDataDesciption}
                    className={classNameDataDesciption}
                    style={fontColor ? { color: fontColor } : {}}
                >
                    {description}
                </Text>
            </ContentWidth>
        </>
    );
};
export default InfoProfileBase;
