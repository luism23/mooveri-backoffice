import { PropsWithChildren } from 'react';
import InfoProfileBase, {
    InfoProfileBaseProps,
} from '@/components/InfoProfile/Base';
import * as styles from '@/components/InfoProfile/styles';

import { Theme, ThemesType } from '@/config/theme';

export const InfoProfileStyle = { ...styles } as const;

export type InfoProfileStyles = keyof typeof InfoProfileStyle;

export interface InfoProfileProps extends InfoProfileBaseProps {
    styleTemplate?: InfoProfileStyles | ThemesType;
}

export const InfoProfile = ({
    styleTemplate = Theme.styleTemplate ?? '_default',
    ...props
}: PropsWithChildren<InfoProfileProps>) => {
    const config = {
        ...(InfoProfileStyle[styleTemplate as InfoProfileStyles] ??
            InfoProfileStyle._default),
        ...props,
    };

    return <InfoProfileBase {...config} />;
};
export default InfoProfile;
