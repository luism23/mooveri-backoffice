import { Story, Meta } from "@storybook/react";

import { InfoProfileProps, InfoProfile } from "./index";

export default {
    title: "Info/InfoProfile",
    component: InfoProfile,
} as Meta;

const Template: Story<InfoProfileProps> = (args) => (
    <InfoProfile {...args}>Test Children</InfoProfile>
);

export const Index = Template.bind({});
Index.args = {
    
};
