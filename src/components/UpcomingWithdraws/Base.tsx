import Theme, { ThemesType } from '@/config/theme';
import money from '@/functions/money';
import { useLang } from '@/lang/translate';
import UnicornWealthy from '@/lottin/UnicornWealthy';
import MasterCard from '@/svg/masterCard';
import Button from '../Button';
import ContentWidth from '../ContentWidth';
import Text, { TextStyles } from '../Text';

export interface UpcomingWithdrawsClassProps {
    styleTemplateTitle?: TextStyles | ThemesType;
    classNameContentSubscriptions?: string;
    classNameContentTitle?: string;
    title?: string;
    classNameSubscriptionsIcon?: string;
    icon?: any;
    iconCard?: any;
    classNameContentIconCard?: string;
    classNameContentCardMoney?: string;
}

export interface UpcomingWithdrawsBaseProps {
    cardNumber: string;
    moneyWithdraw: any;
}

export interface UpcomingWithdrawsProps
    extends UpcomingWithdrawsClassProps,
        UpcomingWithdrawsBaseProps {}

export const UpcomingWithdrawsBase = ({
    styleTemplateTitle = Theme.styleTemplate ?? '_default',
    classNameContentSubscriptions = '',
    classNameContentCardMoney = '',
    classNameContentTitle = '',
    title = 'Withdraw on the way',
    classNameSubscriptionsIcon = '',
    classNameContentIconCard = '',
    cardNumber,
    icon = <UnicornWealthy />,
    iconCard = <MasterCard size={30} />,
    moneyWithdraw,
}: UpcomingWithdrawsProps) => {
    const _t = useLang();

    return (
        <>
            <div className={classNameContentSubscriptions}>
                <Text
                    styleTemplate={styleTemplateTitle}
                    className={classNameContentTitle}
                >
                    {_t(title)}
                </Text>
                <div className={classNameSubscriptionsIcon}>{icon}</div>
                <div className={classNameContentCardMoney}>
                    <span className={classNameContentIconCard}>{iconCard}</span>
                    <div className="m-r-auto">
                        <Text
                            className="font-w-700 color-black"
                            styleTemplate="tolinkme4"
                        >
                            xxxx xxxx xxxx {cardNumber}
                        </Text>
                        <Text
                            styleTemplate="tolinkme9"
                            className=" flex flex-align-center"
                        >
                            {_t('Arrival')}:
                            <Text styleTemplate="tolinkme33" className="p-l-5">
                                23 - 26 June 2023
                            </Text>
                        </Text>
                    </div>
                    <Text styleTemplate="tolinkme9">
                        {money(moneyWithdraw)} USD
                    </Text>
                </div>
            </div>
            <ContentWidth
                className=" 
                    pos-f
                    bottom-20
                    z-index-1
                    width-p-100
                    p-h-17
                    left-0
                    right-0
                    m-auto
                    flex
                    flex-justify-center
                    "
                size={400}
            >
                <Button classNameBtn="text-center  m-auto">
                    {_t('Withdraw')}
                </Button>
            </ContentWidth>
        </>
    );
};
export default UpcomingWithdrawsBase;
