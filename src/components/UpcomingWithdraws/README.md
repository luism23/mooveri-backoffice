# UpcomingWithdraws

## Dependencies

[UpcomingWithdraws](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/components/UpcomingWithdraws)

```js
import { UpcomingWithdraws } from '@/components/UpcomingWithdraws';
```

## Import

```js
import {
    UpcomingWithdraws,
    UpcomingWithdrawsStyles,
} from '@/components/UpcomingWithdraws';
```

## Props

```tsx
interface UpcomingWithdrawsProps {}
```

## Use

```js
<UpcomingWithdraws>UpcomingWithdraws</UpcomingWithdraws>
```
