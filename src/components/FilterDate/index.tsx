import { useMemo } from 'react';

import * as styles from '@/components/FilterDate/styles';

import { Theme, ThemesType } from '@/config/theme';

import {
    FilterDateBaseProps,
    FilterDateBase,
} from '@/components/FilterDate/Base';

export const FilterDateStyle = { ...styles } as const;

export type FilterDateStyles = keyof typeof FilterDateStyle;

export interface FilterDateProps extends FilterDateBaseProps {
    styleTemplate?: FilterDateStyles | ThemesType;
}

export const FilterDate = ({
    styleTemplate = Theme?.styleTemplate ?? '_default',
    ...props
}: FilterDateProps) => {
    const Style = useMemo(
        () =>
            FilterDateStyle[styleTemplate as FilterDateStyles] ??
            FilterDateStyle._default,
        [styleTemplate]
    );

    return <FilterDateBase {...Style} {...props} />;
};
export default FilterDate;
