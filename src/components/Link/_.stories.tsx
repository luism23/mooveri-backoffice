import { Story, Meta } from "@storybook/react";

import { LinkProps, Link } from "./index";

export default {
    title: "Button/Link",
    component: Link,
} as Meta;

const Template: Story<LinkProps> = (args) => (
    <Link {...args}>Test Children</Link>
);

export const Index = Template.bind({});
Index.args = {};
