export const _default = `
    font-16 font-montserrat
    color-black color-blue-hover
`;
export const noDecoration = `
    text-decoration-none
    color-currentColor
`;

export const noStyleYesUnderline = `
    text-decoration-underline
    color-currentColor
`;

export const noStyleYesUnderlineDarkAqua = `
    color-darkAqua color-brightPink-hover
    text-decoration-underline
`;
