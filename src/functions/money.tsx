export const money = (price: number) => {
    return '$' + price.toLocaleString('en-US');
    let letter = '';
    if (price > 999_999) {
        price /= 1_000_000;
        letter = 'M';
    }
    const options = { style: 'currency', currency: 'USD' };
    const numberFormat = new Intl.NumberFormat('en-US', options);
    return numberFormat.format(price).split('.')[0] + letter;
};
export default money;
