import ButtonRsView from '@/components/ButtonRsView';
import { RSLinkConfigDataProps } from '@/components/RS/LinkConfig/Base';
import {
    BorderRadiusButtonValues,
    ButtonConfig,
    SizeButtonImgValue,
    SizeButtonValues,
} from '@/interfaces/Button';
import { getBg } from './getBg';
import { getText } from './getText';

export const getBtn: (
    btn: RSLinkConfigDataProps,
    style: ButtonConfig
) => any = (btn: RSLinkConfigDataProps, style: ButtonConfig) => {
    const text = ['custom', 'file'].includes(btn?.rs ?? '')
        ? btn?.customTitle && btn?.customTitle != ''
            ? btn?.customTitle
            : btn?.url
        : btn?.rs;
    return (
        <div
            className={`
                pos-r
                ${
                    BorderRadiusButtonValues[
                        style?.borderRadius ?? 'semi-rounded'
                    ]
                }
                ${SizeButtonValues[style?.size ?? 'regular']}
                overflow-hidden
                flex
                flex-justify-between
                flex-align-center
                ${style.className ?? ''}
            `}
            style={{
                borderWidth: `${style?.border?.size ?? 0}px`,
                borderStyle: `${style?.border?.type ?? 'solid'}`,
                borderColor: `${style?.border?.color ?? 'transparent'}`,
                display: 'grid',
                gridTemplateColumns: '1fr 3fr 1fr',
                boxShadow: `${
                    style.boxShadow?.type == 'inset' ? 'inset' : ''
                } ${style.boxShadow?.x ?? 0}px ${style.boxShadow?.y ?? 0}px ${
                    style.boxShadow?.blur ?? 0
                }px ${style.boxShadow?.size ?? 0}px ${
                    style.boxShadow?.color ?? '#fff'
                }`,
            }}
        >
            {getBg(
                style?.background ?? {},
                'pos-a inset-0 width-p-100 height-p-100',
                {
                    boxShadow: `${
                        style.boxShadow?.type == 'inset' ? 'inset' : ''
                    } ${style.boxShadow?.x ?? 0}px ${
                        style.boxShadow?.y ?? 0
                    }px ${style.boxShadow?.blur ?? 0}px ${
                        style.boxShadow?.size ?? 0
                    }px ${style.boxShadow?.color ?? '#fff'}`,
                }
            )}
            {style?.icon == 'con' ? (
                <>
                    <div className={`pos-a- inset-0 flex flex-align-center`}>
                        {['custom', 'file'].includes(btn?.rs ?? '') ? (
                            <>
                                {btn?.customImg && btn?.customImg != '' ? (
                                    <ButtonRsView
                                        Icon={
                                            <img
                                                src={btn?.customImg}
                                                className={`${SizeButtonImgValue} ${
                                                    BorderRadiusButtonValues[
                                                        style.iconConfig
                                                            ?.borderRadius ??
                                                            'rounded'
                                                    ]
                                                }`}
                                                style={{
                                                    width: `${
                                                        (style.iconConfig
                                                            ?.size ?? 16) / 16
                                                    }rem`,
                                                    height: 'auto',
                                                }}
                                            />
                                        }
                                        config={style.iconConfig ?? {}}
                                        styleTemplate="tolinkme"
                                        img={true}
                                    />
                                ) : (
                                    <></>
                                )}
                            </>
                        ) : (
                            <ButtonRsView
                                Icon={
                                    <img
                                        src={`/image/tolinkme/rs/${btn?.rs?.toLocaleLowerCase()}.png`}
                                        className={`${SizeButtonImgValue} ${
                                            BorderRadiusButtonValues[
                                                style.iconConfig
                                                    ?.borderRadius ?? 'rounded'
                                            ]
                                        }`}
                                        style={{
                                            width: `${
                                                (style.iconConfig?.size ?? 16) /
                                                16
                                            }rem`,
                                            height: 'auto',
                                        }}
                                    />
                                }
                                config={style.iconConfig ?? {}}
                                styleTemplate="tolinkme"
                                img={true}
                            />
                        )}
                    </div>
                </>
            ) : (
                <div></div>
            )}
            <div className="pos-r text-capitalize text-center">
                {getText(style?.text ?? {}, text)}
            </div>
            <div></div>
        </div>
    );
};
