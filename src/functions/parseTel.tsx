import { PhoneProps } from '@/interfaces/Phone';

export const parseTel: (phone: string) => PhoneProps = (phone: string) => {
    const s = phone.split('-');

    const t: PhoneProps = {
        code: phone.split(`-${s[s.length - 1]}`)[0],
        tel: s[s.length - 1],
    };
    return t;
};
