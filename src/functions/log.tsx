export const log = (
    name: string,
    data: any,
    color = 'white',
    options?: {
        json?: boolean;
        save?: boolean;
    }
) => {
    if (process?.env?.['NEXT_PUBLIC_LOG'] === 'TRUE') {
        console.log(
            `%c [${name.toLocaleUpperCase()}]`,
            `color:${color};`,
            data
        );
        if (options?.json) {
            console.log(
                `%c [${name.toLocaleUpperCase()}]`,
                `color:${color};`,
                JSON.stringify(data)
            );
        }
        if (options?.save && typeof localStorage != 'undefined') {
            const localStorageSaves = localStorage.getItem(`log-${name}`);
            let save: any[] = [];
            try {
                save = JSON.parse((localStorageSaves as string) ?? '[]');
            } catch (error) {
                save = [];
            }
            if (!save.includes(data)) {
                save.push(data);
            }
            localStorage.setItem(`log-${name}`, JSON.stringify(save));
        }
    }
};
export default log;
