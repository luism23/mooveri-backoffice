export const parseHorariosMooveri = (item: string | string[]) => {
    const items = [item == '' ? '00:00-00:00' : item].flat(2);
    return items.map((e) => {
        const [start, end] = e.split('-');
        return {
            start,
            end,
        };
    });
};
