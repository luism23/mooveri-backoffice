import { parseIntPriceToNumberOptions } from '@/interfaces/PriceButtonBtn';

export const parseIntPrice = (price: string | number) => {
    const p = `${price}`.replace(/[*+?^${}()|[\]\\]/g, '');
    let pFloat = 0;
    try {
        pFloat = parseFloat(p);
    } catch (error) {
        pFloat = 0;
    }
    const textPriceTwoD = pFloat.toFixed(2);
    pFloat = parseFloat(textPriceTwoD);
    pFloat *= 100;
    return Number(parseInt(`${pFloat}`));
};

export const parseIntPriceToNumber = (
    price: string | number,
    options?: parseIntPriceToNumberOptions
) => {
    if (options?.noUse === true) {
        return Number(price);
    }
    const p = `${price}`.replace(/[*+?^${}()|[\]\\]/g, '');
    let pFloat = 0;
    try {
        pFloat = parseFloat(p);
    } catch (error) {
        pFloat = 0;
    }
    const textPrice0D = pFloat.toFixed(0);
    pFloat = parseFloat(textPrice0D);
    pFloat /= 100;
    if (options?.notDecimals === true) {
        return Number(parseInt(`${pFloat}`));
    }
    return Number(parseFloat(`${pFloat}`));
};
