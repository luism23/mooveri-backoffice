import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

export const ForgotPasswordPaymentYup = ValidateObject({
    email: ValidateText({
        errors: DATAERROR.email.default,
        type: 'email',
    }),
});
