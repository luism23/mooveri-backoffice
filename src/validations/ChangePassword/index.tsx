import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataChangePassword } from '@/interfaces/ChangePassword';

export const ChangePasswordYup = (
    data: DataChangePassword,
    inputs: DataChangePassword<boolean>
) => {
    const y: DataChangePassword<any> = {};

    if (inputs.password) {
        y.password = ValidateText({
            errors: DATAERROR.password.default,
            min: 6,
            max: 20,
        });
    }
    if (inputs.repeatPassword) {
        y.repeatPassword = ValidateText({
            require: true,
            errors: DATAERROR.password.default,
        }).oneOf([data.password], DATAERROR.password.default.noRepeat);
    }

    return ValidateObject(y);
};
