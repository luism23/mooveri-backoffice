import * as DATAERROR from '@/data/error';

import ValidateText from '@/validations/text';

export const userName = ValidateText({
    errors: DATAERROR.name.ErrorName,
});

export const firstName = ValidateText({
    errors: DATAERROR.name.ErrorFirstName,
});
export const lastName = ValidateText({
    errors: DATAERROR.name.ErrorLastName,
});
