import { ValidateObject } from '@/validations/object';
import * as Yup from 'yup';
import * as DATAERROR from '@/data/error';

import { DataVerifyIdentity } from '@/interfaces/VerifyIdentity';
import ValidateText from '../text';

export const VerifyIdentityYup = (
    data: DataVerifyIdentity,
    inputs: DataVerifyIdentity<boolean>
) => {
    const y: DataVerifyIdentity<any> = {};

    if (inputs.FrontIdentification) {
        y.FrontIdentification = Yup.object().shape({
            fileData: ValidateText({
                errors: DATAERROR.frontIdentification.ErrorFrontIdentification,
            }),
        });
    }
    if (inputs.LaterIdentification) {
        y.LaterIdentification = Yup.object().shape({
            fileData: ValidateText({
                errors: DATAERROR.laterIdentification.ErrorLaterIdentification,
            }),
        });
    }

    return ValidateObject(y);
};
