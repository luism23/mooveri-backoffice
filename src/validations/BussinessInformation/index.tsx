import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';
import { ValidateDate } from '@/validations/date';
import { ValidateTel } from '@/validations/tel';

import { DataBussinessInformation } from '@/interfaces/BussinessInformation';
import { InputTelValue } from '@/components/Input/Tel/Base';

export const BussinessInformationYup = (
    data: DataBussinessInformation<string | Date | InputTelValue>,
    inputs: DataBussinessInformation<boolean>
) => {
    const y: DataBussinessInformation<any> = {};
    const j: DataBussinessInformation<any> = {};

    if (inputs.company) {
        y.company = ValidateText({
            errors: DATAERROR.company.ErrorCompany,
            require: true,
        });
        j.company = y.company;
    }

    if (inputs.legalName) {
        y.legalName = ValidateText({
            errors: DATAERROR.legalName.ErrorLegalName,
            require: true,
        });
        j.legalName = y.legalName;
    }
    if (inputs.yearFounded) {
        y.yearFounded = ValidateDate({
            require: true,
            errors: DATAERROR.yearFounded.ErrorYearFounded,
        });
        j.yearFounded = y.yearFounded;
    }
    if (inputs.contactName) {
        y.contactName = ValidateText({
            errors: DATAERROR.contactName.ErrorContactName,
            require: true,
        });
        j.contactName = ValidateText({
            errors: DATAERROR.contactName.ErrorContactName,
            require: true,
        });
    }
    if (inputs.tel) {
        y.tel = ValidateTel({
            require: true,
            errors: DATAERROR.tel.ErrorTel,
        });
        j.tel = y.tel;
    }
    if (inputs.alternativeTel) {
        y.alternativeTel = ValidateTel({
            require: true,
            errors: DATAERROR.alternativeTel.ErrorAlternativeTel,
        });
        j.alternativeTel = y.alternativeTel;
    }

    return {
        v: ValidateObject(y),
        y: j,
        j,
    };
};
