import * as Yup from 'yup';
export type ValidateObjectProps = {
    [id: string]: any;
};

export const ValidateObject = (data: ValidateObjectProps) =>
    Yup.object().shape(data);
export default ValidateObject;
