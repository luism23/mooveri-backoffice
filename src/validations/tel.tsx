import * as Yup from 'yup';

import { ValidateText } from '@/validations/text';

import { ErrorTel } from '@/data/error/tel';

export interface ValidateTelProps {
    require?: boolean;
    errors?: any;
}

export const ValidateTel = ({
    require = true,
    errors = ErrorTel,
}: ValidateTelProps) => {
    const yup = Yup.object().shape({
        code: ValidateText({
            require,
            errors,
        }),
        tel: ValidateText({
            require,
            errors,
        }),
    });
    return yup;
};
export const ValidateTelCode = ({ require = true }: ValidateTelProps) => {
    const yup = Yup.object().shape({
        code: ValidateText({
            require,
            errors: ErrorTel,
        }),
    });
    return yup;
};
export default ValidateTel;
