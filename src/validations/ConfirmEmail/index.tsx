import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataConfirmEmail } from '@/interfaces/ConfirmEmail';
import log from '@/functions/log';

export const ConfirEmailYup = (
    data: DataConfirmEmail,
    inputs: DataConfirmEmail<boolean>
) => {
    const y: DataConfirmEmail<any> = {};
    log('DataConfirmEmail', {
        data,
        inputs,
    });
    if (inputs.email) {
        y.email = ValidateText({
            errors: DATAERROR.email.default,
            type: 'email',
        });
    }

    return ValidateObject(y);
};
