// import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
// import ValidateText from '@/validations/text';

import { CompanyProps } from '@/interfaces/Company';
import log from '@/functions/log';

export const CompanyYup = (data: CompanyProps) => {
    log('UserProps', data);
    const y: any = {};

    // if (inputs.name) {
    //     y.name = ValidateText({
    //         errors: DATAERROR.name.ErrorName,
    //     });
    // }

    // if (inputs.email) {
    //     y.email = ValidateText({
    //         errors: DATAERROR.email.default,
    //         type: 'email',
    //     });
    // }

    return ValidateObject(y);
};
