import * as Yup from 'yup';

export const ValidateArray = (data: any) => Yup.array().of(data);
export default ValidateArray;
