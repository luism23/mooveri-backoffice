import { ValidateObject } from '@/validations/object';

import * as Yup from 'yup';

import { DataFormAddPayment } from '@/interfaces/FormAddPayment';

export const FormAddPaymentYup = (
    data: DataFormAddPayment,
    inputs: DataFormAddPayment<boolean>
) => {
    const y: DataFormAddPayment<any> = {};
    const j: DataFormAddPayment<any> = {};

    if (inputs.stripe_before) {
        y.stripe_before = ValidateObject({
            complete: Yup.mixed().oneOf([true]),
        });
        j.stripe_before = y.stripe_before;
    }
    return {
        y,
        v: ValidateObject(y),
    };
};
