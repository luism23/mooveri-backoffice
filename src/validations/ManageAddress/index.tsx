import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataManageAddress } from '@/interfaces/ManageAddress';

export const ManageAddressYup = (
    data: DataManageAddress,
    inputs: DataManageAddress<boolean>
) => {
    const y: DataManageAddress<any> = {};

    if (inputs.address) {
        y.address = ValidateText({
            errors: DATAERROR.address.ErrorAddress,
            require: true,
        });
    }

    if (inputs.city) {
        y.city = ValidateText({
            errors: DATAERROR.city.ErrorCity,
            require: true,
        });
    }

    if (inputs.apt) {
        y.apt = ValidateText({
            errors: DATAERROR.aptSuiteEtc.ErrorAptSuiteEtc,
            require: true,
        });
    }

    if (inputs.zipcode) {
        y.zipcode = ValidateText({
            errors: DATAERROR.zipcode.ErrorZipCode,
            require: true,
        });
    }

    if (inputs.location) {
        y.location = ValidateText({
            errors: DATAERROR.location.ErrorLocation,
            require: true,
        });
    }
    return {
        y,
        v: ValidateObject(y),
    };
};
