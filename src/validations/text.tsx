import * as Yup from 'yup';

export interface ValidateTextProps {
    require?: boolean;
    min?: false | number;
    max?: false | number;
    integer?: false | number;
    type?: false | 'email' | 'url';
    errors?: {
        required?: string;
        invalid?: string;
        min?: string;
        max?: string;
        integer?: string;
    };
}

export const ValidateText = ({
    require = true,
    min = false,
    max = false,
    type = false,
    errors = {
        required: 'This is Required',
        invalid: 'This Invalid',
        min: 'This is min',
        max: 'This is max',
    },
}: ValidateTextProps) => {
    let yup = Yup.string();

    if (require) {
        yup = yup.required(errors.required);
    }
    if (type !== false) {
        yup = yup[type](errors.invalid);
    }
    if (min !== false) {
        yup = yup.min(min, errors.min);
    }
    if (max !== false) {
        yup = yup.max(max, errors.max);
    }

    return yup;
};
export default ValidateText;
