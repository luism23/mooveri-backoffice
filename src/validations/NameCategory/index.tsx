import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import { ValidateArray } from '@/validations/array';
import ValidateText from '@/validations/text';

export const NameCategoryYup = ValidateObject({
    name: ValidateText({
        errors: DATAERROR.name.default,
    }),
    categories: ValidateArray(
        ValidateObject({
            id: ValidateText({
                require: true,
                errors: DATAERROR.category.default,
            }),
            name: ValidateText({
                require: true,
                errors: DATAERROR.category.default,
            }),
        })
    ),
});
