import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataRegister } from '@/interfaces/Register';

export const RegisterYup = (
    data: DataRegister,
    inputs: DataRegister<boolean>
) => {
    const y: DataRegister<any> = {};

    if (inputs.userName) {
        y.userName = ValidateText({
            errors: DATAERROR.name.ErrorName,
        });
    }

    if (inputs.firstName) {
        y.firstName = ValidateText({
            errors: DATAERROR.name.ErrorFirstName,
        });
    }

    if (inputs.lastName) {
        y.lastName = ValidateText({
            errors: DATAERROR.name.ErrorLastName,
        });
    }

    if (inputs.email) {
        y.email = ValidateText({
            errors: DATAERROR.email.default,
            type: 'email',
        });
    }

    if (inputs.password) {
        y.password = ValidateText({
            errors: DATAERROR.password.default,
            min: 6,
            // max: 20,
        });
    }

    if (inputs.repeatPassword) {
        y.repeatPassword = ValidateText({
            require: true,
            errors: DATAERROR.password.default,
        }).oneOf([data.password], DATAERROR.password.default.noRepeat);
    }
    return ValidateObject(y);
};
