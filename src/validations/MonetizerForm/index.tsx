import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';
import * as Yup from 'yup';

import { DataFormMonetizerForm } from '@/interfaces/FormMonetizerForm';

export const FormMonetizerFormYup = (
    data: DataFormMonetizerForm,
    inputs: DataFormMonetizerForm<boolean>
) => {
    const y: DataFormMonetizerForm<any> = {};

    if (inputs.fullName) {
        y.fullName = ValidateText({
            errors: DATAERROR.name.ErrorName,
            require: true,
        });
    }

    if (inputs.lastName) {
        y.lastName = ValidateText({
            errors: DATAERROR.name.ErrorLastName,
            require: true,
        });
    }

    if (inputs.Street) {
        y.Street = ValidateText({
            errors: DATAERROR.street.ErrorStreet,
            require: true,
        });
    }

    if (inputs.country) {
        y.country = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.state) {
        y.state = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.city) {
        y.city = ValidateObject({
            text: ValidateText({
                errors: DATAERROR.street.ErrorStreet,
                require: true,
            }),
        });
    }

    if (inputs.zipPostalCode) {
        y.zipPostalCode = ValidateText({
            errors: DATAERROR.zipPostalCode.ErrorZipPostalCode,
            require: true,
        });
    }

    if (inputs.confirm18) {
        y.confirm18 = Yup.mixed().oneOf([true]);
    }
    return {
        y,
        v: ValidateObject(y),
    };
};
