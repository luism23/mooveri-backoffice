import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataMyAccount } from '@/interfaces/MyAccount';

export const MyAccountYup = (data: DataMyAccount) =>
    ValidateObject({
        name: ValidateText({
            errors: DATAERROR.name.default,
        }),
        email: ValidateText({
            errors: DATAERROR.email.default,
            type: 'email',
        }),
        tel: ValidateObject({
            code: ValidateText({
                errors: DATAERROR.code.default,
            }),
            tel: ValidateText({
                errors: DATAERROR.tel.default,
            }),
        }),
        password: ValidateText({
            errors: DATAERROR.password.default,
            require: false,
        }),
        repeatPassword: ValidateText({
            require: false,
            errors: DATAERROR.password.default,
        }).oneOf([data.password], DATAERROR.password.default.noRepeat),
    });
