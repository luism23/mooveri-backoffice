import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataProfile } from '@/interfaces/Profile';
import { SelectOptionProps } from '@/components/Input/Select';
import { GoogleAddres } from '@/interfaces/Google/addres';

export const ProfileEditYup = (
    data: DataProfile<
        string | Date | SelectOptionProps | GoogleAddres | boolean | number
    >,
    inputs: DataProfile<boolean>
) => {
    const y: DataProfile<any> = {};
    const j: DataProfile<any> = {};

    if (inputs.companyName) {
        y.companyName = ValidateText({
            errors: DATAERROR.company.ErrorCompany,
            require: true,
        });
        j.companyName = y.companyName;
    }

    if (inputs.legalName) {
        y.legalName = ValidateText({
            errors: DATAERROR.legalName.ErrorLegalName,
            require: true,
        });
        j.legalName = y.legalName;
    }

    if (inputs.description) {
        y.description = ValidateText({
            errors: DATAERROR.description.ErrorDescription,
            require: true,
        });
        j.description = y.description;
    }

    return {
        v: ValidateObject(y),
        y: j,
    };
};
