import * as DATAERROR from '@/data/error';

import { ValidateObject } from '@/validations/object';
import ValidateText from '@/validations/text';

import { DataChatMesseges } from '@/interfaces/ChatMesseges';
import { InputTelValue } from '@/components/Input/Tel/Base';

export const ChatMessegesYup = (
    data: DataChatMesseges<string | Date | InputTelValue>,
    inputs: DataChatMesseges<boolean>
) => {
    const y: DataChatMesseges<any> = {};
    const j: DataChatMesseges<any> = {};

    if (inputs.chat) {
        y.chat = ValidateText({
            errors: DATAERROR.chat.ErrorChatMesseges,
            require: true,
        });
        j.chat = y.chat;
    }
    return {
        v: ValidateObject(y),
        y: j,
        j,
    };
};
