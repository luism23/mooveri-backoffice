import { ValidateObject } from '@/validations/object';
import * as Yup from 'yup';

import { DataMonetizeCategory } from '@/interfaces/MonetizeCategory';

export const MonetizeCategoryYup = (
    data: DataMonetizeCategory,
    inputs: DataMonetizeCategory<boolean>
) => {
    const y: DataMonetizeCategory<any> = {};
    if (inputs.confirm) {
        y.confirm = Yup.mixed().oneOf([true]);
    }
    if (inputs.privacyPolicy) {
        y.privacyPolicy = Yup.mixed().oneOf([true]);
    }

    return {
        y,
        v: ValidateObject(y),
    };
};
