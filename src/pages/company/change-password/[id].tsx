import { PageChangePassword } from '@/components/Pages/ChangePassword';

export const ChangePassword = () => <PageChangePassword company={true} />;

export default ChangePassword;
