import { PageSuccesfullActivated } from '@/components/Pages/SuccesfullActivated';

export const SuccesfullActivated = () => (
    <PageSuccesfullActivated company={true} />
);

export default SuccesfullActivated;
