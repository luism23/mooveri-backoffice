import { PageRegister } from '@/components/Pages/Register';

export const Register = () => <PageRegister company={true} />;

export default Register;
