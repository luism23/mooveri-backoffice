import Document, { Html, Head, Main, NextScript } from 'next/document';
import Script from 'next/script';
import TagManageHead from '@/tagmanager/Head';
import TagManageBody from '@/tagmanager/Body';

class WebDocument extends Document {
    render() {
        return (
            <Html lang="en-US">
                <Head>
                    <Script
                        strategy="afterInteractive"
                        dangerouslySetInnerHTML={{
                            __html: TagManageHead(),
                        }}
                    ></Script>
                </Head>
                <body>
                    <noscript
                        dangerouslySetInnerHTML={{
                            __html: TagManageBody(),
                        }}
                    ></noscript>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        );
    }
}
export default WebDocument;
