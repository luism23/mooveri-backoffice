import { useRouter } from 'next/router';
import { useUser } from '@/hook/useUser';
import { useEffect } from 'react';
import url from '@/data/routes';

const Home = () => {
    const route = useRouter();
    const { onLogOut, load } = useUser();
    useEffect(() => {
        if (load) {
            onLogOut();
            route.push(url.login);
        }
    }, [load]);

    return <></>;
};

export default Home;
