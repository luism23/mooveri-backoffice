import { GetServerSideProps } from 'next';

import { PageID } from '@/components/Pages/ID';
import { TolinkmeProps } from '@/components/Pages/ID/content/tolinkme';
import * as ProfileApi from '@/api/tolinkme/profile';

export const getServerSideProps: GetServerSideProps = async (context) => {
    const id = context.query.id;

    const profileResult: '404' | TolinkmeProps = await ProfileApi.GET_PUBLIC({
        username: `${id ?? ''}`,
    });

    if (profileResult == '404') {
        return {
            props: {},
        };
    }

    return {
        props: {
            title:
                profileResult?.style.username_title &&
                profileResult?.style.username_title != ''
                    ? profileResult?.style.username_title
                    : profileResult?.style.name,
            favicon: profileResult?.style?.avatar,
            description: profileResult?.style?.description,
        },
    };
};
export default PageID;
