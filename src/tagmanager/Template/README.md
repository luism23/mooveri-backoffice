# Template

## Dependencies

[Template](https://gitlab.com/franciscoblancojn/fenextjs/-/blob/develop/src/tagmanager/Template)

```js
import { Template } from '@/tagmanager/Template';
```

## Import

```js
import { Template, TemplateStyles } from '@/tagmanager/Template';
```

## Props

```tsx
interface TemplateProps {}
```

## Use

```js
<Template>Template</Template>
```
