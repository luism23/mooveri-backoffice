import { BodyClassProps } from '@/tagmanager/Body/Base';

export const tolinkme: BodyClassProps = {
    content: `
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K7D6V5V"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
    `,
};
