import * as styles from '@/tagmanager/Body/styles';

import { Theme } from '@/config/theme';
import { BodyClassProps } from '@/tagmanager/Body/Base';

export const BodyStyle = { ...styles } as const;

export type BodyStyles = keyof typeof BodyStyle;

export const Body: () => string = () => {
    const Style: BodyClassProps =
        BodyStyle[(Theme?.styleTemplate ?? '_default') as BodyStyles] ??
        BodyStyle._default;

    return Style.content ?? '';
};
export default Body;
