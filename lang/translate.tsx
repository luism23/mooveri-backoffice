import { useCallback, useMemo } from "react";
import { useLocalStorage } from "uselocalstoragenextjs";
import * as _l from "@/lang/languages";
import log from "@/functions/log";

export const langs = { ..._l } as const;
export const langsKeys = Object.keys(langs);
export type langsType = keyof typeof langs;
export const defaultLang: langsType = "en";

type TranslateType = { [key: string]: string | undefined };

export const useLang = () => {
    const { value, load } = useLocalStorage({
        name: "lang",
        defaultValue: defaultLang,
    });

    const translate = useMemo<TranslateType>(() => {
        if (!load) {
            return {};
        }
        const langSelect: langsType = value ?? defaultLang;
        return langs[langSelect ?? defaultLang];
    }, [value]);

    const _t = useCallback(
        (text: string) => {
            if (
                text != "" &&
                process?.env?.["NEXT_PUBLIC_SAVE_TRANLATE"] === "TRUE" &&
                translate["_"] == "_" &&
                translate[text] == undefined
            ) {
                //KEY
                // "]:"traducir",\n["
                log("translate", text, "white", {
                    save: true,
                });
            }
            return translate[text] ?? text;
        },
        [translate]
    );
    return _t;
};
export const useStateLang = () => {
    const { value, setLocalStorage } = useLocalStorage({
        name: "lang",
        defaultValue: defaultLang,
    });
    const lang: langsType = useMemo<langsType>(() => value ?? defaultLang, [value]);
    return {
        lang,
        setLang: (lang: langsType) => {
            setLocalStorage(lang);
        },
    };
};
