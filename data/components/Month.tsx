export const MonthsConst = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
] as const;

export const Months = [...MonthsConst];
export type MonthsType = (typeof MonthsConst)[number];
