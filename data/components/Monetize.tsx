export const MonetizeRecurrent = [
    {
        label: 'Recurrent',
    },
    {
        label: 'Flat_Rate',
    },
];

export const PeriodRecurrent = [
    {
        label: 'Monthly',
    },
    {
        label: 'Quarterly',
    },
    {
        label: 'SemiAnnual',
    },
    {
        label: 'Annually',
    },
];

export const TypeRecurrent = [
    {
        label: 'Donation',
    },
    {
        label: 'Tip',
    },
    {
        label: 'Gift',
    },
    {
        label: 'Freelance_Service',
    },
];
