export const ErrorNumber = {
    required: 'Number is Required',
    invalid: 'Number Invalid',
    min: 'Number is short',
    max: 'Number is long',
};
