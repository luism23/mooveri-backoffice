export const ErrorLaterIdentification = {
    required: 'LaterIdentification is Required',
    invalid: 'LaterIdentification invalid',
};
export default ErrorLaterIdentification;
