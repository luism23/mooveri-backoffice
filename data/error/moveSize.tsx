export const ErrorMoveSize = {
    required: 'Move size is Required',
    invalid: 'Move size invalid',
};
export default ErrorMoveSize;
