export const ErrorZipPostalCode = {
    required: 'Zip Postal Code is Required',
    invalid: 'Zip Postal Code invalid',
};
export default ErrorZipPostalCode;
