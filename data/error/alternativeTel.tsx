export const ErrorAlternativeTel = {
    required: 'Alternative Tel name is Required',
    invalid: 'Alternative Tel name invalid',
};
export default ErrorAlternativeTel;
