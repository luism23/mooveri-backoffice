export const ErrorContactName = {
    required: 'Contact Name is Required',
    invalid: 'Contact Name invalid',
};
export default ErrorContactName;
