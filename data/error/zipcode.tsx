export const ErrorZipCode = {
    required: 'Zip Code is Required',
    invalid: 'Zip Code invalid',
};
export default ErrorZipCode;
