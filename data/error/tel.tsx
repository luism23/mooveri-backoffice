export const ErrorTel = {
    required: 'Tel is Required',
    invalid: 'Tel invalid',
    min: 'Tel is short',
    max: 'Tel is long',
};
export default ErrorTel;
