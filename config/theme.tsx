export type ThemesType = "_default" | "tolinkme" | "mooveri" | "mooveriBackoffice";

export interface ThemeProps {
    styleTemplate?: ThemesType;
}

const NEXT_PUBLIC_PROJECT = process?.env?.["NEXT_PUBLIC_PROJECT"] as ThemesType;

export const Theme: ThemeProps = {
    styleTemplate: NEXT_PUBLIC_PROJECT ?? "tolinkme",
};
export default Theme;
