/** @type {import('next').NextConfig} */
let c = {}
try {
    c = require("./config/proyects.env.js")
} catch (error) {
    c = {}
}

const nextConfig = {
    reactStrictMode: true,
    swcMinify: true,
    pageExtensions: ["tsx", "ts", "jsx", "js"],
    env: c[process.env["NEXT_PUBLIC_PROJECT"]] || {},
    trailingSlash: true,
};

module.exports = nextConfig;
